#!/usr/bin/python3
###############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2025 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import pakfire

import tests

class PakfireTest(tests.TestCase):
	"""
		This tests the main Pakfire class
	"""
	def test_repr(self):
		p = self.setup_pakfire()

		self.assertIn("Pakfire", repr(p))

	def test_properties(self):
		p = self.setup_pakfire()

		# Fetch the path and check if it is absolute
		self.assertTrue(p.path.startswith("/"))

		self.assertIn(p.arch, pakfire.supported_arches())

	def test_repo(self):
		p = self.setup_pakfire()

		# Fetch a repository
		repo = p.get_repo("@system")
		self.assertIsInstance(repo, pakfire.Repo)

		# Fetch a repo that does not exist
		repo = p.get_repo("does-not-exist")
		self.assertIsNone(repo)

		# Fetch all repositories
		self.assertIsInstance(p.repos, list)

		# Check that we only got repos
		for repo in p.repos:
			self.assertIsInstance(repo, pakfire.Repo)

	def test_whatprovides_whatrequires(self):
		p = self.setup_pakfire()

		self.assertIsInstance(p.whatprovides("bash"), list)
		self.assertIsInstance(p.whatrequires("bash"), list)


if __name__ == "__main__":
	tests.main()
