#!/usr/bin/python3

import datetime
import os.path
import pakfire
import stat

import tests

class ArchiveTest(tests.TestCase):
	"""
		This tests the Package class
	"""
	def setUp(self):
		self.pakfire = self.setup_pakfire()

	def test_open(self):
		"""
			Test if we can open an archive
		"""
		path = self.path("beep-1.3-2.ip3.x86_64.pfm")

		# Open the archive
		archive = self.pakfire.open(path)

		# Read back the path
		self.assertEqual(archive.path, path)

		# The format should be 6
		self.assertEqual(archive.format, 6)

	def test_filelist(self):
		"""
			Test parsing the filelist that is stored in the archive
		"""
		path = self.path("beep-1.3-2.ip3.x86_64.pfm")

		# Open the archive
		archive = self.pakfire.open(path)

		# Check that we read the entire list of 7 files
		self.assertEqual(len(archive.filelist), 7)

		# Check that we got files
		for file in archive.filelist:
			self.assertIsInstance(file, pakfire.File)

		# Check attributes of a few files
		for file in archive.filelist:
			# Check attributes of /usr/bin/beep
			if file.path == "/usr/bin/beep":
				self.assertEqual(file.type, stat.S_IFREG)

				# Size
				self.assertEqual(file.size, 17192)

				# Mode
				self.assertEqual(file.mode, 0o100755)

				# Ownership
				self.assertEqual(file.uname, "root")
				self.assertEqual(file.gname, "root")

				# Creation/Modification Time
				self.assertEqual(file.ctime, datetime.datetime(2023, 2, 28, 18, 23, 48))
				self.assertEqual(file.mtime, datetime.datetime(2023, 2, 28, 18, 23, 48))

				# This file has no capabilities
				self.assertIsNone(file.capabilities)

				# This file has no mimetype
				self.assertIsNone(file.mimetype)

	def test_read(self):
		"""
			Test whether we can read a file from the archive
		"""
		# XXX needs tests for reading symlinks

		path = self.path("beep-1.3-2.ip3.x86_64.pfm")

		# Open the archive
		archive = self.pakfire.open(path)

		# Try opening /usr/bin/beep
		f = archive.read("/usr/bin/beep")

		# Check if we got the correct type
		self.assertIsInstance(f, pakfire.ArchiveFile)

		# Read the entire payload
		payload = f.readall()

		# Check type
		self.assertIsInstance(payload, bytes)

		# Make sure we read the entire file
		self.assertEqual(len(payload), 17192)

		# Check that the payload starts like an ELF file
		self.assertEqual(payload[:4], b"\x7fELF")

		# Try to read more after we have read everything
		self.assertEqual(b"", f.read())

		# Try reading a file which does not exist
		with self.assertRaises(FileNotFoundError):
			archive.read("/usr/bin/does-not-exist")

		# Try reading a directory
		with self.assertRaises(FileNotFoundError):
			archive.read("/usr/bin")

	def test_extract(self):
		"""
			Tests extracting an archive
		"""
		path = self.path("beep-1.3-2.ip3.x86_64.pfm")

		# Open the archive
		archive = self.pakfire.open(path)

		with self.tempdir() as t:
			# Perform extraction
			archive.extract(path=t)

			self.assertTrue(os.path.exists("%s/usr" % t))
			self.assertTrue(os.path.exists("%s/usr/bin" % t))
			self.assertTrue(os.path.exists("%s/usr/bin/beep" % t))
			self.assertTrue(os.path.exists("%s/usr/share" % t))
			self.assertTrue(os.path.exists("%s/usr/share/man" % t))
			self.assertTrue(os.path.exists("%s/usr/share/man/man1" % t))
			self.assertTrue(os.path.exists("%s/usr/share/man/man1/beep.1.xz" % t))

			# XXX how can we check that there isn't anything else?

	def test_package(self):
		"""
			Tests reading package metadata
		"""
		path = self.path("beep-1.3-2.ip3.x86_64.pfm")

		# Open the archive
		archive = self.pakfire.open(path)

		# Fetch the package metadata
		package = archive.get_package()

		self.assertIsInstance(package, pakfire.Package)

		# The rest will be tested in packages.py

	def test_open_stub(self):
		# Launch Pakfire in stub mode
		p = self.setup_pakfire(stub=True)

		# Make path
		path = self.path("beep-1.3-2.ip3.x86_64.pfm")

		# Open the archive
		archive = self.pakfire.open(path)

		# Fetch the filelist
		self.assertIsInstance(archive.filelist, list)

if __name__ == "__main__":
	tests.main()
