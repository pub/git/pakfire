#!/usr/bin/python3

import tests

class PackageTest(tests.TestCase):
	"""
		This tests the Package class
	"""
	def setUp(self):
		self.pakfire = self.setup_pakfire()

	def test_open(self):
		path = self.path("beep-1.3-2.ip3.x86_64.pfm")

		# Open the archive
		a = self.pakfire.open(path)

		# Fetch the package
		p = a.get_package()

		# Check metadata
		self.assertEqual(p.name, "beep")
		self.assertEqual(p.evr, "1.3-2.ipfire3")
		self.assertEqual(p.arch, "x86_64")

		self.assertEqual(p.summary, "Beep the PC speaker any number of ways.")
		self.assertEqual(p.description,
			"Beep allows the user to control the PC speaker with precision, "
			"allowing different sounds to indicate different events. While "
			"it can be run quite happily on the commandline, it's intended "
			"place of residence is within shell/perl scripts, notifying the "
			"user when something interesting occurs. Of course, it has no "
			"notion of what's interesting, but it's real good at that "
			"notifying part.")
		self.assertEqual(p.url, "http://www.johnath.com/beep/")
		self.assertEqual(p.license, "GPLv2+")
		self.assertEqual(p.vendor, "IPFire Project")
		self.assertEqual(p.distribution, "ipfire3")
		self.assertEqual(p.groups, ["Applications/System"])

		self.assertEqual(p.uuid, "a20dddff-d8e8-4544-aaee-8bab618b0b3f")
		self.assertIsNone(p.packager)

		self.assertEqual(p.filename, "beep-1.3-2.ip3.x86_64.pfm")
		self.assertEqual(p.downloadsize, 11271)
		self.assertEqual(p.installsize, 40980)

		self.assertEqual(p.build_id, "d62b07b8-6e61-4406-9d36-d31c21830215")
		self.assertEqual(p.buildhost, "michael.sof.ipfire.org")
		self.assertEqual(p.buildtime, 1677608628)

		# Dependencies
		self.assertEqual(p.prerequires, [])
		self.assertEqual(p.requires, [
			"pakfire(Digest-SHA3-512)",
			"pakfire(Digest-BLAKE2b512)",
			"/lib64/ld-linux-x86-64.so.2",
			"libc.so.6()(64bit)",
			"libc.so.6(GLIBC_2.2.5)(64bit)",
			"libc.so.6(GLIBC_2.3.4)(64bit)",
			"libc.so.6(GLIBC_2.34)(64bit)",
			"libc.so.6(GLIBC_2.4)(64bit)",
			"libc.so.6(GLIBC_2.7)(64bit)",
			"pakfire(Compress-Zstandard)",
			"pakfire(PackageFormat-6)",
		])
		self.assertEqual(p.provides, ["beep = 1.3-2.ipfire3"])
		self.assertEqual(p.obsoletes, [])
		self.assertEqual(p.conflicts, [])
		self.assertEqual(p.suggests, [])
		self.assertEqual(p.recommends, [])

		# Source
		self.assertEqual(p.source_package, "beep-1.3-2.ipfire3.src")
		self.assertEqual(p.source_name, "beep")
		self.assertEqual(p.source_arch, "src")
		self.assertEqual(p.source_evr, "1.3-2.ipfire3")

		self.assertEqual(p.build_arches, [])

		# Digest
		algo, checksum = p.checksum

		self.assertEqual(algo, "sha2-512")
		self.assertEqual(checksum, b"BP+\xb256\xc3\x12w\xf0l\xc2\xd7\x87j\xe3\xf0\x84\x85v\xc56B|\x8e\x99\x95\xf1 Z\xbf\xe7U\xa0C\xc8\xf1hklg{\x89\xa5\x98\x9e\nG\xdb!\ng\xbe\xa0\xcdt6\x04\x00\xeby\xd18\x91")


if __name__ == "__main__":
	tests.main()
