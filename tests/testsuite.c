/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2017 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <linux/limits.h>
#include <stdlib.h>

#include "testsuite.h"

#include <pakfire/config.h>
#include <pakfire/logging.h>
#include <pakfire/mount.h>
#include <pakfire/pakfire.h>
#include <pakfire/util.h>

#define TMP_TEMPLATE "/tmp/pakfire-test.XXXXXX"

struct testsuite ts;

static int test_run(int i, struct test* t) {
	struct pakfire_ctx* ctx = NULL;
	struct pakfire* p = NULL;
	int r;

	LOG("running %s\n", t->name);

	// Create a new context
	r = pakfire_ctx_create(&t->ctx, TEST_CONFIG_FILE);
	if (r) {
		LOG("Could not create context: %m\n");
		goto ERROR;
	}

	// Set the log level to DEBUG
	pakfire_ctx_set_log_level(t->ctx, LOG_DEBUG);

	// Log everything to the console
	pakfire_ctx_set_log_callback(t->ctx, pakfire_log_stderr, NULL);

	// Create a configuration object
	r = pakfire_config_create(&t->config);
	if (r < 0) {
		LOG("Could not create configuration object: %s\n", strerror(-r));
		goto ERROR;
	}

	// Read the configuration file
	r = pakfire_config_read_path(t->config, TEST_CONFIG_FILE);
	if (r < 0) {
		LOG("Could not read the configuration: %s\n", strerror(-r));
		goto ERROR;
	}

	// Create a pakfire instance (if requested)
	if (t->flags & TEST_WANTS_PAKFIRE) {
		r = pakfire_create(&t->pakfire, t->ctx, t->config, TEST_STUB_ROOT, NULL, 0);
		if (r < 0) {
			LOG("ERROR: Could not initialize pakfire: %s\n", strerror(-r));
			goto ERROR;
		}

		// Check if the instance was created properly
		if (r == 0 && !t->pakfire) {
			LOG("ERROR: Pakfire was not initialized, but no error was raised: %m\n");
			goto ERROR;
		}
	}

	// Create a HTTP client (if requested)
	if (t->flags & TEST_WANTS_HTTPCLIENT) {
		r = pakfire_httpclient_create(&t->httpclient, t->ctx, NULL);
		if (r < 0) {
			LOG("ERROR: Could not initialize the HTTP client: %s\n", strerror(-r));
			goto ERROR;
		}
	}

	// Run test
	r = t->func(t);
	if (r)
		LOG("Test failed with error code: %d\n", r);

ERROR:
	// Release HTTP client
	if (t->httpclient)
		pakfire_httpclient_unref(t->httpclient);

	// Release pakfire
	if (t->pakfire) {
		p = pakfire_unref(t->pakfire);

		// Check if Pakfire was actually released
		if (p) {
			LOG("Error: Pakfire instance was not released in test %d\n", i);
			r = 1;
		}

		// Reset pointer (just in case)
		t->pakfire = NULL;
	}

	// Release context
	if (t->ctx) {
		ctx = pakfire_ctx_unref(t->ctx);

		// Check if the context was actually released
		if (ctx) {
			LOG("Error: Context was not released in test %s\n", t->name);
			r = 1;
		}

		// Reset pointer (just in case)
		t->ctx = NULL;
	}

	if (t->config)
		pakfire_config_unref(t->config);

	return r;
}

int __testsuite_add_test(const char* name, int (*func)(const struct test* t), int flags) {
	// Check if any space is left
	if (ts.num >= MAX_TESTS) {
		LOG("ERROR: We are out of space for tests\n");
		exit(EXIT_FAILURE);
	}

	struct test* test = &ts.tests[ts.num++];

	// Set parameters
	test->name = name;
	test->func = func;
	test->flags = flags;

	return 0;
}

static int check_whether_to_run(const struct test* t, const int argc, const char* argv[]) {
	// Run all tests when nothing has been selected
	if (argc < 2)
		return 1;

	// Check if this test has been listed
	for (int i = 1; i < argc; i++) {
		if (strcmp(t->name, argv[i]) == 0)
			return 1;
	}

	return 0;
}

int testsuite_run(int argc, const char* argv[]) {
	for (unsigned int i = 0; i < ts.num; i++) {
		struct test* test = &ts.tests[i];

		// Skip any tests that should not be run
		if (!check_whether_to_run(test, argc, argv))
			continue;

		// Run the test
		int r = test_run(i, test);
		if (r)
			exit(r);
	}

	return EXIT_SUCCESS;
}

FILE* test_mktemp(char** path) {
	char* p = NULL;

	// Reset path
	if (path)
		*path = NULL;

	int r = asprintf(&p, "%s", TMP_TEMPLATE);
	if (r < 0)
		return NULL;

	int fd = mkstemp(p);
	if (fd < 0)
		return NULL;

	// If we want a named temporary file, we set path
	if (path) {
		*path = p;

	// Otherwise we unlink the path and free p
	} else {
		unlink(p);
		free(p);
	}

	return fdopen(fd, "w+");
}

char* test_mkdtemp(void) {
	char path[] = TMP_TEMPLATE;

	char* p = mkdtemp(path);
	if (!p)
		return NULL;

	return strdup(path);
}
