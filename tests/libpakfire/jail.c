/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2022 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <sys/mount.h>

#include <pakfire/cgroup.h>
#include <pakfire/jail.h>

#include "../testsuite.h"

static const char* cmd_hello_world[] = {
	"/command", "echo", "Hello World!", NULL,
};

static const char* cmd_exhaust_memory[] = {
	"/command", "exhaust-memory", NULL,
};

static const char* cmd_fork_bomb[] = {
	"/command", "fork-bomb", NULL,
};

static const char* cmd_stat_ownership[] = {
	"/command", "stat-ownership", NULL,
};

static int test_create(const struct test* t) {
	struct pakfire_jail* jail = NULL;

	// Create a new jail
	ASSERT_SUCCESS(pakfire_jail_create(&jail, t->pakfire));

	// Destroy it
	ASSERT_NULL(pakfire_jail_unref(jail));

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_exit_code(const struct test* t) {
	struct pakfire_jail* jail = NULL;
	int r = EXIT_FAILURE;

	const char* argv[] = {
		"/command", "exit-with-code", "123", NULL,
	};

	// Create a new jail
	ASSERT_SUCCESS(pakfire_jail_create(&jail, t->pakfire));

	// Check if we receive the correct exit code
	ASSERT(pakfire_jail_exec_command(jail, argv, NULL, 0) == 123);

	// Success
	r = EXIT_SUCCESS;

FAIL:
	if (jail)
		pakfire_jail_unref(jail);

	return r;
}

static int test_segv(const struct test* t) {
	struct pakfire_jail* jail = NULL;
	int r = EXIT_FAILURE;

	const char* argv[] = {
		"/command", "segv", NULL,
	};

	// Create a new jail
	ASSERT_SUCCESS(pakfire_jail_create(&jail, t->pakfire));

	// Check if we receive the correct exit code
	ASSERT(pakfire_jail_exec_command(jail, argv, NULL, 0) == 139);

	// Success
	r = EXIT_SUCCESS;

FAIL:
	if (jail)
		pakfire_jail_unref(jail);

	return r;
}

static int test_exec(const struct test* t) {
	struct pakfire_jail* jail = NULL;
	int r = EXIT_FAILURE;
	char* output = NULL;
	size_t length = 0;

	// Create a new jail
	ASSERT_SUCCESS(pakfire_jail_create(&jail, t->pakfire));

	// Try to execute something
	ASSERT_SUCCESS(pakfire_jail_exec_capture_output(jail, cmd_hello_world, NULL, 0, &output, &length));

	// We should have some output
	ASSERT_STRING_EQUALS(output, "Hello World!\n");
	ASSERT_EQUALS(length, strlen("Hello World!\n"));

	// Destroy it
	ASSERT_NULL(pakfire_jail_unref(jail));

	// Success
	r = EXIT_SUCCESS;

FAIL:
	if (output)
		free(output);

	return r;
}

static int test_launch_into_cgroup(const struct test* t) {
	struct pakfire_cgroup* cgroup = NULL;
	struct pakfire_jail* jail = NULL;
	int r = EXIT_FAILURE;

	// Create a new cgroup
	ASSERT_SUCCESS(pakfire_cgroup_create(&cgroup, t->ctx, NULL, "pakfire-test", 0));

	// Create a new jail
	ASSERT_SUCCESS(pakfire_jail_create(&jail, t->pakfire));

	// Connect jail to the cgroup
	ASSERT_SUCCESS(pakfire_jail_set_cgroup(jail, cgroup));

	// Run command
	ASSERT(pakfire_jail_exec_command(jail, cmd_hello_world, NULL, 0) == 0);

	r = EXIT_SUCCESS;

FAIL:
	if (cgroup) {
		pakfire_cgroup_destroy(cgroup, PAKFIRE_CGROUP_DESTROY_RECURSIVE);
		pakfire_cgroup_unref(cgroup);
	}
	if (jail)
		pakfire_jail_unref(jail);

	return r;
}

static int test_nice(const struct test* t) {
	struct pakfire_jail* jail = NULL;
	char* output = NULL;
	int r = EXIT_FAILURE;

	const char* argv[] = {
		"/command", "print-nice", NULL,
	};

	// Create a new jail
	ASSERT_SUCCESS(pakfire_jail_create(&jail, t->pakfire));

	// Set invalid nice levels
	ASSERT_ERRNO(pakfire_jail_nice(jail,  100), EINVAL);
	ASSERT_ERRNO(pakfire_jail_nice(jail, -100), EINVAL);

	// Set something sane
	ASSERT_SUCCESS(pakfire_jail_nice(jail, 5));

	// Check if the nice level has been set
	ASSERT_SUCCESS(pakfire_jail_exec_capture_output(jail, argv, NULL, 0, &output, NULL));
	ASSERT_STRING_EQUALS(output, "5\n");

	// Success
	r = EXIT_SUCCESS;

FAIL:
	if (jail)
		pakfire_jail_unref(jail);
	if (output)
		free(output);

	return r;
}

static int test_memory_limit(const struct test* t) {
	struct pakfire_cgroup* cgroup = NULL;
	struct pakfire_jail* jail = NULL;
	int r = EXIT_FAILURE;


	// Create cgroup
	ASSERT_SUCCESS(pakfire_cgroup_create(&cgroup, t->ctx, NULL, "pakfire-test", 0));

	// Create jail
	ASSERT_SUCCESS(pakfire_jail_create(&jail, t->pakfire));

	// Connect jail to the cgroup
	ASSERT_SUCCESS(pakfire_jail_set_cgroup(jail, cgroup));

	// Set a memory limit of 100 MiB
	ASSERT_SUCCESS(pakfire_cgroup_set_memory_limit(cgroup, 100 * 1024 * 1024));

	// Try to exhaust all memory
	ASSERT_FAILURE(pakfire_jail_exec_command(jail, cmd_exhaust_memory, NULL, 0));

	// A fork bomb should also exhaust all memory
	ASSERT_FAILURE(pakfire_jail_exec_command(jail, cmd_fork_bomb, NULL, 0));

	// Success
	r = EXIT_SUCCESS;

FAIL:
	if (jail)
		pakfire_jail_unref(jail);
	if (cgroup) {
		pakfire_cgroup_destroy(cgroup, PAKFIRE_CGROUP_DESTROY_RECURSIVE);
		pakfire_cgroup_unref(cgroup);
	}

	return r;
}

static int test_pid_limit(const struct test* t) {
	struct pakfire_cgroup* cgroup = NULL;
	struct pakfire_jail* jail = NULL;
	int r = EXIT_FAILURE;

	// Create cgroup
	ASSERT_SUCCESS(pakfire_cgroup_create(&cgroup, t->ctx, NULL, "pakfire-test", 0));

	// Create jail
	ASSERT_SUCCESS(pakfire_jail_create(&jail, t->pakfire));

	// Connect jail to the cgroup
	ASSERT_SUCCESS(pakfire_jail_set_cgroup(jail, cgroup));

	// Set a PID limit of 100 processes
	ASSERT_SUCCESS(pakfire_cgroup_set_pid_limit(cgroup, 100));

	// Try to fork as many processes as possible
	ASSERT_FAILURE(pakfire_jail_exec_command(jail, cmd_fork_bomb, NULL, 0));

	// Success
	r = EXIT_SUCCESS;

FAIL:
	if (jail)
		pakfire_jail_unref(jail);
	if (cgroup) {
		pakfire_cgroup_destroy(cgroup, PAKFIRE_CGROUP_DESTROY_RECURSIVE);
		pakfire_cgroup_unref(cgroup);
	}

	return r;
}

static int test_file_ownership(const struct test* t) {
	int r = EXIT_FAILURE;
	char* output = NULL;

	// Execute a simple command
	ASSERT_SUCCESS(pakfire_jail_run(t->pakfire, cmd_stat_ownership, NULL, 0, &output, NULL));

	// Check if the file has been mapped to root/root
	ASSERT_STRING_EQUALS(output, "uid=0 gid=0\n");

	// Success
	r = EXIT_SUCCESS;

FAIL:
	if (output)
		free(output);

	return r;
}

static int test_bind(const struct test* t) {
	struct pakfire_jail* jail = NULL;
	int r = EXIT_FAILURE;

	const char* source = "/";
	const char* target = "/oldroot";

	const char* argv[] = {
		"/command", "check-mountpoint", target, NULL,
	};

	// Create a new jail
	ASSERT_SUCCESS(pakfire_jail_create(&jail, t->pakfire));

	// Bind-mount nonsense
	ASSERT_ERRNO(pakfire_jail_bind(jail, NULL, target, 0), EINVAL);
	ASSERT_ERRNO(pakfire_jail_bind(jail, source, NULL, 0), EINVAL);

	// Bind-mount something
	ASSERT_SUCCESS(pakfire_jail_bind(jail, source, target, MS_RDONLY));

	// Check if the mount actually works
	ASSERT_SUCCESS(pakfire_jail_exec_command(jail, argv, NULL, 0));

	// Success
	r = EXIT_SUCCESS;

FAIL:
	if (jail)
		pakfire_jail_unref(jail);

	return r;
}

static ssize_t callback_stdin(struct pakfire_ctx* ctx, void* data, char* buffer, size_t length) {
	int* lines = (int*)data;
	ssize_t bytes_written;

	// We are done
	if (!*lines)
		return 0;

	// Send another line
	bytes_written = snprintf(buffer, length, "LINE %d\n", *lines);
	if (bytes_written < 0) {
		LOG_ERROR("Could not write line (%d) to stdin: %m\n", *lines);

		return 1;
	}

	// Decrement the lines counter
	(*lines)--;

	return bytes_written;
}

static int test_communicate(const struct test* t) {
	struct pakfire_jail* jail = NULL;
	int r = EXIT_FAILURE;

	// How many lines to send?
	int lines = 65535;

	const char* argv[] = {
		"/command", "pipe", NULL,
	};

	// Create a new jail
	ASSERT_SUCCESS(pakfire_jail_create(&jail, t->pakfire));

	// Check if the mount actually works
	ASSERT_SUCCESS(pakfire_jail_communicate(jail, argv, NULL, 0,
		callback_stdin, &lines, NULL, NULL));

	// Success
	r = EXIT_SUCCESS;

FAIL:
	if (jail)
		pakfire_jail_unref(jail);

	return r;
}

static int test_send_one_signal(const struct test* t,
		struct pakfire_jail* jail, const char* signal) {
	const char* argv[] = {
		"/command", "send-signal", signal, NULL,
	};

	// Perform the command
	return pakfire_jail_exec_command(jail, argv, NULL, 0);
}

static int test_send_signal(const struct test* t) {
	struct pakfire_jail* jail = NULL;
	int r = EXIT_FAILURE;

	// Create a new jail
	ASSERT_SUCCESS(pakfire_jail_create(&jail, t->pakfire));

	// Sending SIGTERM to ourselves
	ASSERT(test_send_one_signal(t, jail, "15") == 0);

	// Sending SIGKILL to ourselves
	ASSERT(test_send_one_signal(t, jail, "9") == 0);

	// Sending SIGSTOP to ourselves (this should be ignored by the jail)
	ASSERT(test_send_one_signal(t, jail, "23") == 0);

	// Success
	r = EXIT_SUCCESS;

FAIL:
	if (jail)
		pakfire_jail_unref(jail);

	return r;
}

static int test_timeout(const struct test* t) {
	struct pakfire_jail* jail = NULL;
	int r = EXIT_FAILURE;

	const char* argv[] = {
		"/command", "sleep", "5", NULL,
	};

	// Create a new jail
	ASSERT_SUCCESS(pakfire_jail_create(&jail, t->pakfire));

	// Set a timeout of one second
	ASSERT_SUCCESS(pakfire_jail_set_timeout(jail, 1));

	// Check if we receive the correct exit code
	ASSERT(pakfire_jail_exec_command(jail, argv, NULL, 0) == 139);

	// Success
	r = EXIT_SUCCESS;

FAIL:
	if (jail)
		pakfire_jail_unref(jail);

	return r;
}

static int __callback(struct pakfire_ctx* ctx, void* data) {
	int* i = data;

	// Log a message
	INFO(ctx, "Callback called, returning %d\n", *i);

	return *i;
}

static int test_callback(const struct test* t) {
	struct pakfire_jail* jail = NULL;
	int r = EXIT_FAILURE;

	int i = 123;

	// Create a new jail
	ASSERT_SUCCESS(pakfire_jail_create(&jail, t->pakfire));

	// Check if we receive the correct exit code
	ASSERT(pakfire_jail_exec(jail, __callback, &i, 0) == 123);

	// Success
	r = EXIT_SUCCESS;

FAIL:
	if (jail)
		pakfire_jail_unref(jail);

	return r;
}

int main(int argc, const char* argv[]) {
	testsuite_add_test(test_create, TEST_WANTS_PAKFIRE);
	testsuite_add_test(test_exit_code, TEST_WANTS_PAKFIRE);
	testsuite_add_test(test_segv, TEST_WANTS_PAKFIRE);
	testsuite_add_test(test_exec, TEST_WANTS_PAKFIRE);
#if 0
	testsuite_add_test(test_launch_into_cgroup, TEST_WANTS_PAKFIRE);
#endif
	testsuite_add_test(test_nice, TEST_WANTS_PAKFIRE);
#if 0
	testsuite_add_test(test_memory_limit, TEST_WANTS_PAKFIRE);
	testsuite_add_test(test_pid_limit, TEST_WANTS_PAKFIRE);
	testsuite_add_test(test_file_ownership, TEST_WANTS_PAKFIRE);
#endif
	testsuite_add_test(test_bind, TEST_WANTS_PAKFIRE);
	testsuite_add_test(test_communicate, TEST_WANTS_PAKFIRE);
	testsuite_add_test(test_send_signal, TEST_WANTS_PAKFIRE);
	testsuite_add_test(test_timeout, TEST_WANTS_PAKFIRE);
	testsuite_add_test(test_callback, TEST_WANTS_PAKFIRE);

	return testsuite_run(argc, argv);
}
