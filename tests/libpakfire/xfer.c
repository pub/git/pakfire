/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <pakfire/hashes.h>
#include <pakfire/xfer.h>

#include "../testsuite.h"

#define DOWNLOAD_URL "file://" ABS_TOP_SRCDIR "/tests/data/beep-1.3-2.ip3.x86_64.pfm"

#define ONLINE_DOWNLOAD_URL "https://mirror1.ipfire.org/releases/pakfire/pakfire-0.9.27.tar.gz"

// Just a file with some random data
#define RANDOM_FILE "file://" ABS_TOP_SRCDIR "/tests/data/digest/random"

static const unsigned char RANDOM_FILE_sha2_512[] = {
	0xa1, 0x98, 0x97, 0x9b, 0x13, 0x44, 0xf3, 0x64, 0x48, 0x0e, 0x0f, 0xd1,
	0xbc, 0xde, 0x8f, 0x50, 0xf3, 0x3c, 0x92, 0x8e, 0xf0, 0xe7, 0x5d, 0x6e,
	0x1a, 0x28, 0x67, 0x1e, 0x16, 0x49, 0x7b, 0xc2, 0xd6, 0x64, 0xe5, 0x21,
	0x12, 0x60, 0x0f, 0x56, 0x3d, 0x8f, 0x0a, 0x7c, 0x24, 0x1a, 0x7e, 0x5a,
	0x16, 0xe7, 0xe7, 0x27, 0x33, 0xd9, 0x66, 0xb5, 0x0a, 0x39, 0x6d, 0xa8,
	0xb2, 0xb4, 0xac, 0x05,
};

static int test_create(const struct test* t) {
	struct pakfire_xfer* xfer = NULL;
	int r = EXIT_FAILURE;

	// Create a new transfer
	ASSERT_SUCCESS(pakfire_xfer_create_simple(&xfer, t->ctx, "file://invalid"));

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (xfer)
		pakfire_xfer_unref(xfer);

	return r;
}

static int test_download(const struct test* t) {
	struct pakfire_xfer* xfer = NULL;
	int r = EXIT_FAILURE;

	// Create a new transfer
	ASSERT_SUCCESS(pakfire_xfer_create_simple(&xfer, t->ctx, DOWNLOAD_URL));

	// Run it!
	ASSERT_SUCCESS(pakfire_xfer_run(xfer, PAKFIRE_XFER_NO_PROGRESS));

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (xfer)
		pakfire_xfer_unref(xfer);

	return r;
}

static int test_download_into_buffer(const struct test* t) {
	struct pakfire_xfer* xfer = NULL;
	char* buffer = NULL;
	size_t length = 0;
	int r = EXIT_FAILURE;

	// Create a new transfer
	ASSERT_SUCCESS(pakfire_xfer_create_simple(&xfer, t->ctx, DOWNLOAD_URL));

	// Write the data to the buffer
	ASSERT_SUCCESS(pakfire_xfer_set_output_buffer(xfer, &buffer, &length));

	// Run it!
	ASSERT_SUCCESS(pakfire_xfer_run(xfer, PAKFIRE_XFER_NO_PROGRESS));

	// Check that the buffer contains some data
	ASSERT(buffer);
	ASSERT(length > 0);

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (xfer)
		pakfire_xfer_unref(xfer);
	if (buffer)
		free(buffer);

	return r;
}

static int test_download_check_digest(const struct test* t) {
	struct pakfire_hashes hashes = {};
	struct pakfire_xfer* xfer = NULL;
	char* buffer = NULL;
	size_t length = 0;
	int r = EXIT_FAILURE;

	ASSERT_SUCCESS(pakfire_hashes_set(&hashes,
		PAKFIRE_HASH_SHA2_512, RANDOM_FILE_sha2_512, sizeof(RANDOM_FILE_sha2_512)));

	// Create a new transfer
	ASSERT_SUCCESS(pakfire_xfer_create_simple(&xfer, t->ctx, RANDOM_FILE));

	// Write the data to the buffer
	ASSERT_SUCCESS(pakfire_xfer_set_output_buffer(xfer, &buffer, &length));

	// Set the digest value
	ASSERT_SUCCESS(pakfire_xfer_verify_hashes(xfer, &hashes));

	// Run it!
	ASSERT_SUCCESS(pakfire_xfer_run(xfer, PAKFIRE_XFER_NO_PROGRESS));

	// Check that the buffer contains some data
	ASSERT(buffer);
	ASSERT(length > 0);

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (xfer)
		pakfire_xfer_unref(xfer);
	if (buffer)
		free(buffer);

	return r;
}

static int test_download_check_incorrect_digest(const struct test* t) {
	struct pakfire_hashes hashes = {};
	struct pakfire_xfer* xfer = NULL;
	char* buffer = NULL;
	size_t length = 0;
	int r = EXIT_FAILURE;

	// Store the hash
	ASSERT_SUCCESS(pakfire_hashes_set(&hashes,
		PAKFIRE_HASH_SHA3_512, RANDOM_FILE_sha2_512, sizeof(RANDOM_FILE_sha2_512)));

	// Create a new transfer
	ASSERT_SUCCESS(pakfire_xfer_create_simple(&xfer, t->ctx, RANDOM_FILE));

	// Write the data to the buffer
	ASSERT_SUCCESS(pakfire_xfer_set_output_buffer(xfer, &buffer, &length));

	// Set the digest value, but use a wrong digest method
	ASSERT_SUCCESS(pakfire_xfer_verify_hashes(xfer, &hashes));

	// Run it!
	ASSERT(pakfire_xfer_run(xfer, PAKFIRE_XFER_NO_PROGRESS) == PAKFIRE_XFER_DIGEST_MISMATCH);

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (xfer)
		pakfire_xfer_unref(xfer);
	if (buffer)
		free(buffer);

	return r;
}

#ifdef ENABLE_ONLINE_TESTS
static int test_online_download(const struct test* t) {
	struct pakfire_xfer* xfer = NULL;
	int r = EXIT_FAILURE;

	// Create a new transfer
	ASSERT_SUCCESS(pakfire_xfer_create_simple(&xfer, t->ctx, ONLINE_DOWNLOAD_URL));

	// Run it!
	ASSERT_SUCCESS(pakfire_xfer_run(xfer, PAKFIRE_XFER_NO_PROGRESS));

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (xfer)
		pakfire_xfer_unref(xfer);

	return r;
}
#endif /* ENABLE_ONLINE_TESTS */

int main(int argc, const char* argv[]) {
	testsuite_add_test(test_create, 0);

	// Download files
	testsuite_add_test(test_download, 0);
	testsuite_add_test(test_download_into_buffer, 0);

	// Check digests
	testsuite_add_test(test_download_check_digest, 0);
	testsuite_add_test(test_download_check_incorrect_digest, 0);

#ifdef ENABLE_ONLINE_TESTS
	testsuite_add_test(test_online_download, 0);
#endif /* ENABLE_ONLINE_TESTS */

	return testsuite_run(argc, argv);
}
