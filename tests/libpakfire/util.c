/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2019 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <pakfire/base64.h>
#include <pakfire/path.h>
#include <pakfire/util.h>

#include "../testsuite.h"

static int test_mkdir(const struct test* t) {
	char path[PATH_MAX];

	// Create some random path
	ASSERT_SUCCESS(pakfire_path(t->pakfire, path, "%s", "/a/b/c/d/e"));

	// Create this directory
	ASSERT_SUCCESS(pakfire_mkdir(path, 0755));

	// Check if this exists
	ASSERT_SUCCESS(!pakfire_path_exists(path));

	// Create path again (nothing should happen)
	ASSERT_SUCCESS(pakfire_mkdir(path, 0755));

	// Create the parent directory for /test/1/file.txt
	ASSERT_SUCCESS(pakfire_mkparentdir("/test/1/file.txt", 0755));

	// Check that only the parent directory exists
	ASSERT_SUCCESS(!pakfire_path_exists("/test/1"));
	ASSERT_SUCCESS(!!pakfire_path_exists("/test/1/file.txt"));

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_base64(const struct test* t) {
	int r = EXIT_FAILURE;

	const char data[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
	char* base64 = NULL;
	void* output = NULL;
	size_t length = 0;

	// Encode data as base64
	ASSERT_SUCCESS(pakfire_b64encode(t->ctx, &base64, data, sizeof(data)));

	// Print the encoded data
	printf("%s\n", base64);

	// Decode the data
	ASSERT_SUCCESS(pakfire_b64decode(t->ctx, &output, &length, base64));

	// Print the decoded data
	printf("%.*s\n", (int)length, (char*)output);

	// Check that we encoded the correct amount of data
	ASSERT_EQUALS(length, sizeof(data));

	// Check that the output matches the input
	ASSERT(memcmp(data, output, sizeof(data)) == 0);

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (base64)
		free(base64);
	if (output)
		free(output);

	return r;
}

int main(int argc, const char* argv[]) {
	testsuite_add_test(test_mkdir, TEST_WANTS_PAKFIRE);
	testsuite_add_test(test_base64, 0);

	return testsuite_run(argc, argv);
}
