/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2019 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <linux/limits.h>
#include <stdlib.h>
#include <string.h>

#include <pakfire/parser.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

#include "../testsuite.h"

static int test_parser(const struct test* t) {
	struct pakfire_parser* parser = NULL;
	struct pakfire_parser* subparser = NULL;
	int r = EXIT_FAILURE;
	char* s = NULL;

	// Create a new parser
	ASSERT_SUCCESS(pakfire_parser_create(&parser, t->pakfire, NULL, NULL, 0));

	// Retrieve a value that does not exist
	ASSERT_NULL(pakfire_parser_get(parser, NULL, "null"));

	// Set a value
	ASSERT_SUCCESS(pakfire_parser_set(parser, NULL, "a", "a", 0));

	// Retrieve the value again
	ASSERT_STRING_EQUALS_FREE(pakfire_parser_get(parser, NULL, "a"), "a");

	// Append something to the value
	ASSERT_SUCCESS(pakfire_parser_append(parser, NULL, "a", "b"));

	// Retrieve the value again
	ASSERT_STRING_EQUALS_FREE(pakfire_parser_get(parser, NULL, "a"), "a b");

	// Make a child parser
	subparser = pakfire_parser_create_child(parser, "child");
	ASSERT(subparser);

	// Try to get a again
	ASSERT_STRING_EQUALS_FREE(pakfire_parser_get(subparser, NULL, "a"), "a b");

	// Append something to the subparser
	ASSERT_SUCCESS(pakfire_parser_append(subparser, NULL, "a", "c"));

	// The subparser should return "a b c"
	ASSERT_STRING_EQUALS_FREE(pakfire_parser_get(subparser, NULL, "a"), "a b c");

	// The original parser should remain unchanged
	ASSERT_STRING_EQUALS_FREE(pakfire_parser_get(parser, NULL, "a"), "a b");

	// Set another value
	ASSERT_SUCCESS(pakfire_parser_append(subparser, NULL, "b", "1"));

	// Merge the two parsers
	pakfire_parser_merge(parser, subparser);

	// Set a variable
	ASSERT_SUCCESS(pakfire_parser_set(parser, NULL, "c", "%{b}", 0));

	// Get the value of c
	ASSERT_STRING_EQUALS_FREE(pakfire_parser_get(parser, NULL, "c"), "");

	// Dump the parser
	s = pakfire_parser_dump(parser);
	printf("%s\n", s);

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (subparser)
		pakfire_parser_unref(subparser);
	if (parser)
		pakfire_parser_unref(parser);
	if (s)
		free(s);

	return r;
}

static const char* files[] = {
	"data/parser/test-comments.txt",
	"data/parser/test-conditionals.txt",
	"data/parser/test-declarations.txt",
	"data/parser/test-subparsers.txt",

	// Some real life examples that caused trouble
	"data/parser/perl.info",
	NULL,
};

static int test_parser_files(const struct test* t) {
	struct pakfire_parser* parser = NULL;
	const char** file = files;
	char path[PATH_MAX];
	int r = EXIT_FAILURE;

	while (*file) {
		ASSERT_SUCCESS(pakfire_string_format(path, "%s/%s", TEST_SRC_PATH, *file));

		// Create a new parser
		ASSERT_SUCCESS(pakfire_parser_create(&parser, t->pakfire, NULL, NULL, 0));

		FILE* f = fopen(path, "r");
		ASSERT(f);

		r = pakfire_parser_read(parser, f, NULL);
		if (r) {
			fprintf(stderr, "Could not parse %s\n", path);
			return EXIT_FAILURE;
		}

		fclose(f);
		pakfire_parser_unref(parser);

		// Next file
		file++;
	}

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	return r;
}

static int test_append(const struct test* t) {
	struct pakfire_parser* parser = NULL;
	int r = EXIT_FAILURE;
	char* s = NULL;

	// Create a new parser
	ASSERT_SUCCESS(pakfire_parser_create(&parser, t->pakfire, NULL, NULL, 0));

	const char* str1 =
		"a  = 1\n"
		"a += 2\n";

	// Parse something
	ASSERT_SUCCESS(pakfire_parser_parse(parser, str1, strlen(str1), NULL));

	ASSERT_STRING_EQUALS(pakfire_parser_get(parser, NULL, "a"), "1 2");

	const char* str2 =
		"build\n"

		// Append something to a variable that exists outside of this block
		"	a += 3\n"

		// Check if we can inherit a local value correctly
		"	b = %{a}\n"

		// Define a new variable in this block and append something
		"	c = 10\n"
		"	c += 20\n"
		"end\n";

	ASSERT_SUCCESS(pakfire_parser_parse(parser, str2, strlen(str2), NULL));

	ASSERT_STRING_EQUALS_FREE(pakfire_parser_get(parser, "build", "a"), "1 2 3");
	ASSERT_STRING_EQUALS_FREE(pakfire_parser_get(parser, "build", "b"), "1 2 3");
	ASSERT_STRING_EQUALS_FREE(pakfire_parser_get(parser, "build", "c"), "10 20");

	// Dump the parser
	s = pakfire_parser_dump(parser);
	printf("%s\n", s);

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (parser)
		pakfire_parser_unref(parser);
	if (s)
		free(s);

	return r;
}
#if 0
static int test_parser_command(const struct test* t) {
	const char* command = "%(echo \"ABC\")";
	struct pakfire_parser* parser = NULL;
	int r = EXIT_FAILURE;

	ASSERT_SUCCESS(pakfire_parser_create(&parser, t->pakfire, NULL, NULL, PAKFIRE_PARSER_FLAGS_EXPAND_COMMANDS));

	ASSERT_SUCCESS(pakfire_parser_set(parser, NULL, "command", command, 0));

	// Retrieve the expanded value
	ASSERT_STRING_EQUALS_FREE(pakfire_parser_get(parser, NULL, "command"), "ABC");

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (parser)
		pakfire_parser_unref(parser);

	return r;
}
#endif

int main(int argc, const char* argv[]) {
	testsuite_add_test(test_parser, TEST_WANTS_PAKFIRE);
	testsuite_add_test(test_parser_files, TEST_WANTS_PAKFIRE);
	testsuite_add_test(test_append, TEST_WANTS_PAKFIRE);
#if 0
	testsuite_add_test(test_parser_command, TEST_WANTS_PAKFIRE);
#endif

	return testsuite_run(argc, argv);
}
