/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2024 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <sys/time.h>

#include <pakfire/log_buffer.h>

#include "../testsuite.h"

static int test_simple(const struct test* t) {
	struct pakfire_log_buffer* buffer = NULL;
	struct timeval timestamp = {};
	char* line = NULL;
	size_t length = 0;
	int priority = 0;
	int r = EXIT_FAILURE;

	ASSERT_SUCCESS(pakfire_log_buffer_create(&buffer, t->ctx, 0));

	// Enqueue three strings
	ASSERT_SUCCESS(pakfire_log_buffer_enqueue(buffer, LOG_DEBUG, "A", -1));
	ASSERT_SUCCESS(pakfire_log_buffer_enqueue(buffer, LOG_DEBUG, "BB", -1));
	ASSERT_SUCCESS(pakfire_log_buffer_enqueue(buffer, LOG_DEBUG, "CCC", -1));

	// Dequeue the first string
	ASSERT_SUCCESS(pakfire_log_buffer_dequeue(buffer, &timestamp, &priority, &line, &length));

	ASSERT_EQUALS(priority, LOG_DEBUG);
	ASSERT_STRING_EQUALS(line, "A");
	ASSERT_EQUALS(length, 1);
	free(line);

	// Dequeue the second string
	ASSERT_SUCCESS(pakfire_log_buffer_dequeue(buffer, &timestamp, &priority, &line, &length));

	ASSERT_EQUALS(priority, LOG_DEBUG);
	ASSERT_STRING_EQUALS(line, "BB");
	ASSERT_EQUALS(length, 2);
	free(line);

	// Dequeue the third string
	ASSERT_SUCCESS(pakfire_log_buffer_dequeue(buffer, &timestamp, &priority, &line, &length));

	ASSERT_EQUALS(priority, LOG_DEBUG);
	ASSERT_STRING_EQUALS(line, "CCC");
	ASSERT_EQUALS(length, 3);
	free(line);

	// Dequeue more than we put in
	ASSERT_SUCCESS(pakfire_log_buffer_dequeue(buffer, &timestamp, &priority, &line, &length));

	ASSERT_EQUALS(priority, -1);
	ASSERT_EQUALS(line, NULL);
	ASSERT_EQUALS(length, 0);

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (buffer)
		pakfire_log_buffer_unref(buffer);

	return r;
}

static int test_wrong_usage(const struct test* t) {
	struct pakfire_log_buffer* buffer = NULL;
	struct timeval timestamp = {};
	int priority = -1;
	char* line = NULL;
	int r = EXIT_FAILURE;

	// Create buffer
	ASSERT_SUCCESS(pakfire_log_buffer_create(&buffer, t->ctx, 0));

	// Wrong enqueue
	ASSERT_ERROR(pakfire_log_buffer_enqueue(buffer, -1, "", -1), EINVAL);
	ASSERT_ERROR(pakfire_log_buffer_enqueue(buffer, LOG_DEBUG, NULL, -1), EINVAL);

	// Wrong dequeue
	ASSERT_ERROR(pakfire_log_buffer_dequeue(buffer, &timestamp, NULL, &line, NULL), EINVAL);
	ASSERT_ERROR(pakfire_log_buffer_dequeue(buffer, &timestamp, &priority, NULL, NULL), EINVAL);
	ASSERT_ERROR(pakfire_log_buffer_dequeue(buffer, NULL, &priority, NULL, NULL), EINVAL);

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (buffer)
		pakfire_log_buffer_unref(buffer);

	return r;
}

static int test_full(const struct test* t) {
	struct pakfire_log_buffer* buffer = NULL;
	struct timeval timestamp = {};
	char* line = NULL;
	size_t length = 0;
	int priority = 0;
	int r = EXIT_FAILURE;

	// Create buffer
	ASSERT_SUCCESS(pakfire_log_buffer_create(&buffer, t->ctx, 2));

	// Enqueue something
	ASSERT_SUCCESS(pakfire_log_buffer_enqueue(buffer, LOG_DEBUG, "Line 1", -1));
	ASSERT_SUCCESS(pakfire_log_buffer_enqueue(buffer, LOG_DEBUG, "Line 2", -1));

	// There should be no more space for a third line, so the first line should get dropped
	ASSERT_SUCCESS(pakfire_log_buffer_enqueue(buffer, LOG_DEBUG, "Line 3", -1));

	// On dequeue we should now miss Line 1, but get Line 2 instead
	ASSERT_SUCCESS(pakfire_log_buffer_dequeue(buffer, &timestamp, &priority, &line, &length));
	ASSERT_STRING_EQUALS(line, "Line 2");
	free(line);

	// On the next iteration, we should get Line 3
	ASSERT_SUCCESS(pakfire_log_buffer_dequeue(buffer, &timestamp, &priority, &line, &length));
	ASSERT_STRING_EQUALS(line, "Line 3");
	free(line);

	// On the next iteration, the buffer should be empty
	ASSERT_SUCCESS(pakfire_log_buffer_dequeue(buffer, &timestamp, &priority, &line, &length));
	ASSERT(line == NULL);

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (buffer)
		pakfire_log_buffer_unref(buffer);

	return r;
}

int main(int argc, const char* argv[]) {
	testsuite_add_test(test_simple, 0);
	testsuite_add_test(test_wrong_usage, 0);
	testsuite_add_test(test_full, 0);

	return testsuite_run(argc, argv);
}
