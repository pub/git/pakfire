/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2017 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <stdlib.h>
#include <string.h>

#include <pakfire/key.h>
#include <pakfire/util.h>

#include "../testsuite.h"

const char DATA[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
const size_t DATA_LENGTH = sizeof(DATA);

static int test_generate(const struct test* t) {
	struct pakfire_key* key = NULL;
	int r = EXIT_FAILURE;

	// Try to call pakfire_key_generate() with some invalid inputs
	ASSERT(pakfire_key_generate(&key, t->ctx, PAKFIRE_KEY_ALGO_NULL, "Key 1") == -EINVAL);

	// Generate a new key using ed25519
	ASSERT_SUCCESS(pakfire_key_generate(&key, t->ctx,
		PAKFIRE_KEY_ALGO_ED25519, "Key 2"));

	// Write the public key to the console
	ASSERT_SUCCESS(pakfire_key_export(key, stdout, PAKFIRE_KEY_EXPORT_MODE_PUBLIC));

	// Write the private key to the console
	ASSERT_SUCCESS(pakfire_key_export(key, stdout, PAKFIRE_KEY_EXPORT_MODE_PRIVATE));

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (key)
		pakfire_key_unref(key);

	return r;
}

static int test_sign_and_verify(const struct test* t) {
	struct pakfire_key* key = NULL;
	int r = EXIT_FAILURE;
	FILE* f = NULL;

	// Create a file handle to write the signature to
	ASSERT(f = test_mktemp(NULL));

	// Generate a new key using ed25519
	ASSERT_SUCCESS(pakfire_key_generate(&key, t->ctx,
		PAKFIRE_KEY_ALGO_ED25519, "Key 1"));

	// Write the public key to the console
	ASSERT_SUCCESS(pakfire_key_export(key, stdout, PAKFIRE_KEY_EXPORT_MODE_PUBLIC));

	// Write the private key to the console
	ASSERT_SUCCESS(pakfire_key_export(key, stdout, PAKFIRE_KEY_EXPORT_MODE_PRIVATE));

	// Sign!
	ASSERT_SUCCESS(pakfire_key_sign_string(key, f, DATA, DATA_LENGTH, "UNTRUSTED COMMENT"));

	// Rewind the file
	rewind(f);

	// Verify the signature
	ASSERT_SUCCESS(pakfire_key_verify(key, f, DATA, DATA_LENGTH));

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (key)
		pakfire_key_unref(key);
	if (f)
		fclose(f);

	return r;
}

static int test_import_public(const struct test* t) {
	struct pakfire_key* key = NULL;
	FILE* f = NULL;
	int r = EXIT_FAILURE;

	// Open the public key
	ASSERT(f = fopen(TEST_DATA_DIR "/keys/key1.pub", "r"));

	// Try to import the key
	ASSERT_SUCCESS(pakfire_key_import(&key, t->ctx, f));

	// Write the imported key to the console
	ASSERT_SUCCESS(pakfire_key_export(key, stdout, PAKFIRE_KEY_EXPORT_MODE_PUBLIC));

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (key)
		pakfire_key_unref(key);
	if (f)
		fclose(f);

	return r;
}

static int test_import_secret(const struct test* t) {
	struct pakfire_key* key = NULL;
	FILE* f = NULL;
	int r = EXIT_FAILURE;

	// Open the public key
	ASSERT(f = fopen(TEST_DATA_DIR "/keys/key1.sec", "r"));

	// Try to import the key
	ASSERT_SUCCESS(pakfire_key_import(&key, t->ctx, f));

	// Write the imported key to the console
	ASSERT_SUCCESS(pakfire_key_export(key, stdout, PAKFIRE_KEY_EXPORT_MODE_PRIVATE));

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (key)
		pakfire_key_unref(key);
	if (f)
		fclose(f);

	return r;
}

int main(int argc, const char* argv[]) {
	testsuite_add_test(test_generate, 0);
	testsuite_add_test(test_sign_and_verify, 0);
	testsuite_add_test(test_import_public, 0);
	testsuite_add_test(test_import_secret, 0);

	return testsuite_run(argc, argv);
}
