/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2025 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <pakfire/log_stream.h>
#include <pakfire/util.h>

#include "../testsuite.h"

static int log_callback(struct pakfire_log_stream* stream,
		const char* line, size_t length, void* data) {
	int* lines_read = data;

	// Increment the counter
	if (lines_read)
		(*lines_read)++;

	printf("LINE = %.*s", (int)length, line);

	return 0;
}

static int test_simple(const struct test* t) {
	struct pakfire_log_stream* stream = NULL;
	int r = EXIT_FAILURE;

	ASSERT_SUCCESS(pakfire_log_stream_create(&stream, t->ctx, NULL, NULL));

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (stream)
		pakfire_log_stream_unref(stream);

	return r;
}

static int test_fork(const struct test* t) {
	struct pakfire_log_stream* stream = NULL;
	sd_event* loop = NULL;
	int r = EXIT_FAILURE;

	// How many lines to write
	const int lines_to_write = 1000;
	int lines_read = 0;

	// Fetch the default event loop
	ASSERT(sd_event_default(&loop) >= 0);

	ASSERT_SUCCESS(pakfire_log_stream_create(&stream, t->ctx, log_callback, &lines_read));

	// Fork the process
	pid_t pid = fork();
	if (pid < 0)
		abort();

	// Child Code
	else if (pid == 0) {
		ASSERT_SUCCESS(pakfire_log_stream_in_child(stream));

		// Write a message into the log stream
		for (int i = 0; i < lines_to_write; i++)
			pakfire_log_stream_printf(stream, "LINE %d\n", i);

		// End here
		_exit(0);

	// Parent Code
	} else {
		ASSERT_SUCCESS(pakfire_log_stream_in_parent(stream, loop));

		// Run the event loop
		for (;;) {
			r = sd_event_run(loop, S_TO_US(0.1));
			if (r == 0)
				break;
		}
	}

	// Check if we have received the entire input
	ASSERT_EQUALS(lines_to_write, lines_read);

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (stream)
		pakfire_log_stream_unref(stream);
	if (loop)
		sd_event_unref(loop);

	return r;
}


int main(int argc, const char* argv[]) {
	testsuite_add_test(test_simple, 0);
	testsuite_add_test(test_fork, 0);

	return testsuite_run(argc, argv);
}
