/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2025 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <pakfire/daemon.h>

#include "../testsuite.h"

static int test_daemon(const struct test* t) {
	struct pakfire_daemon* daemon = NULL;
	const char* url = NULL;
	int r = EXIT_FAILURE;

	// We can only create the daemon
	ASSERT_SUCCESS(pakfire_daemon_create(&daemon, t->ctx));

	// Fetch the URL
	ASSERT(url = pakfire_daemon_url(daemon));
	ASSERT_STRING_EQUALS(url, "https://pakfire.ipfire.org/");

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (daemon)
		pakfire_daemon_unref(daemon);

	return r;
}

int main(int argc, const char* argv[]) {
	testsuite_add_test(test_daemon, 0);

	return testsuite_run(argc, argv);
}
