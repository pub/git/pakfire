/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <pakfire/os.h>

#include "../testsuite.h"

// This test parses /proc/cpuinfo
static int test_cpuinfo(const struct test* t) {
	struct pakfire_cpuinfo cpuinfo = {};

	ASSERT_SUCCESS(pakfire_cpuinfo(&cpuinfo));

	// We can only check is something has been read
	ASSERT(*cpuinfo.vendor);
	ASSERT(*cpuinfo.model);

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

// This test parses /proc/stat
static int test_cpustat(const struct test* t) {
	struct pakfire_cpustat cpustat = {};

	ASSERT_SUCCESS(pakfire_cpustat(&cpustat));

	ASSERT(cpustat.user       >= 0);
	ASSERT(cpustat.nice       >= 0);
	ASSERT(cpustat.system     >= 0);
	ASSERT(cpustat.idle       >= 0);
	ASSERT(cpustat.iowait     >= 0);
	ASSERT(cpustat.irq        >= 0);
	ASSERT(cpustat.softirq    >= 0);
	ASSERT(cpustat.steal      >= 0);
	ASSERT(cpustat.guest      >= 0);
	ASSERT(cpustat.guest_nice >= 0);

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

// This test parses /proc/loadavg
static int test_loadavg(const struct test* t) {
	struct pakfire_loadavg loadavg = {};

	ASSERT_SUCCESS(pakfire_loadavg(&loadavg));

	ASSERT(loadavg.load1  >= 0);
	ASSERT(loadavg.load5  >= 0);
	ASSERT(loadavg.load15 >= 0);

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

// This test parses /proc/meminfo
static int test_meminfo(const struct test* t) {
	struct pakfire_meminfo meminfo = {};

	ASSERT_SUCCESS(pakfire_meminfo(&meminfo));

	// We can only check is something has been read
	ASSERT(meminfo.total > 0);
	ASSERT(meminfo.used > 0);
	ASSERT(meminfo.free > 0);
	ASSERT(meminfo.available > 0);
	ASSERT(meminfo.buffers > 0);
	ASSERT(meminfo.cached > 0);
	ASSERT(meminfo.active > 0);
	ASSERT(meminfo.inactive > 0);

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

// This test parses /etc/os-release
static int test_distro(const struct test* t) {
	struct pakfire_distro distro = {};

	ASSERT_SUCCESS(pakfire_distro(&distro, NULL));

	ASSERT(*distro.name);
	ASSERT(*distro.version || *distro.version_id);

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

int main(int argc, const char* argv[]) {
	testsuite_add_test(test_cpuinfo, 0);
	testsuite_add_test(test_cpustat, 0);
	testsuite_add_test(test_loadavg, 0);
	testsuite_add_test(test_meminfo, 0);
	testsuite_add_test(test_distro, 0);

	return testsuite_run(argc, argv);
}
