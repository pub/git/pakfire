/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <grp.h>
#include <linux/limits.h>
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef HAVE_SUBID
#include <shadow/subid.h>
#endif

#include <pakfire/ctx.h>
#include <pakfire/logging.h>
#include <pakfire/pakfire.h>
#include <pakfire/pwd.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

#define ETC_SUBUID "/etc/subuid"
#define ETC_SUBGID "/etc/subgid"

static struct passwd* pakfire_getpwent(struct pakfire* pakfire,
		int(*cmp)(struct passwd* entry, const void* value), const void* value) {
	struct passwd* entry = NULL;
	char path[PATH_MAX];
	int r;

	// Get path to /etc/passwd
	r = pakfire_path(pakfire, path, "%s", "/etc/passwd");
	if (r)
		return NULL;

	FILE* f = fopen(path, "r");
	if (!f)
		goto END;

	for (;;) {
		// Parse entry
		entry = fgetpwent(f);
		if (!entry)
			goto END;

		// Check if this is what we are looking for
		if (cmp(entry, value))
			break;
		else
			entry = NULL;
	}

END:
	if (f)
		fclose(f);

	return entry;
}

static int __pakfire_getpwnam(struct passwd* entry, const void* value) {
	const char* name = (const char*)value;

	if (strcmp(entry->pw_name, name) == 0)
		return 1;

	return 0;
}

struct passwd* pakfire_getpwnam(struct pakfire* pakfire, const char* name) {
	return pakfire_getpwent(pakfire, __pakfire_getpwnam, name);
}

static int __pakfire_getpwuid(struct passwd* entry, const void* value) {
	uid_t* uid = (uid_t*)value;

	if (entry->pw_uid == *uid)
		return 1;

	return 0;
}

struct passwd* pakfire_getpwuid(struct pakfire* pakfire, uid_t uid) {
	return pakfire_getpwent(pakfire, __pakfire_getpwuid, &uid);
}

static struct group* pakfire_getgrent(struct pakfire* pakfire,
		int(*cmp)(struct group* entry, const void* value), const void* value) {
	struct group* entry = NULL;
	char path[PATH_MAX];
	int r;

	// Get path to /etc/group
	r = pakfire_path(pakfire, path, "%s", "/etc/group");
	if (r)
		return NULL;

	FILE* f = fopen(path, "r");
	if (!f)
		goto END;

	for (;;) {
		// Parse entry
		entry = fgetgrent(f);
		if (!entry)
			goto END;

		// Check if this is what we are looking for
		if (cmp(entry, value))
			break;
		else
			entry = NULL;
	}

END:
	if (f)
		fclose(f);

	return entry;
}

static int __pakfire_getgrnam(struct group* entry, const void* value) {
	const char* name = (const char*)value;

	if (strcmp(entry->gr_name, name) == 0)
		return 1;

	return 0;
}

struct group* pakfire_getgrnam(struct pakfire* pakfire, const char* name) {
	return pakfire_getgrent(pakfire, __pakfire_getgrnam, name);
}

static int __pakfire_getgrgid(struct group* entry, const void* value) {
	gid_t* gid = (gid_t*)value;

	if (entry->gr_gid == *gid)
		return 1;

	return 0;
}

struct group* pakfire_getgrgid(struct pakfire* pakfire, gid_t gid) {
	return pakfire_getgrent(pakfire, __pakfire_getgrgid, &gid);
}

// SUBUID/SUBGID

#ifdef HAVE_SUBID

static int pakfire_getsubid(struct pakfire* pakfire, const char* owner,
		struct pakfire_subid* subid, int (callback)(const char* owner, struct subid_range** ranges)) {
	struct subid_range* ranges = NULL;
	int count;
	int r = -1;

	struct pakfire_ctx* ctx = pakfire_ctx(pakfire);

	r = subid_init(PACKAGE_NAME, stderr);
	if (r) {
		ERROR(ctx, "Could not setup subid: %m\n");
		goto ERROR;
	}

	count = callback(owner, &ranges);
	if (count < 0) {
		ERROR(ctx, "Could not fetch subids for %s: %m\n", owner);
		goto ERROR;
	}

	// Store the result
	for (int i = 0; i < count; i++) {
		subid->id     = ranges[i].start;
		subid->length = ranges[i].count;
		r = 0;
		break;
	}

ERROR:
	if (ranges)
		free(ranges);
	if (ctx)
		pakfire_ctx_unref(ctx);

	return r;
}

int pakfire_getsubuid(struct pakfire* pakfire, const char* owner, struct pakfire_subid* subid) {
	return pakfire_getsubid(pakfire, owner, subid, subid_get_uid_ranges);
}

int pakfire_getsubgid(struct pakfire* pakfire, const char* owner, struct pakfire_subid* subid) {
	return pakfire_getsubid(pakfire, owner, subid, subid_get_gid_ranges);
}

# else /* Our own implementation */

static int pakfire_fgetsubid(struct pakfire* pakfire, struct pakfire_subid* subid, FILE* f) {
	int r;

	char* line = NULL;
	size_t length = 0;
	char* p = NULL;

	struct pakfire_ctx* ctx = pakfire_ctx(pakfire);

	// Read the next line
	while (1) {
		r = getline(&line, &length, f);
		if (r < 0)
			goto ERROR;

		// Try reading the next line if this one was empty
		else if (r == 0)
			continue;

		// Fall through
		else
			break;
	}

	int i = 0;

	char* token = strtok_r(line, ":", &p);
	while (token) {
		switch (i++) {
			// First field has the name
			case 0:
				pakfire_string_set(subid->name, token);
				break;

			// Second field has the ID
			case 1:
				subid->id = strtoul(token, NULL, 10);
				break;

			// Third field has the length
			case 2:
				subid->length = strtoul(token, NULL, 10);
				break;
		}

		token = strtok_r(NULL, ":", &p);
	}

	// Check if length is greater than zero
	if (subid->length == 0) {
		DEBUG(ctx, "Length equals zero: %s\n", line);
		r = 1;
		goto ERROR;
	}

	// Reset r
	r = 0;

ERROR:
	if (line)
		free(line);

	if (!r)
		DEBUG(ctx, "Parsed SUBID entry: name=%s, id=%u, length=%zu\n",
			subid->name, subid->id, subid->length);

	if (ctx)
		pakfire_ctx_unref(ctx);

	return r;
}

/*
	This function searches for a SUBUID/SUBGID entry for the given user.

	If something is found, subid is configured accordingly and the function returns 0.
	If nothing was found, the function returns 1.
	If there was an error, the function may return something else.
*/
static int pakfire_getsubid(struct pakfire* pakfire, const char* path, const char* owner,
		struct pakfire_subid* subid) {
	struct pakfire_subid entry = {};
	int r;

	// Do not lookup root
	if (!owner)
		return 1;

	struct pakfire_ctx* ctx = pakfire_ctx(pakfire);

	DEBUG(ctx, "Fetching SUBID from %s for %s\n", path, owner);

	// Open /etc/subuid
	FILE* f = fopen(path, "r");
	if (!f) {
		switch (errno) {
			case ENOENT:
				break;

			default:
				ERROR(ctx, "Could not open %s: %m\n", path);
				break;
		}

		// Nothing found
		r = 1;
		goto END;
	}

	// Walk through all entries
	while (1) {
		r = pakfire_fgetsubid(pakfire, &entry, f);
		switch (r) {
			case 0:
				break;

			case EOF:
				r = 1;
				goto END;

			default:
				goto END;
		}

		// Check for match
		if (strcmp(entry.name, owner) == 0) {
			subid->id     = entry.id;
			subid->length = entry.length;

			r = 0;
			goto END;
		}
	}

	// No match found
	DEBUG(ctx, "No match found for %s\n", owner);
	r = 1;

END:
	if (f)
		fclose(f);
	if (ctx)
		pakfire_ctx_unref(ctx);

	return r;
}

int pakfire_getsubuid(struct pakfire* pakfire, const char* owner, struct pakfire_subid* subid) {
	return pakfire_getsubid(pakfire, ETC_SUBUID, owner, subid);
}

int pakfire_getsubgid(struct pakfire* pakfire, const char* owner, struct pakfire_subid* subid) {
	return pakfire_getsubid(pakfire, ETC_SUBGID, owner, subid);
}

#endif
