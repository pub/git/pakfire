/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2017 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <stdlib.h>

#include <solv/policy.h>

#include <pakfire/constants.h>
#include <pakfire/i18n.h>
#include <pakfire/logging.h>
#include <pakfire/pakfire.h>
#include <pakfire/problem.h>
#include <pakfire/solution.h>
#include <pakfire/string.h>
#include <pakfire/transaction.h>
#include <pakfire/util.h>

struct pakfire_solution {
	struct pakfire* pakfire;
	int nrefs;

	struct pakfire_problem* problem;
	Id id;
	char* string;
};

int pakfire_solution_create(struct pakfire_solution** solution,
		struct pakfire* pakfire, struct pakfire_problem* problem, Id id) {
	struct pakfire_solution* s = calloc(1, sizeof(*s));
	if (!s)
		return -errno;

	// Store a reference to pakfire
	s->pakfire = pakfire_ref(pakfire);

	// Initialize the ref counter
	s->nrefs = 1;

	// Store a reference to the problem
	s->problem = pakfire_problem_ref(problem);

	// Store the ID
	s->id = id;

	*solution = s;
	return 0;
}

struct pakfire_solution* pakfire_solution_ref(struct pakfire_solution* solution) {
	solution->nrefs++;

	return solution;
}

static void pakfire_solution_free(struct pakfire_solution* solution) {
	pakfire_problem_unref(solution->problem);

	if (solution->string)
		free(solution->string);

	pakfire_unref(solution->pakfire);
	free(solution);
}

struct pakfire_solution* pakfire_solution_unref(struct pakfire_solution* solution) {
	if (!solution)
		return NULL;

	if (--solution->nrefs > 0)
		return solution;

	pakfire_solution_free(solution);
	return NULL;
}

struct pakfire_problem* pakfire_solution_get_problem(struct pakfire_solution* solution) {
	return pakfire_problem_ref(solution->problem);
}

Id pakfire_solution_get_id(struct pakfire_solution* solution) {
	return solution->id;
}

static char* pakfire_solution_make_string(struct pakfire_solution* solution) {
	struct pakfire_transaction* transaction = pakfire_problem_get_transaction(solution->problem);
	Solver* solver = pakfire_transaction_get_solver(transaction);
	Pool* pool = solver->pool;

	// Fetch the problem ID
	Id problem_id = pakfire_problem_get_id(solution->problem);

	// How many elements do we have?
	unsigned int count = solver_solutionelement_count(solver, problem_id, solution->id);
	if (!count) {
		pakfire_transaction_unref(transaction);
		return NULL;
	}

	// Allocate memory for all strings
	char* elements[count + 1];

	// Initialize elements
	for (unsigned int i = 0; i < count + 1; i++)
		elements[i] = NULL;

	// The result string
	char* s = NULL;

	Id p;
	Id rp;
	Id e = 0;
	int r;

	for (unsigned int i = 0; i < count; i++) {
		e = solver_next_solutionelement(solver, problem_id, solution->id, e, &p, &rp);
		if (!e)
			break;

		char* element = NULL;

		if (p == SOLVER_SOLUTION_JOB || p == SOLVER_SOLUTION_POOLJOB) {
			if (p == SOLVER_SOLUTION_JOB)
				rp += solver->pooljobcnt;

			Id how  = solver->job.elements[rp-1];
			Id what = solver->job.elements[rp];

			// XXX pool_job2str must be localised
			r = asprintf(&element, _("do not ask to %s"), pool_job2str(pool, how, what, 0));

		} else if (p == SOLVER_SOLUTION_INFARCH) {
			Solvable* solvable = pool->solvables + rp;

			if (pool->installed && solvable->repo == pool->installed)
				r = asprintf(&element, _("keep %s despite the inferior architecture"),
					pool_solvable2str(pool, solvable));
			else
				r = asprintf(&element, _("install %s despite the inferior architecture"),
					pool_solvable2str(pool, solvable));

		} else if (p == SOLVER_SOLUTION_DISTUPGRADE) {
			Solvable* solvable = pool->solvables + rp;

			if (pool->installed && solvable->repo == pool->installed)
				r = asprintf(&element, _("keep obsolete %s"), pool_solvable2str(pool, solvable));
			else
				r = asprintf(&element, _("install %s"), pool_solvable2str(pool, solvable));

		} else if (p == SOLVER_SOLUTION_BEST) {
			Solvable* solvable = pool->solvables + rp;

			if (pool->installed && solvable->repo == pool->installed)
				r = asprintf(&element, _("keep old %s"), pool_solvable2str(pool, solvable));
			else
				r = asprintf(&element, _("install %s despite the old version"),
					pool_solvable2str(pool, solvable));

		} else if (p > 0 && rp == 0)
			r = asprintf(&element, _("allow deinstallation of %s"),
				pool_solvid2str(pool, p));

		else if (p > 0 && rp > 0)
			r = asprintf(&element, _("allow replacement of %s with %s"),
				pool_solvid2str(pool, p), pool_solvid2str(pool, rp));

		else
			r = asprintf(&element, _("bad solution element"));

		if (r < 0)
			goto ERROR;

		// Save line in elements array
		elements[i] = element;
	}

	// Terminate the array
	elements[count] = NULL;

	// All okay, concat result string
	s = pakfire_string_join((const char**)elements, "\n");

ERROR:
	if (transaction)
		pakfire_transaction_unref(transaction);

	// Free all elements
	for (char** element = elements; *element; element++)
		free(*element);

	return s;
}

const char* pakfire_solution_to_string(struct pakfire_solution* solution) {
	if (!solution->string)
		solution->string = pakfire_solution_make_string(solution);

	return solution->string;
}
