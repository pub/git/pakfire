/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2024 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <stdlib.h>

#include <pakfire/ctx.h>
#include <pakfire/env.h>
#include <pakfire/string.h>

#define ENVIRON_SIZE     128

struct pakfire_env {
	struct pakfire_ctx* ctx;
	int nrefs;

	char* env[ENVIRON_SIZE];
};

static void pakfire_env_free(struct pakfire_env* env) {
	// Free environment
	for (unsigned int i = 0; env->env[i]; i++)
		free(env->env[i]);

	if (env->ctx)
		pakfire_ctx_unref(env->ctx);
	free(env);
}

int pakfire_env_create(struct pakfire_env** env, struct pakfire_ctx* ctx) {
	struct pakfire_env* e = NULL;

	// Allocate a new environment
	e = calloc(1, sizeof(*e));
	if (!e)
		return -errno;

	// Store a reference to the context
	e->ctx = pakfire_ctx_ref(ctx);

	// Initialize the reference counter
	e->nrefs = 1;

	// Return the pointer
	*env = e;

	return 0;
}

struct pakfire_env* pakfire_env_ref(struct pakfire_env* env) {
	++env->nrefs;

	return env;
}

struct pakfire_env* pakfire_env_unref(struct pakfire_env* env) {
	if (--env->nrefs > 0)
		return env;

	pakfire_env_free(env);
	return NULL;
}

char** pakfire_env_get_envp(struct pakfire_env* env) {
	if (!env->env[0])
		return NULL;

	return env->env;
}

// Returns the length of the environment
static unsigned int pakfire_env_length(struct pakfire_env* env) {
	unsigned int i = 0;

	// Count everything in the environment
	for (char** e = env->env; *e; e++)
		i++;

	return i;
}

// Finds an existing environment variable and returns its index or -1 if not found
static int pakfire_env_find(struct pakfire_env* env, const char* key) {
	if (!key)
		return -EINVAL;

	const size_t length = strlen(key);

	for (unsigned int i = 0; env->env[i]; i++) {
		if ((pakfire_string_startswith(env->env[i], key)
				&& *(env->env[i] + length) == '=')) {
			return i;
		}
	}

	// Nothing found
	return -1;
}

// Returns the value of an environment variable or NULL
const char* pakfire_env_get(struct pakfire_env* env, const char* key) {
	int i = pakfire_env_find(env, key);
	if (i < 0)
		return NULL;

	return env->env[i] + strlen(key) + 1;
}

// Sets an environment variable
int pakfire_env_set(struct pakfire_env* env, const char* key, const char* format, ...) {
	char value[PATH_MAX];
	va_list args;
	int r;

	// Find the index where to write this value to
	int i = pakfire_env_find(env, key);
	if (i < 0)
		i = pakfire_env_length(env);

	// Return ENOBUFS when the environment is full
	if (i >= ENVIRON_SIZE)
		return -ENOBUFS;

	// Format the value
	va_start(args, format);
	r = pakfire_string_vformat(value, format, args);
	va_end(args);
	if (r < 0)
		return r;

	// Free any previous value
	if (env->env[i])
		free(env->env[i]);

	// Format and set environment variable
	r = asprintf(&env->env[i], "%s=%s", key, value);
	if (r < 0) {
		ERROR(env->ctx, "Could not set environment variable %s: %m\n", key);
		return -errno;
	}

	DEBUG(env->ctx, "Set environment variable: %s\n", env->env[i]);

	return 0;
}

// Appends something to an environment variable, separated by :
int pakfire_env_append(struct pakfire_env* env, const char* key, const char* format, ...) {
	const char* old_value = NULL;
	char value[PATH_MAX];
	va_list args;
	int r;

	// Format the value
	va_start(args, format);
	r = pakfire_string_vformat(value, format, args);
	va_end(args);
	if (r < 0)
		return r;

	// Fetch the old value
	old_value = pakfire_env_get(env, key);

	// If there was no previous value, we will just set the new value
	if (!old_value)
		return pakfire_env_set(env, key, "%s", value);

	// Otherwise we will append it separated by :
	return pakfire_env_set(env, key, "%s:%s", old_value, value);
}

// Imports an environment
int pakfire_env_import(struct pakfire_env* env, const char** e) {
	char* key = NULL;
	char* val = NULL;
	int r;

	// Is there anything to import?
	if (!e)
		return 0;

	// Copy environment variables
	for (unsigned int i = 0; e[i]; i++) {
		r = pakfire_string_partition(e[i], "=", &key, &val);
		if (r < 0)
			continue;

		// Set value
		r = pakfire_env_set(env, key, "%s", val);

		if (key)
			free(key);
		if (val)
			free(val);

		// Break on error
		if (r)
			return r;
	}

	return 0;
}

int pakfire_env_merge(struct pakfire_env* env1, struct pakfire_env* env2) {
	return pakfire_env_import(env1, (const char**)env2->env);
}
