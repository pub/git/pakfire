/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_PACKAGER_H
#define PAKFIRE_PACKAGER_H

#include <pakfire/file.h>
#include <pakfire/filelist.h>
#include <pakfire/hashes.h>
#include <pakfire/package.h>
#include <pakfire/scriptlet.h>

#define PAKFIRE_PACKAGER_HASHES (PAKFIRE_HASH_SHA3_512|PAKFIRE_HASH_BLAKE2B512)

struct pakfire_packager;

int pakfire_packager_create(struct pakfire_packager** packager,
	struct pakfire* pakfire, struct pakfire_package* pkg);

struct pakfire_packager* pakfire_packager_ref(struct pakfire_packager* packager);
struct pakfire_packager* pakfire_packager_unref(struct pakfire_packager* packager);

char* pakfire_packager_filename(struct pakfire_packager* packager);

int pakfire_packager_write_archive(struct pakfire_packager* self,
	struct pakfire_archive** archive);

int pakfire_packager_finish(struct pakfire_packager* packager, FILE* f);

int pakfire_packager_add_file(
	struct pakfire_packager* packager, struct pakfire_file* file);
int pakfire_packager_add_files(struct pakfire_packager* packager,
	struct pakfire_filelist* filelist);
int pakfire_packager_add(struct pakfire_packager* packager,
	const char* sourcepath, const char* path);

int pakfire_packager_add_scriptlet(struct pakfire_packager* packager,
	struct pakfire_scriptlet* scriptlet);

int pakfire_packager_cleanup(struct pakfire_packager* packager);

#endif /* PAKFIRE_PACKAGER_H */
