/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2013 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <fcntl.h>
#include <ftw.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/resource.h>

#include <uuid/uuid.h>

#define PCRE2_CODE_UNIT_WIDTH 8
#include <pcre2.h>

#include <pakfire/ctx.h>
#include <pakfire/path.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

#define BUFFER_SIZE 64 * 1024

const char* pakfire_path_relpath(const char* root, const char* path) {
	// Check inputs
	if (!root || !path) {
		errno = EINVAL;
		return NULL;
	}

	if (pakfire_string_startswith(path, root))
		return path + strlen(root);

	return NULL;
}

int pakfire_file_write(struct pakfire* pakfire, const char* path,
		uid_t owner, gid_t group, mode_t mode, const char* format, ...) {
	va_list args;
	int r = 1;

	// Open the destination file
	FILE* f = fopen(path, "w");
	if (!f)
		goto ERROR;

	// Fetch file-descriptor
	int fd = fileno(f);

	// Set owner/group
	if (owner && group) {
		r = fchown(fd, owner, group);
		if (r)
			goto ERROR;
	}

	// Set mode
	if (mode) {
		r = fchmod(fd, mode);
		if (r)
			goto ERROR;
	}

	// Write data
	va_start(args, format);
	r = vfprintf(f, format, args);
	va_end(args);

	// Check for writing error
	if (r < 0)
		goto ERROR;

	// Close the file
	return fclose(f);

ERROR:
	if (f)
		fclose(f);

	return r;
}

/*
	Maps a file descriptor into memory
*/
int pakfire_mmap(int fd, char** data, size_t* length) {
	char* buffer = NULL;
	struct stat st = {};
	int r;

	// Check inputs
	if (fd < 0)
		return -EBADF;
	else if (!data || !length)
		return -EINVAL;

	// Stat the file
	r = fstat(fd, &st);
	if (r < 0)
		return -errno;

	// Map the data
	buffer = mmap(NULL, st.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
	if (buffer == MAP_FAILED)
		return -errno;

	// Return the values
	*data   = buffer;
	*length = st.st_size;

	return 0;
}

char* pakfire_generate_uuid(void) {
	uuid_t uuid;

	// Generate a new random value
	uuid_generate_random(uuid);

	char* ret = malloc(UUID_STR_LEN + 1);
	if (!ret)
		return NULL;

	// Convert it to string
	uuid_unparse_lower(uuid, ret);

	// Terminate string
	ret[UUID_STR_LEN] = '\0';

	return ret;
}

int pakfire_uuid_is_valid(const char* s) {
	uuid_t uuid;
	int r;

	// Check if we can parse the UUID
	r = uuid_parse(s, uuid);

	return (r == 0);
}

int pakfire_touch(const char* path, mode_t mode) {
	int r = 1;

	FILE* f = fopen(path, "w");
	if (!f)
		goto ERROR;

	// Set the requested mode
	if (mode) {
		r = fchmod(fileno(f), mode);
		if (r)
			goto ERROR;
	}

	return fclose(f);

ERROR:
	if (f)
		fclose(f);

	return r;
}

int pakfire_mkparentdir(const char* path, mode_t mode) {
	char dirname[PATH_MAX];
	int r;

	r = pakfire_path_dirname(dirname, path);
	if (r)
		return r;

	if (!*dirname)
		return 0;

	return pakfire_mkdir(dirname, mode);
}

static int pakfire_try_mkdir(const char* path, const mode_t mode) {
	struct stat st;
	int r;

	// Call stat() on path
	r = stat(path, &st);

	// Path exists, but is it a directory?
	if (r == 0) {
		if (S_ISDIR(st.st_mode))
			return 0;

		// Not a directory
		errno = ENOTDIR;
		return 1;

	// Path does not exist
	} else if (r && errno == ENOENT) {
		// Try to create it
		r = mkdir(path, mode);
		if (r)
			return r;

	// Raise any other errors
	} else {
		return r;
	}

	// Success
	return 0;
}

int pakfire_mkdir(const char* path, mode_t mode) {
	char buffer[PATH_MAX];
	int r;

	// Copy dirname into buffer
	r = pakfire_string_set(buffer, path);
	if (r)
		return r;

	// Recursively create all directories
	for (char* p = buffer + 1; *p; p++) {
		if (*p == '/') {
			// Cut off at slash
			*p = '\0';

			// Try to create partition directory
			r = pakfire_try_mkdir(buffer, mode);
			if (r)
				return r;

			// Reset slash
			*p = '/';
		}
	}

	// Create final directory
	return pakfire_try_mkdir(path, mode);
}

FILE* pakfire_mktemp(char* path, const mode_t mode) {
	int r = pakfire_mkparentdir(path, 0755);
	if (r)
		return NULL;

	// Create a temporary result file
	int fd = mkostemp(path, O_CLOEXEC);
	if (fd < 0)
		return NULL;

	// Set desired mode
	if (mode) {
		r = fchmod(fd, mode);
		if (r)
			return NULL;
	}

	// Re-open as file handle
	return fdopen(fd, "w+");
}

char* pakfire_mkdtemp(char* path) {
	int r = pakfire_mkparentdir(path, 0755);
	if (r)
		return NULL;

	return mkdtemp(path);
}

int pakfire_symlink(struct pakfire_ctx* ctx, const char* target, const char* linkpath) {
	char path[PATH_MAX];
	int r;

	// Find the dirname of the target
	r = pakfire_path_dirname(path, linkpath);
	if (r)
		return r;

	// Make the symlink relative
	r = pakfire_path_relative(path, path, target);
	if (r)
		return r;

	DEBUG(ctx, "Creating symlink %s -> %s (%s)\n", target, linkpath, path);

	// Create the symlink
	r = symlink(path, linkpath);
	if (r) {
		ERROR(ctx, "Could not create symlink %s (%s): %m\n", linkpath, path);
		return r;
	}

	return 0;
}

static int _unlink(const char* path, const struct stat* stat,
		const int type, struct FTW* ftwbuf) {
	// Delete directories using rmdir()
	if (type & FTW_D)
		return rmdir(path);

	// unlink() everything else
	return unlink(path);
}

int pakfire_rmtree(const char* path, int flags) {
	// Save errno
	int saved_errno = errno;

	// Walk through the entire tree and unlink everything
	int r = nftw(path, _unlink, 64, flags|FTW_DEPTH|FTW_PHYS);

	// Ignore if path didn't exist
	if (r < 0 && errno == ENOENT)
		r = 0;

	// Restore errno
	errno = saved_errno;

	return r;
}

int __pakfire_which(struct pakfire* pakfire, char* path, const size_t length,
		const char* what) {
	char buffer[PATH_MAX];
	int r;

	// Check input
	if (!what)
		return -EINVAL;

	static const char* paths[] = {
		"/usr/sbin",
		"/sbin",
		"/usr/bin",
		"/bin",
		NULL,
	};

	// Clear path
	*path = '\0';

	for (const char** p = paths; *p; p++) {
		// Compose path
		r = pakfire_path(pakfire, buffer, "%s/%s", *p, what);
		if (r)
			return r;

		// If the path exists and is executable we are done
		if (access(buffer, X_OK) == 0) {
			const char* relpath = pakfire_relpath(pakfire, buffer);

			// Store the result in path
			return __pakfire_string_set(path, length, relpath);
		}
	}

	return 0;
}

// Resource Limits

int pakfire_rlimit_set(struct pakfire_ctx* ctx, int limit) {
	struct rlimit rl;
	int r;

	// Sanity check
	if (limit < 3)
		return -EINVAL;

	// Fetch current configuration
	r = getrlimit(RLIMIT_NOFILE, &rl);
	if (r < 0) {
		ERROR(ctx, "Could not read RLIMIT_NOFILE: %m\n");
		return -errno;
	}

	// Do not attempt to set higher than maximum
	if ((long unsigned int)limit > rl.rlim_max)
		limit = rl.rlim_max;

	rl.rlim_cur = limit;

	// Set the new limit
	r = setrlimit(RLIMIT_NOFILE, &rl);
	if (r < 0) {
		ERROR(ctx, "Could not set RLIMIT_NOFILE to %lu: %m\n", rl.rlim_cur);
		return -errno;
	}

	DEBUG(ctx, "RLIMIT_NOFILE set to %d\n", limit);
	return 0;
}

/*
	Resets RLIMIT_NOFILE to FD_SETSIZE (e.g. 1024)
	for compatibility with software that uses select()
*/
int pakfire_rlimit_reset_nofile(struct pakfire_ctx* ctx) {
	return pakfire_rlimit_set(ctx, FD_SETSIZE);
}

// Regex

int pakfire_compile_regex(struct pakfire_ctx* ctx, pcre2_code** regex, const char* pattern) {
	int pcre2_errno;
	size_t pcre2_offset;
	PCRE2_UCHAR errmsg[256];

	// Compile the regular expression
	*regex = pcre2_compile((PCRE2_SPTR)pattern, PCRE2_ZERO_TERMINATED, 0,
		&pcre2_errno, &pcre2_offset, NULL);

	if (!*regex) {
		pcre2_get_error_message(pcre2_errno, errmsg, sizeof(errmsg));
		ERROR(ctx, "PCRE2 compilation failed for '%s' at offset %zu: %s\n",
			pattern, pcre2_offset, errmsg);
		return 1;
	}

	// Enable JIT
	pcre2_errno = pcre2_jit_compile(*regex, PCRE2_JIT_COMPLETE);
	if (pcre2_errno) {
		pcre2_get_error_message(pcre2_errno, errmsg, sizeof(errmsg));
		ERROR(ctx, "Enabling JIT on '%s' failed: %s\n", pattern, errmsg);
		return 1;
	}

	return 0;
}

// Copy

int pakfire_copy(struct pakfire_ctx* ctx, FILE* src, FILE* dst) {
	char buffer[BUFFER_SIZE];
	size_t bytes_read;
	size_t bytes_written;

	while (!feof(src)) {
		// Read some data into the buffer
		bytes_read = fread(buffer, 1, sizeof(buffer), src);

		// Check for any errors
		if (ferror(src)) {
			ERROR(ctx, "Could not read data: %m\n");
			return -errno;
		}

		// Break if we could not read anything
		if (!bytes_read)
			break;

		// Write the data
		bytes_written = fwrite(buffer, 1, bytes_read, dst);
		if (bytes_written < bytes_read) {
			ERROR(ctx, "Could not write data: %m\n");
			return -errno;
		}
	}

	return 0;
}
