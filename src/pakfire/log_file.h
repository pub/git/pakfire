/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2025 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_LOG_FILE_H
#define PAKFIRE_LOG_FILE_H

#include <pakfire/ctx.h>

/*
	Writes the log into a compressed file
*/

struct pakfire_log_file;

enum pakfire_log_file_flags {
	PAKFIRE_LOG_FILE_COMPRESS = (1 << 0),
};

int pakfire_log_file_create(struct pakfire_log_file** file,
	struct pakfire_ctx* ctx, const char* path, const char* filename, int flags);
struct pakfire_log_file* pakfire_log_file_ref(struct pakfire_log_file* self);
struct pakfire_log_file* pakfire_log_file_unref(struct pakfire_log_file* self);

const char* pakfire_log_file_filename(struct pakfire_log_file* self);
const char* pakfire_log_file_path(struct pakfire_log_file* self);

int pakfire_log_file_close(struct pakfire_log_file* self);

int pakfire_log_file_write(struct pakfire_log_file* self,
	int priority, const char* buffer, ssize_t length);

#endif /* PAKFIRE_LOG_FILE_H */
