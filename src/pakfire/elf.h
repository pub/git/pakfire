/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2025 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_ELF_H
#define PAKFIRE_ELF_H

struct pakfire_elf;

#include <pakfire/ctx.h>
#include <pakfire/file.h>

int pakfire_elf_open(struct pakfire_elf** elf,
	struct pakfire_ctx* ctx, const char* path, int fd);
int pakfire_elf_open_file(struct pakfire_elf** elf,
	struct pakfire_ctx* ctx, struct pakfire_file* file);

struct pakfire_elf* pakfire_elf_ref(struct pakfire_elf* self);
struct pakfire_elf* pakfire_elf_unref(struct pakfire_elf* self);

const char* pakfire_elf_path(struct pakfire_elf* self);
int pakfire_elf_type(struct pakfire_elf* self);
int pakfire_elf_machine(struct pakfire_elf* self);
int pakfire_elf_is_elf64(struct pakfire_elf* self);
int pakfire_elf_endianess(struct pakfire_elf* self);
const char* pakfire_elf_build_id(struct pakfire_elf* self);
const char* pakfire_elf_debuglink(struct pakfire_elf* self);
const char* pakfire_elf_interpreter(struct pakfire_elf* elf);
const char* pakfire_elf_soname(struct pakfire_elf* self);

int pakfire_elf_is_pie(struct pakfire_elf* self);
int pakfire_elf_has_ssp(struct pakfire_elf* self);
int pakfire_elf_has_execstack(struct pakfire_elf* self);
int pakfire_elf_is_fully_relro(struct pakfire_elf* self);
int pakfire_elf_is_partially_relro(struct pakfire_elf* self);
int pakfire_elf_has_cf_protection(struct pakfire_elf* self);
int pakfire_elf_has_runpaths(struct pakfire_elf* self, char*** runpaths);
int pakfire_elf_is_stripped(struct pakfire_elf* self);

// Return bitmap for pakfire_elf_has_cf_protection()
enum {
	PAKFIRE_ELF_MISSING_BTI   = (1 << 0),
	PAKFIRE_ELF_MISSING_PAC   = (1 << 1),
	PAKFIRE_ELF_MISSING_IBT   = (1 << 2),
	PAKFIRE_ELF_MISSING_SHSTK = (1 << 3),
};

// Dependencies
int pakfire_elf_provides(struct pakfire_elf* self, char*** provides);
int pakfire_elf_requires(struct pakfire_elf* self, char*** requires);

// Source Files
typedef int (*pakfire_elf_foreach_source_file_callback)
	(struct pakfire_ctx* ctx, struct pakfire_elf* elf, const char* filename, void* data);

int pakfire_elf_foreach_source_file(struct pakfire_elf* self,
	pakfire_elf_foreach_source_file_callback callback, void* data);

#endif /* PAKFIRE_ELF_H */
