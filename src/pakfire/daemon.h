/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_DAEMON_H
#define PAKFIRE_DAEMON_H

struct pakfire_daemon;

#include <pakfire/buildservice.h>
#include <pakfire/ctx.h>
#include <pakfire/job.h>

#include <json.h>

int pakfire_daemon_create(struct pakfire_daemon** daemon, struct pakfire_ctx* ctx);

struct pakfire_daemon* pakfire_daemon_ref(struct pakfire_daemon* daemon);
struct pakfire_daemon* pakfire_daemon_unref(struct pakfire_daemon* daemon);

sd_event* pakfire_daemon_loop(struct pakfire_daemon* daemon);
struct pakfire_buildservice* pakfire_daemon_buildservice(struct pakfire_daemon* daemon);
struct pakfire_httpclient* pakfire_daemon_httpclient(struct pakfire_daemon* daemon);

const char* pakfire_daemon_url(struct pakfire_daemon* daemon);

int pakfire_daemon_main(struct pakfire_daemon* daemon);

int pakfire_daemon_job_finished(struct pakfire_daemon* daemon, struct pakfire_job* job);

// Stream Logs
int pakfire_daemon_stream_logs(struct pakfire_daemon* self);

// Send message
int pakfire_daemon_send_message(struct pakfire_daemon* self, struct json_object* message);

#endif /* PAKFIRE_DAEMON_H */
