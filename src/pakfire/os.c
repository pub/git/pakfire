/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <pakfire/os.h>
#include <pakfire/path.h>
#include <pakfire/parse.h>
#include <pakfire/string.h>

// System Info

#define pakfire_read_dmi(buffer, key) \
	__pakfire_read_dmi(buffer, sizeof(buffer), key)

static int __pakfire_read_dmi(char* buffer, size_t length, const char* key) {
	char path[PATH_MAX];
	FILE* f = NULL;
	int r;

	// Make the path
	r = pakfire_path_append(path, "/sys/class/dmi/id", key);
	if (r < 0)
		return r;

	// Open the file
	f = fopen(path, "r");
	if (!f) {
		r = -errno;
		goto ERROR;
	}

	// Read the file into the buffer
	size_t bytes_read = fread(buffer, 1, length - 1, f);

	// Always terminate the array
	buffer[bytes_read] = '\0';

	// Did we encounter an error reading the file?
	if (ferror(f)) {
		r = -errno;
		goto ERROR;
	}

	// Did we read the entire file?
	if (!feof(f)) {
		r = -ENOBUFS;
		goto ERROR;
	}

	// Trim any whitespace
	pakfire_string_strip(buffer);

ERROR:
	if (f)
		fclose(f);

	return r;
}

int pakfire_sysinfo(struct pakfire_sysinfo* sysinfo) {
	int r;

	// Check input
	if (!sysinfo)
		return -EINVAL;

	// Read the system vendor
	r = pakfire_read_dmi(sysinfo->vendor, "sys_vendor");
	if (r < 0)
		return r;

	// Read the product name
	r = pakfire_read_dmi(sysinfo->name, "product_name");
	if (r < 0)
		return r;

	return 0;
}

// CPU Info

#ifdef __aarch64__
/*
 * On aarch64, /proc/cpuinfo is very stripped down and does not give us a lot
 * of information about the CPU. This function tries to add some extra stuff.
 */
static int pakfire_cpuinfo_aarch64(struct pakfire_cpuinfo* cpuinfo) {
	const char* vendor = NULL;
	const char* model = NULL;
	uint64_t midr = 0;
	int r;

	// Skip this if we already have a CPU vendor and model
	if (*cpuinfo->vendor || *cpuinfo->model)
		return 0;

	// Read MIDR EL1
	asm volatile("mrs %0, MIDR_EL1" : "=r"(midr));

	// Decode the register
	uint8_t implementer = (midr >> 24) & 0x0ff;
	//uint8_t variant     = (midr >> 20) & 0x00f;
	uint16_t partnum    = (midr >>  4) & 0xfff;
	//uint8_t revision    = (midr >>  0) & 0x00f;

	// Find vendors
	switch (implementer) {
		// ARM
		case 0x41:
			vendor = "ARM";

			// Find models
			switch (partnum) {
				// Neoverse-N1
				case 0xd0c:
					model = "Neoverse-N1";
					break;

				// Neoverse-V1
				case 0xd40:
					model = "Neoverse-V1";
					break;

				// Neoverse-N2
				case 0xd49:
					model = "Neoverse-N2";
					break;

				// Neoverse-V2
				case 0xd4f:
					model = "Neoverse-V2";
					break;

				// Neoverse-V3
				case 0xd84:
					model = "Neoverse-V3";
					break;

				// Neoverse-N3
				case 0xd8e:
					model = "Neoverse-N3";
					break;
			}
	}

	// Store vendor (if found)
	if (vendor) {
		r = pakfire_string_set(cpuinfo->vendor, vendor);
		if (r < 0)
			return r;
	}

	// Store the model (if found)
	if (model) {
		r = pakfire_string_set(cpuinfo->model, model);
		if (r < 0)
			return r;
	}

	return 0;
}
#endif /* __aarch64__ */

static int pakfire_parse_cpuinfo(char* line, size_t length, void* data) {
	struct pakfire_cpuinfo* cpuinfo = data;
	int r;

	// Key & Value
	char* k = NULL;
	char* v = NULL;

	// Split the line
	r = pakfire_parse_split_line(line, length, &k, &v, ':');
	if (r)
		return r;

	// If we didn't get a result we skip this line
	if (!k || !v)
		return 0;

	// Vendor
	if (strcmp(k, "vendor_id") == 0) {
		r = pakfire_string_set(cpuinfo->vendor, v);
		if (r)
			return r;

	// Model Name
	} else if (strcmp(k, "model name") == 0) {
		r = pakfire_string_set(cpuinfo->model, v);
		if (r)
			return r;
	}

	return 0;
}

int pakfire_cpuinfo(struct pakfire_cpuinfo* cpuinfo) {
	int r;

	// Parse /proc/cpuinfo
	r = pakfire_parse_file("/proc/cpuinfo", pakfire_parse_cpuinfo, cpuinfo);
	if (r)
		return r;

	// Run architecture-specific stuff
#ifdef __aarch64__
	r = pakfire_cpuinfo_aarch64(cpuinfo);
	if (r)
		return r;
#endif /* __aarch64__ */

	// Fetch the number of processors
	cpuinfo->count = sysconf(_SC_NPROCESSORS_CONF);

	return 0;
}

// CPU Stats

static int pakfire_parse_cpustat(char* line, size_t length, void* data) {
	struct pakfire_cpustat* cpustat = data;
	struct cpustat {
		unsigned long int user;
		unsigned long int nice;
		unsigned long int system;
		unsigned long int idle;
		unsigned long int iowait;
		unsigned long int irq;
		unsigned long int softirq;
		unsigned long int steal;
		unsigned long int guest;
		unsigned long int guest_nice;
	} stat;
	int r;

	// Only care about the line of interest
	if (pakfire_string_startswith(line, "cpu ")) {
		r = sscanf(line, "cpu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu",
			&stat.user, &stat.nice, &stat.system, &stat.idle, &stat.iowait,
			&stat.irq, &stat.softirq, &stat.steal, &stat.guest, &stat.guest_nice);
		if (r < 10)
			return -EINVAL;

		// Fetch how many ticks a second
		unsigned long int ticks = stat.user + stat.nice + stat.system + stat.idle
			+ stat.iowait + stat.irq + stat.softirq + stat.steal + stat.guest + stat.guest_nice;

		// Convert to relative terms
		cpustat->user       = (double)stat.user       / ticks;
		cpustat->nice       = (double)stat.nice       / ticks;
		cpustat->system     = (double)stat.system     / ticks;
		cpustat->idle       = (double)stat.idle       / ticks;
		cpustat->iowait     = (double)stat.iowait     / ticks;
		cpustat->irq        = (double)stat.irq        / ticks;
		cpustat->softirq    = (double)stat.softirq    / ticks;
		cpustat->steal      = (double)stat.steal      / ticks;
		cpustat->guest      = (double)stat.guest      / ticks;
		cpustat->guest_nice = (double)stat.guest_nice / ticks;
	}

	return 0;
}

int pakfire_cpustat(struct pakfire_cpustat* cpustat) {
	return pakfire_parse_file("/proc/stat", pakfire_parse_cpustat, cpustat);
}

// Load Average

static int pakfire_parse_loadavg(char* line, size_t length, void* data) {
	struct pakfire_loadavg* loadavg = data;
	int r;

	// Parse the first three values
	r = sscanf(line, "%lf %lf %lf", &loadavg->load1, &loadavg->load5, &loadavg->load15);
	if (r < 3)
		return -EINVAL;

	return 0;
}

int pakfire_loadavg(struct pakfire_loadavg* loadavg) {
	return pakfire_parse_file("/proc/loadavg", pakfire_parse_loadavg, loadavg);
}

// Meminfo

static int pakfire_parse_meminfo_value(uint64_t* mem, const char* s) {
	char* remainder = NULL;
	uint64_t m = 0;

	// Parse the numeric value
	m = strtoul(s, &remainder, 10);
	if (m == ULONG_MAX)
		return -errno;

	// Convert into bytes
	if (strcmp(remainder, " kB") == 0) {
		// Check if the multiplication won't overflow
		if (m > UINT64_MAX / 1024)
			return -EOVERFLOW;

		m *= 1024;

	// Fail on anything else
	} else if (*remainder)
		return -EINVAL;

	// Store the value
	*mem = m;

	return 0;
}

static int pakfire_parse_meminfo(char* line, size_t length, void* data) {
	struct pakfire_meminfo* meminfo = data;
	int r;

	// Key & Value
	char* k = NULL;
	char* v = NULL;

	// Split the line
	r = pakfire_parse_split_line(line, length, &k, &v, ':');
	if (r)
		return r;

	// If we didn't get a result we skip this line
	if (!k || !v)
		return 0;

	// Total
	if (strcmp(k, "MemTotal") == 0)
		return pakfire_parse_meminfo_value(&meminfo->total, v);

	// Free
	else if (strcmp(k, "MemFree") == 0)
		return pakfire_parse_meminfo_value(&meminfo->free, v);

	// Available
	else if (strcmp(k, "MemAvailable") == 0)
		return pakfire_parse_meminfo_value(&meminfo->available, v);

	// Buffers
	else if (strcmp(k, "Buffers") == 0)
		return pakfire_parse_meminfo_value(&meminfo->buffers, v);

	// Cached
	else if (strcmp(k, "Cached") == 0)
		return pakfire_parse_meminfo_value(&meminfo->cached, v);

	// Shared
	else if (strcmp(k, "Shmem") == 0)
		return pakfire_parse_meminfo_value(&meminfo->shared, v);

	// Active
	else if (strcmp(k, "Active") == 0)
		return pakfire_parse_meminfo_value(&meminfo->active, v);

	// Inactive
	else if (strcmp(k, "Inactive") == 0)
		return pakfire_parse_meminfo_value(&meminfo->inactive, v);

	// Swap Total
	else if (strcmp(k, "SwapTotal") == 0)
		return pakfire_parse_meminfo_value(&meminfo->swap_total, v);

	// Swap Free
	else if (strcmp(k, "SwapFree") == 0)
		return pakfire_parse_meminfo_value(&meminfo->swap_free, v);

	return 0;
}

int pakfire_meminfo(struct pakfire_meminfo* meminfo) {
	int r;

	// Parse /proc/meminfo
	r = pakfire_parse_file("/proc/meminfo", pakfire_parse_meminfo, meminfo);
	if (r)
		return r;

	// Set used memory
	meminfo->used = meminfo->total - meminfo->free;

	// Set used swap
	meminfo->swap_used = meminfo->swap_total - meminfo->swap_free;

	return 0;
}

// Distro

static int pakfire_parse_distro(char* line, size_t length, void* data) {
	struct pakfire_distro* distro = data;
	int r;

	// Key & Value
	char* k = NULL;
	char* v = NULL;

	// Split the line
	r = pakfire_parse_split_line(line, length, &k, &v, '=');
	if (r)
		return r;

	// If we didn't get a result we skip this line
	if (!k || !v)
		return 0;

	// Unquote the strings
	pakfire_string_unquote(v);

	// PRETTY_NAME
	if (strcmp(k, "PRETTY_NAME") == 0)
		return pakfire_string_set(distro->pretty_name, v);

	// NAME
	else if (strcmp(k, "NAME") == 0)
		return pakfire_string_set(distro->name, v);

	// ID
	else if (strcmp(k, "ID") == 0)
		return pakfire_string_set(distro->id, v);

	// VERSION
	else if (strcmp(k, "VERSION") == 0)
		return pakfire_string_set(distro->version, v);

	// VERSION_CODENAME
	else if (strcmp(k, "VERSION_CODENAME") == 0)
		return pakfire_string_set(distro->version_codename, v);

	// VERSION_ID
	else if (strcmp(k, "VERSION_ID") == 0)
		return pakfire_string_set(distro->version_id, v);

	return 0;
}

int pakfire_distro(struct pakfire_distro* distro, const char* path) {
	if (!path)
		path = "/etc/os-release";

	return pakfire_parse_file(path, pakfire_parse_distro, distro);
}
