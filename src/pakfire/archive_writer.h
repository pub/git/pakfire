/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2025 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_ARCHIVE_WRITER_H
#define PAKFIRE_ARCHIVE_WRITER_H

#include <stdio.h>

// json-c
#include <json.h>

#include <pakfire/filelist.h>
#include <pakfire/pakfire.h>

struct pakfire_archive_writer;

typedef enum pakfire_archive_writer_formats {
	PAKFIRE_FORMAT_ARCHIVE,
	PAKFIRE_FORMAT_SOURCE_ARCHIVE,

	// OCI Images
	PAKFIRE_FORMAT_OCI,
	PAKFIRE_FORMAT_OCI_LAYER,
} pakfire_archive_writer_format;

int pakfire_archive_writer_create(struct pakfire_archive_writer** writer,
		struct pakfire* pakfire, pakfire_archive_writer_format format, FILE* f);

struct pakfire_archive_writer* pakfire_archive_writer_ref(struct pakfire_archive_writer* self);
struct pakfire_archive_writer* pakfire_archive_writer_unref(struct pakfire_archive_writer* self);

int pakfire_archive_writer_set_title(struct pakfire_archive_writer* self,
	const char* format, ...) __attribute__((format(printf, 2, 3)));

int pakfire_archive_writer_write_files(
	struct pakfire_archive_writer* self, struct pakfire_filelist* files);

int pakfire_archive_writer_write_everything(
	struct pakfire_archive_writer* self, const char* root);

int pakfire_archive_writer_create_file(struct pakfire_archive_writer* self,
	const char* path, mode_t mode, const char* payload, const size_t length);
int pakfire_archive_writer_create_file_from_json(struct pakfire_archive_writer* self,
	const char* path, mode_t mode, struct json_object* json);
int pakfire_archive_writer_create_file_from_file(
	struct pakfire_archive_writer* self, const char* path, mode_t mode, FILE* f);

#endif /* PAKFIRE_ARCHIVE_WRITER_H */
