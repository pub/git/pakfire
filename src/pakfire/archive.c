/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2014 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/sendfile.h>
#include <sys/stat.h>

// libarchive
#include <archive.h>
#include <archive_entry.h>

// JSON-C
#include <json.h>

#include <pakfire/archive.h>
#include <pakfire/ctx.h>
#include <pakfire/deps.h>
#include <pakfire/file.h>
#include <pakfire/filelist.h>
#include <pakfire/hasher.h>
#include <pakfire/hashes.h>
#include <pakfire/hex.h>
#include <pakfire/i18n.h>
#include <pakfire/jail.h>
#include <pakfire/linter.h>
#include <pakfire/logging.h>
#include <pakfire/package.h>
#include <pakfire/pakfire.h>
#include <pakfire/path.h>
#include <pakfire/repo.h>
#include <pakfire/scriptlet.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

// Compute checksums using SHA2-512
#define PAKFIRE_ARCHIVE_CHECKSUM PAKFIRE_HASH_SHA2_512

#define MAX_SCRIPTLETS 9

// The maximum number of symlinks to follow when reading a file from an archive
#define MAX_FOLLOW_SYMLINKS 10

#define ARCHIVE_READ_BLOCK_SIZE 512 * 1024

struct pakfire_archive {
	struct pakfire_ctx* ctx;
	struct pakfire* pakfire;
	int nrefs;

	char path[PATH_MAX];
	FILE* f;
	struct stat stat;

	struct pakfire_package* package;

	// metadata
	unsigned int format;
	struct json_object* metadata;

	struct pakfire_filelist* filelist;

	// Scriptlets
	struct pakfire_scriptlet* scriptlets[MAX_SCRIPTLETS];
	unsigned int num_scriptlets;

	// Hashes
	struct pakfire_hashes hashes;

	// Progress (when extracting)
	struct pakfire_progress* progress;

	// Verify Status
	int verify;
};

struct pakfire_archive_file {
	FILE* f;

	// Buffer to store a block of data
	char buffer[ARCHIVE_READ_BLOCK_SIZE];

	enum {
		PAKFIRE_ARCHIVE_FILE_CANT_SEEK = (1 << 0),
	} flags;
};

static ssize_t archive_file_read(struct archive* a, void* data, const void** buffer) {
	struct pakfire_archive_file* file = data;

	// Read a block of data into the buffer
	size_t bytes_read = fread(file->buffer, 1, sizeof(file->buffer), file->f);

	if (unlikely(bytes_read < sizeof(file->buffer) && ferror(file->f))) {
		archive_set_error(a, errno, "Error reading file");
	}

	// Return the buffer
	*buffer = file->buffer;

	return bytes_read;
}

static int64_t archive_file_skip(struct archive* a, void* data, off_t skip) {
	struct pakfire_archive_file* file = data;
	int r;

	// Skip if we don't support seek
	if (unlikely(file->flags & PAKFIRE_ARCHIVE_FILE_CANT_SEEK))
		return 0;

	// Skip if we have been asked to do nothing
	if (unlikely(skip == 0))
		return 0;

	// Perform seek()
	r = fseeko(file->f, skip, SEEK_CUR);
	if (r < 0) {
		file->flags |= PAKFIRE_ARCHIVE_FILE_CANT_SEEK;
		return 0;
	}

	return skip;
}

static int archive_file_close(struct archive* a, void* data) {
	struct pakfire_archive_file* file = data;

	// Close the file
	if (file->f)
		fclose(file->f);

	free(file);

	return ARCHIVE_OK;
}

/*
	This is a custom open function to an archive.

	I clones the file descriptor so that we can have an independent
	file descriptor for multiple operations.
*/
static int archive_read_file_open(struct archive* a, FILE* f) {
	struct pakfire_archive_file* file = NULL;
	int fd = -EBADF;
	int r;

	// Fetch the underlying file descriptor
	fd = fileno(f);

	// Reset so we won't accidentally close it
	f = NULL;

	if (fd < 0) {
		r = -EBADF;
		goto ERROR;
	}

	// Duplicate the file descriptor
	fd = dup(fd);
	if (fd < 0) {
		r = -errno;
		goto ERROR;
	}

	// Let the kernel know, that we will read the file sequentially
	r = posix_fadvise(fd, 0, 0, POSIX_FADV_SEQUENTIAL);
	if (r < 0) {
		r = -errno;
		goto ERROR;
	}

	// Re-open the file handle
	f = fdopen(fd, "r");
	if (!f) {
		r = -errno;
		goto ERROR;
	}

	// Reset file descriptor
	fd = -EBADF;

	// Start reading at the beginning
	r = pakfire_rewind(f);
	if (r < 0)
		goto ERROR;

	// Allocate an object
	file = calloc(1, sizeof(*file));
	if (!file) {
		r = -errno;
		goto ERROR;
	}

	// Store the file handle
	file->f = f;

	// Open the archive
	return archive_read_open2(a,
		file, NULL, archive_file_read, archive_file_skip, archive_file_close);

ERROR:
	// Store an error message
	archive_set_error(a, -r, "%s", strerror(-r));

	if (fd >= 0)
		close(fd);
	if (f)
		fclose(f);

	return ARCHIVE_FATAL;
}

/*
	A helper function that opens the archive for reading
*/
static int pakfire_archive_open_archive(struct pakfire_archive* archive,
		struct archive** __archive) {
	struct archive* a = NULL;
	int r;

	// Create a new archive object
	a = archive_read_new();
	if (!a)
		return -errno;

	// Archives must be uncompressed tarballs
	archive_read_support_format_tar(a);

	// Archives are compressed using Zstandard
	archive_read_support_filter_zstd(a);

	// Try opening the archive file
	r = archive_read_file_open(a, archive->f);
	switch (r) {
		case ARCHIVE_OK:
			break;

		case ARCHIVE_FATAL:
			r = -ENOMSG;
			goto ERROR;

		default:
			ERROR(archive->ctx, "Could not open archive %s: %s\n",
				archive->path, archive_error_string(a));
			goto ERROR;
	}

	// Return pointer
	*__archive = a;

	// Success
	return 0;

ERROR:
	if (a)
		archive_read_free(a);

	return r;
}

static int pakfire_archive_compute_hashes(struct pakfire_archive* archive) {
	int r;

	// Start reading at the beginning
	r = pakfire_rewind(archive->f);
	if (r < 0) {
		ERROR(archive->ctx, "Could not rewind %s: %m\n", archive->path);
		return -errno;
	}

	// Calculate hashes
	r = pakfire_hash_file(archive->ctx, archive->f,PAKFIRE_ARCHIVE_CHECKSUM, &archive->hashes);
	if (r < 0) {
		ERROR(archive->ctx, "Could not calculate checksums of %s: %s\n",
			archive->path, strerror(-r));
		return r;
	}

	return 0;
}

/*
	Helper function to conditionally walk through an archive
	and perform actions based on the callback.
*/
static int pakfire_archive_walk(struct pakfire_archive* archive, struct archive* a,
		pakfire_archive_walk_callback walk_callback,
		pakfire_archive_walk_filter_callback filter_callback, void* p) {
	struct archive_entry* entry = NULL;
	int r;

	// Walk through the archive
	for (;;) {
		r = archive_read_next_header(a, &entry);

		// Handle the return code
		switch (r) {
			// Fall through if everything is okay
			case ARCHIVE_OK:
				break;

			// Return OK when we reached the end of the archive
			case ARCHIVE_EOF:
				return 0;

			// Raise any other errors
			default:
				return r;
		}

		// Call the filter callback before we call the actual callback
		if (filter_callback) {
			r = filter_callback(archive, entry, p);

			// Handle the return code
			switch (r) {
				case PAKFIRE_WALK_OK:
					break;

				case PAKFIRE_WALK_END:
					DEBUG(archive->ctx, "Filter callback sent END\n");
					return 0;

				case PAKFIRE_WALK_SKIP:
					DEBUG(archive->ctx, "Filter callback sent SKIP\n");
					continue;

				case PAKFIRE_WALK_DONE:
					DEBUG(archive->ctx, "Filter callback sent DONE\n");

					// Clear the callback function
					filter_callback = NULL;
					break;

				case PAKFIRE_WALK_AGAIN:
					DEBUG(archive->ctx, "Filter callback sent AGAIN\n");
					return -EAGAIN;

				// Raise any other errors
				default:
					DEBUG(archive->ctx, "Filter callback returned an error: %d\n", r);
					return r;
			}
		}

		// Run callback
		if (walk_callback) {
			r = walk_callback(archive, a, entry, p);

			// Handle the return code
			switch (r) {
				case PAKFIRE_WALK_OK:
					break;

				case PAKFIRE_WALK_DONE:
					DEBUG(archive->ctx, "Callback sent DONE\n");
					return 0;

				// Raise any other errors
				default:
					return r;
			}
		}
	}

	return 0;
}

static int pakfire_archive_open_and_walk(struct pakfire_archive* archive,
		pakfire_archive_walk_callback walk_callback,
		pakfire_archive_walk_filter_callback filter_callback, void* data) {
	struct archive* a = NULL;
	int r;

	// Open the archive
	r = pakfire_archive_open_archive(archive, &a);
	if (r < 0)
		goto ERROR;

	// Walk...
	r = pakfire_archive_walk(archive, a, walk_callback, filter_callback, data);

ERROR:
	if (a)
		archive_read_free(a);

	return r;
}

static void pakfire_archive_free(struct pakfire_archive* archive) {
	// Close the file
	if (archive->f)
		fclose(archive->f);

	// Free scriptlets
	for (unsigned int i = 0; i < archive->num_scriptlets; i++)
		pakfire_scriptlet_unref(archive->scriptlets[i]);

	if (archive->progress)
		pakfire_progress_unref(archive->progress);
	if (archive->filelist)
		pakfire_filelist_unref(archive->filelist);
	if (archive->package)
		pakfire_package_unref(archive->package);
	if (archive->metadata)
		json_object_put(archive->metadata);
	if (archive->pakfire)
		pakfire_unref(archive->pakfire);
	if (archive->ctx)
		pakfire_ctx_unref(archive->ctx);
	free(archive);
}

struct pakfire_archive* pakfire_archive_ref(struct pakfire_archive* archive) {
	++archive->nrefs;

	return archive;
}

struct pakfire_archive* pakfire_archive_unref(struct pakfire_archive* archive) {
	if (--archive->nrefs > 0)
		return archive;

	pakfire_archive_free(archive);
	return NULL;
}

static struct pakfire_package* pakfire_archive_get_package(struct pakfire_archive* archive) {
	if (!archive->package) {
		int r = pakfire_archive_make_package(archive, NULL, &archive->package);
		if (r)
			return NULL;
	}

	return pakfire_package_ref(archive->package);
}

// Metadata

static int pakfire_archive_parse_json_metadata(struct pakfire_archive* archive,
		const char* data, const size_t length) {
	int r = 1;

	// Create tokener
	struct json_tokener* tokener = json_tokener_new();
	if (!tokener) {
		ERROR(archive->ctx, "Could not allocate JSON tokener: %m\n");
		goto ERROR;
	}

	// Parse JSON from buffer
	archive->metadata = json_tokener_parse_ex(tokener, data, length);
	if (!archive->metadata) {
		enum json_tokener_error error = json_tokener_get_error(tokener);

		ERROR(archive->ctx, "JSON parsing error: %s\n",
			json_tokener_error_desc(error));
		goto ERROR;
	}

	DEBUG(archive->ctx, "Successfully parsed package metadata:\n%s\n",
		json_object_to_json_string_ext(archive->metadata,
			JSON_C_TO_STRING_PRETTY|JSON_C_TO_STRING_PRETTY_TAB));

	// Success
	r = 0;

ERROR:
	if (tokener)
		json_tokener_free(tokener);

	return r;
}

static int pakfire_archive_parse_format(struct pakfire_archive* archive,
		const char* data, const size_t length) {
	// Check if format has already been set
	if (archive->format) {
		ERROR(archive->ctx, "Archive format has already been parsed\n");
		return -EINVAL;
	}

	// Parse the format
	archive->format = strtoul(data, NULL, 10);

	switch (archive->format) {
		// Handle all supported formats
		case 6:
			break;

		// Break on anything else
		default:
			ERROR(archive->ctx, "This version of Pakfire does not support "
				"archive format %u\n", archive->format);
			return -ENOTSUP;
	}

	return 0;
}

static int pakfire_archive_parse_scriptlet(struct pakfire_archive* archive,
		const char* path, const char* data, const size_t length) {
	struct pakfire_scriptlet* scriptlet = NULL;
	const char* type = NULL;
	int r;

	// Check for any available space
	if (archive->num_scriptlets >= MAX_SCRIPTLETS) {
		ERROR(archive->ctx, "Too many scriptlets\n");
		return -ENOBUFS;
	}

	// Determine type
	type = pakfire_path_relpath(".scriptlets/", path);
	if (!type) {
		ERROR(archive->ctx, "Could not determine the scriptlet type from '%s'\n", path);
		return -EINVAL;
	}

	// Allocate a scriptlet
	r = pakfire_scriptlet_create(&scriptlet, archive->ctx, type, data, length);
	if (r)
		return r;

	// Store scriptlet
	archive->scriptlets[archive->num_scriptlets++] = scriptlet;

	return 0;
}

static int pakfire_archive_copy_data_to_buffer(struct pakfire_archive* archive,
		struct archive* a, struct archive_entry* entry, char** data, size_t* length) {
	size_t bytes_read = 0;
	char* buffer = NULL;
	int r;

	// Fetch how large the buffer needs to be
	size_t required_size = archive_entry_size(entry);
	if (!required_size)
		return 0;

	// Allocate a block of the required size
	buffer = calloc(1, required_size + 1);
	if (!buffer)
		return -errno;

	// Read the data into the buffer
	bytes_read = archive_read_data(a, buffer, required_size);
	if (bytes_read < required_size) {
		ERROR(archive->ctx, "Could not read from archive: %s\n", archive_error_string(a));
		r = -errno;
		goto ERROR;
	}

	// Return the output
	*data = buffer;
	*length = bytes_read;

	return 0;

ERROR:
	if (buffer)
		free(buffer);

	return r;
}

static int __pakfire_archive_read_metadata(struct pakfire_archive* archive,
		struct archive* a, struct archive_entry* entry, void* p) {
	char* data = NULL;
	size_t length = 0;
	int r;

	const char* path = archive_entry_pathname(entry);

	DEBUG(archive->ctx, "Reading metadata file: %s\n", path);

	// Load the file into memory
	r = pakfire_archive_copy_data_to_buffer(archive, a, entry, &data, &length);
	if (r) {
		ERROR(archive->ctx, "Could not read data from archive: %s\n", archive_error_string(a));
		goto ERROR;
	}

	// Make the static analyzer happy which thinks this could be NULL
	assert(data);

	// Format >= 6
	if (likely(archive->format >= 6)) {
		// Parse PKGINFO
		if (strcmp(path, ".PKGINFO") == 0) {
			r = pakfire_archive_parse_json_metadata(archive, data, length);
			if (r)
				goto ERROR;

		// Parse scriptlets
		} else if (pakfire_string_startswith(path, ".scriptlets/")) {
			r = pakfire_archive_parse_scriptlet(archive, path, data, length);
			if (r)
				goto ERROR;
		}

	// pakfire-format
	} else if (strcmp(path, "pakfire-format") == 0) {
		r = pakfire_archive_parse_format(archive, data, length);
		if (r)
			goto ERROR;

		DEBUG(archive->ctx, "Archive format is %u (%s using %s)\n",
			archive->format, archive_format_name(a), archive_filter_name(a, 0));
	}

ERROR:
	if (data)
		free(data);

	return r;
}

static int __pakfire_archive_filter_metadata(struct pakfire_archive* archive,
		struct archive_entry* entry, void* p) {
	const char* path = archive_entry_pathname(entry);

	// Format >= 6
	if (likely(archive->format >= 6)) {
		// Anything that starts with "." is a metadata file
		if (*path == '.')
			return PAKFIRE_WALK_OK;

		// Otherwise, the payload begins
		return PAKFIRE_WALK_END;

	// The pakfire-format file is part of the metadata
	} else if (strcmp(path, "pakfire-format") == 0) {
		return PAKFIRE_WALK_OK;
	}

	// Unknown file
	return PAKFIRE_WALK_ERROR;
}

static int pakfire_archive_read_metadata(struct pakfire_archive* archive) {
	int r;

	DEBUG(archive->ctx, "Reading archive metadata...\n");

	// Check if the archive file actually has any contect
	if (!archive->stat.st_size) {
		ERROR(archive->ctx, "Trying to open an empty archive file\n");
		return -EINVAL;
	}

	// Walk through the archive
	r = pakfire_archive_open_and_walk(archive,
			__pakfire_archive_read_metadata, __pakfire_archive_filter_metadata, NULL);
	if (r < 0)
		return r;

	// Check if we could successfully read something
	if (!archive->format) {
		DEBUG(archive->ctx, "Archive has an unknown format\n");
		return -ENOMSG;
	}

	// Check if we have read some metadata
	if (!archive->metadata) {
		DEBUG(archive->ctx, "Archive has no metadata\n");
		return -ENOMSG;
	}

	return 0;
}

int pakfire_archive_open(struct pakfire_archive** archive,
		struct pakfire* pakfire, const char* path) {
	struct pakfire_archive* a = NULL;
	int r;

	// Allocate a new object
	a = calloc(1, sizeof(*a));
	if (!a)
		return -errno;

	// Store a reference to the context
	a->ctx = pakfire_ctx(pakfire);

	// Store a reference to pakfire
	a->pakfire = pakfire_ref(pakfire);

	// Initialize the reference counter
	a->nrefs = 1;

	// Store path
	r = pakfire_string_set(a->path, path);
	if (r < 0)
		goto ERROR;

	DEBUG(a->ctx, "Opening archive %s\n", a->path);

	// Open the file (and keep the file descriptor open)
	a->f = fopen(a->path, "r");
	if (!a->f) {
		ERROR(a->ctx, "Could not open archive %s: %m\n", a->path);
		r = -errno;
		goto ERROR;
	}

	// Call stat() on f
	r = fstat(fileno(a->f), &a->stat);
	if (r < 0) {
		ERROR(a->ctx, "Could not stat archive %s: %m\n", a->path);
		r = -errno;
		goto ERROR;
	}

	// This must be a regular file
	if (!S_ISREG(a->stat.st_mode)) {
		// Fail on directories
		if (S_ISDIR(a->stat.st_mode)) {
			r = -EISDIR;
			goto ERROR;

		// Fail for everything else
		} else {
			r = -EINVAL;
			goto ERROR;
		}
	}

	// Read all package metadata
	r = pakfire_archive_read_metadata(a);
	if (r < 0)
		goto ERROR;

	// Return the pointer
	*archive = pakfire_archive_ref(a);

ERROR:
	if (a)
		pakfire_archive_unref(a);

	return r;
}

static struct json_object* pakfire_archive_metadata_get_object(
		struct pakfire_archive* archive, const char* key1, const char* key2) {
	struct json_object* object = archive->metadata;
	int r;

	const char* keys[] = {
		key1,
		key2,
		NULL,
	};

	// Walk through all keys
	for (const char** key = keys; *key; key++) {
		// Try finding a matching JSON object
		r = json_object_object_get_ex(object, *key, &object);
		if (!r) {
			DEBUG(archive->ctx, "Could not find JSON object at '%s': %m\n", *key);
			break;
		}
	}

	return object;
}

static const char* pakfire_archive_metadata_get(
		struct pakfire_archive* archive, const char* key1, const char* key2) {
	// Try finding an object
	struct json_object* object = pakfire_archive_metadata_get_object(archive, key1, key2);
	if (!object)
		return NULL;

	// Return the object as string
	return json_object_get_string(object);
}

static int64_t pakfire_archive_metadata_get_int64(
		struct pakfire_archive* archive, const char* key1, const char* key2) {
	// Try finding an object
	struct json_object* object = pakfire_archive_metadata_get_object(archive, key1, key2);
	if (!object)
		return 0;

	// Return the object as integer
	return json_object_get_int64(object);
}

static int pakfire_archive_filter_payload(struct pakfire_archive* archive,
		struct archive_entry* entry, void* p) {
	const char* path = archive_entry_pathname(entry);
	if (!path)
		return PAKFIRE_WALK_ERROR;

	switch (*path) {
		case 'p':
			if (strcmp(path, "pakfire-format") == 0)
				return PAKFIRE_WALK_SKIP;
			break;

		case '.':
			return PAKFIRE_WALK_SKIP;

		default:
			break;
	}

	// The first file that isn't metadata, so we are done calling the filter callback
	return PAKFIRE_WALK_DONE;
}

int pakfire_archive_walk_payload(struct pakfire_archive* archive,
		pakfire_archive_walk_callback callback, void* data) {
	return pakfire_archive_open_and_walk(
		archive, callback, pakfire_archive_filter_payload, data);
}

/*
	Read files from the archive
*/
struct pakfire_archive_read_cookie {
	// A reference to the archive
	struct pakfire_archive* archive;

	// The path we are reading
	char path[PATH_MAX];

	// The opened archive
	struct archive* a;

	// File handle opened by the cookie
	FILE* f;

	// Some flags
	int flags;

	// Count how many symlinks we have followed
	unsigned int followed_symlinks;
};

static ssize_t pakfire_archive_cookie_read(void* c, char* buffer, size_t size) {
	struct pakfire_archive_read_cookie* cookie = (struct pakfire_archive_read_cookie*)c;

	// Read the data directly from the archive
	return archive_read_data(cookie->a, buffer, size);
}

static int pakfire_archive_cookie_close(void* c) {
	struct pakfire_archive_read_cookie* cookie = (struct pakfire_archive_read_cookie*)c;

	if (cookie->archive)
		pakfire_archive_unref(cookie->archive);
	if (cookie->a)
		archive_read_free(cookie->a);

	// Free the cookie
	free(cookie);

	return 0;
}

static cookie_io_functions_t pakfire_archive_read_functions = {
	.read  = pakfire_archive_cookie_read,
	.close = pakfire_archive_cookie_close,
};

// Tries to find a matching file in the archive
static int pakfire_archive_read_filter(struct pakfire_archive* archive,
		struct archive_entry* e, void* data) {
	struct pakfire_archive_read_cookie* cookie = data;
	const char* symlink = NULL;
	int r;

	// Fetch path
	const char* p = archive_entry_pathname(e);
	if (!p)
		return PAKFIRE_WALK_ERROR;

	// We found a match
	if (strcmp(cookie->path + 1, p) == 0) {
		if (cookie->flags & PAKFIRE_ARCHIVE_READ_FOLLOW_SYMLINKS) {
			switch (archive_entry_filetype(e)) {
				case AE_IFLNK:
					break;

				default:
					return PAKFIRE_WALK_DONE;
			}

			// Resolve the destination of the symlink
			symlink = archive_entry_symlink(e);
			if (!symlink)
				return PAKFIRE_WALK_ERROR;

			// Update path
			r = pakfire_path_merge(cookie->path, cookie->path, symlink);
			if (r)
				return PAKFIRE_WALK_ERROR;

			// Increment counter
			if (++cookie->followed_symlinks >= MAX_FOLLOW_SYMLINKS) {
				ERROR(archive->ctx, "Reached maximum number of symlinks to follow\n");
				return PAKFIRE_WALK_ERROR;
			}

			return PAKFIRE_WALK_AGAIN;
		}

		return PAKFIRE_WALK_DONE;
	}

	// Otherwise we skip the file
	return PAKFIRE_WALK_SKIP;
}

// Reads a matching file into memory
static int __pakfire_archive_read(struct pakfire_archive* archive,
		struct archive* a, struct archive_entry* e, void* data) {
	struct pakfire_archive_read_cookie* cookie = data;

	// Create a file descriptor
	cookie->f = fopencookie(cookie, "r", pakfire_archive_read_functions);
	if (!cookie->f) {
		ERROR(archive->ctx, "Could not open /%s: %m\n", cookie->path);
		return PAKFIRE_WALK_ERROR;
	}

	return PAKFIRE_WALK_DONE;
}

FILE* pakfire_archive_read(struct pakfire_archive* archive, const char* path, int flags) {
	struct pakfire_archive_read_cookie* cookie = NULL;
	int r;

	// Check if path is absolute
	if (!path || *path != '/') {
		errno = EINVAL;
		return NULL;
	}

	// Allocate a cookie
	cookie = calloc(1, sizeof(*cookie));
	if (!cookie) {
		ERROR(archive->ctx, "Could not allocate a cookie: %m\n");
		goto ERROR;
	}

	// Store a reference to the archive
	cookie->archive = pakfire_archive_ref(archive);

	// Store flags
	cookie->flags = flags;

	// Store the path
	r = pakfire_string_set(cookie->path, path);
	if (r) {
		ERROR(archive->ctx, "Could not set path: %m\n");
		goto ERROR;
	}

	for (;;) {
		// Close any previously opened archives
		if (cookie->a)
			archive_read_free(cookie->a);

		// Open the archive
		r = pakfire_archive_open_archive(archive, &cookie->a);
		if (r < 0)
			goto ERROR;

		// Walk through the archive
		r = pakfire_archive_walk(archive, cookie->a,
				__pakfire_archive_read, pakfire_archive_read_filter, cookie);
		switch (-r) {
			// Success
			case 0:
				break;

			// We have updated the path and need to search again...
			case EAGAIN:
				continue;

			default:
				goto ERROR;
		}

		// Nothing found
		if (!cookie->f) {
			ERROR(archive->ctx, "Could not find /%s\n", path);

			// No such file or directory
			errno = ENOENT;
			goto ERROR;
		}

		return cookie->f;
	}

ERROR:
	if (cookie)
		pakfire_archive_cookie_close(cookie);

	return NULL;
}

/*
	Unlinks the archive.

	We will keep the archive open so it can still be used.
*/
int pakfire_archive_unlink(struct pakfire_archive* archive) {
	int r;

	// We cannot do this if we don't have a path
	if (!*archive->path)
		return -ENOTSUP;

	DEBUG(archive->ctx, "Unlinking %s\n", archive->path);

	// Unlink the file
	r = unlink(archive->path);
	if (r < 0) {
		switch (errno) {
			case ENOENT:
				break;

			default:
				ERROR(archive->ctx, "Could not unlink %s: %m\n", archive->path);
				return -errno;
		}
	}

	return 0;
}

int pakfire_archive_copy(struct pakfire_archive* archive, const char* path) {
	FILE* f = NULL;
	int r;

	if (!path)
		return -EINVAL;

	// Determine the file size
	ssize_t size = pakfire_archive_get_size(archive);
	if (size < 0)
		return -EINVAL;

	DEBUG(archive->ctx, "Copying %s to %s...\n", archive->path, path);

	// Ensure we copy from the very beginning
	r = pakfire_rewind(archive->f);
	if (r < 0) {
		ERROR(archive->ctx, "Could not rewind %s: %m\n", archive->path);
		r = -errno;
		goto ERROR;
	}

	// Ensure the parent directory exists
	pakfire_mkparentdir(path, 0755);

	// Open destination file
	f = fopen(path, "w");
	if (!f) {
		r = -errno;
		goto ERROR;
	}

	// Copy everything
	ssize_t bytes_written = sendfile(fileno(f), fileno(archive->f), NULL, size);
	if (bytes_written < size) {
		ERROR(archive->ctx,
			"Could not copy archive (%zd byte(s) written): %m\n", bytes_written);
		r = -errno;
		goto ERROR;
	}

	// Success
	r = 0;

ERROR:
	if (f)
		fclose(f);

	// Delete the file on error
	if (r)
		unlink(path);

	return r;
}

static int pakfire_archive_link(struct pakfire_archive* archive, const char* path) {
	int r;

	// Check if path is set
	if (!path)
		return -EINVAL;

	DEBUG(archive->ctx, "Linking %s to %s...\n", archive->path, path);

	// Delete the destination file (if it exists)
	unlink(path);

	// Create the new link
	r = link(archive->path, path);
	if (r) {
		DEBUG(archive->ctx, "Could not create hardlink %s: %m\n", path);
		return r;
	}

	return 0;
}

int pakfire_archive_link_or_copy(struct pakfire_archive* archive, const char* path) {
	int r;

	// Make sure the parent directory exists
	r = pakfire_mkparentdir(path, 0755);
	if (r)
		return r;

	// Try to create a hardlink
	r = pakfire_archive_link(archive, path);
	switch (-r) {
		// Try to copy the file if we could not create a hardlink
		case EPERM:
			r = pakfire_archive_copy(archive, path);

		default:
			break;
	}

	return r;
}

/*
	Extraction
*/

struct pakfire_extract_state {
	struct pakfire_archive* archive;

	// Archive
	struct archive* a;

	// Prefix
	const char* prefix;

	// Writer
	struct archive* writer;
};

static void pakfire_extract_progress(void* data) {
	struct pakfire_extract_state* state = data;

	// Fetch how many bytes have been read
	const size_t position = archive_filter_bytes(state->a, -1);

	// Update progress
	pakfire_progress_update(state->archive->progress, position);
}

static int pakfire_archive_extract_one(struct pakfire_archive* archive,
		struct archive* a, struct archive_entry* entry, void* data) {
	struct pakfire_file* file = NULL;
	struct vfs_cap_data cap_data = {};
	char buffer[PATH_MAX];
	int r;

	struct pakfire_extract_state* state = data;

	// Fetch path
	const char* path = archive_entry_pathname(entry);

	// Make sure we have a leading slash on the filelist
	if (!pakfire_string_startswith(path, "/")) {
		r = pakfire_string_format(buffer, "/%s", path);
		if (r < 0)
			goto ERROR;

		// Store the new name
		archive_entry_set_pathname(entry, buffer);

		// Update the path pointer
		path = archive_entry_pathname(entry);
	}

	// Generate a file object
	r = pakfire_file_create_from_archive_entry(&file, archive->pakfire, entry);
	if (r < 0)
		goto ERROR;

	const int configfile = pakfire_file_has_flag(file, PAKFIRE_FILE_CONFIG);

	// Prepend the prefix
	if (*state->prefix) {
		// Compose file path
		r = pakfire_path_append(buffer, state->prefix, path);
		if (r < 0) {
			ERROR(archive->ctx, "Could not compose file path: %m\n");
			goto ERROR;
		}

		// Set file path
		archive_entry_set_pathname(entry, buffer);

		// Update hardlink destination
		const char* link = archive_entry_hardlink(entry);
		if (link) {
			r = pakfire_path_append(buffer, state->prefix, link);
			if (r < 0) {
				ERROR(archive->ctx, "Could not compose hardlink path: %m\n");
				goto ERROR;
			}

			// Set hardlink path
			archive_entry_set_hardlink(entry, buffer);
		}
	}

	if (configfile) {
		// Fetch path again since we changed it
		path = archive_entry_pathname(entry);

		if (pakfire_path_exists(path)) {
			DEBUG(archive->ctx, "The configuration file %s exists\n",
				pakfire_file_get_path(file));

			r = pakfire_string_format(buffer, "%s.paknew", path);
			if (r < 0) {
				ERROR(archive->ctx, "Could not compose path for configuration file: %m\n");
				goto ERROR;
			}

			// Set the path again
			archive_entry_set_pathname(entry, buffer);
		}
	}

	// Write the file...

	// Fetch path again since we changed it
	path = archive_entry_pathname(entry);

	DEBUG(archive->ctx, "Extracting %s\n", path);

	// Remove any extended attributes which we never write to disk
	archive_entry_xattr_clear(entry);

	// Set capabilities
	if (pakfire_file_has_caps(file)) {
		r = pakfire_file_write_fcaps(file, &cap_data);
		if (r)
			goto ERROR;

		// Store capabilities in archive entry
		archive_entry_xattr_add_entry(entry,
			"security.capability", &cap_data, sizeof(cap_data));
	}

	// Write payload
	r = archive_read_extract2(state->a, entry, state->writer);
	switch (r) {
		case ARCHIVE_OK:
			r = 0;
			break;

		case ARCHIVE_WARN:
			ERROR(archive->ctx, "%s\n", archive_error_string(state->writer));

			// Pretend everything has been okay
			r = 0;
			break;

		case ARCHIVE_FATAL:
			ERROR(archive->ctx, "%s\n", archive_error_string(state->writer));
			r = 1;
			break;
	}

ERROR:
	if (file)
		pakfire_file_unref(file);

	return r;
}

static int __pakfire_archive_extract(struct pakfire_archive* archive, const char* path, int flags) {
	struct pakfire_package* pkg = NULL;
	char prefix[PATH_MAX] = "/";
	struct archive* writer = NULL;
	struct archive* a = NULL;
	int r;

	DEBUG(archive->ctx, "Extracting %s\n", archive->path);

	// Fetch package
	pkg = pakfire_archive_get_package(archive);
	if (!pkg) {
		r = -errno;
		goto ERROR;
	}

	// Open the archive
	r = pakfire_archive_open_archive(archive, &a);
	if (r < 0)
		goto ERROR;

	// Setup the writer
	writer = pakfire_get_disk_writer(archive->pakfire);
	if (!writer) {
		r = -errno;
		goto ERROR;
	}

	// Fetch NEVRA
	const char* nevra = pakfire_package_get_string(pkg, PAKFIRE_PKG_NEVRA);

	// Create the progress indicator
	r = pakfire_progress_create(&archive->progress,
			archive->ctx, PAKFIRE_PROGRESS_SHOW_PERCENTAGE, NULL);
	if (r < 0)
		goto ERROR;

	// Set the title
	r = pakfire_progress_set_title(archive->progress, "%s", nevra);
	if (r < 0)
		goto ERROR;

	// Copy everything to path if set
	if (path) {
		r = pakfire_string_set(prefix, path);
		if (r < 0)
			goto ERROR;

	// Set prefix for source packages
	} else if (pakfire_package_is_source(pkg)) {
		r = pakfire_path(archive->pakfire, prefix, "/usr/src/packages/%s", nevra);
		if (r < 0)
			goto ERROR;

	// Otherwise extract relative to the pakfire root
	} else {
		r = pakfire_path(archive->pakfire, prefix, "%s", "/");
		if (r < 0)
			goto ERROR;
	}

	// Create state
	struct pakfire_extract_state state = {
		.archive = archive,
		.a       = a,
		.prefix  = prefix,
		.writer  = writer,
	};

	// Register progress callback
	archive_read_extract_set_progress_callback(a, pakfire_extract_progress, &state);

	// Start progress
	r = pakfire_progress_start(archive->progress, archive->stat.st_size);
	if (r < 0)
		goto ERROR;

	// Walk through the entire archive and extract everything
	r = pakfire_archive_walk(archive, a,
			pakfire_archive_extract_one, pakfire_archive_filter_payload, &state);
	if (r < 0)
		goto ERROR;

ERROR:
	// Finish the progress
	pakfire_progress_finish(archive->progress);

	if (pkg)
		pakfire_package_unref(pkg);
	if (a)
		archive_read_free(a);

	return r;
}

int pakfire_archive_extract(struct pakfire_archive* archive,
		const char* path, const int flags) {
	return __pakfire_archive_extract(archive, path, flags);
}

const char* pakfire_archive_get_path(struct pakfire_archive* archive) {
	return archive->path;
}

unsigned int pakfire_archive_get_format(struct pakfire_archive* archive) {
	return archive->format;
}

static int __pakfire_archive_load_filelist(struct pakfire_archive* archive,
		struct archive* a, struct archive_entry* entry, void* data) {
	struct pakfire_filelist* filelist = data;
	struct pakfire_file* file = NULL;
	int r;

	// Generate a file object
	r = pakfire_file_create_from_archive_entry(&file, archive->pakfire, entry);
	if (r < 0)
		goto ERROR;

	// Add entry to filelist
	r = pakfire_filelist_add(filelist, file);
	if (r < 0)
		goto ERROR;

ERROR:
	if (file)
		pakfire_file_unref(file);

	return r;
}

static int pakfire_archive_load_filelist(struct pakfire_archive* self) {
	struct pakfire_filelist* filelist = NULL;
	int r;

	// Create a new filelist
	r = pakfire_filelist_create(&filelist, self->pakfire);
	if (r < 0)
		goto ERROR;

	// Walk through the entire archive and extract everything
	r = pakfire_archive_open_and_walk(self,
			__pakfire_archive_load_filelist, pakfire_archive_filter_payload, filelist);
	if (r < 0)
		goto ERROR;

	// Free any previous data
	if (self->filelist)
		pakfire_filelist_unref(self->filelist);

	// Store the filelist
	self->filelist = pakfire_filelist_ref(filelist);

ERROR:
	if (filelist)
		pakfire_filelist_unref(filelist);

	return r;
}

int pakfire_archive_get_filelist(struct pakfire_archive* self, struct pakfire_filelist** filelist) {
	int r;

	// Load the filelist if we don't have one, yet
	if (!self->filelist) {
		r = pakfire_archive_load_filelist(self);
		if (r < 0)
			return r;
	}

	*filelist = pakfire_filelist_ref(self->filelist);
	return 0;
}

int pakfire_archive_verify(struct pakfire_archive* archive, int* status) {
	// XXX currently not implemented
	return 0;
}

ssize_t pakfire_archive_get_size(struct pakfire_archive* archive) {
	return archive->stat.st_size;
}

int pakfire_archive_verify_checksum(struct pakfire_archive* archive,
		const enum pakfire_hash_type type, const unsigned char* checksum, const size_t length) {
	struct pakfire_hashes expected_hashes = {};
	int r;

	// Store the checksum
	r = pakfire_hashes_set(&expected_hashes, type, checksum, length);
	if (r < 0)
		return r;

	// Compute the checksums
	r = pakfire_archive_compute_hashes(archive);
	if (r < 0)
		return r;

	// Compare the hashes
	r = pakfire_hashes_compare(archive->ctx, &expected_hashes, &archive->hashes);
	if (r) {
		ERROR(archive->ctx, "Archive checksum does not match for %s:\n", archive->path);
		ERROR(archive->ctx, " Expected:\n");
		pakfire_hashes_dump(archive->ctx, &expected_hashes, LOG_ERR);
		ERROR(archive->ctx, " Computed:\n");
		pakfire_hashes_dump(archive->ctx, &archive->hashes, LOG_ERR);
	}

	return r;
}

static int pakfire_archive_import_filelist_from_json(
		struct pakfire_archive* archive, struct pakfire_package* package) {
	struct json_object* array = NULL;
	int r;

	// Fetch the array with the filelist
	array = pakfire_archive_metadata_get_object(archive, "filelist", NULL);
	if (!array) {
		ERROR(archive->ctx, "Archive has no filelist: %m\n");
		return 1;
	}

	// Determine the length of the array
	const size_t length = json_object_array_length(array);

	// End here if the array is empty
	if (!length)
		return 0;

	// Walk through all items in this array
	for (unsigned int i = 0; i < length; i++) {
		struct json_object* item = json_object_array_get_idx(array, i);
		if (!item)
			continue;

		// Extract the path value
		const char* path = json_object_get_string(item);
		if (!path)
			continue;

		// Append the file to the package
		r = pakfire_package_append_file(package, path);
		if (r)
			return r;
	}

	return 0;
}

static int pakfire_archive_make_package_from_json(struct pakfire_archive* archive,
		struct pakfire_repo* repo, struct pakfire_package** package) {
	struct pakfire_package* pkg = NULL;
	char path[PATH_MAX];
	int r;

	// Calculate checksums
	r = pakfire_archive_compute_hashes(archive);
	if (r)
		return r;

	// Fetch the most basic package information
	const char* name = pakfire_archive_metadata_get(archive, "name", NULL);
	const char* evr  = pakfire_archive_metadata_get(archive, "evr", NULL);
	const char* arch = pakfire_archive_metadata_get(archive, "arch", NULL);

	// Create a new package object
	r = pakfire_package_create(&pkg, archive->pakfire, repo, name, evr, arch);
	if (r)
		return r;

#ifdef ENABLE_DEBUG
	const char* nevra = pakfire_package_get_string(pkg, PAKFIRE_PKG_NEVRA);

	DEBUG(archive->ctx, "Created package %s (%p) from archive %p\n",
		nevra, pkg, archive);
#endif

	// Set checksum
	switch (PAKFIRE_ARCHIVE_CHECKSUM) {
		case PAKFIRE_HASH_SHA2_512:
			r = pakfire_package_set_checksum(pkg, PAKFIRE_ARCHIVE_CHECKSUM,
					archive->hashes.sha2_512, sizeof(archive->hashes.sha2_512));
			if (r < 0)
				goto ERROR;
			break;

		case PAKFIRE_HASH_SHA2_256:
			r = pakfire_package_set_checksum(pkg, PAKFIRE_ARCHIVE_CHECKSUM,
					archive->hashes.sha2_256, sizeof(archive->hashes.sha2_256));
			if (r < 0)
				goto ERROR;
			break;

		case PAKFIRE_HASH_UNDEFINED:
			r = -EINVAL;
			goto ERROR;
	}

	// Vendor
	const char* vendor = pakfire_archive_metadata_get(archive, "vendor", NULL);
	if (vendor) {
		r = pakfire_package_set_string(pkg, PAKFIRE_PKG_VENDOR, vendor);
		if (r)
			goto ERROR;
	}

	// UUID
	const char* uuid = pakfire_archive_metadata_get(archive, "uuid", NULL);
	if (uuid) {
		r = pakfire_package_set_string(pkg, PAKFIRE_PKG_UUID, uuid);
		if (r)
			goto ERROR;
	}

	// Groups
	struct json_object* groups = pakfire_archive_metadata_get_object(archive, "groups", NULL);

	if (groups) {
		if (json_object_is_type(groups, json_type_array)) {
			const size_t length = json_object_array_length(groups);

			for (unsigned int i = 0; i < length; i++) {
				struct json_object* item = json_object_array_get_idx(groups, i);
				if (!item)
					continue;

				const char* group = json_object_get_string(item);
				if (!group)
					continue;

				r = pakfire_package_add_string(pkg, PAKFIRE_PKG_GROUPS, group);
				if (r)
					goto ERROR;
			}

		// Compatibility code for when groups where stored as a simple string
		} else if (json_object_is_type(groups, json_type_string)) {
			r = pakfire_package_add_string(pkg,
					PAKFIRE_PKG_GROUPS, json_object_get_string(groups));
			if (r < 0)
				goto ERROR;
		}
	}

	// Distribution
	const char* distro = pakfire_archive_metadata_get(archive, "distribution", NULL);
	if (distro) {
		r = pakfire_package_set_string(pkg, PAKFIRE_PKG_DISTRO, distro);
		if (r)
			goto ERROR;
	}

	// Packager
	const char* packager = pakfire_archive_metadata_get(archive, "packager", NULL);
	if (packager) {
		r = pakfire_package_set_string(pkg, PAKFIRE_PKG_PACKAGER, packager);
		if (r)
			goto ERROR;
	}

	// URL
	const char* url = pakfire_archive_metadata_get(archive, "url", NULL);
	if (url) {
		r = pakfire_package_set_string(pkg, PAKFIRE_PKG_URL, url);
		if (r)
			goto ERROR;
	}

	// License
	const char* license = pakfire_archive_metadata_get(archive, "license", NULL);
	if (license) {
		r = pakfire_package_set_string(pkg, PAKFIRE_PKG_LICENSE, license);
		if (r)
			goto ERROR;
	}

	// Summary
	const char* summary = pakfire_archive_metadata_get(archive, "summary", NULL);
	if (summary) {
		r = pakfire_package_set_string(pkg, PAKFIRE_PKG_SUMMARY, summary);
		if (r)
			goto ERROR;
	}

	// Description
	const char* description = pakfire_archive_metadata_get(archive, "description", NULL);
	if (description) {
		r = pakfire_package_set_string(pkg, PAKFIRE_PKG_DESCRIPTION, description);
		if (r)
			goto ERROR;
	}

	// Installed package size
	size_t installsize = pakfire_archive_metadata_get_int64(archive, "size", NULL);
	if (installsize) {
		r = pakfire_package_set_num(pkg, PAKFIRE_PKG_INSTALLSIZE, installsize);
		if (r)
			goto ERROR;
	}

	// Download size
	r = pakfire_package_set_num(pkg,
		PAKFIRE_PKG_DOWNLOADSIZE, pakfire_archive_get_size(archive));
	if (r)
		goto ERROR;

	// Build Host
	const char* build_host = pakfire_archive_metadata_get(archive, "build", "host");
	if (build_host) {
		r = pakfire_package_set_string(pkg, PAKFIRE_PKG_BUILD_HOST, build_host);
		if (r)
			goto ERROR;
	}

	// Build ID
	const char* build_id = pakfire_archive_metadata_get(archive, "build", "id");
	if (build_id) {
		r = pakfire_package_set_string(pkg, PAKFIRE_PKG_BUILD_ID, build_id);
		if (r)
			goto ERROR;
	}

	// Build Time
	time_t build_time = pakfire_archive_metadata_get_int64(archive, "build", "time");
	if (build_time) {
		r = pakfire_package_set_num(pkg, PAKFIRE_PKG_BUILD_TIME, build_time);
		if (r)
			goto ERROR;
	}

	// Build arches
	struct json_object* build_arches = pakfire_archive_metadata_get_object(archive, "build", "arches");
	if (build_arches && json_object_is_type(build_arches, json_type_array)) {
		const size_t length = json_object_array_length(build_arches);

		for (unsigned int i = 0; i < length; i++) {
			struct json_object* item = json_object_array_get_idx(build_arches, i);
			if (!item)
				continue;

			const char* build_arch = json_object_get_string(item);
			if (!build_arch)
				continue;

			r = pakfire_package_add_string(pkg, PAKFIRE_PKG_BUILD_ARCHES, build_arch);
			if (r)
				goto ERROR;
		}
	}

	// Source package
	const char* source_name = pakfire_archive_metadata_get(archive, "build", "source-name");
	if (source_name) {
		r = pakfire_package_set_string(pkg, PAKFIRE_PKG_SOURCE_NAME, source_name);
		if (r)
			goto ERROR;
	}

	// Source EVR
	const char* source_evr = pakfire_archive_metadata_get(archive, "build", "source-evr");
	if (source_evr) {
		r = pakfire_package_set_string(pkg, PAKFIRE_PKG_SOURCE_EVR, source_evr);
		if (r)
			goto ERROR;
	}

	// Source arch
	const char* source_arch = pakfire_archive_metadata_get(archive, "build", "source-arch");
	if (source_arch) {
		r = pakfire_package_set_string(pkg, PAKFIRE_PKG_SOURCE_ARCH, source_arch);
		if (r)
			goto ERROR;
	}

	// Dependencies
	for (const struct pakfire_dep* dep = pakfire_deps; dep->key; dep++) {
		struct json_object* array = pakfire_archive_metadata_get_object(
			archive, "dependencies", dep->name);
		if (!array)
			continue;

		// Determine the length of the array
		const size_t length = json_object_array_length(array);
		if (!length)
			continue;

		// Walk through all items in this array
		for (unsigned int i = 0; i < length; i++) {
			struct json_object* item = json_object_array_get_idx(array, i);
			if (!item)
				continue;

			// Extract the string value
			const char* string = json_object_get_string(item);
			if (!string)
				continue;

			// Add the dependency to the package
			r = pakfire_package_add_dep(pkg, dep->key, "%s", string);
			if (r)
				goto ERROR;
		}
	}

	// Import the filelist
	r = pakfire_archive_import_filelist_from_json(archive, pkg);
	if (r)
		goto ERROR;

	// Make the path where this archive should be stored
	r = pakfire_repo_make_path(repo, path, archive, pkg);
	if (r < 0)
		goto ERROR;

	// Set path
	r = pakfire_package_set_string(pkg, PAKFIRE_PKG_PATH, path);
	if (r < 0)
		goto ERROR;

	// Return the package (if requested)
	if (package)
		*package = pakfire_package_ref(pkg);

	// Success!
	r = 0;

ERROR:
	if (pkg)
		pakfire_package_unref(pkg);

	return r;
}

/*
	Copy all metadata from this archive to the package object
*/
int pakfire_archive_make_package(struct pakfire_archive* archive,
		struct pakfire_repo* repo, struct pakfire_package** package) {
	struct pakfire_repo* dummy = NULL;
	int r;

	// Use dummy repo if no repository was passed
	if (!repo) {
		dummy = pakfire_get_repo(archive->pakfire, PAKFIRE_REPO_DUMMY);
		if (!dummy)
			return 1;

		repo = dummy;
	}

	// Make package from JSON metadata
	r = pakfire_archive_make_package_from_json(archive, repo, package);

	// Free dummy repository
	if (dummy)
		pakfire_repo_unref(dummy);

	return r;
}

struct pakfire_scriptlet* pakfire_archive_get_scriptlet(
		struct pakfire_archive* archive, const char* type) {
	struct pakfire_scriptlet* scriptlet = NULL;

	for (unsigned int i = 0; i < archive->num_scriptlets; i++) {
		scriptlet = archive->scriptlets[i];

		// Fetch type
		const char* t = pakfire_scriptlet_get_type(scriptlet);

		// Compare type
		if (strcmp(t, type) == 0)
			return pakfire_scriptlet_ref(scriptlet);
	}

	return NULL;
}

/*
	systemd sysusers
*/
static int pakfire_archive_filter_systemd_sysusers(struct pakfire_archive* archive,
		struct archive_entry* e, void* data) {
	const char* path = archive_entry_pathname(e);

	if (pakfire_path_match("usr/lib/sysusers.d/*.conf", path))
		return PAKFIRE_WALK_OK;

	return PAKFIRE_WALK_SKIP;
}

static ssize_t pakfire_archive_stream_payload(
		struct pakfire_ctx* ctx, void* data, char* buffer, size_t length) {
	struct archive* a = data;
	ssize_t bytes_read;

	// Fill the buffer with data from the archive
	bytes_read = archive_read_data(a, buffer, length);
	if (bytes_read < 0)
		ERROR(ctx, "Could not read from archive: %s\n", archive_error_string(a));

	return bytes_read;
}

static int pakfire_archive_handle_systemd_sysusers(struct pakfire_archive* archive,
		struct archive* a, struct archive_entry* e, void* data) {
	struct pakfire_jail* jail = NULL;
	char replace[PATH_MAX];
	int r;

	// Fetch path
	const char* path = archive_entry_pathname(e);

	// Format --replace
	r = pakfire_string_format(replace, "--replace=/%s", path);
	if (r < 0)
		goto ERROR;

	const char* argv[] = {
		"/usr/bin/systemd-sysusers",
		"--no-pager",
		replace,
		"-",
		NULL,
	};

	// Create a new jail
	r = pakfire_jail_create(&jail, archive->pakfire);
	if (r)
		goto ERROR;

	// Run!
	r = pakfire_jail_communicate(jail, argv, NULL, PAKFIRE_JAIL_NOENT_OK,
		pakfire_archive_stream_payload, a, NULL, NULL);

ERROR:
	if (jail)
		pakfire_jail_unref(jail);

	return r;
}

int pakfire_archive_apply_systemd_sysusers(struct pakfire_archive* archive) {
	pakfire_archive_open_and_walk(archive, pakfire_archive_handle_systemd_sysusers,
		pakfire_archive_filter_systemd_sysusers, NULL);

	return 0;
}

int pakfire_archive_lint(struct pakfire_archive* archive,
		pakfire_linter_result_callback callback, void* data) {
	struct pakfire_linter* linter = NULL;
	int r;

	// Create a new linter
	r = pakfire_linter_create(&linter, archive->pakfire, archive);
	if (r < 0)
		goto ERROR;

	// Set callback
	if (callback)
		pakfire_linter_set_result_callback(linter, callback, data);

	// Lint, lint, lint...
	r = pakfire_linter_lint(linter);
	if (r < 0)
		goto ERROR;

ERROR:
	if (linter)
		pakfire_linter_unref(linter);

	return r;
}
