/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/queue.h>

// libarchive
#include <archive.h>
#include <archive_entry.h>

#include <pakfire/archive.h>
#include <pakfire/fhs.h>
#include <pakfire/file.h>
#include <pakfire/filelist.h>
#include <pakfire/linter.h>
#include <pakfire/linter-file.h>
#include <pakfire/logging.h>
#include <pakfire/package.h>
#include <pakfire/pakfire.h>
#include <pakfire/string.h>

struct pakfire_linter_result {
	TAILQ_ENTRY(pakfire_linter_result) nodes;

	// File
	struct pakfire_file* file;

	// Priority
	int priority;

	// Comment
	char* comment;
};

struct pakfire_linter {
	struct pakfire_ctx* ctx;
	int nrefs;

	// Pakfire
	struct pakfire* pakfire;

	// Archive
	struct pakfire_archive* archive;

	// Package
	struct pakfire_package* pkg;

	// Filelist
	struct pakfire_filelist* filelist;

	// Results
	TAILQ_HEAD(results, pakfire_linter_result) results;

	// Stats
	struct pakfire_linter_stats {
		unsigned int results;

		// Count errors and warnings
		unsigned int errors;
		unsigned int warnings;
	} stats;

	// Result Callback
	pakfire_linter_result_callback result_callback;
	void* result_data;
};

static void pakfire_linter_result_free(struct pakfire_linter_result* result) {
	if (result->file)
		pakfire_file_unref(result->file);
	if (result->comment)
		free(result->comment);
	free(result);
}

int pakfire_linter_result(struct pakfire_linter* linter, struct pakfire_file* file,
		int priority, const char* format, ...) {
	struct pakfire_linter_result* result = NULL;
	va_list args;
	int r;

	// Check inputs
	if (!format)
		return -EINVAL;

	// Allocate a new result
	result = calloc(1, sizeof(*result));
	if (!result)
		return -errno;

	// Store file
	if (file) {
		result->file = pakfire_file_ref(file);

		// Add the file to the filelist
		r = pakfire_filelist_add(linter->filelist, file);
		if (r < 0)
			goto ERROR;
	}

	// Store priority
	result->priority = priority;

	// Store the comment
	va_start(args, format);
	r = vasprintf(&result->comment, format, args);
	va_end(args);

	// Fail on error
	if (r < 0)
		goto ERROR;

	// Store the result
	TAILQ_INSERT_TAIL(&linter->results, result, nodes);

	// Update stats
	linter->stats.results++;

	switch (priority) {
		case PAKFIRE_LINTER_WARNING:
			linter->stats.warnings++;
			break;

		case PAKFIRE_LINTER_ERROR:
			linter->stats.errors++;
			break;

		default:
			break;
	}

	// Call the callback
	if (linter->result_callback) {
		r = linter->result_callback(linter->ctx, linter->archive, linter->pkg,
			result->file, priority, result->comment, linter->result_data);
		if (r < 0) {
			ERROR(linter->ctx, "Linter result callback failed: %s\n", strerror(-r));
			return r;
		}
	}

	return 0;

ERROR:
	if (result)
		pakfire_linter_result_free(result);

	return r;
}

int pakfire_linter_create(struct pakfire_linter** linter,
		struct pakfire* pakfire, struct pakfire_archive* archive) {
	struct pakfire_linter* l = NULL;
	int r;

	// Allocate a new object
	l = calloc(1, sizeof(*l));
	if (!l)
		return -errno;

	// Reference the context
	l->ctx = pakfire_ctx(pakfire);

	// Reference Pakfire
	l->pakfire = pakfire_ref(pakfire);

	// Initialize reference counter
	l->nrefs = 1;

	// Initialize results
	TAILQ_INIT(&l->results);

	// Store the archive
	l->archive = pakfire_archive_ref(archive);

	// Fetch the package
	r = pakfire_archive_make_package(l->archive, NULL, &l->pkg);
	if (r < 0) {
		ERROR(l->ctx, "Could not open the package in %s: %s\n",
			pakfire_archive_get_path(l->archive), strerror(-r));
		goto ERROR;
	}

	// Create a filelist
	r = pakfire_filelist_create(&l->filelist, l->pakfire);
	if (r < 0)
		goto ERROR;

	// Return the pointer
	*linter = pakfire_linter_ref(l);

ERROR:
	if (l)
		pakfire_linter_unref(l);

	return r;
}

static void pakfire_linter_free(struct pakfire_linter* linter) {
	struct pakfire_linter_result* result = NULL;

	// Free all results
	for (;;) {
		result = TAILQ_LAST(&linter->results, results);
		if (!result)
			break;

		TAILQ_REMOVE(&linter->results, result, nodes);
		pakfire_linter_result_free(result);
	}

	if (linter->filelist)
		pakfire_filelist_unref(linter->filelist);
	if (linter->archive)
		pakfire_archive_unref(linter->archive);
	if (linter->pkg)
		pakfire_package_unref(linter->pkg);
	if (linter->pakfire)
		pakfire_unref(linter->pakfire);
	if (linter->ctx)
		pakfire_ctx_unref(linter->ctx);
	free(linter);
}

struct pakfire_linter* pakfire_linter_ref(struct pakfire_linter* linter) {
	++linter->nrefs;

	return linter;
}

struct pakfire_linter* pakfire_linter_unref(struct pakfire_linter* linter) {
	if (--linter->nrefs > 0)
		return linter;

	pakfire_linter_free(linter);
	return NULL;
}

void pakfire_linter_set_result_callback(struct pakfire_linter* linter,
		pakfire_linter_result_callback callback, void* data) {
	linter->result_callback = callback;
	linter->result_data     = data;
}

static int pakfire_linter_read_file(
		struct pakfire_linter* linter, struct pakfire_file* file, struct archive* a) {
	int fd = -EBADF;
	int r;

	// Fetch path
	const char* path = pakfire_file_get_path(file);

	// Allocate a new buffer
	fd = memfd_create(path, MFD_ALLOW_SEALING|MFD_CLOEXEC);
	if (fd < 0) {
		ERROR(linter->ctx, "memfd_create() failed: %m\n");
		r = -errno;
		goto ERROR;
	}

	// Fetch file size
	size_t size = pakfire_file_get_size(file);

	// Tell the kernel how large the file would be
	r = ftruncate(fd, size);
	if (r < 0) {
		ERROR(linter->ctx, "fallocate() failed: %m\n");
		r = -errno;
		goto ERROR;
	}

	// Read the payload into the file descriptor
	r = archive_read_data_into_fd(a, fd);
	if (r) {
		ERROR(linter->ctx, "Could not read %s: %s\n", path, archive_error_string(a));
		r = -errno;
		goto ERROR;
	}

	// Check how much data we have read
	off_t offset = lseek(fd, 0, SEEK_CUR);
	if (offset < 0) {
		ERROR(linter->ctx, "lseek() failed: %m\n");
		r = -errno;
		goto ERROR;
	}

	DEBUG(linter->ctx, "Read %jd byte(s)\n", offset);

	// Check if we didn't read too little data
	if ((size_t)offset < size) {
		r = pakfire_linter_error(linter, "%s is actually smaller than %zu byte(s)", path, size);
		if (r < 0)
			goto ERROR;

	// Check if we have not read more data
	} else if ((size_t)offset > size) {
		r = pakfire_linter_error(linter, "/%s is actually larger than %zu byte(s)", path, size);
		if (r < 0)
			goto ERROR;
	}

	// Rewind
	offset = lseek(fd, 0, SEEK_SET);
	if (offset) {
		ERROR(linter->ctx, "Could not rewind: %m\n");
		r = -errno;
		goto ERROR;
	}

	// Seal the file so that nothing can be changed
	r = fcntl(fd, F_ADD_SEALS,
			F_SEAL_SEAL|F_SEAL_SHRINK|F_SEAL_GROW|F_SEAL_WRITE|F_SEAL_FUTURE_WRITE);
	if (r < 0) {
		ERROR(linter->ctx, "Could not seal the file: %m\n");
		r = -errno;
		goto ERROR;
	}

	return fd;

ERROR:
	if (fd >= 0)
		close(fd);

	return r;
}

static int pakfire_linter_payload(
		struct pakfire_linter* linter, struct pakfire_file* file, struct archive* a) {
	struct pakfire_linter_file* lfile = NULL;
	int fd = -EBADF;
	int r;

	// Skip kernel modules
	if (pakfire_file_matches(file, "/usr/lib/modules/*/**.ko")
			|| pakfire_file_matches(file, "/usr/lib/modules/*/**.ko.*"))
		return 0;

	// Skip firmware files
	if (pakfire_file_matches(file, "/usr/lib/firmware/**"))
		return 0;

	// Skip GRUB files
	else if (pakfire_file_matches(file, "/usr/lib/grub/**"))
		return 0;

	// Skip valgrind files
	else if (pakfire_file_matches(file, "/usr/lib/valgrind/*"))
		return 0;

	// Fetch path
	const char* path = pakfire_file_get_path(file);

	DEBUG(linter->ctx, "Checking payload of %s\n", path);

	// Read the file
	fd = r = pakfire_linter_read_file(linter, file, a);
	if (r < 0)
		goto ERROR;

	// Create a linter file
	r = pakfire_linter_file_create(&lfile, linter->ctx, linter, file, fd);
	if (r < 0) {
		ERROR(linter->ctx, "Could not create linter file for %s: %s\n", path, strerror(-r));
		goto ERROR;
	}

	// Lint the file
	r = pakfire_linter_file_lint(lfile);
	if (r < 0) {
		ERROR(linter->ctx, "Could not lint %s: %s\n", path, strerror(-r));
		goto ERROR;
	}

ERROR:
	if (lfile)
		pakfire_linter_file_unref(lfile);
	if (fd >= 0)
		close(fd);

	return r;
}

static int pakfire_linter_name(struct pakfire_linter* linter) {
	int r;

	// Fetch the package name
	const char* name = pakfire_package_get_string(linter->pkg, PAKFIRE_PKG_NAME);
	if (!name)
		return -errno;

	// Check for whitespace
	if (pakfire_string_contains_whitespace(name)) {
		r = pakfire_linter_error(linter, "Package name contains whitespace");
		if (r < 0)
			return r;
	}



	return 0;
}

static int pakfire_linter_lint_source(
		struct pakfire_linter* linter, struct pakfire_file* file) {
	return 0;
}

static int pakfire_linter_lint_fhs(
		struct pakfire_linter* linter, struct pakfire_file* file) {
	int r;

	// Checking against FHS
	r = pakfire_fhs_check_file(linter->ctx, file);
	if (r < 0)
		 return r;

	const char* path = pakfire_file_get_path(file);

	// File must not exist
	if (r & PAKFIRE_FHS_MUSTNOTEXIST) {
		r = pakfire_linter_error(linter, "FHS: %s must not exist", path);
		if (r < 0)
			return r;
	}

	if (r & PAKFIRE_FHS_UNAME_MISMATCH) {
		r = pakfire_linter_error(linter, "FHS: Invalid user for %s", path);
		if (r < 0)
			return r;
	}

	if (r & PAKFIRE_FHS_GNAME_MISMATCH) {
		r = pakfire_linter_error(linter, "FHS: Invalid group for %s", path);
		if (r < 0)
			return r;
	}

	if (r & PAKFIRE_FHS_PERMS_MISMATCH) {
		r = pakfire_linter_error(linter, "FHS: Invalid permissions for %s", path);
		if (r < 0)
			return r;
	}

	if (r & PAKFIRE_FHS_WORLDWRITABLE) {
		r = pakfire_linter_error(linter, "FHS: %s is world-writable", path);
		if (r < 0)
			return r;
	}

	if (r & PAKFIRE_FHS_NOEXEC) {
		r = pakfire_linter_error(linter, "FHS: %s is executable", path);
		if (r < 0)
			return r;
	}

	return 0;
}

static int pakfire_linter_file(struct pakfire_archive* archive,
		struct archive* a, struct archive_entry* e, void* data) {
	struct pakfire_linter* linter = data;
	struct pakfire_file* file = NULL;
	int r;

	// Fetch a file object
	r = pakfire_file_create_from_archive_entry(&file, linter->pakfire, e);
	if (r < 0)
		goto ERROR;

	DEBUG(linter->ctx, "Linting %s...\n", pakfire_file_get_path(file));

	// Source Packages
	if (pakfire_package_is_source(linter->pkg)) {
		r = pakfire_linter_lint_source(linter, file);
		if (r < 0)
			goto ERROR;

	// Binary Packages
	} else {
		// Checking against FHS
		r = pakfire_linter_lint_fhs(linter, file);
		if (r < 0)
			 goto ERROR;
	}

	// Lint the payload for regular files
	if (pakfire_file_has_payload(file)) {
		r = pakfire_linter_payload(linter, file, a);
		if (r < 0)
			goto ERROR;
	}

ERROR:
	if (file)
		pakfire_file_unref(file);

	return r;
}

static int pakfire_linter_dump_results(
		struct pakfire_linter* linter, struct pakfire_file* file, const char* prefix) {
	struct pakfire_linter_result* result = NULL;

	TAILQ_FOREACH(result, &linter->results, nodes) {
		// Also process if the file matches
		if (result->file == file) {
			switch (result->priority) {
				case PAKFIRE_LINTER_INFO:
					INFO(linter->ctx, "%s%s\n", prefix, result->comment);
					break;

				case PAKFIRE_LINTER_WARNING:
					WARN(linter->ctx, "%s%s\n", prefix, result->comment);
					break;

				case PAKFIRE_LINTER_ERROR:
					ERROR(linter->ctx, "%s%s\n", prefix, result->comment);
					break;
			}
		}
	}

	return 0;
}

static int __pakfire_linter_dump_file(
		struct pakfire_ctx* ctx, struct pakfire_file* file, void* data) {
	struct pakfire_linter* linter = data;

	// Show information about the file
	char* dump = pakfire_file_dump(file, PAKFIRE_FILE_DUMP_FULL);
	if (!dump)
		return -errno;

	// Show the file information
	if (linter->stats.errors)
		ERROR(linter->ctx, "    %s:\n", dump);
	else if (linter->stats.warnings)
		WARN(linter->ctx, "    %s:\n", dump);
	else
		INFO(linter->ctx, "    %s:\n", dump);

	// Show all results
	return pakfire_linter_dump_results(linter, file, "        ");
}

/*
	Dumps the result of the linter...
*/
static int pakfire_linter_dump(struct pakfire_linter* linter) {
	int r;

	// Fetch nevra
	const char* nevra = pakfire_package_get_string(linter->pkg, PAKFIRE_PKG_NEVRA);

	// Show a simply OK message if there have been no problems
	if (!linter->stats.results) {
		INFO(linter->ctx, "%s: OK\n", nevra);

		return 0;

	// Show an error header if we have errors
	} else if (linter->stats.errors) {
		ERROR(linter->ctx, "%s failed linting:\n", nevra);

	// Show a warning header if we have warnings
	} else if (linter->stats.warnings) {
		WARN(linter->ctx, "%s has warnings:\n", nevra);

	// As a last resort we only show a simple header
	} else {
		INFO(linter->ctx, "%s:\n", nevra);
	}

	// Show everything about the package
	r = pakfire_linter_dump_results(linter, NULL, "    ");
	if (r < 0)
		return r;

	// Show package results
	r = pakfire_filelist_walk(linter->filelist, __pakfire_linter_dump_file, linter, 0, NULL);
	if (r < 0)
		return r;

	// Newline at the end
	INFO(linter->ctx, "\n");

	return 0;
}

int pakfire_linter_lint(struct pakfire_linter* linter) {
	int r;

	// Lint the name
	r = pakfire_linter_name(linter);
	if (r < 0)
		return r;

	// Lint the payload
	r = pakfire_archive_walk_payload(linter->archive, pakfire_linter_file, linter);
	if (r < 0)
		return r;

	// Dump the result
	r = pakfire_linter_dump(linter);
	if (r < 0)
		return r;

	// Return how many errors we had
	return linter->stats.errors;
}
