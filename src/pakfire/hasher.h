/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2025 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_HASHER_H
#define PAKFIRE_HASHER_H

#include <stdio.h>

#include <pakfire/ctx.h>
#include <pakfire/hashes.h>

struct pakfire_hasher;

int pakfire_hasher_create(struct pakfire_hasher** hasher,
	struct pakfire_ctx* ctx, enum pakfire_hash_type types);

struct pakfire_hasher* pakfire_hasher_ref(struct pakfire_hasher* self);
struct pakfire_hasher* pakfire_hasher_unref(struct pakfire_hasher* self);

int pakfire_hasher_update(struct pakfire_hasher* self, const char* buffer, const size_t length);
int pakfire_hasher_finalize(struct pakfire_hasher* self, struct pakfire_hashes* computed_hashes);

int pakfire_hash_buffer(struct pakfire_ctx* ctx, const char* buffer, const size_t length,
	const enum pakfire_hash_type types, struct pakfire_hashes* hashes);
int pakfire_hash_file(struct pakfire_ctx* ctx,
	FILE* f, enum pakfire_hash_type types, struct pakfire_hashes* hashes);
int pakfire_hash_path(struct pakfire_ctx* ctx,
	const char* path, const enum pakfire_hash_type types, struct pakfire_hashes* hashes);

#endif /* PAKFIRE_HASHER_H */
