/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2019 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <fnmatch.h>
#include <linux/limits.h>
#include <regex.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <time.h>
#include <unistd.h>

#define PCRE2_CODE_UNIT_WIDTH 8
#include <pcre2.h>

#include <pakfire/ctx.h>
#include <pakfire/deps.h>
#include <pakfire/env.h>
#include <pakfire/jail.h>
#include <pakfire/logging.h>
#include <pakfire/package.h>
#include <pakfire/parser.h>
#include <pakfire/pakfire.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

// Enable to get more debugging output
//#define PAKFIRE_DEBUG_PARSER

// Regular expressions
static struct pakfire_parser_regexes {
	pcre2_code* variable;
	pcre2_code* command;
} regexes = {};

struct pakfire_parser {
	struct pakfire_ctx* ctx;
	struct pakfire* pakfire;
	int nrefs;

	struct pakfire_parser* parent;
	int flags;

	// Namespace
	char namespace[NAME_MAX];

	struct pakfire_parser_declaration** declarations;
	size_t num_declarations;
};

static void pakfire_parser_free_declaration(struct pakfire_parser_declaration* d) {
	if (d->value)
		free(d->value);
	free(d);
}

static void pakfire_parser_free_declarations(struct pakfire_parser* parser) {
	if (parser->declarations) {
		for (unsigned int i = 0; i < parser->num_declarations; i++)
			pakfire_parser_free_declaration(parser->declarations[i]);

		free(parser->declarations);
		parser->declarations = NULL;
	}
}

static void pakfire_parser_free(struct pakfire_parser* parser) {
	pakfire_parser_free_declarations(parser);

	if (parser->parent)
		pakfire_parser_unref(parser->parent);
	if (parser->pakfire)
		pakfire_unref(parser->pakfire);
	if (parser->ctx)
		pakfire_ctx_unref(parser->ctx);
	free(parser);
}

int pakfire_parser_create(struct pakfire_parser** parser,
		struct pakfire* pakfire, struct pakfire_parser* parent, const char* namespace, int flags) {
	struct pakfire_parser* self = NULL;
	int r;

	// Allocate some memory
	self = calloc(1, sizeof(*self));
	if (!self)
		return -errno;

	// Store a reference to the context
	self->ctx = pakfire_ctx(pakfire);

	// Store a reference to Pakfire
	self->pakfire = pakfire_ref(pakfire);

	// Initialize the reference counter
	self->nrefs = 1;

	// Store a reference to the parent parser if we have one
	if (parent)
		self->parent = pakfire_parser_ref(parent);

	// Store flags
	self->flags = flags;

	// Make namespace
	r = pakfire_parser_set_namespace(self, namespace);
	if (r < 0)
		goto ERROR;

	// Return the pointer
	*parser = self;
	return 0;

ERROR:
	pakfire_parser_free(self);

	return r;
}

struct pakfire_parser* pakfire_parser_create_child(struct pakfire_parser* parser, const char* namespace) {
	struct pakfire_parser* child = NULL;
	int r;

	// Create the parser
	r = pakfire_parser_create(&child, parser->pakfire, parser, namespace, parser->flags);
	if (r < 0)
		return NULL;

	return child;
}

struct pakfire_parser* pakfire_parser_ref(struct pakfire_parser* parser) {
	++parser->nrefs;

	return parser;
}

struct pakfire* pakfire_parser_get_pakfire(struct pakfire_parser* parser) {
	return pakfire_ref(parser->pakfire);
}

struct pakfire_parser* pakfire_parser_unref(struct pakfire_parser* parser) {
	if (--parser->nrefs > 0)
		return parser;

	pakfire_parser_free(parser);
	return NULL;
}

struct pakfire_parser* pakfire_parser_get_parent(struct pakfire_parser* parser) {
	if (parser->parent)
		return pakfire_parser_ref(parser->parent);

	return NULL;
}

static struct pakfire_parser_declaration* pakfire_parser_get_declaration(
		struct pakfire_parser* parser, const char* namespace, const char* name) {
	if (!name) {
		errno = EINVAL;
		return NULL;
	}

	if (!namespace)
		namespace = "";

#ifdef PAKFIRE_DEBUG_PARSER
	if (*namespace)
		DEBUG(parser->ctx, "%p: Looking up %s.%s\n", parser, namespace, name);
	else
		DEBUG(parser->ctx, "%p: Looking up %s\n", parser, name);
#endif /* PAKFIRE_DEBUG_PARSER */

	struct pakfire_parser_declaration* d;
	for (unsigned i = 0; i < parser->num_declarations; i++) {
		d = parser->declarations[i];
		if (!d)
			break;

		// Skip if namespace does not match
		if (!pakfire_string_equals(d->namespace, namespace))
			continue;

		// Skip if name does not match
		else if (!pakfire_string_equals(d->name, name))
			continue;

#ifdef PAKFIRE_DEBUG_PARSER
		DEBUG(parser->ctx, "%p: Found result = %s\n", parser, d->value);
#endif /* PAKFIRE_DEBUG_PARSER */

		return d;
	}

#ifdef PAKFIRE_DEBUG_PARSER
	DEBUG(parser->ctx, "%p: Nothing found\n", parser);
#endif /* PAKFIRE_DEBUG_PARSER */

	return NULL;
}

static struct pakfire_parser_declaration* pakfire_parser_get_declaration_recursive(
		struct pakfire_parser* parser, const char* namespace, const char* name) {
	struct pakfire_parser_declaration* d = pakfire_parser_get_declaration(
			parser, namespace, name);
	if (d)
		return d;

	// Continue searching in parent parser
	if (parser->parent)
		return pakfire_parser_get_declaration_recursive(parser->parent, namespace, name);

	return NULL;
}

static void pakfire_parser_strip_namespace(char* s) {
	char* pos = strrchr(s, '.');

	if (pos)
		*pos = '\0';
	else
		*s = '\0';
}

static char* pakfire_parser_join(const char* c, const char* val1, const char* val2) {
	char* result = NULL;
	int r;

	if (!val1)
		val1 = "";

	if (!val2)
		val2 = "";

	// Reset the delimiter if we don't need it
	if (!*val1 || !*val2)
		c = "";

	// Join both strings
	r = asprintf(&result, "%s%s%s", val1, c, val2);
	if (r < 0)
		return NULL;

	return result;
}

static struct pakfire_parser_declaration* pakfire_parser_find_declaration(
		struct pakfire_parser* parser, const char* namespace, const char* name) {
	struct pakfire_parser_declaration* d = NULL;
	char n[NAME_MAX] = "";
	int r;

	// Create a working copy of namespace
	if (namespace) {
		r = pakfire_string_set(n, namespace);
		if (r < 0)
			return NULL;
	}

	for (;;) {
		d = pakfire_parser_get_declaration_recursive(parser, n, name);
		if (d && d->value)
			return d;

		// End if we have exhausted the namespace
		if (!*n)
			break;

		// Strip namespace
		pakfire_parser_strip_namespace(n);
	}

	// Nothing found
	return NULL;
}

int pakfire_parser_set(struct pakfire_parser* parser,
		const char* namespace, const char* name, const char* value, int flags) {
	struct pakfire_parser_declaration* d = NULL;
	char* buffer = NULL;
	int r;

	// Check inputs
	if (!name)
		return -EINVAL;

	if (!namespace)
		namespace = "";

	// Handle when name already exists
	d = pakfire_parser_get_declaration(parser, namespace, name);
	if (d) {
		// Append?
		if (flags & PAKFIRE_PARSER_DECLARATION_APPEND) {
			buffer = pakfire_parser_join(" ", d->value, value);
			if (!buffer)
				return -errno;

			// Reset the append flag
			flags &= ~PAKFIRE_PARSER_DECLARATION_APPEND;

		// Copy?
		} else {
			buffer = strdup(value);
			if (!buffer)
				return -errno;
		}

		// Replace value
		if (d->value)
			free(d->value);
		d->value = buffer;

		// Update flags
		if (flags)
			d->flags = flags;

#ifdef PAKFIRE_DEBUG_PARSER
		DEBUG(parser->ctx, "%p: Updated declaration: %s.%s = %s\n",
			parser, d->namespace, d->name, d->value);
#endif /* PAKFIRE_DEBUG_PARSER */

		// All done
		return 0;
	}

	// Allocate a new declaration
	d = calloc(1, sizeof(*d));
	if (!d)
		return -errno;

	// Store namespace
	r = pakfire_string_set(d->namespace, namespace);
	if (r < 0)
		goto ERROR;

	// Import name
	r = pakfire_string_set(d->name, name);
	if (r < 0)
		goto ERROR;

	// Import value
	if (value) {
		d->value = strdup(value);
		if (!d->value) {
			r = -errno;
			goto ERROR;
		}
	}

	// Import flags
	d->flags = flags;

#ifdef PAKFIRE_DEBUG_PARSER
	DEBUG(parser->ctx, "%p: New declaration (%u): %s.%s %s= %s\n",
		parser,
		d->flags,
		d->namespace,
		d->name,
		(d->flags & PAKFIRE_PARSER_DECLARATION_APPEND) ? "+" : "",
		d->value);
#endif /* PAKFIRE_DEBUG_PARSER */

	// Grow the array
	parser->declarations = reallocarray(parser->declarations,
			parser->num_declarations + 1, sizeof(*parser->declarations));
	if (!parser->declarations) {
		r = -errno;
		goto ERROR;
	}

	// Store the declaration
	parser->declarations[parser->num_declarations++] = d;

	return 0;

ERROR:
	if (d)
		pakfire_parser_free_declaration(d);

	return r;
}

int pakfire_parser_apply_declaration(struct pakfire_parser* parser,
		struct pakfire_parser_declaration* declaration) {
	return pakfire_parser_set(parser, declaration->namespace,
		declaration->name, declaration->value, declaration->flags);
}

static int pakfire_parser_find_template(struct pakfire_parser* parser,
		char* template, const size_t length, const char* namespace) {
	struct pakfire_parser_declaration* d = NULL;
	const char* value = "MAIN";

	DEBUG(parser->ctx, "Looking up template in namespace '%s'\n", namespace);

	// Fetch the declaration
	d = pakfire_parser_get_declaration(parser, namespace, "template");
	if (d && d->value && *d->value)
		value = d->value;

	// Format full variable name
	return __pakfire_string_format(template, length, "packages.template:%s", value);
}

static const char* pakfire_parser_get_raw(struct pakfire_parser* parser, const char* namespace, const char* name) {
	struct pakfire_parser_declaration* d = NULL;
	char template[NAME_MAX];
	int r;

	// First, perform a simple lookup
	d = pakfire_parser_get_declaration_recursive(parser, namespace, name);

	// Return a match when it actually contains a string
	if (d && d->value)
		return d->value;

	// If we couldn't find anything, we check if there is a template, and if that
	// has our value...
	if (namespace && pakfire_string_startswith(namespace, "packages.package:")) {
		r = pakfire_parser_find_template(parser, template, sizeof(template), namespace);
		if (r)
			return NULL;

		d = pakfire_parser_find_declaration(parser, template, name);
		if (d && d->value)
			return d->value;

		// If still nothing was found, search in the "build" namespace
		d = pakfire_parser_find_declaration(parser, "build", name);
		if (d && d->value)
			return d->value;
	}

	// Otherwise we walk up the namespace to find a match
	d = pakfire_parser_find_declaration(parser, namespace, name);
	if (d && d->value)
		return d->value;

	return NULL;
}

int pakfire_parser_append(struct pakfire_parser* parser,
		const char* namespace, const char* name, const char* value) {
	char* buffer = NULL;

	// Fetch the value of the current declaration
	const char* old_value = pakfire_parser_get_raw(parser, namespace, name);

	DEBUG(parser->ctx, "%p: Old value for %s.%s = %s\n",
		parser, namespace, name, old_value);

	// Set the new value when there is no old one
	if (!old_value)
		return pakfire_parser_set(parser, namespace, name, value, 0);

	// Concat value
	int r = asprintf(&buffer, "%s %s", old_value, value);
	if (r < 0)
		return r;

	// Set the new value
	r = pakfire_parser_set(parser, namespace, name, buffer, 0);
	free(buffer);

	return r;
}

static int pakfire_parser_expand_commands(struct pakfire_parser* parser, char** buffer) {
	pcre2_match_data* match = NULL;
	PCRE2_UCHAR* command = NULL;
	PCRE2_SIZE command_length;
	PCRE2_UCHAR* pattern = NULL;
	PCRE2_SIZE pattern_length;
	int r = 0;

	DEBUG(parser->ctx, "Searching for commands in:\n%s\n", *buffer);

	// Compile the regular expression
	if (!regexes.command) {
		r = pakfire_compile_regex(parser->ctx, &regexes.command, "%(\\(((?>[^()]|(?1))*)\\))");
		if (r < 0) {
			ERROR(parser->ctx, "Failed to compile the command regex: %s\n", strerror(-r));
			goto ERROR;
		}
	}

	// Allocate memory for results
	match = pcre2_match_data_create_from_pattern(regexes.command, NULL);

	// Arguments passed to pakfire_execute
	const char* argv[4] = {
		"/bin/sh", "-c", NULL /* will be replaced by command later */, NULL,
	};

	while (buffer && *buffer) {
		// Perform matching
		r = pcre2_jit_match(regexes.command,
			(PCRE2_UCHAR*)*buffer, strlen(*buffer), 0, 0, match, NULL);

		// End loop when we have expanded all variables
		if (r == PCRE2_ERROR_NOMATCH) {
			DEBUG(parser->ctx, "No (more) matches found\n");
			r = 0;
			break;
		}

		// Extract the command
		r = pcre2_substring_get_bynumber(match, 2, &command, &command_length);
		if (r)
			goto ERROR;

#ifdef PAKFIRE_DEBUG_PARSER
		DEBUG(parser->ctx, "Expanding command: %s\n", command);
#endif /* PAKFIRE_DEBUG_PARSER */

		// Update argv
		argv[2] = (const char*)command;

		// The output of the command
		char* output = NULL;
		size_t length = 0;

		// Execute the command inside the Pakfire environment
		r = pakfire_jail_run(parser->pakfire, argv, NULL, 0, &output, &length);
		if (r) {
			// Just log this and continue
			DEBUG(parser->ctx, "Command '%s' failed with return code %d\n", command, r);
		}

		// Strip newline from output
		if (output)
			pakfire_string_rstrip(output);

		// Find the entire matched pattern
		r = pcre2_substring_get_bynumber(match, 0, &pattern, &pattern_length);
		if (r) {
			if (output)
				free(output);

			goto ERROR;
		}

		// Replace all occurrences
		char* tmp = pakfire_string_replace(*buffer, (const char*)pattern, output);
		if (!tmp) {
			if (output)
				free(output);

			goto ERROR;
		}

		// Replace buffer
		free(*buffer);
		*buffer = tmp;

		// Free resources
		pcre2_substring_free(command);
		command = NULL;

		pcre2_substring_free(pattern);
		pattern = NULL;

		if (output)
			free(output);
	}

ERROR:
	if (match)
		pcre2_match_data_free(match);
	if (command)
		pcre2_substring_free(command);
	if (pattern)
		pcre2_substring_free(pattern);

	return r;
}

static int pakfire_parser_expand_variables(struct pakfire_parser* parser,
		const char* namespace, char** buffer) {
	char parent_namespace[NAME_MAX];
	pcre2_match_data* match = NULL;
	PCRE2_UCHAR* variable = NULL;
	PCRE2_SIZE variable_length = 0;
	PCRE2_UCHAR* pattern = NULL;
	PCRE2_SIZE pattern_length = 0;
	int r = 0;

	// Do not work on an empty buffer
	if (!buffer)
		return -EINVAL;

	// Compile the regular expression
	if (!regexes.variable) {
		r = pakfire_compile_regex(parser->ctx, &regexes.variable, "%\\{([A-Za-z0-9_\\-]+)\\}");
		if (r < 0) {
			ERROR(parser->ctx, "Failed to compile the variable regex: %s\n", strerror(-r));
			goto ERROR;
		}
	}

	// Allocate memory for results
	match = pcre2_match_data_create_from_pattern(regexes.variable, NULL);

	// Search for any variables
	while (buffer && *buffer) {
		// Perform matching
		r = pcre2_jit_match(regexes.variable,
			(PCRE2_UCHAR*)*buffer, strlen(*buffer), 0, 0, match, NULL);

		// End loop when we have expanded all variables
		if (r == PCRE2_ERROR_NOMATCH) {
			DEBUG(parser->ctx, "No (more) matches found in: %s\n", *buffer);
			r = 0;
			break;
		}

		// Find the entire matched pattern
		r = pcre2_substring_get_bynumber(match, 0, &pattern, &pattern_length);
		if (r)
			goto ERROR;

		// Find the variable name
		r = pcre2_substring_get_bynumber(match, 1, &variable, &variable_length);
		if (r)
			goto ERROR;

#ifdef PAKFIRE_DEBUG_PARSER
		DEBUG(parser->ctx, "Expanding variable: %s\n", variable);
#endif /* PAKFIRE_DEBUG_PARSER */

		// Search for a declaration of this variable
		const char* repl = pakfire_parser_get_raw(parser, namespace, (const char*)variable);

		// Is this a recursive pattern?
		if (repl && pakfire_string_matches(repl, (const char*)pattern)) {
			DEBUG(parser->ctx, "Recursion detected in %s\n", pattern);

			// Move up one step and lookup there
			if (namespace && *namespace) {
				r = pakfire_string_set(parent_namespace, namespace);
				if (r < 0)
					goto ERROR;

				// Strip
				pakfire_parser_strip_namespace(parent_namespace);

				repl = pakfire_parser_get_raw(parser, parent_namespace, (const char*)variable);

			// If we have already reached the top namespace, we replace with an empty string
			} else {
				repl = NULL;
			}
		}

#ifdef PAKFIRE_DEBUG_PARSER
		// What is its value?
		if (repl) {
			DEBUG(parser->ctx, "Replacing %%{%s} with '%s'\n", variable, repl);
		} else {
			DEBUG(parser->ctx, "Replacing %%{%s} with an empty string\n", variable);
		}
#endif /* PAKFIRE_DEBUG_PARSER */

		// Replace all occurrences
		char* tmp = pakfire_string_replace(*buffer, (const char*)pattern, repl);
		if (!tmp) {
			r = -errno;
			goto ERROR;
		}

		// Replace buffer
		free(*buffer);
		*buffer = tmp;

		// Free resources
		if (variable) {
			pcre2_substring_free(variable);
			variable = NULL;
		}
		if (pattern) {
			pcre2_substring_free(pattern);
			pattern = NULL;
		}
	}

ERROR:
	if (match)
		pcre2_match_data_free(match);
	if (variable)
		pcre2_substring_free(variable);
	if (pattern)
		pcre2_substring_free(pattern);

	return r;
}

char* pakfire_parser_expand(struct pakfire_parser* parser,
		const char* namespace, const char* value) {
	char* buffer = NULL;
	int r;

	// Return NULL when the value is NULL
	if (!value)
		return NULL;

	// Create a working copy of the string we are expanding
	buffer = strdup(value);
	if (!buffer)
		goto ERROR;

	// There is nothing to do for empty strings
	if (!*buffer)
		return buffer;

	// Fast path to check if there are any variables in here whatsoever
	char* pos = strchr(buffer, '%');
	if (!pos)
		return buffer;

	// Expand all variables
	r = pakfire_parser_expand_variables(parser, namespace, &buffer);
	if (r < 0) {
		DEBUG(parser->ctx, "Failed to expand variables in '%s': %s\n", value, strerror(-r));
		goto ERROR;
	}

	// Expand all commands
	if (parser->flags & PAKFIRE_PARSER_FLAGS_EXPAND_COMMANDS) {
		r = pakfire_parser_expand_commands(parser, &buffer);
		if (r < 0) {
			DEBUG(parser->ctx, "Failed to expand commands in '%s': %s\n", value, strerror(-r));
			goto ERROR;
		}
	}

	return buffer;

ERROR:
	if (buffer)
		free(buffer);

	return NULL;
}

char* pakfire_parser_get(struct pakfire_parser* parser, const char* namespace, const char* name) {
	const char* value = pakfire_parser_get_raw(parser, namespace, name);

	// Return NULL when nothing was found
	if (!value)
		return NULL;

	// Otherwise return the expanded value
	return pakfire_parser_expand(parser, namespace, value);
}

int pakfire_parser_get_filelist(struct pakfire_parser* parser, const char* namespace,
		const char* name, char*** includes, char*** excludes) {
	const char* file = NULL;
	char* p = NULL;
	int r = 0;

	// Fetch the list
	char* list = pakfire_parser_get(parser, namespace, name);

	// Nothing to do for empty lists
	if (!list)
		goto ERROR;

	// Split the string
	file = strtok_r(list, " \n", &p);

	// Split into includes and excludes
	while (file) {
		// Excludes
		if (excludes && *file == '!') {
			r = pakfire_strings_append(excludes, file + 1);
			if (r < 0)
				goto ERROR;

		// Includes
		} else if (includes) {
			r = pakfire_strings_append(includes, file);
			if (r < 0)
				goto ERROR;
		}

		// Move on to the next token
		file = strtok_r(NULL, " \n", &p);
	}

ERROR:
	if (list)
		free(list);

	return r;
}

char** pakfire_parser_list_namespaces(struct pakfire_parser* parser, const char* filter) {
	struct pakfire_parser_declaration* d = NULL;
	char** namespaces = NULL;
	int r;

	for (unsigned int i = 0; i < parser->num_declarations; i++) {
		d = parser->declarations[i];

		// Filter out anything
		if (filter) {
			r = fnmatch(filter, d->namespace, 0);
			if (r == FNM_NOMATCH)
				continue;

			// Check for any errors
			else if (r) {
				ERROR(parser->ctx, "fnmatch failed: %m\n");
				goto ERROR;
			}
		}

		// Skip if already on the list
		if (namespaces && pakfire_strings_contain(namespaces, d->namespace))
			continue;

		// Append to the list
		r = pakfire_strings_append(&namespaces, d->namespace);
		if (r < 0)
			goto ERROR;
	}

	return namespaces;

ERROR:
	if (namespaces)
		pakfire_strings_free(namespaces);

	return NULL;
}

int pakfire_parser_merge(struct pakfire_parser* parser1, struct pakfire_parser* parser2) {
	struct pakfire_parser_declaration* d = NULL;
	char* namespace = NULL;
	char* value = NULL;
	int r = 0;

	if (!parser1 || !parser2)
		return -EINVAL;

	// Do not try to merge a parser with itself
	if (parser1 == parser2)
		return -ENOTSUP;

	for (unsigned int i = 0; i < parser2->num_declarations; i++) {
		d = parser2->declarations[i];
		if (!d)
			break;

		// Make the new namespace
		namespace = pakfire_parser_join(".", parser2->namespace, d->namespace);
		if (!namespace) {
			r = -errno;
			goto OUT;
		}

		const char* old_value = NULL;

		// Fetch the old value if we are supposed to be appending
		if (d->flags & PAKFIRE_PARSER_DECLARATION_APPEND) {
			old_value = pakfire_parser_get_raw(parser1, namespace, d->name);

			// XXX This is not ideal, to only reset once we have found an old
			// value because we might carry this over too far.
			// However, this is the only way to fix #12997
			if (old_value)
				d->flags &= ~PAKFIRE_PARSER_DECLARATION_APPEND;
		}

		// Make the new value
		value = pakfire_parser_join(" ", old_value, d->value);
		if (!value) {
			r = -errno;
			goto OUT;
		}

		// Set everything in parser 1
		r = pakfire_parser_set(parser1, namespace, d->name, value, d->flags);
		if (r < 0)
			goto OUT;

OUT:
		if (namespace) {
			free(namespace);
			namespace = NULL;
		}
		if (value) {
			free(value);
			value = NULL;
		}

		if (r)
			break;
	}

	return r;
}

int pakfire_parser_read(struct pakfire_parser* parser, FILE* f,
		struct pakfire_parser_error** error) {
	char* data = NULL;
	size_t length = 0;
	int fd = -EBADF;
	int r;

	// Fetch the file descriptor
	fd = fileno(f);
	if (fd < 0)
		return fd;

	// Map the file
	r = pakfire_mmap(fd, &data, &length);
	if (r < 0) {
		ERROR(parser->ctx, "Could not map parser data into memory: %s\n", strerror(-r));
		return r;
	}

	// Run the parser
	r = pakfire_parser_parse_data(parser, data, length, error);
	if (r < 0)
		goto ERROR;

ERROR:
	if (data)
		munmap(data, length);

	return r;
}

int pakfire_parser_read_file(struct pakfire_parser* parser, const char* path,
		struct pakfire_parser_error** error) {
	FILE* f = NULL;
	int r;

	DEBUG(parser->ctx, "Parsing %s...\n", path);

	// Open the file
	f = fopen(path, "r");
	if (!f)
		return -errno;

	// Run the parser
	r = pakfire_parser_read(parser, f, error);

	// Close the file
	if (f)
		fclose(f);

	return r;
}

int pakfire_parser_parse(struct pakfire_parser* parser,
		const char* data, size_t size, struct pakfire_parser_error** error) {
	return pakfire_parser_parse_data(parser, data, size, error);
}

char* pakfire_parser_dump(struct pakfire_parser* parser) {
	char buffer[NAME_MAX*2 + 1];
	char* s = NULL;
	int r;

	for (unsigned int i = 0; i < parser->num_declarations; i++) {
		struct pakfire_parser_declaration* d = parser->declarations[i];

		if (d) {
			if (*d->namespace)
				pakfire_string_format(buffer, "%s.%s", d->namespace, d->name);
			else
				pakfire_string_set(buffer, d->name);

			r = asprintf(&s, "%s%-24s = %s\n", (s) ? s : "", buffer, d->value);
			if (r < 0)
				return NULL;
		}
	}

	return s;
}

const char* pakfire_parser_get_namespace(struct pakfire_parser* parser) {
	return parser->namespace;
}

int pakfire_parser_set_namespace(struct pakfire_parser* parser, const char* namespace) {
	return pakfire_string_set(parser->namespace, namespace);
}

int pakfire_parser_set_env(struct pakfire_parser* parser, struct pakfire_env* env) {
	struct pakfire_parser_declaration* d = NULL;
	char* value = NULL;
	int r;

	for (unsigned int i = 0; i < parser->num_declarations; i++) {
		d = parser->declarations[i];
		if (!d)
			continue;

		// Is the export flag set?
		if (d->flags & PAKFIRE_PARSER_DECLARATION_EXPORT) {
			value = pakfire_parser_expand(parser, d->namespace, d->value);
			if (!value)
				continue;

			// Store the value
			r = pakfire_env_set(env, d->name, "%s", value);
			free(value);
			if (r < 0)
				return r;
		}
	}

	return 0;
}

int pakfire_parser_create_package(struct pakfire_parser* parser,
		struct pakfire_package** pkg, struct pakfire_repo* repo, const char* namespace, const char* default_arch) {
	int r = 1;

	char* name = NULL;
	char* evr = NULL;
	char* arch = NULL;
	char* deps = NULL;
	char* build_arches = NULL;

	DEBUG(parser->ctx, "Building package from namespace '%s'\n", namespace);

	// Fetch name
	name = pakfire_parser_get(parser, namespace, "name");
	if (!name || !*name) {
		ERROR(parser->ctx, "Name is empty\n");
		goto CLEANUP;
	}

	// Fetch EVR
	evr = pakfire_parser_get(parser, namespace, "evr");
	if (!evr || !*evr) {
		ERROR(parser->ctx, "EVR is empty\n");
		goto CLEANUP;
	}

	// Fetch arch
	arch = pakfire_parser_get(parser, namespace, "arch");
	if (!arch || !*arch) {
		if (!default_arch) {
			ERROR(parser->ctx, "Arch is empty\n");
			goto CLEANUP;
		}
	}

	// Create a new package object
	r = pakfire_package_create(pkg, parser->pakfire, repo, name, evr, (arch) ? arch : default_arch);
	if (r < 0) {
		ERROR(parser->ctx, "Could not create package: %m\n");
		goto CLEANUP;
	}

	// Is this a source package?
	int is_source = pakfire_package_is_source(*pkg);

	// Assign a new UUID to this package
	char* uuid = pakfire_generate_uuid();
	if (!uuid) {
		ERROR(parser->ctx, "Generating a UUID failed: %m\n");
		goto CLEANUP;
	}

	pakfire_package_set_string(*pkg, PAKFIRE_PKG_UUID, uuid);
	free(uuid);

	// Set build time
	time_t now = time(NULL);

	pakfire_package_set_num(*pkg, PAKFIRE_PKG_BUILD_TIME, now);

	// Assign more attributes
	const struct attribute {
		const char* name;
		const enum pakfire_package_key key;
	} attributes[] = {
		{ "summary", PAKFIRE_PKG_SUMMARY },
		{ "description", PAKFIRE_PKG_DESCRIPTION, },
		{ "license", PAKFIRE_PKG_LICENSE, },
		{ "url", PAKFIRE_PKG_URL, },
		{ "groups", PAKFIRE_PKG_GROUPS, },
		{ "vendor", PAKFIRE_PKG_VENDOR, },
		{ "packager", PAKFIRE_PKG_PACKAGER, },
		{ NULL },
	};

	for (const struct attribute* a = attributes; a->name; a++) {
		char* value = pakfire_parser_get(parser, namespace, a->name);
		if (!value)
			continue;

		r = pakfire_package_set_string(*pkg, a->key, value);
		free(value);

		if (r) {
			ERROR(parser->ctx, "Could not set %s: %m\n", a->name);
			goto CLEANUP;
		}
	}

	// Fetch build dependencies
	if (is_source) {
		deps = pakfire_parser_get(parser, "build", "requires");
		if (deps) {
			r = pakfire_str2deps(parser->pakfire, *pkg, PAKFIRE_PKG_REQUIRES, deps);
			if (r)
				goto CLEANUP;
		}
	} else {
		for (const struct pakfire_dep* dep = pakfire_deps; dep->key; dep++) {
			deps = pakfire_parser_get(parser, namespace, dep->name);
			if (deps) {
				r = pakfire_str2deps(parser->pakfire, *pkg, dep->key, deps);
				if (r)
					goto CLEANUP;
			}
		}
	}

	// Add supported architectures
	if (is_source) {
		build_arches = pakfire_parser_get(parser, "build", "arches");
		if (build_arches) {
			r = pakfire_package_set_strings_from_string(*pkg, PAKFIRE_PKG_BUILD_ARCHES, build_arches);
			if (r)
				goto CLEANUP;
		}
	}

	// All okay
	r = 0;

CLEANUP:
	if (r)
		ERROR(parser->ctx, "Could not create package: %m\n");

	if (name)
		free(name);
	if (evr)
		free(evr);
	if (arch)
		free(arch);
	if (deps)
		free(deps);
	if (build_arches)
		free(build_arches);

	return r;
}

// Error

struct pakfire_parser_error {
	struct pakfire_parser* parser;
	int nrefs;

	char* filename;
	int line;
	char* message;
};

int pakfire_parser_error_create(struct pakfire_parser_error** error,
		struct pakfire_parser* parser, const char* filename, int line, const char* message) {
	struct pakfire_parser_error* e = calloc(1, sizeof(*e));
	if (!e)
		return ENOMEM;

	// Initialize reference counter
	e->nrefs = 1;

	e->parser = pakfire_parser_ref(parser);

	// Copy all input values
	if (filename)
		e->filename = strdup(filename);

	e->line = line;

	if (message)
		e->message = strdup(message);

	*error = e;

	return 0;
}

struct pakfire_parser_error* pakfire_parser_error_ref(
		struct pakfire_parser_error* error) {
	++error->nrefs;

	return error;
}

static void pakfire_parser_error_free(struct pakfire_parser_error* error) {
	if (error->parser)
		pakfire_parser_unref(error->parser);
	if (error->filename)
		free(error->filename);
	if (error->message)
		free(error->message);
	free(error);
}

struct pakfire_parser_error* pakfire_parser_error_unref(
		struct pakfire_parser_error* error) {
	if (--error->nrefs > 0)
		return error;

	pakfire_parser_error_free(error);
	return NULL;
}

const char* pakfire_parser_error_get_filename(
		struct pakfire_parser_error* error) {
	return error->filename;
}

int pakfire_parser_error_get_line(struct pakfire_parser_error* error) {
	return error->line;
}

const char* pakfire_parser_error_get_message(
		struct pakfire_parser_error* error) {
	return error->message;
}
