/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2024 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_STRIPPER_H
#define PAKFIRE_STRIPPER_H

#define BUILD_SRC_DIR "/build/source"
#define DEBUG_SRC_DIR "/usr/src/debug"

struct pakfire_stripper;

#include <pakfire/jail.h>
#include <pakfire/pakfire.h>

int pakfire_stripper_create(struct pakfire_stripper** stripper,
	struct pakfire* pakfire, struct pakfire_jail* jail, const char* path);

struct pakfire_stripper* pakfire_stripper_ref(struct pakfire_stripper* self);
struct pakfire_stripper* pakfire_stripper_unref(struct pakfire_stripper* self);

int pakfire_stripper_run(struct pakfire_stripper* self);

#endif /* PAKFIRE_STRIPPER_H */
