/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2014 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_FILE_H
#define PAKFIRE_FILE_H

#include <sys/capability.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

// libarchive
#include <archive.h>
#include <archive_entry.h>

struct pakfire_file;

#include <pakfire/hashes.h>
#include <pakfire/pakfire.h>

enum pakfire_file_flags {
	PAKFIRE_FILE_CONFIG               = (1 << 0),
};

int pakfire_file_create(struct pakfire_file** file, struct pakfire* pakfire, const char* path);

struct pakfire_file* pakfire_file_ref(struct pakfire_file* file);
struct pakfire_file* pakfire_file_unref(struct pakfire_file* file);

int pakfire_file_has_flag(struct pakfire_file* file, int flag);
int pakfire_file_set_flags(struct pakfire_file* file, int flag);

int pakfire_file_cmp(struct pakfire_file* file1, struct pakfire_file* file2);

const char* pakfire_file_get_path(struct pakfire_file* file);
int pakfire_file_set_path(struct pakfire_file* file, const char* path);

const char* pakfire_file_get_hardlink(struct pakfire_file* file);
void pakfire_file_set_hardlink(struct pakfire_file* file, const char* link);
const char* pakfire_file_get_symlink(struct pakfire_file* file);
void pakfire_file_set_symlink(struct pakfire_file* file, const char* link);

nlink_t pakfire_file_get_nlink(struct pakfire_file* file);
void pakfire_file_set_nlink(struct pakfire_file* file, const nlink_t nlink);

ino_t pakfire_file_get_inode(struct pakfire_file* file);
void pakfire_file_set_inode(struct pakfire_file* file, const ino_t ino);

dev_t pakfire_file_get_dev(struct pakfire_file* file);
void pakfire_file_set_dev(struct pakfire_file* file, const dev_t dev);

int pakfire_file_get_type(struct pakfire_file* file);

ssize_t pakfire_file_get_size(struct pakfire_file* file);
void pakfire_file_set_size(struct pakfire_file* file, ssize_t size);

const char* pakfire_file_get_uname(struct pakfire_file* file);
int pakfire_file_set_uname(struct pakfire_file* file, const char* uname);

const char* pakfire_file_get_gname(struct pakfire_file* file);
int pakfire_file_set_gname(struct pakfire_file* file, const char* gname);

mode_t pakfire_file_get_mode(struct pakfire_file* file);
void pakfire_file_set_mode(struct pakfire_file* file, mode_t mode);

mode_t pakfire_file_get_perms(struct pakfire_file* file);
void pakfire_file_set_perms(struct pakfire_file* file, const mode_t perms);

time_t pakfire_file_get_ctime(struct pakfire_file* file);
void pakfire_file_set_ctime(struct pakfire_file* file, time_t time);
time_t pakfire_file_get_mtime(struct pakfire_file* file);
void pakfire_file_set_mtime(struct pakfire_file* file, time_t time);

// Checksums
int pakfire_file_get_checksum(struct pakfire_file* file, const enum pakfire_hash_type type,
	const unsigned char** checksum, size_t* checksum_length);
int pakfire_file_set_checksum(struct pakfire_file* file, const enum pakfire_hash_type type,
	const unsigned char* checksum, const size_t checksum_length);

// Capabilities
int pakfire_file_has_caps(struct pakfire_file* file);
char* pakfire_file_get_caps(struct pakfire_file* file);

// MIME Type
const char* pakfire_file_get_mimetype(struct pakfire_file* file);
int pakfire_file_set_mimetype(struct pakfire_file* file, const char* mimetype);

struct pakfire_file* pakfire_file_parse_from_file(const char* list, unsigned int format);

int pakfire_file_matches(struct pakfire_file* file, const char* pattern);

enum pakfire_file_classes {
	PAKFIRE_FILE_UNKNOWN         = 0,

	// Simple types
	PAKFIRE_FILE_REGULAR         = (1 << 0),
	PAKFIRE_FILE_DIRECTORY       = (1 << 1),
	PAKFIRE_FILE_SYMLINK         = (1 << 2),
	PAKFIRE_FILE_CHARACTER       = (1 << 3),
	PAKFIRE_FILE_BLOCK           = (1 << 4),
	PAKFIRE_FILE_FIFO            = (1 << 5),
	PAKFIRE_FILE_SOCKET          = (1 << 6),

	// Is the file executable?
	PAKFIRE_FILE_EXECUTABLE      = (1 << 7),

	// The rest
	PAKFIRE_FILE_ELF             = (1 << 8),
	PAKFIRE_FILE_PERL            = (1 << 10),
};

int pakfire_file_is_executable(struct pakfire_file* file);

int pakfire_file_has_payload(struct pakfire_file* file);

int pakfire_file_write_fcaps(struct pakfire_file* file, struct vfs_cap_data* cap_data);

int pakfire_file_create_from_archive_entry(struct pakfire_file** file, struct pakfire* pakfire,
	struct archive_entry* entry);
struct archive_entry* pakfire_file_archive_entry(struct pakfire_file* file, const enum pakfire_hash_type hashes);

enum pakfire_file_dump_flags {
	PAKFIRE_FILE_DUMP_MODE         = (1 << 0),
	PAKFIRE_FILE_DUMP_SIZE         = (1 << 1),
	PAKFIRE_FILE_DUMP_TIME         = (1 << 2),
	PAKFIRE_FILE_DUMP_OWNERSHIP    = (1 << 3),
	PAKFIRE_FILE_DUMP_LINK_TARGETS = (1 << 4),

	PAKFIRE_FILE_DUMP_FULL = \
		PAKFIRE_FILE_DUMP_MODE | \
		PAKFIRE_FILE_DUMP_SIZE | \
		PAKFIRE_FILE_DUMP_TIME | \
		PAKFIRE_FILE_DUMP_OWNERSHIP | \
		PAKFIRE_FILE_DUMP_LINK_TARGETS,
};

char* pakfire_file_dump(struct pakfire_file* file, int flags);

const char* pakfire_file_get_abspath(struct pakfire_file* file);

int pakfire_file_open(struct pakfire_file* file, int flags);
FILE* pakfire_file_fopen(struct pakfire_file* file, const char* mode);

int pakfire_file_contains(struct pakfire_file* file, const char* needle, ssize_t length);

enum pakfire_file_cleanup_flags {
	PAKFIRE_FILE_CLEANUP_TIDY = (1 << 0),
};
int pakfire_file_cleanup(struct pakfire_file* file, int flags);

int pakfire_file_symlink_target_exists(struct pakfire_file* file);

// MIME Type
int pakfire_file_detect_mimetype(struct pakfire_file* file);

int pakfire_file_classify(struct pakfire_file* file);
int pakfire_file_matches_class(struct pakfire_file* file, const int class);

int pakfire_file_verify(struct pakfire_file* file, int* status);

int pakfire_file_consume(struct pakfire_file* file, int srcfd);

int pakfire_file_fix_interpreter(struct pakfire_file* file);

#endif /* PAKFIRE_FILE_H */
