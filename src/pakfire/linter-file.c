/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2024 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <stdlib.h>
#include <sys/mman.h>

// libelf
#include <gelf.h>

#include <pakfire/ctx.h>
#include <pakfire/elf.h>
#include <pakfire/file.h>
#include <pakfire/linter.h>
#include <pakfire/linter-file.h>
#include <pakfire/path.h>
#include <pakfire/string.h>

struct pakfire_linter_file {
	struct pakfire_ctx* ctx;
	int nrefs;

	// Linter
	struct pakfire_linter* linter;

	// File
	struct pakfire_file* file;

	// ELF Object
	struct pakfire_elf* elf;

	// File Descriptor
	int fd;

	// The length of the mapped file
	size_t length;

	// Mapped Data
	char* data;

	// Path
	const char* path;
};

#define pakfire_linter_file_info(lfile, format, ...) \
	pakfire_linter_result(lfile->linter, lfile->file, PAKFIRE_LINTER_INFO, format, ## __VA_ARGS__)
#define pakfire_linter_file_warning(lfile, format, ...) \
	pakfire_linter_result(lfile->linter, lfile->file, PAKFIRE_LINTER_WARNING, format, ## __VA_ARGS__)
#define pakfire_linter_file_error(lfile, format, ...) \
	pakfire_linter_result(lfile->linter, lfile->file, PAKFIRE_LINTER_ERROR, format, ## __VA_ARGS__)

/*
	Maps the file into memory
*/
static int pakfire_linter_file_map(struct pakfire_linter_file* lfile) {
	// Store the length
	lfile->length = lseek(lfile->fd, 0, SEEK_END);
	if (lfile->length <= 0)
		return -errno;

	// Map the data
	lfile->data = mmap(NULL, lfile->length, PROT_READ, MAP_PRIVATE, lfile->fd, 0);
	if (lfile->data == MAP_FAILED)
		return -errno;

	return 0;
}

static void pakfire_linter_file_free(struct pakfire_linter_file* lfile) {
	int r;

	if (lfile->elf)
		pakfire_elf_unref(lfile->elf);

	if (lfile->data) {
		r = munmap(lfile->data, lfile->length);
		if (r < 0)
			ERROR(lfile->ctx, "Could not unmmap %s: %m\n", lfile->path);
	}

	if (lfile->fd >= 0)
		close(lfile->fd);

	if (lfile->linter)
		pakfire_linter_unref(lfile->linter);
	if (lfile->file)
		pakfire_file_unref(lfile->file);
	if (lfile->ctx)
		pakfire_ctx_unref(lfile->ctx);
	free(lfile);
}

int pakfire_linter_file_create(struct pakfire_linter_file** lfile,
		struct pakfire_ctx* ctx, struct pakfire_linter* linter, struct pakfire_file* file, int fd) {
	struct pakfire_linter_file* l = NULL;
	int r = 0;

	// Check input
	if (fd < 0)
		return -EBADF;

	// Allocate some memory
	l = calloc(1, sizeof(*l));
	if (!l)
		return -errno;

	// Store a reference to the context
	l->ctx = pakfire_ctx_ref(ctx);

	// Initialize the reference counter
	l->nrefs = 1;

	// Store a reference to the linter
	l->linter = pakfire_linter_ref(linter);

	// Store a reference to the file
	l->file = pakfire_file_ref(file);

	// Cache path
	l->path = pakfire_file_get_path(l->file);
	if (!l->path) {
		r = -EINVAL;
		goto ERROR;
	}

	// Store the file descriptor
	l->fd = dup(fd);
	if (l->fd < 0) {
		ERROR(l->ctx, "Could not duplicate file descriptor: %m\n");
		r = -errno;
		goto ERROR;
	}

	// Map the file
	r = pakfire_linter_file_map(l);
	if (r < 0) {
		ERROR(l->ctx, "Could not map the file: %s\n", strerror(-r));
		goto ERROR;
	}

	// Open an ELF object
	r = pakfire_elf_open(&l->elf, l->ctx, l->path, l->fd);
	if (r < 0) {
		switch (-r) {
			// This does not seem to be an ELF file
			case ENOTSUP:
				break;

			// Something else happened
			default:
				goto ERROR;
		}
	}

	// Return the pointer
	*lfile = l;
	return 0;

ERROR:
	pakfire_linter_file_free(l);

	return r;
}


struct pakfire_linter_file* pakfire_linter_file_ref(struct pakfire_linter_file* lfile) {
	++lfile->nrefs;

	return lfile;
}

struct pakfire_linter_file* pakfire_linter_file_unref(struct pakfire_linter_file* lfile) {
	if (--lfile->nrefs > 0)
		return lfile;

	pakfire_linter_file_free(lfile);
	return NULL;
}

static int pakfire_linter_file_check_caps(struct pakfire_linter_file* lfile) {
	// Files cannot have capabilities but not be executable
	if (!pakfire_file_is_executable(lfile->file) && pakfire_file_has_caps(lfile->file))
		return pakfire_linter_file_error(lfile, "File has capabilities but is not executable");

	return 0;
}

#define pakfire_linter_file_get_script_interpreter(lfile, interpreter) \
	__pakfire_linter_file_get_script_interpreter(lfile, interpreter, sizeof(interpreter))

static int __pakfire_linter_file_get_script_interpreter(struct pakfire_linter_file* lfile,
		char* interpreter, size_t length) {
	char shebang[PATH_MAX];
	char* eol = NULL;
	char* p = NULL;
	int r;

	// Check inputs
	if (!interpreter)
		return -EINVAL;

	// Only run for executable files
	if (!pakfire_file_is_executable(lfile->file))
		return 0;

	// If the file is shorter than four bytes there is nothing to do here
	if (lfile->length <= 4)
		return 0;

	// The file must start with #!
	if (lfile->data[0] == '#' && lfile->data[1] == '!') {
		// Scan for the end of the first line
		eol = memchr(lfile->data, '\n', lfile->length);
		if (!eol) {
			ERROR(lfile->ctx, "Could not find end-of-line in %s: %m\n", lfile->path);
			return -errno;
		}

		int l = eol - lfile->data;

		// Copy the line into a buffer
		r = pakfire_string_format(shebang, "%.*s", l, lfile->data);
		if (r < 0)
			return r;

		// Remove #!
		memmove(shebang, shebang + 2, l);
		l -= 2;

		// Set p to the beginning of the buffer
		p = shebang;

		// Consume any space between #! and the path
		while (*p && isspace(*p))
			memmove(shebang, shebang + 1, --l);

		// Find the end of the command (cuts off any options)
		while (*p) {
			if (isspace(*p)) {
				*p = '\0';
				break;
			}

			p++;
		}

		// Copy whatever is left to the output buffer
		r = __pakfire_string_set(interpreter, length, shebang);
		if (r < 0)
			return r;

		// Return non-zero if we found something
		return 1;
	}

	return 0;
}

static int pakfire_linter_check_script_interpreter(struct pakfire_linter_file* lfile) {
	char interpreter[PATH_MAX];
	int r;

	// Don't run this if we already know that this is an ELF file
	if (lfile->elf)
		return 0;

	// Fetch the interpreter
	r = pakfire_linter_file_get_script_interpreter(lfile, interpreter);
	if (r <= 0)
		return r;

	// Check if the interpreter is absolute
	if (!pakfire_string_startswith(interpreter, "/")) {
		r = pakfire_linter_file_error(lfile,
			"Interpreter must be absolute (is '%s')", interpreter);
		if (r < 0)
			return r;

	// Interpreter cannot be in /usr/local
	} else if (pakfire_path_match(interpreter, "/usr/local/**")) {
		r = pakfire_linter_file_error(lfile, "Interpreter cannot be in /usr/local");
		if (r < 0)
			return r;

	// /usr/bin/env is not allowed
	} else if (strcmp(interpreter, "/usr/bin/env") == 0) {
		r = pakfire_linter_file_error(lfile, "Interpreter cannot be /usr/bin/env");
		if (r < 0)
			return r;
	}

	// XXX Check if the interpreter is provides by something

	return 0;
}

static int pakfire_linter_file_check_pie(struct pakfire_linter_file* lfile) {
	if (!pakfire_elf_is_pie(lfile->elf))
		return pakfire_linter_file_error(lfile, "Missing PIE");

	return 0;
}

static int pakfire_linter_file_check_ssp(struct pakfire_linter_file* lfile) {
	// This check will be skipped for these files
	static const char* whitelist[] = {
		// Runtime Linker
		"/usr/lib*/ld-*.so*",

		// GCC
		"/usr/lib64/libgcc_s.so.*",
		"/usr/lib64/libmvec.so.*",

		// valgrind
		"/usr/bin/vgdb",
		"/usr/bin/valgrind",
		"/usr/bin/valgrind-*",
		NULL,
	};

	// Check if this file is whitelisted
	for (const char** path = whitelist; *path; path++) {
		if (pakfire_file_matches(lfile->file, *path)) {
			DEBUG(lfile->ctx, "Skipping SSP check for whitelisted file %s\n", lfile->path);
			return 0;
		}
	}

	// Report an error if there is not SSP
	if (!pakfire_elf_has_ssp(lfile->elf))
		return pakfire_linter_file_error(lfile, "Missing Stack Smashing Protection");

	return 0;
}

static int pakfire_linter_file_check_execstack(struct pakfire_linter_file* lfile) {
	if (pakfire_elf_has_execstack(lfile->elf))
		return pakfire_linter_file_error(lfile, "Executable Stack");

	return 0;
}

static int pakfire_linter_file_check_relro(struct pakfire_linter_file* lfile) {
	// If the file is fully RELRO, everything is good
	if (pakfire_elf_is_fully_relro(lfile->elf))
		return 0;

	// Show a warning if the file is only partially RELRO
	else if (pakfire_elf_is_partially_relro(lfile->elf))
		return pakfire_linter_file_warning(lfile, "Is partially RELRO");

	// Return an error if this file is not RELRO at all
	return pakfire_linter_file_error(lfile, "Is not RELRO");
}

static int pakfire_linter_file_check_runpath(struct pakfire_linter_file* lfile) {
	char** runpaths = NULL;
	int r;

	// Fetch any runpaths
	r = pakfire_elf_has_runpaths(lfile->elf, &runpaths);
	if (r < 0)
		goto ERROR;

	// Report any invalid runpaths
	if (runpaths) {
		for (char** runpath = runpaths; *runpath; runpath++) {
			DEBUG(lfile->ctx, "Checking RUNPATH %s\n", *runpath);

			// We allow /usr/lib64 as libtool seems to link it in quite a lot
			if (pakfire_path_match("/usr/lib64", *runpath))
				continue;

			// We allow any subdirectories of /usr/lib64
			if (pakfire_path_match( "/usr/lib64/**", *runpath))
				continue;

			// This seems to be an illegal runpath
			r = pakfire_linter_file_error(lfile, "Has illegal RPATH/RUNPATH %s", *runpath);
			if (r < 0)
				goto ERROR;
		}
	}

ERROR:
	if (runpaths)
		pakfire_strings_free(runpaths);

	return r;
}

static int pakfire_linter_file_check_cf_protection(struct pakfire_linter_file* lfile) {
	int r;

	// Fetch if CF Protection has been enabled
	int flags = pakfire_elf_has_cf_protection(lfile->elf);

	// aarch64: Branch Target Identification
	if (flags & PAKFIRE_ELF_MISSING_BTI) {
		r = pakfire_linter_file_error(lfile, "Branch Target Identification (BTI) is not enabled");
		if (r < 0)
			return r;
	}

	// aarch64: Pointer Authentication
	if (flags & PAKFIRE_ELF_MISSING_PAC) {
		r = pakfire_linter_file_error(lfile, "Pointer Authentication (PAC) is not enabled");
		if (r < 0)
			return r;
	}

	// x86_64: Indirect Branch Tracking
	if (flags & PAKFIRE_ELF_MISSING_IBT) {
		r = pakfire_linter_file_error(lfile, "Indirect Branch Tracking (IBT) is not enabled");
		if (r < 0)
			return r;
	}

	// x86_64: Shadow Stack
	if (flags & PAKFIRE_ELF_MISSING_SHSTK) {
		r = pakfire_linter_file_error(lfile, "Shadow Stack is not enabled");
		if (r < 0)
			return r;
	}

	return 0;
}

static int pakfire_linter_file_is_stripped(struct pakfire_linter_file* lfile) {
	switch (pakfire_elf_type(lfile->elf)) {
		// Do not check Relocatable Objects
		case ET_REL:
			return 0;

		// Check everything else
		default:
			break;
	}

	if (!pakfire_elf_is_stripped(lfile->elf))
		return pakfire_linter_file_error(lfile, "Not Stripped");

	return 0;
}

static int pakfire_linter_file_has_debuglink(struct pakfire_linter_file* lfile) {
	const char* debuglink = NULL;

	// Fetch the debug link
	debuglink = pakfire_elf_debuglink(lfile->elf);
	if (!debuglink)
		return pakfire_linter_file_error(lfile, "Missing Debug Link");

	return 0;
}

static int pakfire_linter_file_has_build_id(struct pakfire_linter_file* lfile) {
	const char* build_id = NULL;

	// Fetch the build ID
	build_id = pakfire_elf_build_id(lfile->elf);
	if (!build_id)
		return pakfire_linter_file_error(lfile, "Missing Build ID");

	return 0;
}

// Checks if files in /usr/lib/debug are correct
static int pakfire_linter_file_check_debug(struct pakfire_linter_file* lfile) {
	// Fail if this file is not an ELF file
	if (!lfile->elf)
		return pakfire_linter_file_error(lfile, "File is not in ELF format");

	// Fail if there is no debugging information in the file
	if (pakfire_elf_is_stripped(lfile->elf))
		return pakfire_linter_file_error(lfile, "Has no debug information");

	return 0;
}

int pakfire_linter_file_lint(struct pakfire_linter_file* lfile) {
	int r = 0;

	// Check capabilities
	r = pakfire_linter_file_check_caps(lfile);
	if (r < 0)
		return r;

	// Check script interpreter
	r = pakfire_linter_check_script_interpreter(lfile);
	if (r < 0)
		return r;

	// Check debug files
	if (pakfire_file_matches(lfile->file, "/usr/lib/debug/**"))
		return pakfire_linter_file_check_debug(lfile);

	// ELF Checks
	if (lfile->elf) {
		// Don't run this for any relocatable files
		switch (pakfire_elf_type(lfile->elf)) {
			case ET_REL:
				return 0;

			default:
				break;
		}

		// Check if stripped
		r = pakfire_linter_file_is_stripped(lfile);
		if (r < 0)
			return r;

		// Check if we have a Build ID
		r = pakfire_linter_file_has_build_id(lfile);
		if (r < 0)
			return r;

		// Check if we have a debug link
		r = pakfire_linter_file_has_debuglink(lfile);
		if (r < 0)
			return r;

		// Check PIE
		r = pakfire_linter_file_check_pie(lfile);
		if (r < 0)
			return r;

		// Check SSP
		r = pakfire_linter_file_check_ssp(lfile);
		if (r < 0)
			return r;

		// Check for Executable Stacks
		r = pakfire_linter_file_check_execstack(lfile);
		if (r < 0)
			return r;

		// Check RELRO
		r = pakfire_linter_file_check_relro(lfile);
		if (r < 0)
			return r;

		// Check RPATH/RUNPATH
		r = pakfire_linter_file_check_runpath(lfile);
		if (r < 0)
			return r;

		// CF Protection
		r = pakfire_linter_file_check_cf_protection(lfile);
		if (r < 0)
			return r;
	}

	return 0;
}
