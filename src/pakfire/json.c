/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2025 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <sys/mman.h>

#include <pakfire/ctx.h>
#include <pakfire/json.h>
#include <pakfire/logging.h>
#include <pakfire/util.h>

struct json_object* pakfire_json_parse(
		struct pakfire_ctx* ctx, const char* buffer, const size_t length) {
	struct json_tokener* tokener = NULL;
	struct json_object* json = NULL;

	// Create tokener
	tokener = json_tokener_new();
	if (!tokener) {
		ERROR(ctx, "Could not allocate JSON tokener: %m\n");
		goto ERROR;
	}

	// Parse JSON from buffer
	json = json_tokener_parse_ex(tokener, buffer, length);
	if (!json) {
		enum json_tokener_error error = json_tokener_get_error(tokener);

		ERROR(ctx, "JSON parsing error: %s\n", json_tokener_error_desc(error));
		goto ERROR;
	}

	// Log what we have parsed
	DEBUG(ctx, "Parsed JSON:\n%s\n",
		json_object_to_json_string_ext(json,
			JSON_C_TO_STRING_SPACED | JSON_C_TO_STRING_PRETTY)
	);

ERROR:
	if (tokener)
		json_tokener_free(tokener);

	return json;
}

static struct json_object* pakfire_json_parse_file(struct pakfire_ctx* ctx, FILE* f) {
	struct json_object* json = NULL;
	char* buffer = NULL;
	size_t length = 0;
	int r;

	// Map everything into memory
	r = pakfire_mmap(fileno(f), &buffer, &length);
	if (r)
		goto ERROR;

	// Parse
	json = pakfire_json_parse(ctx, buffer, length);

ERROR:
	if (buffer)
		munmap(buffer, length);

	return json;
}

struct json_object* pakfire_json_parse_from_file(struct pakfire_ctx* ctx, const char* path) {
	FILE* f = fopen(path, "r");
	if (!f)
		return NULL;

	struct json_object* json = pakfire_json_parse_file(ctx, f);
	fclose(f);

	return json;
}

struct json_object* pakfire_json_new_object(void) {
	return json_object_new_object();
}

int pakfire_json_add_string(struct json_object* json,
		const char* name, const char* value) {
	if (!value)
		return 0;

	return pakfire_json_add_stringn(json, name, value, strlen(value));
}

int pakfire_json_add_stringn(struct json_object* json,
		const char* name, const char* value, size_t length) {
	// No string? Nothing to do
	if (!value)
		return 0;

	// Convert string to JSON object
	struct json_object* object = json_object_new_string_len(value, length);
	if (!object)
		return 1;

	// Add the object
	return json_object_object_add(json, name, object);
}

int pakfire_json_add_stringf(struct json_object* json,
		const char* name, const char* format, ...) {
	char* buffer = NULL;
	va_list args;
	int r;

	// Format the input
	va_start(args, format);
	r = vasprintf(&buffer, format, args);
	va_end(args);
	if (r < 0)
		return r;

	// Add the string
	r = pakfire_json_add_stringn(json, name, buffer, r);

	// Cleanup
	free(buffer);

	return r;
}

int pakfire_json_add_string_array(struct json_object* json, const char* name, char** array) {
	int r = 1;

	// Allocate a new array
	struct json_object* object = json_object_new_array();
	if (!object)
		goto ERROR;

	// Add all items on list to the array
	for (char** item = array; *item; item++) {
		r = json_object_array_add(object, json_object_new_string(*item));
		if (r)
			goto ERROR;
	}

	// Add object
	r = json_object_object_add(json, name, object);
	if (r)
		goto ERROR;

ERROR:
	// Free JSON object on error
	if (r)
		json_object_put(object);

	return r;
}

int pakfire_json_add_int64(struct json_object* json, const char* name, int64_t value) {
	// Convert integer to JSON object
	struct json_object* object = json_object_new_int64(value);
	if (!object)
		return -errno;

	// Add the object
	return json_object_object_add(json, name, object);
}

int pakfire_json_add_uint64(struct json_object* json, const char* name, uint64_t value) {
	struct json_object* object = NULL;

	// Convert the value to JSON
	object = json_object_new_uint64(value);
	if (!object)
		return -errno;

	// Add the object
	return json_object_object_add(json, name, object);
}

int pakfire_json_add_double(struct json_object* json, const char* name, double value) {
	struct json_object* object = NULL;

	// Convert the value to JSON
	object = json_object_new_double(value);
	if (!object)
		return -errno;

	// Add the object
	return json_object_object_add(json, name, object);
}

int pakfire_json_add_object(struct json_object* json, const char* name, struct json_object** o) {
	struct json_object* object = NULL;

	// Make a new object
	object = json_object_new_object();
	if (!object)
		return -errno;

	// Return the object if requested
	if (o)
		*o = object;

	// Add the object
	return json_object_object_add(json, name, object);
}

int pakfire_json_add_array(struct json_object* json, const char* name, struct json_object** array) {
	struct json_object* object = NULL;

	// Make a new array
	object = json_object_new_array();
	if (!object)
		return -errno;

	// Return the object if requested
	if (array)
		*array = object;

	// Add the object
	return json_object_object_add(json, name, object);
}

int pakfire_json_array_add_string(struct json_object* array, const char* s) {
	// Make a new string object
	struct json_object* object = json_object_new_string_len(s, strlen(s));
	if (!object)
		return -errno;

	return json_object_array_add(array, object);
}

int pakfire_json_array_add_stringf(struct json_object* array, const char* format, ...) {
	char* buffer = NULL;
	va_list args;
	int r;

	// Format the input
	va_start(args, format);
	r = vasprintf(&buffer, format, args);
	va_end(args);
	if (r < 0)
		return r;

	// Add the string to the array
	r = pakfire_json_array_add_string(array, buffer);

	// Cleanup
	free(buffer);

	return r;
}

static int __pakfire_json_get_object(struct json_object* json,
		const char* key, const json_type type, struct json_object** o) {
	struct json_object* object = NULL;
	int r;

	// Fetch the object
	r = json_object_object_get_ex(json, key, &object);
	if (!r)
		return -ENOMSG;

	// Check if this is an integer
	r = json_object_is_type(object, type);
	if (!r)
		return -ENOMSG;

	// Return the pointer
	*o = object;

	return 0;
}

int pakfire_json_get_string(struct json_object* json, const char* key, const char** value) {
	struct json_object* object = NULL;
	int r;

	// Fetch the object
	r = __pakfire_json_get_object(json, key, json_type_string, &object);
	if (r < 0)
		return r;

	// Return the value
	*value = json_object_get_string(object);

	return 0;
}

int pakfire_json_get_int64(struct json_object* json, const char* key, int64_t* value) {
	struct json_object* object = NULL;
	int r;

	// Fetch the object
	r = __pakfire_json_get_object(json, key, json_type_int, &object);
	if (r < 0)
		return r;

	// Return the value
	*value = json_object_get_int64(object);

	return 0;
}

int pakfire_json_get_object(struct json_object* json, const char* key, struct json_object** object) {
	return __pakfire_json_get_object(json, key, json_type_object, object);
}

int pakfire_json_get_array(struct json_object* json, const char* key, struct json_object** array) {
	return __pakfire_json_get_object(json, key, json_type_array, array);
}

int pakfire_json_write(struct json_object* json, const char* path) {
	FILE* f = NULL;
	int r;

	// Open the file for writing
	f = fopen(path, "w");
	if (!f)
		return -errno;

	// Write the object to file
	r = json_object_to_fd(fileno(f), json, 0);
	if (r < 0) {
		r = -errno;
		goto ERROR;
	}

ERROR:
	if (f)
		fclose(f);

	return r;
}
