/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2013 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_UTIL_H
#define PAKFIRE_UTIL_H

#include <stdio.h>

#define PCRE2_CODE_UNIT_WIDTH 8
#include <pcre2.h>

#include <pakfire/ctx.h>

// Macros to help the compiler with branch prediction
#define likely(x)		__builtin_expect(!!(x), 1)
#define unlikely(x)		__builtin_expect(!!(x), 0)

/*
	A simple buffer object
*/
struct pakfire_buffer {
	char* data;
	size_t length;
};

/*
	This implementation of realloc frees the original buffer
	if it could not be resized.
*/
static inline void* pakfire_realloc(void* p, size_t size) {
	void* n = realloc(p, size);
	if (!n) {
		if (p)
			free(p);
	}

	return n;
}

int pakfire_mmap(int fd, char** data, size_t* length);

const char* pakfire_path_relpath(const char* root, const char* path);

// File stuff

static inline int pakfire_rewind(FILE* f) {
	return fseek(f, 0, SEEK_SET);
}

int pakfire_file_write(struct pakfire* pakfire, const char* path,
	uid_t owner, gid_t group, mode_t mode, const char* format, ...)
	__attribute__((format(printf, 6, 7)));

int pakfire_touch(const char* path, mode_t mode);
int pakfire_mkparentdir(const char* path, mode_t mode);
int pakfire_mkdir(const char* path, mode_t mode);
FILE* pakfire_mktemp(char* path, const mode_t mode);
char* pakfire_mkdtemp(char* path);
int pakfire_symlink(struct pakfire_ctx* ctx, const char* target, const char* linkpath);
int pakfire_rmtree(const char* path, int flags);

#define pakfire_which(pakfire, path, what) \
	__pakfire_which(pakfire, path, sizeof(path), what)
int __pakfire_which(struct pakfire* pakfire, char* path, const size_t length, const char* what);

// UUID Stuff

int pakfire_uuid_is_valid(const char* s);
char* pakfire_generate_uuid(void);

// Resource Limits

#define PAKFIRE_RLIMIT_NOFILE_MAX (512 * 1024)

int pakfire_rlimit_set(struct pakfire_ctx* ctx, int limit);
int pakfire_rlimit_reset_nofile(struct pakfire_ctx* ctx);

// Regex

int pakfire_compile_regex(struct pakfire_ctx* ctx, pcre2_code** regex, const char* pattern);

// Copy
int pakfire_copy(struct pakfire_ctx* ctx, FILE* src, FILE* dst);

// Time

// seconds to microseconds
#define S_TO_MS(s) (s * 1000)
#define S_TO_US(s) (s * 1000000)
#define S_TO_NS(s) (s * 1000000000)

#define MS_TO_S(s) (s / 1000)
#define US_TO_S(s) (s / 1000000)
#define NS_TO_S(s) (s / 1000000000)

#define US_TO_MS(us) (us / 1000)

static inline double pakfire_timespec_delta(struct timespec* t1, struct timespec* t2) {
	// Compute delta in seconds
	return (
		((t1->tv_sec * 1000) + (t1->tv_nsec / 1000000))
		-
		((t2->tv_sec * 1000) + (t2->tv_nsec / 1000000))
	) / 1000.0;
}

#endif /* PAKFIRE_UTIL_H */
