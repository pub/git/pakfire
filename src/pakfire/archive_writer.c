/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2025 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// json-c
#include <json.h>

// libarchive
#include <archive.h>
#include <archive_entry.h>

#include <pakfire/archive_writer.h>
#include <pakfire/file.h>
#include <pakfire/filelist.h>
#include <pakfire/hashes.h>
#include <pakfire/progress.h>
#include <pakfire/string.h>

struct pakfire_archive_writer {
	struct pakfire_ctx* ctx;
	int nrefs;

	// Pakfire
	struct pakfire* pakfire;

	// Time Created
	time_t time_created;

	// Format
	pakfire_archive_writer_format format;

	// Container
	enum pakfire_archive_writer_container {
		PAKFIRE_CONTAINER_PAX,
		PAKFIRE_CONTAINER_GNU,
	} container;

	// Compression
	enum pakfire_archive_writer_compression {
		PAKFIRE_COMPRESSION_NONE = 0,
		PAKFIRE_COMPRESSION_GZIP,
		PAKFIRE_COMPRESSION_ZSTD,
	} compression;
	unsigned int compression_level;

	// Checksums
	int checksums;

	// File Handle
	FILE* f;

	// Archive
	struct archive* archive;

	// Resolver for hardlinks
	struct archive_entry_linkresolver* linkresolver;

	// Progress Status
	struct pakfire_progress* progress;
};

/*
 * This function configures this object depending on the format.
 */
static int pakfire_archive_writer_setup_format(
		struct pakfire_archive_writer* self, pakfire_archive_writer_format format) {
	switch (format) {
		// The Pakfire Archive Format
		case PAKFIRE_FORMAT_ARCHIVE:
		case PAKFIRE_FORMAT_SOURCE_ARCHIVE:
			// We use the PAX tar format
			self->container = PAKFIRE_CONTAINER_PAX;

			// We use Zstandard compression
			self->compression = PAKFIRE_COMPRESSION_ZSTD;

			// Use good compression for binary archives,
			// but don't try to compress source archives much.
			switch (format) {
				case PAKFIRE_FORMAT_ARCHIVE:
					self->compression_level = 20;
					break;

				case PAKFIRE_FORMAT_SOURCE_ARCHIVE:
					self->compression_level = 1;
					break;

				default:
					break;
			}

			// Hash the files
			self->checksums = PAKFIRE_HASH_SHA3_512|PAKFIRE_HASH_BLAKE2B512;
			break;

		// OCI Image
		case PAKFIRE_FORMAT_OCI:
			// Use the GNU tar format
			self->container = PAKFIRE_CONTAINER_GNU;

			// Use no compression
			self->compression = PAKFIRE_COMPRESSION_NONE;
			break;

		// OCI Layer
		case PAKFIRE_FORMAT_OCI_LAYER:
			// Use the GNU tar format
			self->container = PAKFIRE_CONTAINER_GNU;

			// We use gzip compression
			self->compression = PAKFIRE_COMPRESSION_GZIP;

			// Use some good compression
			self->compression_level = 9;
			break;

		// Fail on invalid inputs
		default:
			ERROR(self->ctx, "Invalid archive format\n");
			return -EINVAL;
	}

	return 0;
}

static int pakfire_archive_writer_setup_archive(struct pakfire_archive_writer* self) {
	char value[64];
	int r;

	// Open a new archive
	self->archive = archive_write_new();
	if (!self->archive) {
		ERROR(self->ctx, "archive_write_new() failed\n");
		return -ENOTSUP;
	}

	// Set the container format
	switch (self->container) {
		// PAX Format
		case PAKFIRE_CONTAINER_PAX:
			r = archive_write_set_format_pax(self->archive);
			if (r) {
				ERROR(self->ctx, "Could not set format to PAX: %s\n",
					archive_error_string(self->archive));
				return -ENOTSUP;
			}

			// Store any extended attributes in the SCHILY headers
			r = archive_write_set_format_option(self->archive, "pax", "xattrheader", "SCHILY");
			if (r) {
				ERROR(self->ctx, "Could not set xattrheader option: %s\n",
					archive_error_string(self->archive));
				return -ENOTSUP;
			}
			break;

		// GNU Tar Format
		case PAKFIRE_CONTAINER_GNU:
			r = archive_write_set_format_gnutar(self->archive);
			if (r) {
				ERROR(self->ctx, "Could not set format to GNU tar: %s\n",
					archive_error_string(self->archive));
				return -ENOTSUP;
			}
			break;
	}

	// Set the compression
	switch (self->compression) {
		case PAKFIRE_COMPRESSION_NONE:
			break;

		// gzip
		case PAKFIRE_COMPRESSION_GZIP:
			r = archive_write_add_filter_gzip(self->archive);
			if (r) {
				ERROR(self->ctx, "Could not enable gzip compression: %s\n",
					archive_error_string(self->archive));
				return -ENOTSUP;
			}
			break;

		// Zstandard
		case PAKFIRE_COMPRESSION_ZSTD:
			r = archive_write_add_filter_zstd(self->archive);
			if (r) {
				ERROR(self->ctx, "Could not enable Zstandard compression: %s\n",
					archive_error_string(self->archive));
				return -ENOTSUP;
			}
			break;
	}

	// Set compression level
	if (self->compression_level) {
		// Format the value
		r = pakfire_string_format(value, "%u", self->compression_level);
		if (r < 0)
			return r;

		// Set the value
		r = archive_write_set_filter_option(self->archive, NULL, "compression-level", value);
		if (r) {
			ERROR(self->ctx, "Could not set compression level %s: %s\n",
				value, archive_error_string(self->archive));
			return -ENOTSUP;
		}
	}

	// Do not pad the last block
	archive_write_set_bytes_in_last_block(self->archive, 1);

	// Write archive to f
	r = archive_write_open_FILE(self->archive, self->f);
	if (r) {
		ERROR(self->ctx, "archive_write_open_FILE() failed: %s\n",
			archive_error_string(self->archive));
		return -ENOTSUP;
	}

	return 0;
}

static int pakfire_archive_writer_setup_linkresolver(struct pakfire_archive_writer* self) {
	// Setup the link resolver
	self->linkresolver = archive_entry_linkresolver_new();
	if (!self->linkresolver) {
		ERROR(self->ctx, "Could not setup link resolver: %m\n");
		return -errno;
	}

	// Set the link resolver strategy
	archive_entry_linkresolver_set_strategy(
		self->linkresolver, archive_format(self->archive));

	return 0;
}

static void pakfire_archive_writer_free(struct pakfire_archive_writer* self) {
	if (self->linkresolver)
		archive_entry_linkresolver_free(self->linkresolver);
	if (self->progress)
		pakfire_progress_unref(self->progress);
	if (self->archive)
		archive_write_free(self->archive);
	if (self->pakfire)
		pakfire_unref(self->pakfire);
	if (self->ctx)
		pakfire_ctx_unref(self->ctx);
	free(self);
}

int pakfire_archive_writer_create(struct pakfire_archive_writer** writer,
		struct pakfire* pakfire, pakfire_archive_writer_format format, FILE* f) {
	struct pakfire_archive_writer* self = NULL;
	int r;

	// Allocate some memory
	self = calloc(1, sizeof(*self));
	if (!self)
		return -errno;

	// Store a reference to the context
	self->ctx = pakfire_ctx(pakfire);

	// Store a reference to Pakfire
	self->pakfire = pakfire_ref(pakfire);

	// Initialize the reference counter
	self->nrefs = 1;

	// Store the file handle
	self->f = f;

	// Fetch the creation time
	self->time_created = time(NULL);
	if (self->time_created < 0) {
		r = -errno;
		goto ERROR;
	}

	// Setup format
	r = pakfire_archive_writer_setup_format(self, format);
	if (r < 0)
		goto ERROR;

	// Setup archive
	r = pakfire_archive_writer_setup_archive(self);
	if (r < 0)
		goto ERROR;

	// Setup link resolver
	r = pakfire_archive_writer_setup_linkresolver(self);
	if (r < 0)
		goto ERROR;

	int progress_flags =
		PAKFIRE_PROGRESS_SHOW_PERCENTAGE |
		PAKFIRE_PROGRESS_SHOW_BYTES_TRANSFERRED |
		PAKFIRE_PROGRESS_SHOW_TRANSFER_SPEED;

	// Create the progress indicator
	r = pakfire_progress_create(&self->progress, self->ctx, progress_flags, NULL);
	if (r < 0)
		goto ERROR;

	// Return the pointer
	*writer = self;
	return 0;

ERROR:
	pakfire_archive_writer_free(self);
	return r;
}

struct pakfire_archive_writer* pakfire_archive_writer_ref(struct pakfire_archive_writer* self) {
	++self->nrefs;

	return self;
}

struct pakfire_archive_writer* pakfire_archive_writer_unref(struct pakfire_archive_writer* self) {
	if (--self->nrefs > 0)
		return self;

	pakfire_archive_writer_free(self);
	return NULL;
}

int pakfire_archive_writer_set_title(struct pakfire_archive_writer* self,
		const char* format, ...) {
	char buffer[128];
	va_list args;
	int r;

	// Format the input
	va_start(args, format);
	r = pakfire_string_vformat(buffer, format, args);
	va_end(args);
	if (r < 0)
		return r;

	return pakfire_progress_set_title(self->progress, "%s", buffer);
}

static int pakfire_archive_writer_write_payload(
		struct pakfire_archive_writer* self, FILE* f) {
	ssize_t bytes_written = 0;
	ssize_t bytes_read = 0;
	char buffer[64 * 1024];
	int r;

	// Loop through the entire length of the file
	while (!feof(f)) {
		// Read a block from file
		bytes_read = fread(buffer, 1, sizeof(buffer), f);

		// Check if any error occured
		if (ferror(f))
			return -errno;

		// Write the block to the archive
		bytes_written = archive_write_data(self->archive, buffer, bytes_read);
		if (bytes_written < bytes_read)
			return -errno;

		// Update progress
		r = pakfire_progress_increment(self->progress, bytes_written);
		if (r < 0)
			return r;
	}

	return 0;
}

static int pakfire_archive_writer_write_entry(struct pakfire_archive_writer* self,
		struct pakfire_file* file, struct archive_entry* entry) {
	FILE* f = NULL;
	int r;

	// Fetch the path of the file in the archive
	const char* path = archive_entry_pathname(entry);

	// Remove any leading slahes
	while (*path == '/')
		path++;

	// Restore the updated path name
	archive_entry_set_pathname(entry, path);

	// Write the header
	r = archive_write_header(self->archive, entry);
	if (r) {
		ERROR(self->ctx, "Error writing file header for %s: %s\n",
			pakfire_file_get_path(file), archive_error_string(self->archive));
		r = -EINVAL;
		goto ERROR;
	}

	// Copy the payload, if libarchive wants us to.
	// It will set the size of the entry to zero if it does not need the payload,
	// for example when we are writing a hardlink.
	if (archive_entry_size(entry)) {
		// Open the file
		f = pakfire_file_fopen(file, "r");
		if (!f) {
			r = -errno;
			goto ERROR;
		}

		// Copy everything
		r = pakfire_archive_writer_write_payload(self, f);
		if (r < 0) {
			ERROR(self->ctx, "Failed to write %s: %s\n", path, strerror(-r));
			goto ERROR;
		}
	}

	// Write trailer
	r = archive_write_finish_entry(self->archive);
	if (r) {
		ERROR(self->ctx, "Failed to write the trailer: %s\n",
			archive_error_string(self->archive));
		r = -EINVAL;
		goto ERROR;
	}

ERROR:
	if (f)
		fclose(f);

	return r;
}

static int pakfire_archive_writer_write_file(
		struct pakfire_ctx* ctx, struct pakfire_file* file, void* data) {
	struct pakfire_archive_writer* self = data;
	struct archive_entry* sparse_entry = NULL;
	struct archive_entry* entry = NULL;
	int r = 0;

	// Generate file metadata into an archive entry
	entry = pakfire_file_archive_entry(file, self->checksums);
	if (!entry) {
		r = -errno;
		goto ERROR;
	}

	// Perform search for hardlinks
	archive_entry_linkify(self->linkresolver, &entry, &sparse_entry);

	// Write the main entry
	if (entry) {
		r = pakfire_archive_writer_write_entry(self, file, entry);
		if (r < 0)
			goto ERROR;
	}

	// Write the sparse entry
	if (sparse_entry) {
		r = pakfire_archive_writer_write_entry(self, file, sparse_entry);
		if (r < 0)
			goto ERROR;
	}

	// Query the link resolver for any more entries
	for (;;) {
		// Free the entry
		if (entry) {
			archive_entry_free(entry);
			entry = NULL;
		}

		// Free the sparse entry
		if (sparse_entry) {
			archive_entry_free(sparse_entry);
			sparse_entry = NULL;
		}

		// Fetch the next entry
		archive_entry_linkify(self->linkresolver, &entry, &sparse_entry);
		if (!entry)
			break;

		// Write the entry to the archive
		r = pakfire_archive_writer_write_entry(self, file, entry);
		if (r < 0)
			goto ERROR;
	}

ERROR:
	if (sparse_entry)
		archive_entry_free(sparse_entry);
	if (entry)
		archive_entry_free(entry);

	return r;
}

int pakfire_archive_writer_write_files(
		struct pakfire_archive_writer* self, struct pakfire_filelist* files) {
	int r;

	// Fetch the total amount of data we are going to write
	const size_t size = pakfire_filelist_total_size(files);

	// Start the progress
	r = pakfire_progress_start(self->progress, size);
	if (r < 0)
		goto ERROR;

	// Walk through the entire filelist
	r = pakfire_filelist_walk(files, pakfire_archive_writer_write_file, self, 0, NULL);
	if (r < 0)
		goto ERROR;

	// Ensure we flush out everything
	r = archive_write_close(self->archive);
	if (r < 0) {
		ERROR(self->ctx, "Failed to close the archive: %s\n",
			archive_error_string(self->archive));
		r = -EINVAL;
		goto ERROR;
	}

ERROR:
	pakfire_progress_finish(self->progress);

	return r;
}

int pakfire_archive_writer_write_everything(
		struct pakfire_archive_writer* self, const char* root) {
	struct pakfire_filelist* files = NULL;
	int r;

	// Create a new filelist
	r = pakfire_filelist_create(&files, self->pakfire);
	if (r < 0)
		goto ERROR;

	// Scan for all files
	r = pakfire_filelist_scan(files, root, NULL, NULL, 0);
	if (r < 0)
		goto ERROR;

	// Write all files
	r = pakfire_archive_writer_write_files(self, files);
	if (r < 0)
		goto ERROR;

ERROR:
	if (files)
		pakfire_filelist_unref(files);

	return r;
}

static int pakfire_archive_writer_create_entry(struct pakfire_archive_writer* self,
		struct archive_entry** entry, const char* path, const mode_t mode, const size_t length) {
	struct archive_entry* e = NULL;
	int r;

	// Create a new file entry
	e = archive_entry_new();
	if (!e) {
		r = -errno;
		goto ERROR;
	}

	// Set path
	archive_entry_set_pathname(e, path);

	// This is a regular file
	archive_entry_set_filetype(e, AE_IFREG);
	archive_entry_set_perm(e, mode);

	// Set length
	archive_entry_set_size(e, length);

	// Set ownership
	archive_entry_set_uname(e, "root");
	archive_entry_set_uid(e, 0);
	archive_entry_set_gname(e, "root");
	archive_entry_set_gid(e, 0);

	// Set times
	archive_entry_set_birthtime(e, self->time_created, 0);
	archive_entry_set_ctime(e, self->time_created, 0);
	archive_entry_set_mtime(e, self->time_created, 0);
	archive_entry_set_atime(e, self->time_created, 0);

	// Return the pointer
	*entry = e;
	return 0;

ERROR:
	if (e)
		archive_entry_free(e);

	return r;
}

/*
 * Creates a new file and writes it to the archive.
 */
int pakfire_archive_writer_create_file(struct pakfire_archive_writer* self,
		const char* path, mode_t mode, const char* payload, const size_t length) {
	struct archive_entry* entry = NULL;
	int r;

	// Create a new file entry
	r = pakfire_archive_writer_create_entry(self, &entry, path, mode, length);
	if (r < 0)
		goto ERROR;

	// Write the header
	r = archive_write_header(self->archive, entry);
	if (r) {
		ERROR(self->ctx, "Error writing header: %s\n",
			archive_error_string(self->archive));
		r = -EINVAL;
		goto ERROR;
	}

	// Write content
	r = archive_write_data(self->archive, payload, length);
	if (r < 0) {
		ERROR(self->ctx, "Error writing data: %s\n",
			archive_error_string(self->archive));
		r = -EINVAL;
		goto ERROR;
	}

	// Write trailer
	r = archive_write_finish_entry(self->archive);
	if (r) {
		ERROR(self->ctx, "Failed to write the trailer: %s\n",
			archive_error_string(self->archive));
		r = -EINVAL;
		goto ERROR;
	}

ERROR:
	if (entry)
		archive_entry_free(entry);

	return r;
}

int pakfire_archive_writer_create_file_from_json(
		struct pakfire_archive_writer* self, const char* path, mode_t mode, struct json_object* json) {
	const char* buffer = NULL;

	// Serialize JSON to string
	buffer = json_object_to_json_string_ext(json, 0);
	if (!buffer) {
		ERROR(self->ctx, "Failed to serialize JSON object: %m\n");
		return -errno;
	}

	// Write it to the archive
	return pakfire_archive_writer_create_file(self, path, mode, buffer, strlen(buffer));
}

int pakfire_archive_writer_create_file_from_file(
		struct pakfire_archive_writer* self, const char* path, mode_t mode, FILE* f) {
	struct archive_entry* entry = NULL;
	struct stat st = {};
	int r;

	// Fetch the file descriptor
	const int fd = fileno(f);
	if (fd < 0)
		return -ENOTSUP;

	// Stat the file
	r = fstat(fd, &st);
	if (r < 0)
		return -errno;

	// Create a new file entry
	r = pakfire_archive_writer_create_entry(self, &entry, path, mode, st.st_size);
	if (r < 0)
		goto ERROR;

	// Write the header
	r = archive_write_header(self->archive, entry);
	if (r) {
		ERROR(self->ctx, "Error writing header: %s\n",
			archive_error_string(self->archive));
		r = -EINVAL;
		goto ERROR;
	}

	// Write payload
	r = pakfire_archive_writer_write_payload(self, f);
	if (r < 0)
		goto ERROR;

	// Write trailer
	r = archive_write_finish_entry(self->archive);
	if (r) {
		ERROR(self->ctx, "Failed to write the trailer: %s\n",
			archive_error_string(self->archive));
		r = -EINVAL;
		goto ERROR;
	}

ERROR:
	if (entry)
		archive_entry_free(entry);

	return r;
}
