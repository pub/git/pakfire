/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2024 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <termios.h>
#include <unistd.h>

#include <systemd/sd-event.h>

#include <pakfire/ctx.h>
#include <pakfire/file.h>
#include <pakfire/filelist.h>
#include <pakfire/pty.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

#define MAX_LINE_LENGTH 16384

struct pakfire_pty_stdio {
	// File Descriptor
	int fd;
	unsigned close_fd:1;

	// Buffer
	char buffer[MAX_LINE_LENGTH];
	size_t buffered;

	// Terminal Attributes
	struct termios attrs;
	unsigned int attrs_saved:1;

	// IO Flags
	enum pakfire_pty_io {
		PAKFIRE_PTY_READY_TO_READ  = (1 << 0),
		PAKFIRE_PTY_READY_TO_WRITE = (1 << 1),
		PAKFIRE_PTY_HANGUP         = (1 << 2),
		PAKFIRE_PTY_EOF            = (1 << 3),
		PAKFIRE_PTY_MAP_CRNL       = (1 << 4),
	} io;

	// Event Source
	sd_event_source* event;

	// Callbacks
	struct pakfire_pty_callback {
		pakfire_pty_stdin_callback stdin_callback;
		pakfire_pty_stdout_callback stdout_callback;
		void* data;
	} callbacks;
};

struct pakfire_pty {
	struct pakfire_ctx* ctx;
	int nrefs;

	// Flags
	int flags;

	// Event Loop
	sd_event* loop;

	// A UNIX socket to pass the file descriptor
	int socket[2];

	// The master PTY
	struct pakfire_pty_stdio master;

	// The path to the PTY
	char path[PATH_MAX];

	// Standard Input
	struct pakfire_pty_stdio stdin;
	struct pakfire_pty_stdio stdout;

	// SIGWINCH Event
	sd_event_source* sigwinch_event;

	// State
	enum {
		PAKFIRE_PTY_STATE_INIT = 0,
		PAKFIRE_PTY_STATE_FORWARDING,
		PAKFIRE_PTY_STATE_DRAINING,
		PAKFIRE_PTY_STATE_DONE,
	} state;

	// Captured Output
	struct iovec output;
};

static int pakfire_pty_has_flag(struct pakfire_pty* pty, int flag) {
	return pty->flags & flag;
}

static int pakfire_pty_store_output(struct pakfire_pty* pty) {
	struct stat buffer;
	int r;

	// Check if we have a file descriptor
	if (pty->stdout.fd < 0)
		return -EBADF;

	// Stat the buffer
	r = fstat(pty->stdout.fd, &buffer);
	if (r < 0) {
		ERROR(pty->ctx, "Could not stat the output buffer: %m\n");
		return -errno;
	}

	DEBUG(pty->ctx, "Read %jd byte(s) of output\n", buffer.st_size);

	// Don't try to map an empty buffer
	if (!buffer.st_size)
		return 0;

	// Map the output into memory
	pty->output.iov_base = mmap(NULL, buffer.st_size, PROT_READ, MAP_SHARED, pty->stdout.fd, 0);
	if (pty->output.iov_base == MAP_FAILED) {
		ERROR(pty->ctx, "Could not map the output into memory: %m\n");
		return -errno;
	}

	// Store the size of the buffer
	pty->output.iov_len = buffer.st_size;

	return 0;
}

static int pakfire_pty_same_inode(struct pakfire_pty* pty, int fd1, int fd2) {
	struct stat stat1;
	struct stat stat2;
	int r;

	// Cannot do this if either file descriptor is not open
	if (fd1 < 0 || fd2 < 0)
		return 0;

	// Stat the first file descriptor
	r = fstat(fd1, &stat1);
	if (r < 0) {
		ERROR(pty->ctx, "Could not stat fd %d: %m\n", fd1);
		return -errno;
	}

	// Stat the second file descriptor
	r = fstat(fd2, &stat2);
	if (r < 0) {
		ERROR(pty->ctx, "Could not stat fd %d: %m\n", fd2);
		return -errno;
	}

	// Mismatched mode
	if ((stat1.st_mode ^ stat2.st_mode) & S_IFMT)
		return 0;

	// Match if the device and inode are identical
	return (stat1.st_dev == stat2.st_dev) && (stat1.st_ino == stat2.st_ino);
}

static int pakfire_pty_restore_attrs(struct pakfire_pty* pty,
		struct pakfire_pty_stdio* stdio) {
	int flags;
	int r;

	// Do nothing if the file descriptor is no longer open
	if (stdio->fd < 0)
		return 0;

	// Fetch the flags
	flags = fcntl(stdio->fd, F_GETFL, 0);
	if (flags < 0) {
		ERROR(pty->ctx, "Could not set flags for file descriptor %d: %s\n",
			stdio->fd, strerror(errno));
		return -errno;
	}

	// Turn on non-blocking mode again
	r = fcntl(stdio->fd, F_SETFL, flags & ~O_NONBLOCK);
	if (r < 0) {
		ERROR(pty->ctx, "Could not set flags for file descriptor %d: %s\n",
			stdio->fd, strerror(errno));
		return -errno;
	}

	// Restore the attributes
	if (stdio->attrs_saved) {
		r = tcsetattr(stdio->fd, TCSANOW, &stdio->attrs);
		if (r) {
			ERROR(pty->ctx, "Could not restore terminal attributes for %d, ignoring: %s\n",
				stdio->fd, strerror(errno));
			return -errno;
		}
	}

	// Close the file descriptor
	if (stdio->close_fd)
		close(stdio->fd);

	stdio->fd = -EBADF;

	return 0;
}

static int pakfire_pty_store_attrs(struct pakfire_pty* pty,
		struct pakfire_pty_stdio* stdio) {
	int r;

	// Store all attributes
	r = tcgetattr(stdio->fd, &stdio->attrs);
	if (r) {
		switch (errno) {
			case ENOTTY:
				return 0;

			default:
				ERROR(pty->ctx, "Could not fetch terminal attributes from fd %d: %s\n",
					stdio->fd, strerror(errno));

				return -errno;
		}
	}

	// Mark the attributes as saved
	stdio->attrs_saved = 1;

	return 0;
}

static int pakfire_pty_disconnect(struct pakfire_pty* pty) {
	// Clear events
	if (pty->master.event)
		pty->master.event = sd_event_source_unref(pty->master.event);
	if (pty->stdin.event)
		pty->stdin.event = sd_event_source_unref(pty->stdin.event);
	if (pty->stdout.event)
		pty->stdout.event = sd_event_source_unref(pty->stdout.event);
	if (pty->sigwinch_event)
		pty->sigwinch_event = sd_event_source_unref(pty->sigwinch_event);

	// Close the PTY
	if (pty->master.fd >= 0) {
		close(pty->master.fd);
		pty->master.fd = -EBADF;
	}

	// Restore any changed terminal attributes
	pakfire_pty_restore_attrs(pty, &pty->stdin);
	pakfire_pty_restore_attrs(pty, &pty->stdout);

	return 0;
}

static int pakfire_pty_drained(struct pakfire_pty* pty) {
	int q;
	int r;

	// We still have data in the buffer
	if (pty->stdout.buffered)
		return 0;

	// There is still data in the PTY buffer
	if (pty->master.io & PAKFIRE_PTY_READY_TO_READ)
		return 0;

	// We are drained if the file descriptor is closed
	if (pty->master.fd < 0)
		return 1;

	// Is there anything in the input buffer?
	r = ioctl(pty->master.fd, TIOCINQ, &q);
	if (r < 0) {
		DEBUG(pty->ctx, "TIOCINQ failed on master fd %d: %m\n", pty->master.fd);
		return -errno;
	}

	if (q)
		return 0;

	// Is there anything in the output buffer?
	r = ioctl(pty->master.fd, TIOCOUTQ, &q);
	if (r < 0) {
		DEBUG(pty->ctx, "TIOCOUTQ failed on master fd %d: %m\n", pty->master.fd);
		return -errno;
	}

	if (q)
		return 0;

	// We are fully drained
	return 1;
}

static int pakfire_pty_done(struct pakfire_pty* pty, int code) {
	int r;

	// Don't run this more than once
	if (pty->state == PAKFIRE_PTY_STATE_DONE)
		return 0;

	// Store the output
	if (pakfire_pty_has_flag(pty, PAKFIRE_PTY_CAPTURE_OUTPUT)) {
		r = pakfire_pty_store_output(pty);
		if (r < 0) {
			ERROR(pty->ctx, "Could not store output: %s\n", strerror(-r));
			return r;
		}
	}

	// Disconnect
	pakfire_pty_disconnect(pty);

	return 0;
}

static int pakfire_pty_send_eof(struct pakfire_pty* pty, int fd) {
	const char eof = VEOF;
	int r;

	// Send EOF (Ctrl-D)
	r = write(fd, &eof, sizeof(eof));
	if (r < 0) {
		ERROR(pty->ctx, "Could not write EOF: %s\n", strerror(errno));
		return -errno;
	}

	return 0;
}

static int pakfire_pty_buffer_is_full(struct pakfire_pty* pty, const struct pakfire_pty_stdio* stdio) {
	if (stdio->buffered >= sizeof(stdio->buffer))
		return 1;

	return 0;
}

static int pakfire_pty_buffer_is_empty(struct pakfire_pty* pty, const struct pakfire_pty_stdio* stdio) {
	if (stdio->buffered == 0)
		return 1;

	return 0;
}

static int pakfire_pty_buffer_has_data(struct pakfire_pty* pty, const struct pakfire_pty_stdio* stdio) {
	if (stdio->buffered)
		return 1;

	return 0;
}

/*
	Maps any CRNL in the buffer to just NL
*/
static void pakfire_pty_map_crnl(struct pakfire_pty_stdio* stdio) {
	char* cr = NULL;

	// Walk through the entire buffer...
	for (char* p = stdio->buffer; p <= stdio->buffer + stdio->buffered; p++) {
		switch (*p) {
			// Remember the position of the last CR
			case '\r':
				cr = p;
				continue;

			// Check if have have found a NL
			case '\n':
				// CR is only set if the previous character was CR
				if (cr)
					memmove(cr, p, stdio->buffered-- - (cr - stdio->buffer));
				break;
		}

		// Reset
		cr = NULL;
	}
}

/*
	Reads as much data as possible into the buffer
*/
static int pakfire_pty_fill_buffer(struct pakfire_pty* pty, int fd, struct pakfire_pty_stdio* stdio) {
	ssize_t bytes_read = 0;

	// Call the callback (if we have one)
	if (stdio->callbacks.stdin_callback) {
		bytes_read = stdio->callbacks.stdin_callback(pty->ctx, stdio->callbacks.data,
				stdio->buffer + stdio->buffered, sizeof(stdio->buffer) - stdio->buffered);

		// Abort on errors
		if (bytes_read < 0)
			return bytes_read;

	// Otherwise read from the file descriptor
	} else if (fd >= 0) {
		bytes_read = read(fd, stdio->buffer + stdio->buffered, sizeof(stdio->buffer) - stdio->buffered);

		// Abort on errors
		if (bytes_read < 0)
			return -errno;
	}

	// Successful read
	stdio->buffered += bytes_read;

	return bytes_read;
}

#define pakfire_pty_sanitize_line(pty, line, buffer, buffer_length) \
	__pakfire_pty_sanitize_line(pty, line, sizeof(line), buffer, buffer_length)

static int __pakfire_pty_sanitize_line(struct pakfire_pty* self,
		char* line, ssize_t line_length, const char* buffer, ssize_t length) {
	ssize_t i = 0;
	int num;

	// The cursor position
	int cursor = 0;

	while (i < length) {
		switch (buffer[i]) {
			// Escape
			case '\x1b':
				// Skip the escape character
				i++;

				// CSI
				if (i < length && buffer[i] == '[') {
					i++;

					// Reset number
					num = 0;

					// Parse the number
					while (i < length) {
						// Skip any ';'
						if (buffer[i] == ';') {
							i++;
							continue;

						// Parse any digits
						} else if (isdigit(buffer[i])) {
							num = num * 10 + (buffer[i] - '0');
							i++;
							continue;
						}

						// Break on all other characters
						break;
					}

					// Parse the command
					if (i < length) {
						char command = buffer[i++];

						switch (command) {
							// Move the cursor to the left
							case 'D':
								cursor -= (num > 0) ? num : 1;

								// Don't go too far left
								if (cursor < 0)
									cursor = 0;
								break;

							// Move the cursor to the right
							case 'C':
								cursor += (num > 0) ? num : 1;

								// Don't go too far right
								if (cursor >= length)
									cursor = length - 1;
								break;

							// Move to a certain column
							case 'G':
								cursor = (num > 0) ? num - 1 : 0;

								// Don't go too far right
								if (cursor >= length)
									cursor = length - 1;
								break;

							// Ignore any other sequences
							default:
								break;
						}
					}
				}
				break;

			// Backspace
			case '\x08':
				if (cursor > 0) {
					cursor--;

					// Erase the previous character
					line[cursor] = ' ';
				}
				i++;
				break;

			// Carriage Return
			case '\x0d':
				// Erase everything up to the cursor
				memset(line, '\0', cursor);

				cursor = 0;
				i++;
				break;

			// Normal characters
			default:
				if (cursor < line_length - 1)
					line[cursor++] = buffer[i];
				i++;
				break;
		}
	}

	return 0;
}

static ssize_t pakfire_pty_send_line(struct pakfire_pty* self,
		struct pakfire_pty_stdio* stdio, const char* buffer, ssize_t length) {
	char line[MAX_LINE_LENGTH] = {};
	int r;

	// Sanitize the line
	r = pakfire_pty_sanitize_line(self, line, buffer, length);
	if (r < 0)
		return r;

	// Call the callback
	r = stdio->callbacks.stdout_callback(self->ctx,
			stdio->callbacks.data, line, strlen(line));
	if (r < 0)
		return r;

	// We have consumed the entire line
	return length;
}

/*
	Writes as much data as possible from the buffer
*/
static int pakfire_pty_drain_buffer(struct pakfire_pty* pty, int fd, struct pakfire_pty_stdio* stdio) {
	ssize_t bytes_written = 0;
	char* eol = NULL;

	// Map any CRNL to just NL
	if (stdio->io & PAKFIRE_PTY_MAP_CRNL)
		pakfire_pty_map_crnl(stdio);

	// Call the callback if possible
	if (stdio->callbacks.stdout_callback) {
		// Try finding the end of a line
		eol = memchr(stdio->buffer, '\n', stdio->buffered);

		// No newline found
		if (!eol) {
			if (!pakfire_pty_buffer_is_full(pty, stdio))
				return -ENOMSG;

			DEBUG(pty->ctx, "Buffer is full. Sending all content\n");
			eol = stdio->buffer + stdio->buffered - 1;
		}

		// Send the line
		bytes_written = pakfire_pty_send_line(pty, stdio, stdio->buffer, eol - stdio->buffer + 1);

		// Abort on error
		if (bytes_written < 0)
			return bytes_written;

	// Otherwise we write to the file descriptor
	} else if (fd >= 0) {
		bytes_written = write(fd, stdio->buffer, stdio->buffered);

		// Abort on error
		if (bytes_written < 0)
			return -errno;
	}

	// Successful write
	memmove(stdio->buffer, stdio->buffer + bytes_written, stdio->buffered - bytes_written);
	stdio->buffered -= bytes_written;

	return bytes_written;
}

/*
	This function handles the forwarding between the different pipes...
*/
static int pakfire_pty_forward(struct pakfire_pty* pty) {
	int r;

	// Make sure we are in the correct state
	switch (pty->state) {
		case PAKFIRE_PTY_STATE_FORWARDING:
		case PAKFIRE_PTY_STATE_DRAINING:
			break;

		default:
			return -EINVAL;
	}

	while (
			((pty->stdin.io & PAKFIRE_PTY_READY_TO_READ) && !pakfire_pty_buffer_is_full(pty, &pty->stdin)) ||
			((pty->master.io & PAKFIRE_PTY_READY_TO_WRITE) && pakfire_pty_buffer_has_data(pty, &pty->stdin)) ||
			((pty->master.io & PAKFIRE_PTY_READY_TO_READ) && !pakfire_pty_buffer_is_full(pty, &pty->stdout)) ||
			((pty->stdout.io & PAKFIRE_PTY_READY_TO_WRITE) && pakfire_pty_buffer_has_data(pty, &pty->stdout))
		) {
		// DEBUG(pty->ctx, "PTY forward stdin=%x %zu, stdout=%x %zu, %x\n",
		//	pty->stdin.io, pty->stdin.buffered, pty->stdout.io, pty->stdout.buffered, pty->master.io);

		// Read from standard input
		if (pty->stdin.io & PAKFIRE_PTY_READY_TO_READ) {
			if (!pakfire_pty_buffer_is_full(pty, &pty->stdin)) {
				r = pakfire_pty_fill_buffer(pty, pty->stdin.fd, &pty->stdin);
				if (r < 0) {
					switch (-r) {
						case EAGAIN:
							pty->stdin.io &= ~PAKFIRE_PTY_READY_TO_READ;
							break;

						case EIO:
							pty->stdin.io |= PAKFIRE_PTY_HANGUP;
							break;

						default:
							ERROR(pty->ctx, "Failed reading from standard input: %s\n",
								strerror(-r));
							goto ERROR;
					}

				// EOF?
				} else if (r == 0) {
					DEBUG(pty->ctx, "Received EOF from standard input\n");

					// We are done reading
					pty->stdin.io &= ~PAKFIRE_PTY_READY_TO_READ;

					// And we have reached EOF
					pty->stdin.io |= PAKFIRE_PTY_EOF;
				}
			}
		}

		// Write to the master
		if (pty->master.io & PAKFIRE_PTY_READY_TO_WRITE) {
			if (pakfire_pty_buffer_has_data(pty, &pty->stdin)) {
				r = pakfire_pty_drain_buffer(pty, pty->master.fd, &pty->stdin);
				if (r < 0) {
					switch (-r) {
						case EAGAIN:
							pty->master.io &= ~PAKFIRE_PTY_READY_TO_WRITE;
							break;

						case EIO:
						case EPIPE:
						case ECONNRESET:
							pty->master.io |= PAKFIRE_PTY_HANGUP;
							break;

						default:
							ERROR(pty->ctx, "Failed writing to the PTY: %s\n", strerror(-r));
							goto ERROR;
					}
				}
			}

			// If the buffer is full drained and we may send EOF
			if (pty->stdin.io & PAKFIRE_PTY_EOF) {
				if (pakfire_pty_buffer_is_empty(pty, &pty->stdin)) {
					r = pakfire_pty_send_eof(pty, pty->master.fd);
					if (r < 0)
						goto ERROR;

					// Don't send EOF again
					pty->stdin.io &= ~PAKFIRE_PTY_EOF;
				}
			}
		}

		// Read from the master
		if (pty->master.io & PAKFIRE_PTY_READY_TO_READ) {
			if (!pakfire_pty_buffer_is_full(pty, &pty->stdout)) {
				r = pakfire_pty_fill_buffer(pty, pty->master.fd, &pty->stdout);
				if (r < 0) {
					switch (-r) {
						case EAGAIN:
							pty->master.io &= ~PAKFIRE_PTY_READY_TO_READ;
							break;

						case EIO:
						case EPIPE:
						case ECONNRESET:
							pty->master.io |= PAKFIRE_PTY_HANGUP;
							break;

						default:
							ERROR(pty->ctx, "Failed reading from the PTY: %s\n", strerror(-r));
							goto ERROR;
					}
				}
			}
		}

		// Write to standard output
		if (pty->stdout.io & PAKFIRE_PTY_READY_TO_WRITE) {
			if (pakfire_pty_buffer_has_data(pty, &pty->stdout)) {
				r = pakfire_pty_drain_buffer(pty, pty->stdout.fd, &pty->stdout);
				if (r < 0) {
					switch (-r) {
						case EAGAIN:
							pty->stdout.io &= ~PAKFIRE_PTY_READY_TO_WRITE;
							break;

						case EIO:
							pty->stdout.io |= PAKFIRE_PTY_HANGUP;
							break;

						// This is a special hack for when we have data in the buffer
						// but cannot send it to the callback, yet, because we have not
						// read to the end of the line.
						// To avoid getting stuck in this loop for forever, we simply
						// exit and let the loop call us again.
						case ENOMSG:
							return 0;

						default:
							ERROR(pty->ctx, "Failed writing to standard output: %s\n", strerror(-r));
							goto ERROR;
					}
				}
			}
		}

		if ((pty->master.io|pty->stdin.io|pty->stdout.io) & PAKFIRE_PTY_HANGUP)
			return pakfire_pty_done(pty, 0);
	}

	// If we have been requested to drain, and are fully drained, we are done
	if (pty->state == PAKFIRE_PTY_STATE_DRAINING && pakfire_pty_drained(pty))
		return pakfire_pty_done(pty, 0);

	return 0;

ERROR:
	// Terminate if we have encountered an error
	return pakfire_pty_done(pty, r);
}

/*
	Forwards any window size changes.
*/
static int pakfire_pty_SIGWINCH(sd_event_source* source, const struct signalfd_siginfo* si, void* data) {
	struct pakfire_pty* pty = data;
	struct winsize size;
	int r;

	// Don't do anything if we have not been set up, yet
	if (pty->stdout.fd < 0 || pty->master.fd < 0)
		return 0;

	// Fetch the new window size
	r = ioctl(pty->stdout.fd, TIOCGWINSZ, &size);
	if (r < 0)
		return 0;

	// Set the new window size
	r = ioctl(pty->master.fd, TIOCSWINSZ, &size);
	if (r < 0)
		return 0;

	return 0;
}

static int pakfire_pty_activity(struct pakfire_pty* pty, struct pakfire_pty_stdio* stdio, uint32_t events) {
	// Do we have data to read?
	if (events & (EPOLLIN|EPOLLHUP))
		stdio->io |= PAKFIRE_PTY_READY_TO_READ;

	// Do we have data to write?
	if (events & (EPOLLOUT|EPOLLHUP))
		stdio->io |= PAKFIRE_PTY_READY_TO_WRITE;

	// Did the file descriptor get closed?
	if (events & EPOLLHUP)
		stdio->io |= PAKFIRE_PTY_HANGUP;

	return pakfire_pty_forward(pty);
}

static int pakfire_pty_master(sd_event_source* source, int fd, uint32_t events, void* data) {
	struct pakfire_pty* pty = data;

	return pakfire_pty_activity(pty, &pty->master, events);
}

static int pakfire_pty_stdin(sd_event_source* source, int fd, uint32_t events, void* data) {
	struct pakfire_pty* pty = data;

	return pakfire_pty_activity(pty, &pty->stdin, events);
}

static int pakfire_pty_stdout(sd_event_source* source, int fd, uint32_t events, void* data) {
	struct pakfire_pty* pty = data;

	return pakfire_pty_activity(pty, &pty->stdout, events);
}

static void pakfire_pty_make_raw(struct termios* termios) {
	// Set input flags
	termios->c_iflag |= (
		// Ignore framing errors and parity errors
		IGNPAR
	);

	// Clear input flags
	termios->c_iflag &= ~(
		// Strip off eighth bit
		ISTRIP |

		// Translate NL to CR on input
		INLCR |

		// Ignore carriage return on input
		IGNCR |

		// Translate carriage return to newline on input (unless IGNCR is set)
		ICRNL |

		// Enable XON/XOFF flow control on output
		IXON |

		// Typing any character will restart stopped output
		IXANY |

		// Enable XON/XOFF flow control on input
		IXOFF |

		// Map uppercase characters to lowercase on input
		IUCLC
	);

	// Clear local flags
	termios->c_lflag &= ~(
		//  Send the SIGTTOU signal to the process group of a background process
		// which tries to write to its controlling terminal
		TOSTOP |

		// When any of the characters INTR, QUIT, SUSP, or DSUSP are received,
		// generate the corresponding signal
		ISIG |

		// Enable canonical mode
		ICANON |

		// Echo input characters
		ECHO |

		// If ICANON is also set, the ERASE character erases the preceding input character,
		// and WERASE erases the preceding word.
		ECHOE |

		// If ICANON is also set, the KILL character erases the current line
		ECHOK |

		// If ICANON is also set, echo the NL character even if ECHO is not set.
		ECHONL |

		// Enable implementation-defined input processing. This flag, as well as ICANON
		// must be enabled for the special characters EOL2, LNEXT, REPRINT, WERASE to be
		// interpreted, and for the IUCLC flag to be effective.
		IEXTEN
	);

	// Set output flags
	termios->c_oflag |= (
		// Map NL to CR-NL on output
		ONLCR |

		// Enable implementation-defined output processing
		OPOST
	);
}

static int pakfire_pty_enable_raw_mode(struct pakfire_pty* pty) {
	struct termios raw_attrs;
	int same;
	int r;

	DEBUG(pty->ctx, "Enabling raw mode\n");

	r = pakfire_pty_same_inode(pty, pty->stdin.fd, pty->stdout.fd);
	if (r < 0)
		return r;

	// Are standard input/output the same inode?
	same = (r > 0);

	// Store attributes for standard input
	if (pty->stdin.fd >= 0) {
		r = pakfire_pty_store_attrs(pty, &pty->stdin);
		if (r < 0)
			return r;

		// Copy the attributes
		raw_attrs = pty->stdin.attrs;

		// Enable raw mode
		pakfire_pty_make_raw(&raw_attrs);

		if (!same)
			raw_attrs.c_oflag = pty->stdin.attrs.c_oflag;

		// Restore the attributes
		r = tcsetattr(pty->stdin.fd, TCSANOW, &raw_attrs);
		if (r) {
			ERROR(pty->ctx, "Could not restore terminal attributes for fd %d: %s\n",
				pty->stdin.fd, strerror(errno));
			return -errno;
		}
	}

	// Store attributes for standard output
	if (!same && pty->stdout.fd >= 0) {
		r = pakfire_pty_store_attrs(pty, &pty->stdout);
		if (r < 0)
			return r;

		// Copy the attributes
		raw_attrs = pty->stdout.attrs;

		// Enable raw mode
		pakfire_pty_make_raw(&raw_attrs);

		raw_attrs.c_iflag = pty->stdout.attrs.c_iflag;
		raw_attrs.c_lflag = pty->stdout.attrs.c_lflag;

		// Restore the attributes
		r = tcsetattr(pty->stdout.fd, TCSANOW, &raw_attrs);
		if (r) {
			ERROR(pty->ctx, "Could not restore terminal attributes for fd %d: %s\n",
				pty->stdout.fd, strerror(errno));
			return -errno;
		}
	}

	return 0;
}

static int pakfire_pty_reopen(struct pakfire_pty* pty, int fd, int flags) {
	char path[PATH_MAX];
	int existing_flags;
	off_t offset;
	int r;

	// Make the path
	r = pakfire_string_format(path, "/proc/self/fd/%d", fd);
	if (r < 0)
		return r;

	// Fetch flags
	existing_flags = fcntl(fd, F_GETFL);
	if (existing_flags < 0)
		return -errno;

	// Apply O_APPEND if set before
	flags |= existing_flags & O_APPEND;

	// Fetch the offset
	offset = lseek(fd, 0, SEEK_CUR);

	// Duplicate the file descriptor
	fd = open(path, flags);
	if (fd < 0)
		return fd;

	DEBUG(pty->ctx, "Re-opened %s as fd %d\n", path, fd);

	// Try to re-apply the offset to the new file descriptor
	if (offset > 0) {
		r = lseek(fd, offset, SEEK_SET);
		if (r < 0)
			DEBUG(pty->ctx, "Failed to apply offset: %m\n");
	}

	return fd;
}

/*
	Sets up PTY forwarding...
*/
static int pakfire_pty_setup_forwarding(struct pakfire_pty* pty) {
	struct winsize size;
	int r;

	DEBUG(pty->ctx, "Setting up PTY Forwarding...\n");

	// Mark as forwarding
	pty->state = PAKFIRE_PTY_STATE_FORWARDING;

	// Connect to standard input
	if (pty->flags & PAKFIRE_PTY_CONNECT_STDIN) {
		pty->stdin.fd = pakfire_pty_reopen(pty, STDIN_FILENO, O_RDONLY|O_CLOEXEC|O_NOCTTY|O_NONBLOCK);
		if (pty->stdin.fd < 0) {
			DEBUG(pty->ctx, "Could not re-open standard input: %s. Ignoring.\n", strerror(-pty->stdin.fd));

			// Use the original file descriptor
			pty->stdin.fd = STDIN_FILENO;

		// Request to close the file descriptor afterwards
		} else {
			pty->stdin.close_fd = 1;
		}
	}

	// Create a buffer to capture the output in
	if (pakfire_pty_has_flag(pty, PAKFIRE_PTY_CAPTURE_OUTPUT)) {
		pty->stdout.fd = memfd_create("pty-output", MFD_CLOEXEC);
		if (pty->stdout.fd < 0) {
			ERROR(pty->ctx, "Could not create the output buffer: %m\n");
			return -errno;
		}

		// Map any CRNL to NL
		pty->stdout.io |= PAKFIRE_PTY_MAP_CRNL;

		// Close the buffer in the end
		pty->stdout.close_fd = 1;

	// Connect to standard output
	} else if (pty->flags & PAKFIRE_PTY_CONNECT_STDOUT) {
		pty->stdout.fd = pakfire_pty_reopen(pty, STDOUT_FILENO, O_WRONLY|O_CLOEXEC|O_NOCTTY|O_NONBLOCK);
		if (pty->stdout.fd < 0) {
			DEBUG(pty->ctx, "Could not re-open standard output: %s. Ignoring.\n", strerror(-pty->stdout.fd));

			// Use the original file descriptor
			pty->stdout.fd = STDOUT_FILENO;

		// Request to close the file descriptor afterwards
		} else {
			pty->stdout.close_fd = 1;
		}
	}

	// Copy the terminal dimensions to the PTY
	if (isatty(pty->stdout.fd)) {
		// Fetch dimensions
		r = ioctl(pty->stdout.fd, TIOCGWINSZ, &size);
		if (r) {
			ERROR(pty->ctx, "Failed to determine terminal dimensions: %s\n", strerror(errno));
			return -errno;
		}

		// Set dimensions
		r = ioctl(pty->master.fd, TIOCSWINSZ, &size);
		if (r) {
			ERROR(pty->ctx, "Failed setting dimensions: %s\n", strerror(errno));
			return -errno;
		}
	}

	// Connect standard input unless we are in read-only mode
	if (pty->stdin.fd >= 0) {
		// Enable RAW mode
		r = pakfire_pty_enable_raw_mode(pty);
		if (r)
			return r;

		// Add standard input to the event loop
		r = sd_event_add_io(pty->loop, &pty->stdin.event,
				pty->stdin.fd, EPOLLIN|EPOLLET, pakfire_pty_stdin, pty);
		if (r)
			return r;

		// Set description
		sd_event_source_set_description(pty->stdin.event, "pty-stdin");
	}

	// Add standard output to the event loop
	if (pty->stdout.fd >= 0) {
		r = sd_event_add_io(pty->loop, &pty->stdout.event,
				pty->stdout.fd, EPOLLOUT|EPOLLET, pakfire_pty_stdout, pty);
		if (r < 0) {
			switch (-r) {
				case EPERM:
					pty->stdout.io |= PAKFIRE_PTY_READY_TO_WRITE;
					break;

				default:
					ERROR(pty->ctx,
						"Could not add standard output to the event loop: %s\n", strerror(-r));
					return r;
			}
		}

		// Set description
		sd_event_source_set_description(pty->stdout.event, "pty-stdout");
	}

	return 0;
}

/*
	Sends the master file descriptor to the parent process...
*/
static int pakfire_pty_send_master(struct pakfire_pty* pty) {
	const size_t payload_length = sizeof(pty->master.fd);
	char buffer[CMSG_SPACE(payload_length)];
	int r;

	DEBUG(pty->ctx, "Sending fd %d to parent\n", pty->master.fd);

	// Header
	struct msghdr msg = {
		.msg_control    = buffer,
		.msg_controllen = sizeof(buffer),
	};

	// Payload
	struct cmsghdr* cmsg = CMSG_FIRSTHDR(&msg);
	cmsg->cmsg_level = SOL_SOCKET;
	cmsg->cmsg_type  = SCM_RIGHTS;
	cmsg->cmsg_len   = CMSG_LEN(payload_length);

	// Set payload
	*((int*)CMSG_DATA(cmsg)) = pty->master.fd;

	// Send the message
	r = sendmsg(pty->socket[1], &msg, 0);
	if (r) {
		ERROR(pty->ctx, "Could not send file descriptor: %s\n", strerror(errno));
		return -errno;
	}

	// We can close the socket now
	close(pty->socket[1]);
	pty->socket[1] = -EBADF;

	return 0;
}

/*
	Received the master file descriptor from the child process...
*/
static int pakfire_pty_recv_master(struct pakfire_pty* pty) {
	const size_t payload_length = sizeof(pty->master.fd);
	char buffer[CMSG_SPACE(payload_length)];
	int r;

	struct msghdr msg = {
		.msg_control    = buffer,
		.msg_controllen = sizeof(buffer),
	};

	// Receive the message
	r = recvmsg(pty->socket[0], &msg, 0);
	if (r) {
		ERROR(pty->ctx, "Could not receive file descriptor: %s\n", strerror(errno));
		return -errno;
	}

	// Fetch the payload
	struct cmsghdr* cmsg = CMSG_FIRSTHDR(&msg);
	if (!cmsg)
		return -EBADMSG;

	// Store the file descriptor
	pty->master.fd = *((int*)CMSG_DATA(cmsg));

	DEBUG(pty->ctx, "Received fd %d from socket %d\n", pty->master.fd, pty->socket[0]);

	// We can close the socket now
	close(pty->socket[0]);
	pty->socket[0] = -EBADF;

	return 0;
}

/*
	Called when the master socket is being received from the child process.
*/
static int pakfire_pty_setup(sd_event_source* source, int fd, uint32_t events, void* data) {
	struct pakfire_pty* pty = data;
	int r;

	// Receive the master file descriptor
	r = pakfire_pty_recv_master(pty);
	if (r)
		return r;

	// Add the master file descriptor to the event loop
	r = sd_event_add_io(pty->loop, &pty->master.event,
			pty->master.fd, EPOLLIN|EPOLLOUT|EPOLLET, pakfire_pty_master, pty);
	if (r < 0) {
		ERROR(pty->ctx, "Could not add the master file descriptor: %s\n", strerror(-r));
		return -errno;
	}

	// Set description
	sd_event_source_set_description(pty->master.event, "pty-master");

	// Listen to SIGWINCH
	r = sd_event_add_signal(pty->loop, &pty->sigwinch_event,
			SIGWINCH|SD_EVENT_SIGNAL_PROCMASK, pakfire_pty_SIGWINCH, pty);
	if (r < 0) {
		ERROR(pty->ctx, "Could not register SIGWINCH: %s\n", strerror(-r));
		return -errno;
	}

	// Setup forwarding
	r = pakfire_pty_setup_forwarding(pty);
	if (r < 0)
		return r;

	return 0;
}

static void pakfire_pty_free(struct pakfire_pty* pty) {
	pakfire_pty_disconnect(pty);

	if (pty->socket[0] >= 0)
		close(pty->socket[0]);
	if (pty->socket[1] >= 0)
		close(pty->socket[1]);

	// Output
	if (pty->output.iov_base)
		munmap(pty->output.iov_base, pty->output.iov_len);

	if (pty->loop)
		sd_event_unref(pty->loop);
	if (pty->ctx)
		pakfire_ctx_unref(pty->ctx);
	free(pty);
}

int pakfire_pty_create(struct pakfire_pty** pty, struct pakfire_ctx* ctx,
		sd_event* loop, int flags) {
	struct pakfire_pty* p = NULL;
	int r;

	// Allocate a new object
	p = calloc(1, sizeof(*p));
	if (!p)
		return -errno;

	// Initialize the reference counter
	p->nrefs = 1;

	// Store a reference to the context
	p->ctx = pakfire_ctx_ref(ctx);

	// Store a reference to the event loop
	p->loop = sd_event_ref(loop);

	// Store the flags
	p->flags = flags;

	if (pakfire_pty_has_flag(p, PAKFIRE_PTY_CAPTURE_OUTPUT))
		p->flags |= PAKFIRE_PTY_CONNECT_STDOUT | PAKFIRE_PTY_CONNECT_STDERR;

	// Initialize the master file descriptor
	p->master.fd = -EBADF;

	// Initialize standard input/output
	p->stdin.fd  = -EBADF;
	p->stdout.fd = -EBADF;

	// Create a UNIX domain socket
	r = socketpair(AF_UNIX, SOCK_DGRAM|SOCK_CLOEXEC, 0, p->socket);
	if (r < 0) {
		ERROR(p->ctx, "Could not create a UNIX socket: %m\n");
		r = -errno;
		goto ERROR;
	}

	// Register the socket to receive the master socket
	r = sd_event_add_io(p->loop, NULL, p->socket[0], EPOLLIN|EPOLLHUP,
		pakfire_pty_setup, p);
	if (r < 0) {
		ERROR(p->ctx, "Could not listen to socket: %s\n", strerror(-r));
		r = -errno;
		goto ERROR;
	}

	// Return the pointer
	*pty = p;

	return 0;

ERROR:
	if (p)
		pakfire_pty_free(p);

	return r;
}

struct pakfire_pty* pakfire_pty_ref(struct pakfire_pty* pty) {
	++pty->nrefs;

	return pty;
}

struct pakfire_pty* pakfire_pty_unref(struct pakfire_pty* pty) {
	if (--pty->nrefs > 0)
		return pty;

	pakfire_pty_free(pty);
	return NULL;
}

static int pakfire_pty_connect_null(struct pakfire_pty* pty, int fileno) {
	int fd = -EBADF;
	int flags = 0;
	int r;

	switch (fileno) {
		case STDIN_FILENO:
			flags |= O_RDONLY;
			break;

		case STDOUT_FILENO:
		case STDERR_FILENO:
			flags |= O_WRONLY;
			break;

		default:
			return -EINVAL;
	}


	// Open /dev/null
	fd = open("/dev/null", flags);
	if (fd < 0) {
		ERROR(pty->ctx, "Failed to open /dev/null: %m\n");
		r = -errno;
		goto ERROR;
	}

	// Copy to the desired file descriptor
	r = dup2(fd, fileno);
	if (r < 0) {
		ERROR(pty->ctx, "Failed to duplicate the file descriptor: %m\n");
		r = -errno;
		goto ERROR;
	}

ERROR:
	if (fd >= 0)
		close(fd);

	return r;
}

/*
	Sets up the terminal in the child process...
*/
static int pakfire_pty_setup_terminal(struct pakfire_pty* pty) {
	int fd = -EBADF;
	int r;

	// Open a new terminal
	fd = open("/dev/console", O_RDWR|O_NOCTTY);
	if (fd < 0) {
		ERROR(pty->ctx, "Failed to open a new terminal: %s\n", strerror(errno));
		r = -errno;
		goto ERROR;
	}

	DEBUG(pty->ctx, "Opened a new terminal %d\n", fd);

	// Connect the new terminal to standard input
	if (pakfire_pty_has_flag(pty, PAKFIRE_PTY_CONNECT_STDIN)) {
		r = dup2(fd, STDIN_FILENO);
		if (r < 0) {
			ERROR(pty->ctx, "Failed to open standard input: %s\n", strerror(errno));
			r = -errno;
			goto ERROR;
		}

	// Otherwise we connect standard input to /dev/null
	} else {
		r = pakfire_pty_connect_null(pty, STDIN_FILENO);
		if (r < 0)
			goto ERROR;
	}

	// Connect the new terminal to standard output
	if (pakfire_pty_has_flag(pty, PAKFIRE_PTY_CONNECT_STDOUT)) {
		r = dup2(fd, STDOUT_FILENO);
		if (r < 0) {
			ERROR(pty->ctx, "Failed to open standard output: %s\n", strerror(errno));
			r = -errno;
			goto ERROR;
		}

	// Otherwise we connect standard output to /dev/null
	} else {
		r = pakfire_pty_connect_null(pty, STDOUT_FILENO);
		if (r < 0)
			goto ERROR;
	}

	// Connect the new terminal to standard error
	if (pakfire_pty_has_flag(pty, PAKFIRE_PTY_CONNECT_STDERR)) {
		r = dup2(fd, STDERR_FILENO);
		if (r < 0) {
			ERROR(pty->ctx, "Failed to open standard error: %s\n", strerror(errno));
			r = -errno;
			goto ERROR;
		}

	// Otherwise we connect standard error to /dev/null
	} else {
		r = pakfire_pty_connect_null(pty, STDERR_FILENO);
		if (r < 0)
			goto ERROR;
	}

ERROR:
	if (fd >= 0)
		close(fd);

	return r;
}

/*
	Allocates a new PTY and must be called from the child process...
*/
int pakfire_pty_open(struct pakfire_pty* pty) {
	int r;

	// Allocate a new PTY
	pty->master.fd = posix_openpt(O_RDWR|O_NONBLOCK|O_NOCTTY|O_CLOEXEC);
	if (pty->master.fd < 0)
		return -errno;

	// Fetch the path
	r = ptsname_r(pty->master.fd, pty->path, sizeof(pty->path));
	if (r)
		return -r;

	DEBUG(pty->ctx, "Allocated console at %s (%d)\n", pty->path, pty->master.fd);

	// Unlock the master device
	r = unlockpt(pty->master.fd);
	if (r) {
		ERROR(pty->ctx, "Could not unlock the PTY: %s\n", strerror(errno));
		return -errno;
	}

	// Create a symlink
	r = pakfire_symlink(pty->ctx, pty->path, "/dev/console");
	if (r)
		return r;

	// Send the master to the parent process
	r = pakfire_pty_send_master(pty);
	if (r)
		return r;

	// Setup the terminal
	r = pakfire_pty_setup_terminal(pty);
	if (r)
		return r;

	// We are done with the master and close it now
	close(pty->master.fd);
	pty->master.fd = -EBADF;

	return 0;
}

int pakfire_pty_drain(struct pakfire_pty* pty) {
	pty->state = PAKFIRE_PTY_STATE_DRAINING;

	return pakfire_pty_drained(pty);
}

/*
	Creates an independent copy of the output buffer
*/
char* pakfire_pty_output(struct pakfire_pty* pty, size_t* length) {
	char* buffer = NULL;

	// Is not operation supported?
	if (!pakfire_pty_has_flag(pty, PAKFIRE_PTY_CAPTURE_OUTPUT)) {
		errno = -ENOTSUP;
		return NULL;
	}

	if (!pty->output.iov_base)
		return NULL;

	// Allocate a new buffer
	buffer = calloc(pty->output.iov_len + 1, sizeof(*buffer));
	if (!buffer)
		return NULL;

	// Copy the output
	memcpy(buffer, pty->output.iov_base, pty->output.iov_len);

	// Return the length
	if (length)
		*length = pty->output.iov_len;

	return buffer;
}

/*
	Standard Input/Output Callbacks
*/
void pakfire_pty_set_stdin_callback(struct pakfire_pty* pty,
		pakfire_pty_stdin_callback callback, void* data) {
	pty->stdin.callbacks.stdin_callback = callback;
	pty->stdin.callbacks.data           = data;

	// We are now ready to read
	pty->stdin.io |= PAKFIRE_PTY_READY_TO_READ;
}

void pakfire_pty_set_stdout_callback(struct pakfire_pty* pty,
		pakfire_pty_stdout_callback callback, void* data) {
	pty->stdout.callbacks.stdout_callback = callback;
	pty->stdout.callbacks.data            = data;

	// We are now ready to write
	pty->stdout.io |= PAKFIRE_PTY_READY_TO_WRITE|PAKFIRE_PTY_MAP_CRNL;

	// Actually connect standard output and error
	pty->flags |= PAKFIRE_PTY_CONNECT_STDOUT | PAKFIRE_PTY_CONNECT_STDERR;
}

ssize_t pakfire_pty_send_buffer(struct pakfire_ctx* ctx,
		void* data, char* buffer, size_t length) {
	struct pakfire_pty_buffer* input = data;

	// Check input
	if (!input)
		return -EINVAL;

	// If there is nothing left to send we are done
	if (!input->length)
		return 0;

	// Cap length if we have less data to send
	if (input->length < length)
		length = input->length;

	// Copy the data
	memcpy(buffer, input->data, length);

	// Advance the buffer
	input->data += length;
	input->length -= length;

	return length;
}

ssize_t pakfire_pty_send_filelist(struct pakfire_ctx* ctx,
		void* data, char* buffer, size_t length) {
	struct pakfire_pty_filelist* input = data;
	struct pakfire_file* file = NULL;
	int r;

	// If there is any path data left, we send that first
	if (input->p) {
		// How much data do we have left?
		size_t l = strlen(input->p);

		// Cap the length of the buffer
		if (l < length)
			length = l;

		memcpy(buffer, input->p, length);

		// If we could not send all data, we will move the pointer forward
		if (l > length)
			input->p += length;

		// If we have sent all data, we reset the pointer
		else if (l == length)
			input->p = NULL;

		return length;
	}

	// Read the next file
	file = pakfire_filelist_get(input->filelist, input->i++);

	// If we could not fetch a file, we have reached the end of the list
	if (!file) {
		// Reset the counter so we can run again
		input->i = 0;

		return 0;
	}

	// Fetch the path
	const char* path = pakfire_file_get_path(file);
	if (!path) {
		r = -EINVAL;
		goto ERROR;
	}

	// Copy the path to the buffer
	r = pakfire_string_format(input->buffer, "%s\n", path);
	if (r < 0)
		goto ERROR;

	// Set the pointer to the start
	input->p = input->buffer;

	// Free the file
	pakfire_file_unref(file);

	// Send the buffer
	return pakfire_pty_send_filelist(ctx, input, buffer, length);

ERROR:
	if (file)
		pakfire_file_unref(file);

	return r;
}
