/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <linux/limits.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/personality.h>
#include <sys/utsname.h>

#include <pakfire/arch.h>
#include <pakfire/constants.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

struct pakfire_arch {
	const char* name;
	const char* platform;
	const char* compatible[5];
	unsigned long personality;
	const char magic[41];
};

static const struct pakfire_arch PAKFIRE_ARCHES[] = {
	// x86
	{
		.name = "x86_64",
		.platform = "x86",
		.personality = PER_LINUX,
		.magic = "7f454c4602010100000000000000000002003e00",
	},

	// ARM
	{
		.name = "aarch64",
		.platform = "arm",
		.personality = PER_LINUX,
		.magic = "7f454c460201010000000000000000000200b700",
	},

	// RISC-V
	{
		.name = "riscv64",
		.platform = "riscv",
		.personality = PER_LINUX,
		.magic = "7f454c460201010000000000000000000200f300",
	},

	// The end
	{ NULL },
};

static const struct pakfire_arch* pakfire_arch_find(const char* name) {
	for (const struct pakfire_arch* arch = PAKFIRE_ARCHES; arch->name; arch++) {
		if (pakfire_string_equals(arch->name, name))
			return arch;
	}

	return NULL;
}

int pakfire_arch_supported(const char* name) {
	const struct pakfire_arch* arch = pakfire_arch_find(name);

	if (arch)
		return 1;

	return 0;
}

static size_t pakfire_num_arches(void) {
	size_t i = 0;

	for (const struct pakfire_arch* arch = PAKFIRE_ARCHES; arch->name; arch++)
		i++;

	return i;
}

const char** pakfire_supported_arches(void) {
	const struct pakfire_arch* arch = NULL;
	static const char** arches = NULL;

	if (!arches) {
		// Count how many architectures we have
		const size_t num_arches = pakfire_num_arches();
		unsigned int i = 0;

		// Allocate a new array
		arches = calloc(num_arches + 1, sizeof(*arches));
		if (!arches)
			return NULL;

		// Copy all architectures
		for (arch = PAKFIRE_ARCHES; arch->name; arch++)
			arches[i++] = arch->name;
	}

	return arches;
}

const char* pakfire_arch_platform(const char* name) {
	const struct pakfire_arch* arch = pakfire_arch_find(name);

	if (arch && arch->platform)
		return arch->platform;

	return NULL;
}

unsigned long pakfire_arch_personality(const char* name) {
	const struct pakfire_arch* arch = pakfire_arch_find(name);

	if (arch)
		return arch->personality;

	return 0;
}

int __pakfire_arch_machine(char* buffer, size_t length, const char* arch, const char* vendor) {
	char buildtarget[1024];

	// Fetch buildtarget
	int r = __pakfire_arch_buildtarget(buildtarget, sizeof(buildtarget), arch, vendor);
	if (r)
		return r;

	// Append -gnu
	return __pakfire_string_format(buffer, length, "%s-gnu", buildtarget);
}

int __pakfire_arch_buildtarget(char* buffer, size_t length, const char* arch, const char* vendor) {
	if (!vendor)
		vendor = "unknown";

	// Determine the length of the vendor string
	int vendor_length = strlen(vendor);

	// Cut off suffix if it contains spaces
	char* space = strchr(vendor, ' ');
	if (space)
		vendor_length = space - vendor;

	// Format string
	int r = __pakfire_string_format(buffer, length, "%s-%.*s-linux",
		arch, vendor_length, vendor);
	if (r)
		return r;

	// Make everything lowercase
	for (char* p = buffer; *p; p++)
		*p = tolower(*p);

	return 0;
}

const char* pakfire_arch_native(void) {
	static struct utsname utsname = {};

	if (!*utsname.machine) {
		if (uname(&utsname) < 0)
			return NULL;
	}

	return utsname.machine;
}

int pakfire_arch_is_compatible(const char* name, const char* compatible_arch) {
	if (!name || !compatible_arch)
		return -EINVAL;

	// Every architecture is compatible with itself
	if (pakfire_string_equals(name, compatible_arch))
		return 1;

	const struct pakfire_arch* arch = pakfire_arch_find(name);
	if (!arch)
		return 0;

	for (unsigned int i = 0; arch->compatible[i]; i++) {
		if (pakfire_string_equals(arch->compatible[i], compatible_arch))
			return 1;
	}

	return 0;
}

/*
	This function figures out which architecture the build environment has -
	which might not be the same as the requested architecture.
*/
static const char* pakfire_arch_is_natively_supported_by_host(const char* name) {
	if (!name) {
		errno = EINVAL;
		return NULL;
	}

	const char* native_arch = pakfire_arch_native();

	// All hosts support noarch natively
	if (pakfire_string_equals(name, "noarch"))
		return native_arch;

	// Check if those two architectures are compatible
	if (pakfire_arch_is_compatible(native_arch, name))
		return name;

	// Not supported
	return NULL;
}

const char* pakfire_arch_is_supported_by_host(const char* name) {
	const char* arch = NULL;

	// Check if we natively support this architecture
	arch = pakfire_arch_is_natively_supported_by_host(name);
	if (arch)
		return arch;

	// Otherwise check if we have an interpreter
	char* interpreter = pakfire_arch_find_interpreter(name);
	if (interpreter) {
		free(interpreter);

		return name;
	}

	// Otherwise this architecture is not supported
	return NULL;
}

static char* find_interpreter(DIR* dir, const char* path, const char* magic) {
	FILE* f = NULL;
	int fd = -1;

	// Open the file
	fd = openat(dirfd(dir), path, O_CLOEXEC);
	if (fd < 0)
		return NULL;

	// Re-open the file as file handle
	f = fdopen(fd, "r");
	if (!f)
		return NULL;

	char* line = NULL;
	size_t length = 0;

	int enabled = 0;
	int match = 0;
	char interpreter[PATH_MAX] = "";

	while (1) {
		ssize_t bytes_read = getline(&line, &length, f);
		if (bytes_read < 0)
			break;

		// Remove the newline
		pakfire_string_rstrip(line);

		// Look for the "enabled" line
		if (pakfire_string_equals("enabled", line)) {
			enabled = 1;

		// Store the interpreter for later
		} else if (pakfire_string_startswith(line, "interpreter ")) {
			pakfire_string_set(interpreter, line + strlen("interpreter "));

		// If we found the magic, we check if it is a match
		} else if (pakfire_string_startswith(line, "magic ")) {
			const char* m = line + strlen("magic ");

			if (pakfire_string_equals(magic, m))
				match = 1;
		}
	}

	// Free resources
	if (line)
		free(line);
	fclose(f);

	// Return the interpreter if it is a match
	if (enabled && match && *interpreter)
		return strdup(interpreter);

	// Otherwise return NULL
	return NULL;
}

char* pakfire_arch_find_interpreter(const char* name) {
	char* interpreter = NULL;
	DIR* dir = NULL;
	struct dirent* entry = NULL;

	// Check inputs
	if (!name) {
		errno = EINVAL;
		return NULL;
	}

	// If the host supports this architecture natively,
	// we do not need to search for the interpreter
	if (pakfire_arch_is_natively_supported_by_host(name))
		goto ERROR;

	const struct pakfire_arch* arch = pakfire_arch_find(name);
	if (!arch)
		goto ERROR;

	// Open /proc/sys/fs/binfmt_misc
	dir = opendir("/proc/sys/fs/binfmt_misc");
	if (!dir)
		goto ERROR;

	for (;;) {
		entry = readdir(dir);
		if (!entry)
			break;

		// Check if the file matches
		interpreter = find_interpreter(dir, entry->d_name, arch->magic);

		// End search if we have found a match
		if (interpreter)
			break;
	}

ERROR:
	if (dir)
		closedir(dir);

	return interpreter;
}
