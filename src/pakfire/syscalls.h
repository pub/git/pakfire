/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2024 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_SYSCALLS_H
#define PAKFIRE_SYSCALLS_H

#include <syscall.h>

static inline int clone3(struct clone_args* args, size_t size) {
	return syscall(__NR_clone3, args, size);
}

static inline int pidfd_send_signal(int pidfd, int sig, siginfo_t* info, unsigned int flags) {
	return syscall(SYS_pidfd_send_signal, pidfd, sig, info, flags);
}

#endif /* PAKFIRE_SYSCALLS_H */
