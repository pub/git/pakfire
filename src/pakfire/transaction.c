/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2013 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <ctype.h>
#include <errno.h>
#include <stdlib.h>

// libsolv
#include <solv/selection.h>
#include <solv/transaction.h>
#include <solv/solverdebug.h>

#include <pakfire/archive.h>
#include <pakfire/ctx.h>
#include <pakfire/db.h>
#include <pakfire/deps.h>
#include <pakfire/filelist.h>
#include <pakfire/hashes.h>
#include <pakfire/i18n.h>
#include <pakfire/jail.h>
#include <pakfire/logging.h>
#include <pakfire/package.h>
#include <pakfire/pakfire.h>
#include <pakfire/path.h>
#include <pakfire/repo.h>
#include <pakfire/string.h>
#include <pakfire/transaction.h>
#include <pakfire/util.h>

struct pakfire_transaction {
	struct pakfire_ctx* ctx;
	struct pakfire* pakfire;
	int nrefs;

	// Flags
	int flags;

	// The solver
	Solver* solver;

	// The jobs to solve
	Queue jobs;

	Transaction* transaction;
	char** userinstalled;

	struct pakfire_archive** archives;
	struct pakfire_package** packages;
	size_t num;
	size_t progress;

	// Callbacks
	struct pakfire_transaction_callbacks {
		// Status
		pakfire_status_callback status;
		void* status_data;
	} callbacks;

	// Changes for file conflicts
	struct pakfire_packagelist* installed_packages;
	struct pakfire_packagelist* new_packages;
};

enum pakfire_actions {
	PAKFIRE_ACTION_NOOP = 0,
	PAKFIRE_ACTION_VERIFY,
	PAKFIRE_ACTION_EXECUTE,
	PAKFIRE_ACTION_PRETRANS,
	PAKFIRE_ACTION_POSTTRANS,
};

enum pakfire_steps {
	PAKFIRE_STEP_UNKNOWN = 0,
	PAKFIRE_STEP_INSTALL,
	PAKFIRE_STEP_REINSTALL,
	PAKFIRE_STEP_REINSTALLED,
	PAKFIRE_STEP_ERASE,
	PAKFIRE_STEP_UPGRADE,
	PAKFIRE_STEP_UPGRADED,
	PAKFIRE_STEP_DOWNGRADE,
	PAKFIRE_STEP_DOWNGRADED,
};

static enum pakfire_steps pakfire_transaction_get_step_type(
		struct pakfire_transaction* transaction, struct pakfire_package* pkg) {
	const Id id = pakfire_package_id(pkg);

	const int flags = SOLVER_TRANSACTION_CHANGE_IS_REINSTALL;

	int type = transaction_type(transaction->transaction, id,
		SOLVER_TRANSACTION_SHOW_ACTIVE|flags);

	// If this step is being ignored in active mode, we try passive mode
	if (type == SOLVER_TRANSACTION_IGNORE)
		type = transaction_type(transaction->transaction, id, flags);

	// Translate solver types into our own types
	switch (type) {
		case SOLVER_TRANSACTION_INSTALL:
			return PAKFIRE_STEP_INSTALL;

		case SOLVER_TRANSACTION_REINSTALL:
			return PAKFIRE_STEP_REINSTALL;

		case SOLVER_TRANSACTION_REINSTALLED:
			return PAKFIRE_STEP_REINSTALLED;

		case SOLVER_TRANSACTION_ERASE:
			return PAKFIRE_STEP_ERASE;

		case SOLVER_TRANSACTION_UPGRADE:
			return PAKFIRE_STEP_UPGRADE;

		case SOLVER_TRANSACTION_UPGRADED:
			return PAKFIRE_STEP_UPGRADED;

		case SOLVER_TRANSACTION_DOWNGRADE:
			return PAKFIRE_STEP_DOWNGRADE;

		case SOLVER_TRANSACTION_DOWNGRADED:
			return PAKFIRE_STEP_DOWNGRADED;

		default:
			ERROR(transaction->ctx,
				"Unhandled step type 0x%x. Ignoring.\n", (unsigned int)type);

			return PAKFIRE_STEP_UNKNOWN;
	}
}

static void pakfire_transaction_free_archives_and_packages(
		struct pakfire_transaction* transaction) {
	if (transaction->archives) {
		for (unsigned int i = 0; i < transaction->num; i++)
			if (transaction->archives[i])
				pakfire_archive_unref(transaction->archives[i]);
		free(transaction->archives);

		transaction->archives = NULL;
	}

	if (transaction->packages) {
		for (unsigned int i = 0; i < transaction->num; i++)
			if (transaction->packages[i])
				pakfire_package_unref(transaction->packages[i]);
		free(transaction->packages);

		transaction->packages = NULL;
	}
}

static int pakfire_transaction_import_transaction(struct pakfire_transaction* transaction) {
	struct pakfire_package* pkg = NULL;
	int new_pkgs;
	Queue pkgs;
	int r = 0;

	// For all installed packages after applying this transaction
	queue_init(&pkgs);

	// Clone the transaction to keep a copy of it
	transaction->transaction = solver_create_transaction(transaction->solver);
	if (!transaction->transaction) {
		ERROR(transaction->ctx, "Could not create transaction from solver: %m\n");
		r = -errno;
		goto ERROR;
	}

	// Order the transaction
	transaction_order(transaction->transaction, 0);

	// Log the transaction
	transaction_print(transaction->transaction);

	// Free any previous content
	pakfire_transaction_free_archives_and_packages(transaction);

	// How many steps?
	transaction->num = transaction->transaction->steps.count;

	// Allocate space for packages
	transaction->packages = calloc(transaction->num, sizeof(*transaction->packages));
	if (!transaction->packages) {
		r = -errno;
		goto ERROR;
	}

	// Allocate space for archives
	transaction->archives = calloc(transaction->num, sizeof(*transaction->archives));
	if (!transaction->archives) {
		r = -errno;
		goto ERROR;
	}

	// Create all packages
	for (unsigned int i = 0; i < transaction->num; i++) {
		r = pakfire_package_create_from_solvable(&transaction->packages[i],
			transaction->pakfire, NULL, transaction->transaction->steps.elements[i]);
		if (r < 0)
			goto ERROR;
	}

	// Fetch a list of all packages that will be installed in the end
	new_pkgs = transaction_installedresult(transaction->transaction, &pkgs);

	// Store all newly installed and overall all installed packages
	for (int i = 0; i < pkgs.count; i++) {
		r = pakfire_package_create_from_solvable(&pkg,
				transaction->pakfire, NULL, pkgs.elements[i]);
		if (r < 0)
			goto ERROR;

		// Add the package to the total list of packages
		r = pakfire_packagelist_add(transaction->installed_packages, pkg);
		if (r < 0)
			goto ERROR;

		// If the package is being newly installed, we add it to that list, too
		if (i < new_pkgs) {
			r = pakfire_packagelist_add(transaction->new_packages, pkg);
			if (r < 0)
				goto ERROR;
		}

		pakfire_package_unref(pkg);
		pkg = NULL;
	}

ERROR:
	if (pkg)
		pakfire_package_unref(pkg);
	queue_free(&pkgs);

	return r;
}

static int pakfire_transaction_import_userinstalled(struct pakfire_transaction* transaction) {
	Queue userinstalled;
	const char* package = NULL;
	int r = 0;

	queue_init(&userinstalled);

	// Cleanup previous content
	if (transaction->userinstalled) {
		pakfire_strings_free(transaction->userinstalled);
		transaction->userinstalled = NULL;
	}

	// Fetch a list of all packages that are installed by the user
	solver_get_userinstalled(transaction->solver, &userinstalled, GET_USERINSTALLED_NAMES);

	// Skip everything if the queue is empty
	if (!userinstalled.count)
		goto OUT;

	transaction->userinstalled = calloc(userinstalled.count + 1, sizeof(*transaction->userinstalled));
	if (!transaction->userinstalled) {
		ERROR(transaction->ctx, "Could not allocate userinstalled: %m\n");
		r = -errno;
		goto ERROR;
	}

	Pool* pool = pakfire_get_solv_pool(transaction->pakfire);

	// Store the names of all userinstalled packages
	for (int i = 0; i < userinstalled.count; i++) {
		package = pool_id2str(pool, userinstalled.elements[i]);

		transaction->userinstalled[i] = strdup(package);

		if (!transaction->userinstalled[i]) {
			r = -errno;
			goto ERROR;
		}
	}

	// Success
	r = 0;

ERROR:
	if (transaction->userinstalled) {
		pakfire_strings_free(transaction->userinstalled);
		transaction->userinstalled = NULL;
	}

OUT:
	queue_free(&userinstalled);

	return r;
}

static void pakfire_transaction_default_status_callback(
		struct pakfire* pakfire, void* data, int progress, const char* status) {
	// XXX perform some default action...
}

static int pakfire_transaction_setup_solver(struct pakfire_transaction* transaction) {
	Pool* pool = pakfire_get_solv_pool(transaction->pakfire);

	// Allocate a new solver
	transaction->solver = solver_create(pool);
	if (!transaction->solver) {
		ERROR(transaction->ctx, "Could not allocate solver: %m\n");
		return -errno;
	}

	// Obey policy when searching for the best solvable
	solver_set_flag(transaction->solver, SOLVER_FLAG_BEST_OBEY_POLICY, 1);

	// Automatically update when installation is requested
	solver_set_flag(transaction->solver, SOLVER_FLAG_INSTALL_ALSO_UPDATES, 1);

#ifdef SOLVER_FLAG_FOCUS_NEW
	// By default, only update as many packages as necessary
	solver_set_flag(transaction->solver, SOLVER_FLAG_FOCUS_NEW, 1);
#endif

	// In build mode, we want to install the best versions
	if (pakfire_has_flag(transaction->pakfire, PAKFIRE_FLAGS_BUILD))
		solver_set_flag(transaction->solver, SOLVER_FLAG_FOCUS_BEST, 1);

	// Can the solver downgrade packages?
	if (transaction->flags & PAKFIRE_TRANSACTION_ALLOW_DOWNGRADE)
		solver_set_flag(transaction->solver, SOLVER_FLAG_ALLOW_DOWNGRADE, 1);

	// Can the solver uninstall packages?
	if (transaction->flags & PAKFIRE_TRANSACTION_ALLOW_UNINSTALL)
		solver_set_flag(transaction->solver, SOLVER_FLAG_ALLOW_UNINSTALL, 1);

	// Do not install any recommended packages
	if (transaction->flags & PAKFIRE_TRANSACTION_WITHOUT_RECOMMENDED)
		solver_set_flag(transaction->solver, SOLVER_FLAG_IGNORE_RECOMMENDED, 1);

	return 0;
}

int pakfire_transaction_create(struct pakfire_transaction** transaction,
		struct pakfire* pakfire, int flags) {
	int r;

	// Allocate the transaction
	struct pakfire_transaction* t = calloc(1, sizeof(*t));
	if (!t)
		return -errno;

	// Store a reference to the context
	t->ctx = pakfire_ctx(pakfire);

	// Store reference to Pakfire
	t->pakfire = pakfire_ref(pakfire);

	// Initialize the reference counter
	t->nrefs = 1;

	// Store flags
	t->flags = flags;

	// Allocate a job queue
	queue_init(&t->jobs);

	// Set the default status callback
	t->callbacks.status = pakfire_transaction_default_status_callback;

	// Setup the solver
	r = pakfire_transaction_setup_solver(t);
	if (r)
		goto ERROR;

	// Setup package lists
	r = pakfire_packagelist_create(&t->installed_packages, t->ctx);
	if (r < 0)
		goto ERROR;

	r = pakfire_packagelist_create(&t->new_packages, t->ctx);
	if (r < 0)
		goto ERROR;

	// Return the transaction
	*transaction = pakfire_transaction_ref(t);

ERROR:
	if (t)
		pakfire_transaction_unref(t);

	return r;
}

static void pakfire_transaction_free(struct pakfire_transaction* transaction) {
	pakfire_transaction_free_archives_and_packages(transaction);

	if (transaction->installed_packages)
		pakfire_packagelist_unref(transaction->installed_packages);
	if (transaction->new_packages)
		pakfire_packagelist_unref(transaction->new_packages);
	if (transaction->userinstalled)
		pakfire_strings_free(transaction->userinstalled);
	if (transaction->transaction)
		transaction_free(transaction->transaction);
	if (transaction->solver)
		solver_free(transaction->solver);
	if (transaction->pakfire)
		pakfire_unref(transaction->pakfire);
	if (transaction->ctx)
		pakfire_ctx_unref(transaction->ctx);
	queue_free(&transaction->jobs);
	free(transaction);
}

struct pakfire_transaction* pakfire_transaction_ref(
		struct pakfire_transaction* transaction) {
	transaction->nrefs++;

	return transaction;
}

struct pakfire_transaction* pakfire_transaction_unref(
		struct pakfire_transaction* transaction) {
	if (--transaction->nrefs > 0)
		return transaction;

	pakfire_transaction_free(transaction);
	return NULL;
}

static int pakfire_transaction_get_progress(struct pakfire_transaction* transaction) {
	return transaction->progress * 100 / transaction->num;
}

static void pakfire_transaction_status(struct pakfire_transaction* transaction,
	const char* message, ...) __attribute__((format(printf, 2, 3)));

static void pakfire_transaction_status(struct pakfire_transaction* transaction,
		const char* message, ...) {
	char* buffer = NULL;
	va_list args;
	int r;

	// Do nothing if callback isn't set
	if (!transaction->callbacks.status)
		return;

	// Format the message
	if (message) {
		va_start(args, message);
		r = vasprintf(&buffer, message, args);
		va_end(args);

		if (r < 0)
			return;
	}

	// Fetch progress
	const int progress = pakfire_transaction_get_progress(transaction);

	// Call the callback
	transaction->callbacks.status(transaction->pakfire,
		transaction->callbacks.status_data, progress, buffer);

	// Cleanup
	if (buffer)
		free(buffer);
}

struct pakfire_problem** pakfire_transaction_get_problems(
		struct pakfire_transaction* transaction) {
	struct pakfire_problem** problems = NULL;
	struct pakfire_problem* problem = NULL;
	unsigned int count = 0;
	Id id = ID_NULL;
	int r;

	// Count problems
	count = solver_problem_count(transaction->solver);

	// Return NULL if there are no problems
	if (!count)
		return NULL;

	// Allocate some space
	problems = calloc(count + 1, sizeof(*problems));
	if (!problems) {
		r = -errno;
		goto ERROR;
	}

	for (unsigned int i = 0; i < count; i++) {
		// Fetch the ID of the next problem
		id = solver_next_problem(transaction->solver, id);
		if (!id)
			break;

		// Create a new problem
		r = pakfire_problem_create(&problem, transaction->pakfire, transaction, id);
		if (r)
			goto ERROR;

		// Store the reference
		problems[i] = problem;
	}

	return problems;

ERROR:
	ERROR(transaction->ctx, "Could not import problems: %s\n", strerror(r));

	if (problems) {
		for (struct pakfire_problem** p = problems; *p; p++)
			pakfire_problem_unref(*p);
		free(problems);
	}

	return NULL;
}

static int pakfire_transaction_append_solutions(
		struct pakfire_transaction* transaction, char** buffer, struct pakfire_solution** solutions) {
	const char* s = NULL;
	int r;

	for (struct pakfire_solution** solution = solutions; *solution; solution++) {
		s = pakfire_solution_to_string(*solution);
		if (!s)
			return -errno;

		// Append the solution
		r = asprintf(buffer, "%s    - %s\n", *buffer, s);
		if (r < 0)
			return -errno;
	}

	return 0;
}

static int pakfire_transaction_append_problems(
		struct pakfire_transaction* transaction, char** buffer, struct pakfire_problem** problems, int flags) {
	struct pakfire_solution** solutions = NULL;
	const char* s = NULL;
	int r;

	for (struct pakfire_problem** problem = problems; *problem; problem++) {
		s = pakfire_problem_to_string(*problem);
		if (!s)
			return -errno;

		// Append the string to the buffer
		r = asprintf(buffer, "%s* %s\n", (*buffer) ? *buffer : "", s);
		if (r < 0)
			return -errno;

		// Show solutions
		if (flags & PAKFIRE_SOLVE_SHOW_SOLUTIONS) {
			// Fetch any solutions
			solutions = pakfire_problem_get_solutions(*problem);

			if (solutions) {
				r = pakfire_transaction_append_solutions(transaction, buffer, solutions);

				// Cleanup
				for (struct pakfire_solution** solution = solutions; *solution; solution++)
					pakfire_solution_unref(*solution);
				free(solutions);

				if (r)
					return r;
			}
		}
	}

	return 0;
}

static char* pakfire_transaction_get_problem_string(
		struct pakfire_transaction* transaction, int flags) {
	struct pakfire_problem** problems = NULL;
	char* buffer = NULL;
	int r;

	// Fetch any problems
	problems = pakfire_transaction_get_problems(transaction);

	// Show problems
	if (problems) {
		// Append all problems
		r = pakfire_transaction_append_problems(transaction, &buffer, problems, flags);
		if (r) {
			if (buffer)
				free(buffer);

			buffer = NULL;
		}

		// Cleanup
		if (problems) {
			for (struct pakfire_problem** problem = problems; *problem; problem++)
				pakfire_problem_unref(*problem);

			free(problems);
		}
	}

	return buffer;
}

Solver* pakfire_transaction_get_solver(struct pakfire_transaction* transaction) {
	return transaction->solver;
}

int pakfire_transaction_solve(struct pakfire_transaction* transaction,
		int flags, char** problems) {
	clock_t solving_start;
	clock_t solving_end;
	char* p = NULL;
	int solved = 0;
	int r;

	// XXX halt if the request has already been solved

	// Prepare pool
	pakfire_pool_internalize(transaction->pakfire);

	Pool* pool = pakfire_get_solv_pool(transaction->pakfire);

	const char* selection = pool_selection2str(pool, &transaction->jobs, 0);
	if (selection)
		DEBUG(transaction->ctx, "Solving: %s\n", selection);

	for (;;) {
		// Save time when we starting solving
		solving_start = clock();

		// Solve the request and get the number of problems
		r = solver_solve(transaction->solver, &transaction->jobs);

		// Save time when we finished solving
		solving_end = clock();

		DEBUG(transaction->ctx, "Solved request in %.4fms\n",
			(double)(solving_end - solving_start) * 1000 / CLOCKS_PER_SEC);

		switch (r) {
			// Solved!
			case 0:
				// Mark as solved
				solved = 1;

				// Import the transaction
				r = pakfire_transaction_import_transaction(transaction);
				if (r)
					goto ERROR;

				// Import userinstalled packages
				r = pakfire_transaction_import_userinstalled(transaction);
				if (r)
					goto ERROR;

#ifdef ENABLE_DEBUG
				// Print all decisions
				solver_printdecisions(transaction->solver);
#endif

				goto SOLVED;

			// Not Solved
			default:
#ifdef ENABLE_DEBUG
				// Print all solutions
				solver_printallsolutions(transaction->solver);
#endif

				// Fetch the reason why we could not solve the request
				p = pakfire_transaction_get_problem_string(transaction, flags);

				if (p) {
					DEBUG(transaction->ctx,
						"Could not solve request: %s\n%s\n", selection, p);

					// Return the problems
					if (problems)
						*problems = strdup(p);
				}

				// Ask the user to pick a solution
				r = pakfire_ctx_pick_solution(transaction->ctx, transaction->pakfire, transaction);
				switch (r) {
					case 0:
						continue;

					default:
						goto ERROR;
				}
				break;
		}
	}

SOLVED:
	// Return zero if solved, otherwise return non-zero
	if (solved)
		r = 0;
	else
		r = 2;

ERROR:
	if (p)
		free(p);

	return r;
}

static int pakfire_transaction_is_file(const char* what) {
	return pakfire_string_endswith(what, ".pfm");
}

static int __pakfire_transaction_add(struct pakfire_transaction* transaction,
		const enum pakfire_job_action action, const Id type, const Id id, int flags) {
	Id job = ID_NULL;

	// Check if type/ID is not set for actions that don't support it
	switch (action) {
		case PAKFIRE_JOB_INSTALL:
		case PAKFIRE_JOB_ERASE:
		case PAKFIRE_JOB_UPDATE:
		case PAKFIRE_JOB_LOCK:
			break;

		default:
			if (type || id)
				return -EINVAL;
	}

	// Essential jobs
	if (flags & PAKFIRE_JOB_ESSENTIAL)
		job |= SOLVER_ESSENTIAL;

	// Install the best package only?
	if (flags & PAKFIRE_JOB_BEST)
		job |= SOLVER_FORCEBEST;

	// Select the correct action
	switch (action) {
		case PAKFIRE_JOB_INSTALL:
			job |= SOLVER_INSTALL;
			break;

		case PAKFIRE_JOB_ERASE:
			job |= SOLVER_ERASE;

			// Should we keep any dependencies?
			if (!(flags & PAKFIRE_JOB_KEEP_DEPS))
				job |= SOLVER_CLEANDEPS;
			break;

		case PAKFIRE_JOB_UPDATE:
			job |= SOLVER_UPDATE;
			break;

		case PAKFIRE_JOB_UPDATE_ALL:
			job |= SOLVER_UPDATE|SOLVER_SOLVABLE_ALL;
			break;

		case PAKFIRE_JOB_SYNC:
			job |= SOLVER_DISTUPGRADE|SOLVER_SOLVABLE_ALL;

			// Drop orphans?
			if (!(flags & PAKFIRE_JOB_KEEP_ORPHANED))
				queue_push2(&transaction->jobs, SOLVER_DROP_ORPHANED, 0);
			break;

		case PAKFIRE_JOB_LOCK:
			job |= SOLVER_LOCK;
			break;

		case PAKFIRE_JOB_VERIFY:
			job |= SOLVER_VERIFY|SOLVER_SOLVABLE_ALL;
			break;
	}

	// Add it to the job queue
	queue_push2(&transaction->jobs, job|type, id);

	return 0;
}

static int pakfire_transaction_add_job(struct pakfire_transaction* transaction,
		const enum pakfire_job_action action, const char* what, int extra_flags) {
	Queue jobs;
	int r;

	Pool* pool = pakfire_get_solv_pool(transaction->pakfire);

	// Make the pool ready
	pakfire_pool_internalize(transaction->pakfire);

	// Initialize jobs
	queue_init(&jobs);

	int flags =
		// Select packages by name
		SELECTION_NAME|
		// Select packages by what they provide
		SELECTION_PROVIDES|
		// Process globbing patterns
		SELECTION_GLOB|
		// Select packages by their canonical name
		SELECTION_CANON|
		// Process .arch
		SELECTION_DOTARCH|
		// Process -release
		SELECTION_REL;

	// Process filelists?
	if (*what == '/')
		flags |= SELECTION_FILELIST;

	// Select all packages
	selection_make(pool, &jobs, what, flags);

	// Did we find anything?
	if (jobs.count == 0) {
		Id id = pakfire_str2dep(transaction->pakfire, what);
		if (!id) {
			r = -errno;
			goto ERROR;
		}

		queue_push2(&jobs, SOLVER_SOLVABLE_PROVIDES, id);
	}

	DEBUG(transaction->ctx, "Found %d match(es) for '%s'\n", jobs.count / 2, what);

	// Set action and global flags
	for (int i = 0; i < jobs.count; i += 2) {
		r = __pakfire_transaction_add(transaction, action,
			jobs.elements[i], jobs.elements[i+1], extra_flags);
		if (r)
			goto ERROR;
	}

	// Success
	r = 0;

ERROR:
	queue_free(&jobs);

	return r;
}

int pakfire_transaction_request(struct pakfire_transaction* transaction,
		const enum pakfire_job_action action, const char* what, int flags) {
	struct pakfire_package* package = NULL;
	int r;

	// Remove leading whitespace
	if (what) {
		while (*what && isspace(*what))
			what++;
	}

	if (!what) {
		r = __pakfire_transaction_add(transaction, action, ID_NULL, ID_NULL, flags);
		if (r)
			goto ERROR;

	// Check if we got given a URL or some file
	} else if (pakfire_string_is_url(what) || pakfire_transaction_is_file(what)) {
		// Add them to the commandline repository
		r = pakfire_commandline_add(transaction->pakfire, what, &package);
		if (r)
			goto ERROR;

		// Then add the package
		r = pakfire_transaction_request_package(transaction, action, package, flags);
		if (r)
			goto ERROR;

	// Otherwise try adding this as a job
	} else {
		r = pakfire_transaction_add_job(transaction, action, what, flags);
		if (r)
			goto ERROR;
	}

ERROR:
	if (package)
		pakfire_package_unref(package);

	return r;
}

int pakfire_transaction_request_package(struct pakfire_transaction* transaction,
		const enum pakfire_job_action action, struct pakfire_package* package, int flags) {
	// Get the solvable ID
	Id id = pakfire_package_id(package);

	// Add it to the job queue
	return __pakfire_transaction_add(transaction, action, SOLVER_SOLVABLE, id, flags);
}

int pakfire_transaction_take_solution(struct pakfire_transaction* transaction,
		struct pakfire_solution* solution) {
	struct pakfire_problem* problem = pakfire_solution_get_problem(solution);

	// Fetch IDs
	Id problem_id = pakfire_problem_get_id(problem);
	Id solution_id = pakfire_solution_get_id(solution);

	// Feed the solution into the solver
	solver_take_solution(transaction->solver, problem_id, solution_id, &transaction->jobs);

	pakfire_problem_unref(problem);
	return 0;
}

size_t pakfire_transaction_count(struct pakfire_transaction* transaction) {
	return transaction->num;
}

static ssize_t pakfire_transaction_installsizechange(struct pakfire_transaction* transaction) {
	ssize_t sizechange = transaction_calc_installsizechange(transaction->transaction);

	// Convert from kbytes to bytes
	return sizechange * 1024;
}

static ssize_t pakfire_transaction_downloadsize(struct pakfire_transaction* transaction) {
	ssize_t size = 0;

	for (unsigned int i = 0; i < transaction->num; i++)
		size += pakfire_package_get_num(
			transaction->packages[i], PAKFIRE_PKG_DOWNLOADSIZE, 0);

	return size;
}

static int pakfire_transaction_append_line(char** s, const char* format, ...)
	__attribute__((format(printf, 2, 3)));

static int pakfire_transaction_append_line(char** s, const char* format, ...) {
	va_list args;
	int r;

	va_start(args, format);
	r = vasprintf(s, format, args);
	va_end(args);

	return r;
}

static int pakfire_transaction_add_headline(char** s, size_t width, const char* headline) {
	return asprintf(s, "%s%s\n", *s, headline);
}

static int pakfire_transaction_add_newline(char** s, size_t width) {
	return asprintf(s, "%s\n", *s);
}

static int pakfire_transaction_add_line(char** s, size_t width, const char* name,
		const char* arch, const char* version, const char* repo, const char* size) {
	return asprintf(s, "%s %-21s %-8s %-21s %-18s %6s \n", *s, name, arch, version, repo, size);
}

static int pakfire_transaction_add_package(char** s, size_t width, struct pakfire_package* pkg) {
	char size[128];
	int r;

	struct pakfire_repo* repo = pakfire_package_get_repo(pkg);

	// Format size
	r = pakfire_format_size(size, pakfire_package_get_size(pkg));
	if (r < 0)
		return r;

	r = pakfire_transaction_add_line(s, width,
		pakfire_package_get_string(pkg, PAKFIRE_PKG_NAME),
		pakfire_package_get_string(pkg, PAKFIRE_PKG_ARCH),
		pakfire_package_get_string(pkg, PAKFIRE_PKG_EVR),
		pakfire_repo_get_name(repo),
		size
	);

	pakfire_repo_unref(repo);

	return r;
}

static int pakfire_transaction_add_package_change(char** s, size_t width,
		struct pakfire_package* old_pkg, struct pakfire_package* new_pkg) {
	int r;

	// Print the new package first
	r = pakfire_transaction_add_package(s, width, new_pkg);
	if (r < 0)
		return r;

	// Then show the change
	r = pakfire_transaction_append_line(s,
		"%s   --> %s\n", *s, pakfire_package_get_string(old_pkg, PAKFIRE_PKG_NEVRA));
	if (r < 0)
		return r;

	return 0;
}

static int pakfire_transaction_add_separator(char** s, size_t width) {
	int r;

	// Write line of =
	for (unsigned int i = 0; i < width; i++) {
		r = asprintf(s, "%s=", (*s) ? *s : "");
		if (r < 0)
			return r;
	}

	// Append newline
	r = asprintf(s, "%s\n", *s);
	if (r < 0)
		return r;

	return 0;
}

static int pakfire_transaction_add_usage_line(char** s, size_t width,
		const char* headline, ssize_t size) {
	char buffer[128];

	int r = pakfire_format_size(buffer, size);
	if (r < 0)
		return r;

	return pakfire_transaction_append_line(s, "%s%-21s: %s\n", *s, headline, buffer);
}

char* pakfire_transaction_dump(struct pakfire_transaction* transaction, size_t width) {
	char headline[1024];
	char* s = NULL;
	int r;

	Queue classes;
	Queue pkgs;

	queue_init(&classes);
	queue_init(&pkgs);

	Pool* pool = transaction->transaction->pool;
	const int mode =
		SOLVER_TRANSACTION_SHOW_OBSOLETES |
		SOLVER_TRANSACTION_OBSOLETE_IS_UPGRADE;

	// Header
	r = pakfire_transaction_add_separator(&s, width);
	if (r < 0)
		goto ERROR;

	// Show the headline
	r = pakfire_transaction_add_line(&s, width,
		_("Package"),
		_("Arch"),
		_("Version"),
		_("Repository"),
		_("Size")
	);
	if (r < 0)
		goto ERROR;

	// Add another separator
	r = pakfire_transaction_add_separator(&s, width);
	if (r < 0)
		goto ERROR;

	// Get all classes
	transaction_classify(transaction->transaction, mode, &classes);

	/*
		The classes queue now contains a list of all classes as a tuple of:
			* The class type
			* The number of packages in this class
			* The from ID (for arch/vendor change)
			* The to ID (for arch/vendor change)
	*/
	for (int i = 0; i < classes.count; i += 4) {
		Id class = classes.elements[i];
		unsigned int count = classes.elements[i+1];

		const char* from = pool_id2str(pool, classes.elements[i+2]);
		const char* to   = pool_id2str(pool, classes.elements[i+3]);

		switch (class) {
			case SOLVER_TRANSACTION_INSTALL:
				if (count > 1)
					r = pakfire_string_format(headline, _("Installing %u packages:"), count);
				else
					r = pakfire_string_set(headline, _("Installing one package:"));

				if (r < 0)
					goto ERROR;
				break;

			case SOLVER_TRANSACTION_REINSTALLED:
				if (count > 1)
					r = pakfire_string_format(headline, _("Reinstalling %u packages:"), count);
				else
					r = pakfire_string_set(headline, _("Reinstalling one package:"));

				if (r < 0)
					goto ERROR;
				break;

			case SOLVER_TRANSACTION_ERASE:
				if (count > 1)
					r = pakfire_string_format(headline, _("Removing %u packages:"), count);
				else
					r = pakfire_string_set(headline, _("Removing one package:"));

				if (r < 0)
					goto ERROR;
				break;

			case SOLVER_TRANSACTION_UPGRADED:
				if (count > 1)
					r = pakfire_string_format(headline, _("Updating %u packages:"), count);
				else
					r = pakfire_string_set(headline, _("Updating one package:"));

				if (r < 0)
					goto ERROR;
				break;

			case SOLVER_TRANSACTION_DOWNGRADED:
				if (count > 1)
					r = pakfire_string_format(headline, _("Downgrading %u packages:"), count);
				else
					r = pakfire_string_set(headline, _("Downgrading one package:"));

				if (r < 0)
					goto ERROR;
				break;

			case SOLVER_TRANSACTION_CHANGED:
				if (count > 1)
					r = pakfire_string_format(headline, _("Changing %u packages:"), count);
				else
					r = pakfire_string_set(headline, _("Changing one package:"));

				if (r < 0)
					goto ERROR;
				break;

			case SOLVER_TRANSACTION_ARCHCHANGE:
				if (count > 1)
					r = pakfire_string_format(headline,
						_("%u architecture changes from '%s' to '%s':"), count, from, to);
				else
					r = pakfire_string_format(headline,
						_("One architecture change from '%s' to '%s':"), from, to);

				if (r < 0)
					goto ERROR;
				break;

			case SOLVER_TRANSACTION_VENDORCHANGE:
				if (count)
					r = pakfire_string_format(headline,
						_("%u vendor changes from '%s' to '%s':"), count, from, to);
				else
					r = pakfire_string_format(headline,
						_("One vendor change from '%s' to '%s':"), from, to);

				if (r < 0)
					goto ERROR;
				break;

			case SOLVER_TRANSACTION_IGNORE:
				continue;
		}

		// Show what we are doing
		r = pakfire_transaction_add_headline(&s, width, headline);
		if (r < 0)
			goto ERROR;

		// Fetch packages in this class
		transaction_classify_pkgs(transaction->transaction, mode, class,
			classes.elements[i+2], classes.elements[i+3], &pkgs);

		// List all packages
		for (int j = 0; j < pkgs.count; j++) {
			struct pakfire_package* old_pkg = NULL;
			struct pakfire_package* new_pkg = NULL;

			r = pakfire_package_create_from_solvable(&old_pkg, transaction->pakfire,
					NULL, pkgs.elements[j]);
			if (r)
				continue;

			switch (class) {
				case SOLVER_TRANSACTION_UPGRADED:
				case SOLVER_TRANSACTION_DOWNGRADED:
					r = pakfire_package_create_from_solvable(&new_pkg, transaction->pakfire,
							NULL, transaction_obs_pkg(transaction->transaction, pkgs.elements[j]));
					if (r)
						continue;

					r = pakfire_transaction_add_package_change(&s, width, old_pkg, new_pkg);
					if (r < 0)
						goto ERROR;
					break;

				default:
					r = pakfire_transaction_add_package(&s, width, old_pkg);
					if (r < 0)
						goto ERROR;
					break;
			}

			pakfire_package_unref(old_pkg);
			if (new_pkg)
				pakfire_package_unref(new_pkg);
		}

		// Newline
		r = pakfire_transaction_add_newline(&s, width);
		if (r < 0)
			goto ERROR;
	}

	// Summary
	r = pakfire_transaction_add_headline(&s, width, _("Transaction Summary"));
	if (r < 0)
		goto ERROR;

	r = pakfire_transaction_add_separator(&s, width);
	if (r < 0)
		goto ERROR;

	// How much do we need to download?
	size_t downloadsize = pakfire_transaction_downloadsize(transaction);

	if (downloadsize > 0) {
		r = pakfire_transaction_add_usage_line(&s, width,
			_("Total Download Size"), downloadsize);
		if (r < 0)
			goto ERROR;
	}

	// How much more space do we need?
	ssize_t sizechange = pakfire_transaction_installsizechange(transaction);

	// Show if we need more space
	if (sizechange > 0) {
		r = pakfire_transaction_add_usage_line(&s, width, _("Required Space"), sizechange);
		if (r < 0)
			goto ERROR;

	// Show if we are freeing space
	} else if (sizechange < 0) {
		r = pakfire_transaction_add_usage_line(&s, width, _("Freed Space"), -sizechange);
		if (r < 0)
			goto ERROR;
	}

	if (s)
		DEBUG(transaction->ctx, "%s", s);

ERROR:
	queue_free(&classes);
	queue_free(&pkgs);

	return s;
}

static int pakfire_transaction_check_fileconflicts(
		struct pakfire_transaction* transaction) {
	/*
		XXX TODO

		Because of a dependency to librpm, we cannot use the functionality from
		libsolv and need to re-implement this.
	*/
	return 0;
}

static int pakfire_transaction_check(struct pakfire_transaction* transaction) {
	int r;

	// Check for any file conflicts
	r = pakfire_transaction_check_fileconflicts(transaction);
	if (r)
		return r;

	return 0;
}

static int pakfire_transaction_verify(struct pakfire_transaction* transaction,
		struct pakfire_package* pkg, struct pakfire_archive* archive) {
	enum pakfire_hash_type hash = PAKFIRE_HASH_UNDEFINED;
	const unsigned char* checksum = NULL;
	size_t checksum_length = 0;
	int r;

	const char* nevra = pakfire_package_get_string(pkg, PAKFIRE_PKG_NEVRA);

	// Nothing to do if this step does not have an archive
	if (!archive) {
		DEBUG(transaction->ctx, "Package %s requires no archive\n", nevra);
		return 0;
	}

	// Fetch digest from package
	r = pakfire_package_get_checksum(pkg, &hash, &checksum, &checksum_length);
	if (r < 0) {
		ERROR(transaction->ctx, "Could not fetch checksum for %s: %s\n", nevra, strerror(-r));
		return r;
	}

	// Fail if there is no checksum
	if (!checksum) {
		ERROR(transaction->ctx, "Package %s has no checksum\n", nevra);
		return -EINVAL;
	}

#if 0
	// Check against the digest of the archive
	return pakfire_archive_check_digest(archive, digest_type, expected_digest, length);
#endif

	return 0;
}

static int pakfire_transaction_run_script(struct pakfire_transaction* transaction,
		struct pakfire_db* db, const char* type, struct pakfire_package* pkg, struct pakfire_archive* archive) {
	struct pakfire_scriptlet* scriptlet = NULL;

	// Fetch scriptlet from archive if possible
	if (archive)
		scriptlet = pakfire_archive_get_scriptlet(archive, type);
	else
		scriptlet = pakfire_db_get_scriptlet(db, pkg, type);

	// Nothing to do if there are no scriptlets
	if (!scriptlet)
		return 0;

	// Execute the scriptlet
	pakfire_scriptlet_execute(scriptlet, transaction->pakfire);

	pakfire_scriptlet_unref(scriptlet);

	return 0;
}

static int pakfire_transaction_extract(struct pakfire_transaction* transaction,
		struct pakfire_package* pkg, struct pakfire_archive* archive) {
	struct pakfire_filelist* filelist = NULL;
	int r;

	const char* nevra = pakfire_package_get_string(pkg, PAKFIRE_PKG_NEVRA);

	// Update status
	pakfire_transaction_status(transaction, _("Installing %s..."), nevra);

	// Extract payload
	r = pakfire_archive_extract(archive, NULL, 0);
	if (r) {
		ERROR(transaction->ctx, "Could not extract package %s: %m\n",
			nevra);
		return r;
	}

	// Is it necessary to call ldconfig?
	r = pakfire_archive_get_filelist(archive, &filelist);
	if (r < 0)
		return r;

	// Update the runtime linker cache
	if (pakfire_filelist_contains(filelist, "*/lib*.so.?"))
		pakfire_jail_ldconfig(transaction->pakfire);

	if (filelist)
		pakfire_filelist_unref(filelist);

	return 0;
}

static int pakfire_transaction_erase(struct pakfire_transaction* transaction,
		struct pakfire_db* db, struct pakfire_package* pkg) {
	struct pakfire_filelist* filelist = NULL;
	int r;

	// Fetch filelist
	r = pakfire_db_package_filelist(db, &filelist, pkg);
	if (r)
		goto ERROR;

	// Remove all files on the filelist
	r = pakfire_filelist_cleanup(filelist, 0);
	if (r)
		goto ERROR;

	// Update the runtime linker cache after all files have been removed
	pakfire_jail_ldconfig(transaction->pakfire);

ERROR:
	if (filelist)
		pakfire_filelist_unref(filelist);

	return r;
}

static const char* pakfire_action_type_string(enum pakfire_actions type) {
	switch (type) {
		case PAKFIRE_ACTION_NOOP:
			return "NOOP";

		case PAKFIRE_ACTION_VERIFY:
			return "VERIFY";

		case PAKFIRE_ACTION_EXECUTE:
			return "EXECUTE";

		case PAKFIRE_ACTION_PRETRANS:
			return "PRETRANS";

		case PAKFIRE_ACTION_POSTTRANS:
			return "POSTTRANS";
	}

	return NULL;
}

static const char* pakfire_step_type_string(enum pakfire_steps type) {
	switch (type) {
		case PAKFIRE_STEP_UNKNOWN:
			return "UNKNOWN";

		case PAKFIRE_STEP_INSTALL:
			return "INSTALL";

		case PAKFIRE_STEP_REINSTALL:
			return "REINSTALL";

		case PAKFIRE_STEP_REINSTALLED:
			return "REINSTALLED";

		case PAKFIRE_STEP_ERASE:
			return "ERASE";

		case PAKFIRE_STEP_UPGRADE:
			return "UPGRADE";

		case PAKFIRE_STEP_UPGRADED:
			return "UPGRADED";

		case PAKFIRE_STEP_DOWNGRADE:
			return "DOWNGRADE";

		case PAKFIRE_STEP_DOWNGRADED:
			return "DOWNGRADED";
	}

	return NULL;
}

static int pakfire_transaction_package_is_userinstalled(
		struct pakfire_transaction* transaction, struct pakfire_package* pkg) {
	// No packages on the list
	if (!transaction->userinstalled)
		return 0;

	const char* name = pakfire_package_get_string(pkg, PAKFIRE_PKG_NAME);

	// Check if the package is on the list
	for (char** elem = transaction->userinstalled; *elem; elem++) {
		if (strcmp(name, *elem) == 0)
			return 1;
	}

	// Not found
	return 0;
}

static int pakfire_transaction_apply_systemd_sysusers(struct pakfire_transaction* transaction,
		struct pakfire_package* pkg, struct pakfire_archive* archive) {
	// Walk through the archive and find all sysuser files
	if (pakfire_package_matches_dep(pkg, PAKFIRE_PKG_REQUIRES, "pakfire(systemd-sysusers)"))
		return pakfire_archive_apply_systemd_sysusers(archive);

	return 0;
}

static int pakfire_transaction_apply_systemd_tmpfiles(
		struct pakfire_transaction* transaction, struct pakfire_package* pkg) {
	// Apply any tmpfiles (ignore any errors)
	if (pakfire_package_matches_dep(pkg, PAKFIRE_PKG_REQUIRES, "pakfire(systemd-tmpfiles)"))
		pakfire_jail_run_systemd_tmpfiles(transaction->pakfire);

	return 0;
}

static int pakfire_transaction_run_step(struct pakfire_transaction* transaction,
		struct pakfire_db* db, const enum pakfire_actions action, struct pakfire_package* pkg, struct pakfire_archive* archive) {
	if (!pkg) {
		errno = EINVAL;
		return 1;
	}

	const char* nevra = pakfire_package_get_string(pkg, PAKFIRE_PKG_NEVRA);
	const enum pakfire_steps type = pakfire_transaction_get_step_type(transaction, pkg);

	DEBUG(transaction->ctx, "Running %s (%s) for %s\n",
		pakfire_action_type_string(action), pakfire_step_type_string(type), nevra);

	int r = 0;
	switch (action) {
		// Verify this step
		case PAKFIRE_ACTION_VERIFY:
			r = pakfire_transaction_verify(transaction, pkg, archive);
			break;

		// Run the pre-transaction scripts
		case PAKFIRE_ACTION_PRETRANS:
			switch (type) {
				case PAKFIRE_STEP_INSTALL:
				case PAKFIRE_STEP_REINSTALL:
					r = pakfire_transaction_run_script(transaction, db,
						"pretransin", pkg, archive);
					break;

				case PAKFIRE_STEP_UPGRADE:
				case PAKFIRE_STEP_DOWNGRADE:
					r = pakfire_transaction_run_script(transaction, db,
						"pretransup", pkg, archive);
					break;

				case PAKFIRE_STEP_ERASE:
					r = pakfire_transaction_run_script(transaction, db,
						"pretransun", pkg, archive);
					break;

				case PAKFIRE_STEP_REINSTALLED:
				case PAKFIRE_STEP_UPGRADED:
				case PAKFIRE_STEP_DOWNGRADED:
				case PAKFIRE_STEP_UNKNOWN:
					break;
			}
			break;

		// Run the post-transaction scripts
		case PAKFIRE_ACTION_POSTTRANS:
			switch (type) {
				case PAKFIRE_STEP_INSTALL:
				case PAKFIRE_STEP_REINSTALL:
					r = pakfire_transaction_run_script(transaction, db,
						"posttransin", pkg, archive);
					break;

				case PAKFIRE_STEP_UPGRADE:
				case PAKFIRE_STEP_DOWNGRADE:
					r = pakfire_transaction_run_script(transaction, db,
						"posttransup", pkg, archive);
					break;

				case PAKFIRE_STEP_ERASE:
					r = pakfire_transaction_run_script(transaction, db,
						"posttransun", pkg, archive);
					break;

				case PAKFIRE_STEP_REINSTALLED:
				case PAKFIRE_STEP_UPGRADED:
				case PAKFIRE_STEP_DOWNGRADED:
				case PAKFIRE_STEP_UNKNOWN:
					break;
			}
			break;

		// Execute the action of this script
		case PAKFIRE_ACTION_EXECUTE:
			// Increment progress
			transaction->progress++;

			// Update progress callback
			pakfire_transaction_status(transaction, NULL);

			switch (type) {
				case PAKFIRE_STEP_INSTALL:
				case PAKFIRE_STEP_REINSTALL:
					// Apply systemd sysusers
					r = pakfire_transaction_apply_systemd_sysusers(transaction, pkg, archive);
					if (r)
						break;

					r = pakfire_transaction_run_script(transaction, db,
						"prein", pkg, archive);
					if (r)
						break;

					r = pakfire_transaction_extract(transaction, pkg, archive);
					if (r)
						break;

					// Remove package metadata first when reinstalling
					if (type == PAKFIRE_STEP_REINSTALL) {
						r = pakfire_db_remove_package(db, pkg);
						if (r)
							break;
					}

					r = pakfire_db_add_package(db, pkg, archive,
							pakfire_transaction_package_is_userinstalled(transaction, pkg));
					if (r)
						break;

					// Apply systemd tmpfiles
					r = pakfire_transaction_apply_systemd_tmpfiles(transaction, pkg);
					if (r)
						break;

					r = pakfire_transaction_run_script(transaction, db,
						"postin", pkg, archive);
					break;

				case PAKFIRE_STEP_UPGRADE:
				case PAKFIRE_STEP_DOWNGRADE:
					// Apply systemd sysusers
					r = pakfire_transaction_apply_systemd_sysusers(transaction, pkg, archive);
					if (r)
						break;

					r = pakfire_transaction_run_script(transaction, db,
						"preup", pkg, archive);
					if (r)
						break;

					r = pakfire_transaction_extract(transaction, pkg, archive);
					if (r)
						break;

					r = pakfire_db_add_package(db, pkg, archive,
							pakfire_transaction_package_is_userinstalled(transaction, pkg));
					if (r)
						break;

					// Apply systemd tmpfiles
					r = pakfire_transaction_apply_systemd_tmpfiles(transaction, pkg);
					if (r)
						break;

					r = pakfire_transaction_run_script(transaction, db,
						"postup", pkg, archive);
					break;

				case PAKFIRE_STEP_ERASE:
					r = pakfire_transaction_run_script(transaction, db,
						"preun", pkg, archive);
					if (r)
						break;

					r = pakfire_transaction_erase(transaction, db, pkg);
					if (r)
						break;

					r = pakfire_db_remove_package(db, pkg);
					if (r)
						break;

					r = pakfire_transaction_run_script(transaction, db,
						"postun", pkg, archive);
					break;

				// Erase the package data without running any scripts
				case PAKFIRE_STEP_UPGRADED:
				case PAKFIRE_STEP_DOWNGRADED:
					r = pakfire_transaction_erase(transaction, db, pkg);
					if (r)
						break;

					r = pakfire_db_remove_package(db, pkg);
					if (r)
						break;
					break;

				case PAKFIRE_STEP_REINSTALLED:
				case PAKFIRE_STEP_UNKNOWN:
					break;
			}
			break;

		// Do nothing
		case PAKFIRE_ACTION_NOOP:
			break;
	}

	if (r)
		ERROR(transaction->ctx, "Step %s (%u) for %s has failed: %m\n",
			pakfire_action_type_string(action), type, nevra);

	return r;
}

static int pakfire_transaction_run_steps(struct pakfire_transaction* transaction,
		struct pakfire_db* db, enum pakfire_actions action) {
	int r = 0;

	// Update status
	switch (action) {
		case PAKFIRE_ACTION_VERIFY:
			pakfire_transaction_status(transaction, _("Verifying packages..."));
			break;

		case PAKFIRE_ACTION_PRETRANS:
			pakfire_transaction_status(transaction, _("Preparing installation..."));
			break;

		case PAKFIRE_ACTION_POSTTRANS:
			pakfire_transaction_status(transaction, _("Finishing up..."));
			break;

		default:
			break;
	}

	// Walk through all steps
	for (unsigned int i = 0; i < transaction->num; i++) {
		r = pakfire_transaction_run_step(transaction, db, action,
			transaction->packages[i], transaction->archives[i]);

		// End loop if action was unsuccessful
		if (r) {
			DEBUG(transaction->ctx, "Step %u failed: %m\n", i);
			break;
		}
	}

	return r;
}

static int pakfire_transaction_open_archives(struct pakfire_transaction* transaction) {
	int r;

	for (unsigned int i = 0; i < transaction->num; i++) {
		struct pakfire_package* pkg = transaction->packages[i];

		// Fetch the type
		enum pakfire_steps type = pakfire_transaction_get_step_type(transaction, pkg);

		// Do we need the archive?
		switch (type) {
			case PAKFIRE_STEP_INSTALL:
			case PAKFIRE_STEP_REINSTALL:
			case PAKFIRE_STEP_UPGRADE:
			case PAKFIRE_STEP_DOWNGRADE:
				break;

			case PAKFIRE_STEP_REINSTALLED:
			case PAKFIRE_STEP_ERASE:
			case PAKFIRE_STEP_UPGRADED:
			case PAKFIRE_STEP_DOWNGRADED:
			case PAKFIRE_STEP_UNKNOWN:
				continue;
		}

		// Open the archive
		r = pakfire_package_get_archive(pkg, &transaction->archives[i]);
		if (r < 0)
			return r;
	}

	return 0;
}

static int pakfire_usrmove_symlink(struct pakfire_ctx* ctx, struct pakfire* pakfire,
		const char* src, const char* dst) {
	char link[PATH_MAX];
	char path[PATH_MAX];
	int r;

	// Compose the link path
	r = pakfire_path(pakfire, link, "%s", src);
	if (r)
		return r;

	// Exit if the link exists
	if (pakfire_path_exists(link))
		return 0;

	// Compose the destination path
	r = pakfire_path(pakfire, path, "%s", dst);
	if (r)
		return r;

	// Make sure the destination exists
	r = pakfire_mkdir(path, 755);
	if (r)
		return r;

	// Create the symlink
	r = symlink(dst, link);
	if (r) {
		DEBUG(ctx, "Could not create symlink %s to %s: %m\n", dst, link);
		return r;
	}

	DEBUG(ctx, "Created symlink %s --> %s\n", link, dst);

	return 0;
}

/*
	This is an ugly helper function that helps us to make sure
	that /bin, /sbin, /lib and /lib64 are symlinks to their
	corresponding path in /usr.
*/
static int pakfire_usrmove(struct pakfire_ctx* ctx, struct pakfire* pakfire) {
	int r;

	r = pakfire_usrmove_symlink(ctx, pakfire, "/bin", "usr/bin");
	if (r)
		return r;

	r = pakfire_usrmove_symlink(ctx, pakfire, "/sbin", "usr/sbin");
	if (r)
		return r;

	r = pakfire_usrmove_symlink(ctx, pakfire, "/lib", "usr/lib");
	if (r)
		return r;

	r = pakfire_usrmove_symlink(ctx, pakfire, "/lib64", "usr/lib64");
	if (r)
		return r;

	return r;
}

static int pakfire_transaction_perform(struct pakfire_transaction* transaction) {
	struct pakfire_repo* repo = NULL;
	struct pakfire_db* db;
	int r;

	DEBUG(transaction->ctx, "Running Transaction %p\n", transaction);

	// Open all archives
	r = pakfire_transaction_open_archives(transaction);
	if (r)
		return r;

	// Open the database
	r = pakfire_db_open(&db, transaction->pakfire, PAKFIRE_DB_READWRITE);
	if (r) {
		ERROR(transaction->ctx, "Could not open the database\n");
		return r;
	}

	// Verify steps
	r = pakfire_transaction_run_steps(transaction, db, PAKFIRE_ACTION_VERIFY);
	if (r)
		goto ERROR;

	// Make sure /usr-move is working
	r = pakfire_usrmove(transaction->ctx, transaction->pakfire);
	if (r)
		goto ERROR;

	// Execute all pre transaction actions
	r = pakfire_transaction_run_steps(transaction, db, PAKFIRE_ACTION_PRETRANS);
	if (r)
		goto ERROR;

	r = pakfire_transaction_run_steps(transaction, db, PAKFIRE_ACTION_EXECUTE);
	if (r)
		goto ERROR;

	// Execute all post transaction actions
	r = pakfire_transaction_run_steps(transaction, db, PAKFIRE_ACTION_POSTTRANS);
	if (r)
		goto ERROR;

	DEBUG(transaction->ctx, "The transaction has finished successfully\n");

	// Reload database for next transaction

	repo = pakfire_get_installed_repo(transaction->pakfire);
	if (!repo)
		goto ERROR;

	// Reload the database
	r = pakfire_db_load(db, repo);

ERROR:
	if (repo)
		pakfire_repo_unref(repo);
	pakfire_db_unref(db);

	return r;
}

static int pakfire_transaction_download_package(struct pakfire_transaction* transaction,
		struct pakfire_httpclient* httpclient, struct pakfire_package* pkg) {
	struct pakfire_repo* repo = NULL;
	struct pakfire_xfer* xfer = NULL;
	int r;

	// Fetch the repository to download from
	repo = pakfire_package_get_repo(pkg);
	if (!repo) {
		r = -errno;
		goto ERROR;
	}

	// Create a xfer for this package
	r = pakfire_repo_download_package(&xfer, repo, pkg);
	if (r)
		goto ERROR;

	// Enqueue the transfer
	r = pakfire_httpclient_enqueue(httpclient, xfer);
	if (r)
		goto ERROR;

ERROR:
	if (xfer)
		pakfire_xfer_unref(xfer);
	if (repo)
		pakfire_repo_unref(repo);

	return r;
}

static int pakfire_transaction_package_needs_download(
		struct pakfire_transaction* transaction, struct pakfire_package* pkg) {
	int r;

	enum pakfire_steps type = pakfire_transaction_get_step_type(transaction, pkg);
	switch (type) {
		case PAKFIRE_STEP_INSTALL:
		case PAKFIRE_STEP_REINSTALL:
		case PAKFIRE_STEP_DOWNGRADE:
		case PAKFIRE_STEP_UPGRADE:
			break;

		// No need to download for these steps
		default:
			return 0;
	}

	// No download required if this package is already installed
	if (pakfire_package_is_installed(pkg))
		return 0;

	// Check if the package is available
	r = pakfire_package_is_available(pkg);
	switch (r) {
		// The package is not available
		case 0:
			// Needs download
			return 1;

		// The package is available
		case 1:
			// No download needed
			return 0;

		// Error
		default:
			return r;
	}
}

int pakfire_transaction_download(struct pakfire_transaction* transaction) {
	struct pakfire_httpclient* httpclient = NULL;
	int r;

	// Initialize the HTTP client
	r = pakfire_httpclient_create(&httpclient, transaction->ctx, NULL);
	if (r) {
		ERROR(transaction->ctx, "Could not initialize HTTP client: %m\n");
		return 1;
	}

	// Add all packages that need to be downloaded
	for (unsigned int i = 0; i < transaction->num; i++) {
		struct pakfire_package* pkg = transaction->packages[i];

		if (!pakfire_transaction_package_needs_download(transaction, pkg))
			continue;

		// Enqueue download
		r = pakfire_transaction_download_package(transaction, httpclient, pkg);
		if (r) {
			const char* nevra = pakfire_package_get_string(pkg, PAKFIRE_PKG_NEVRA);

			ERROR(transaction->ctx, "Could not add download to queue: %s: %m\n", nevra);
			goto ERROR;
		}
	}

	// Run the HTTP client
	r = pakfire_httpclient_run(httpclient, _("Downloading Packages"));

ERROR:
	pakfire_httpclient_unref(httpclient);

	return r;
}

int pakfire_transaction_run(struct pakfire_transaction* transaction) {
	char* dump = NULL;
	int r;

	// Automatically solve if not done, yet
	if (!transaction->transaction) {
		r = pakfire_transaction_solve(transaction, 0, NULL);
		if (r)
			goto ERROR;
	}

	// Skip running an empty transaction
	if (!transaction->num) {
		DEBUG(transaction->ctx, "Empty transaction. Skipping...\n");
		return 0;
	}

	// Show what would be done
	dump = pakfire_transaction_dump(transaction, 80);

	// Check if we should continue
	r = pakfire_ctx_confirm(transaction->ctx, transaction->pakfire, dump, _("Is this okay?"));
	if (r) {
		ERROR(transaction->ctx, "Transaction aborted upon user request\n");
		goto ERROR;
	}

	// Perform a check if this can actually be run
	r = pakfire_transaction_check(transaction);
	if (r)
		goto ERROR;

	// Download what we need
	r = pakfire_transaction_download(transaction);
	if (r)
		goto ERROR;

	// Perform all steps
	r = pakfire_transaction_perform(transaction);
	if (r)
		goto ERROR;

ERROR:
	// Cleanup
	if (dump)
		free(dump);

	return r;
}

int pakfire_transaction_compose_repo(struct pakfire_transaction* transaction,
		struct pakfire_key* key, const char* path) {
	struct pakfire_archive* archive = NULL;
	int r;

	// Allocate an array for all files
	const char* files[transaction->num + 1];
	unsigned int num = 0;

	DEBUG(transaction->ctx, "Writing transaction to %s...\n", path);

	// Open all archives
	r = pakfire_transaction_open_archives(transaction);
	if (r)
		return r;

	// Walk through all steps
	for (unsigned int i = 0; i < transaction->num; i++) {
		archive = transaction->archives[i];

		// Skip steps that don't have an archive
		if (!archive)
			continue;

		// Add the path to the archive
		files[num++] = pakfire_archive_get_path(archive);
	}

	// Terminate the array
	files[num] = NULL;

	// Create the repository
	return pakfire_repo_compose(transaction->pakfire, path, key, files);
}
