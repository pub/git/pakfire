/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2022 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <linux/bpf.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/types.h>

// libbpf
#include <bpf/bpf.h>

#include <pakfire/ctx.h>
#include <pakfire/cgroup.h>
#include <pakfire/logging.h>
#include <pakfire/path.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

#define BUFFER_SIZE			64 * 1024

// Short form of mov, dst_reg = src_reg
#define BPF_MOV64_IMM(DST, IMM) \
	((struct bpf_insn){ \
		.code = BPF_ALU64 | BPF_MOV | BPF_K, \
		.dst_reg = DST, \
		.src_reg = 0, \
		.off = 0, \
		.imm = IMM \
	})

// Program exit
#define BPF_EXIT_INSN() \
	((struct bpf_insn){ \
		.code = BPF_JMP | BPF_EXIT, \
		.dst_reg = 0, \
		.src_reg = 0, \
		.off = 0, \
		.imm = 0 \
	})

struct pakfire_cgroup {
	struct pakfire_ctx* ctx;
	int nrefs;

	// The parent group
	struct pakfire_cgroup* parent;

	// Flags
	int flags;

	// Store the name & path
	char name[NAME_MAX];
	char path[PATH_MAX];

	// Controllers
	unsigned int controllers;

	// File descriptor to cgroup
	int fd;

	// FD to the devices filter program
	int devicesfd;
};

static const char* pakfire_cgroup_path(struct pakfire_cgroup* cgroup) {
	if (!*cgroup->path)
		return "(root)";

	return cgroup->path;
}

static int pakfire_cgroup_setup_devices(struct pakfire_cgroup* cgroup) {
	static char bpf_log_buffer[BPF_LOG_BUF_SIZE];

	LIBBPF_OPTS(bpf_prog_load_opts, opts,
		// Log Buffer
		.log_buf  = bpf_log_buffer,
		.log_size = sizeof(bpf_log_buffer),
	);
	int r;

	struct bpf_insn program[] = {
		BPF_MOV64_IMM(BPF_REG_0, 1), // r0 = 1
		BPF_EXIT_INSN(),             // return r0
	};

	// Load the BPF program
	r = bpf_prog_load(BPF_PROG_TYPE_CGROUP_DEVICE, NULL, "GPL",
		program, sizeof(program) / sizeof(*program), &opts);
	if (r < 0) {
		ERROR(cgroup->ctx, "Could not load BPF program: %m\n");
		return r;
	}

	// Store the file descriptor
	cgroup->devicesfd = r;

	// Attach the program to the cgroup
	r = bpf_prog_attach(cgroup->devicesfd, cgroup->fd,
		BPF_CGROUP_DEVICE, BPF_F_ALLOW_MULTI);
	if (r) {
		ERROR(cgroup->ctx, "Could not attach BPF program to cgroup: %m\n");
		return r;
	}

	return 0;
}

static int pakfire_cgroup_open_root(struct pakfire_cgroup* cgroup) {
	const char* path = "/sys/fs/cgroup";
	int fd;

	// Open the root path
	fd = open(path, O_DIRECTORY|O_CLOEXEC);
	if (fd < 0) {
		ERROR(cgroup->ctx, "Could not open %s: %m\n", path);
		return -errno;
	}

	return fd;
}

/*
	Opens the cgroup and returns a file descriptor.

	If the cgroup does not exist, it will try to create it.

	This function returns a negative value on error.
*/
static int __pakfire_cgroup_open(struct pakfire_cgroup* cgroup) {
	int fd = -EBADF;
	int r;

	// Open the root cgroup
	if (!cgroup->parent)
		return pakfire_cgroup_open_root(cgroup);

	// Try creating the cgroup
	r = mkdirat(cgroup->parent->fd, cgroup->name, 0755);
	if (r < 0) {
		switch (errno) {
			case EEXIST:
				break;

			default:
				ERROR(cgroup->ctx, "Could not create cgroup '%s': %m\n", cgroup->name);
				return -errno;
		}
	}

	// Try opening the cgroup
	fd = openat(cgroup->parent->fd, cgroup->name, O_DIRECTORY|O_CLOEXEC);
	if (fd < 0) {
		ERROR(cgroup->ctx, "Could not open cgroup %s: %m\n", pakfire_cgroup_path(cgroup));
		return -errno;
	}

	return fd;
}

static ssize_t pakfire_cgroup_read(struct pakfire_cgroup* cgroup, const char* path,
		char* buffer, size_t length) {
	ssize_t bytes_read = -1;

	// Check if this cgroup has been destroyed already
	if (cgroup->fd < 0) {
		ERROR(cgroup->ctx, "Trying to read from destroyed cgroup\n");
		return -1;
	}

	// Open the file
	int fd = openat(cgroup->fd, path, O_CLOEXEC);
	if (fd < 0) {
		DEBUG(cgroup->ctx, "Could not open %s/%s: %m\n", pakfire_cgroup_path(cgroup), path);
		goto ERROR;
	}

	// Read file content into buffer
	bytes_read = read(fd, buffer, length);
	if (bytes_read < 0) {
		DEBUG(cgroup->ctx, "Could not read from %s/%s: %m\n", pakfire_cgroup_path(cgroup), path);
		goto ERROR;
	}

	// Terminate the buffer
	if ((size_t)bytes_read < length)
		buffer[bytes_read] = '\0';

ERROR:
	if (fd > 0)
		close(fd);

	return bytes_read;
}

static int pakfire_cgroup_write(struct pakfire_cgroup* cgroup,
		const char* path, const char* format, ...) __attribute__((format(printf, 3, 4)));

static int pakfire_cgroup_write(struct pakfire_cgroup* cgroup,
		const char* path, const char* format, ...) {
	ssize_t bytes_written = 0;
	va_list args;
	int fd = -EBADF;
	int r = 0;

	// Check if this cgroup has been destroyed already
	if (cgroup->fd < 0) {
		ERROR(cgroup->ctx, "Trying to write to destroyed cgroup\n");
		return -EPERM;
	}

	// Open the file
	fd = openat(cgroup->fd, path, O_WRONLY|O_CLOEXEC);
	if (fd < 0) {
		DEBUG(cgroup->ctx, "Could not open %s/%s for writing: %m\n", pakfire_cgroup_path(cgroup), path);
		r = -errno;
		goto ERROR;
	}

	// Write buffer
	va_start(args, format);
	bytes_written = vdprintf(fd, format, args);
	va_end(args);

	// Check if content was written okay
	if (bytes_written < 0) {
		DEBUG(cgroup->ctx, "Could not write to %s/%s: %m\n", pakfire_cgroup_path(cgroup), path);
		r = -errno;
		goto ERROR;
	}

ERROR:
	if (fd > 0)
		close(fd);

	return r;
}

static int pakfire_cgroup_controller_from_string(const char* name) {
	if (!name)
		return 0;

	if (strcmp(name, "cpu") == 0)
		return PAKFIRE_CGROUP_CONTROLLER_CPU;

	else if (strcmp(name, "memory") == 0)
		return PAKFIRE_CGROUP_CONTROLLER_MEMORY;

	else if (strcmp(name, "pids") == 0)
		return PAKFIRE_CGROUP_CONTROLLER_PIDS;

	else if (strcmp(name, "io") == 0)
		return PAKFIRE_CGROUP_CONTROLLER_IO;

	return 0;
}

static const char* pakfire_cgroup_controller_name(int controller) {
	switch (controller) {
		case PAKFIRE_CGROUP_CONTROLLER_CPU:
			return "cpu";

		case PAKFIRE_CGROUP_CONTROLLER_MEMORY:
			return "memory";

		case PAKFIRE_CGROUP_CONTROLLER_PIDS:
			return "pids";

		case PAKFIRE_CGROUP_CONTROLLER_IO:
			return "io";
	}

	return NULL;
}

static int pakfire_cgroup_read_controllers(struct pakfire_cgroup* cgroup) {
	char buffer[4096];
	char* p = NULL;
	int r;

	// Read the data into a buffer
	r = pakfire_cgroup_read(cgroup, "cgroup.subtree_control", buffer, sizeof(buffer));
	if (r < 0)
		return r;

	// Tokenize the string
	char* token = strtok_r(buffer, " ", &p);

	while (token) {
		cgroup->controllers |= pakfire_cgroup_controller_from_string(token);

		// Move on to the next token
		token = strtok_r(NULL, " ", &p);
	}

	return 0;
}

/*
	Enables a cgroup controller.
*/
static int pakfire_cgroup_enable_controller(struct pakfire_cgroup* cgroup, int controller) {
	int r;

	// Do nothing if the controller is already enabled
	if (cgroup->controllers & controller)
		return 0;

	// Ensure this controller is enabled on the parent, too
	if (cgroup->parent) {
		r = pakfire_cgroup_enable_controller(cgroup->parent, controller);
		if (r < 0)
			return r;
	}

	const char* name = pakfire_cgroup_controller_name(controller);

	// Check if we know this controller
	if (!name) {
		ERROR(cgroup->ctx, "Cannot enable unknown controller\n");
		return -ENOTSUP;
	}

	// Try to enable the controller
	r = pakfire_cgroup_write(cgroup, "cgroup.subtree_control", "+%s", name);
	if (r < 0) {
		ERROR(cgroup->ctx, "Could not enable controller '%s': %s\n",
			name, strerror(-r));
		return r;
	}

	return 0;
}

/*
	Entry function to open a new cgroup.

	If the cgroup doesn't exist, it will be created including any parent cgroups.
*/
static int pakfire_cgroup_open(struct pakfire_cgroup** cgroup,
		struct pakfire_ctx* ctx, struct pakfire_cgroup* parent, const char* name, int flags) {
	struct pakfire_cgroup* c = NULL;
	int r;

	// Allocate the cgroup struct
	c = calloc(1, sizeof(*c));
	if (!c)
		return -errno;

	// Store a reference to the context
	c->ctx = pakfire_ctx_ref(ctx);

	// Initialize reference counter
	c->nrefs = 1;

	// Initialize file descriptors
	c->fd = c->devicesfd = -EBADF;

	// Store the parent
	if (parent)
		c->parent = pakfire_cgroup_ref(parent);

	// Store name
	r = pakfire_string_set(c->name, name);
	if (r < 0)
		goto ERROR;

	// Store path
	if (c->parent) {
		r = pakfire_path_append(c->path, c->parent->path, c->name);
		if (r < 0)
			goto ERROR;
	}

	// Copy flags
	c->flags = flags;

	// Open a file descriptor
	c->fd = r = __pakfire_cgroup_open(c);
	if (r < 0)
		goto ERROR;

	// Read controllers
	r = pakfire_cgroup_read_controllers(c);
	if (r < 0)
		goto ERROR;

#if 0
	// Setup the devices filter
	r = pakfire_cgroup_setup_devices(c);
	if (r)
		goto ERROR;
#endif

	DEBUG(c->ctx, "Created cgroup %s\n", pakfire_cgroup_path(c));

	// Return the pointer
	*cgroup = c;

	return 0;

ERROR:
	if (c)
		pakfire_cgroup_unref(c);

	return r;
}

static int pakfire_cgroup_create_recursive(struct pakfire_cgroup** cgroup,
		struct pakfire_ctx* ctx, struct pakfire_cgroup* parent, const char* name, int flags) {
	struct pakfire_cgroup* child = NULL;
	char buffer[NAME_MAX];
	char* p = NULL;
	int r;

	// Name cannot be empty
	if (!name || !*name)
		return -EINVAL;

	// Does the name contain a /?
	p = strchr(name, '/');

	// We have reached the end of the recursion
	if (!p)
		return pakfire_cgroup_open(cgroup, ctx, parent, name, flags);

	// Copy the string to the buffer
	r = pakfire_string_format(buffer, "%.*s", (int)(p - name), name);
	if (r < 0)
		return r;

	// Create the child cgroup
	r = pakfire_cgroup_child(&child, parent, buffer, flags);
	if (r < 0)
		goto ERROR;

	// Continue processing name...
	r = pakfire_cgroup_create_recursive(cgroup, ctx, child, ++p, flags);
	if (r < 0)
		goto ERROR;

	// Done

ERROR:
	if (child)
		pakfire_cgroup_unref(child);

	return r;
}

int pakfire_cgroup_create(struct pakfire_cgroup** cgroup,
		struct pakfire_ctx* ctx, struct pakfire_cgroup* parent, const char* name, int flags) {
	struct pakfire_cgroup* root = NULL;
	int r;

	// Name must be set
	if (!name)
		return -EINVAL;

	// If parent is NULL, we open the root as parent
	if (!parent) {
		r = pakfire_cgroup_open(&root, ctx, NULL, NULL, 0);
		if (r < 0)
			return r;

		parent = root;
	}

	// Recursively create the new cgroup
	r = pakfire_cgroup_create_recursive(cgroup, ctx, parent, name, flags);

	// Cleanup
	if (root)
		pakfire_cgroup_unref(root);

	return r;
}

static void pakfire_cgroup_free(struct pakfire_cgroup* cgroup) {
	if (cgroup->fd >= 0)
		close(cgroup->fd);
	if (cgroup->devicesfd >= 0)
		close(cgroup->devicesfd);
	if (cgroup->parent)
		pakfire_cgroup_unref(cgroup->parent);
	if (cgroup->ctx)
		pakfire_ctx_unref(cgroup->ctx);
	free(cgroup);
}

struct pakfire_cgroup* pakfire_cgroup_ref(struct pakfire_cgroup* cgroup) {
	++cgroup->nrefs;

	return cgroup;
}

struct pakfire_cgroup* pakfire_cgroup_unref(struct pakfire_cgroup* cgroup) {
	if (--cgroup->nrefs > 0)
		return cgroup;

	pakfire_cgroup_free(cgroup);
	return NULL;
}

int pakfire_cgroup_is_valid(struct pakfire_cgroup* self) {
	if (self->controllers)
		return 1;

	return 0;
}

int pakfire_cgroup_child(struct pakfire_cgroup** child,
		struct pakfire_cgroup* cgroup, const char* path, int flags) {
	return pakfire_cgroup_open(child, cgroup->ctx, cgroup, path, flags);
}

static int pakfire_cgroup_destroy_children(struct pakfire_cgroup* cgroup, int flags) {
	struct pakfire_cgroup* child = NULL;
	struct dirent* entry = NULL;
	DIR* dir = NULL;
	int r = 0;

	// Open the directory
	dir = fdopendir(cgroup->fd);
	if (!dir) {
		ERROR(cgroup->ctx, "Could not walk through child cgroups of %s: %m\n",
			pakfire_cgroup_path(cgroup));
		r = -errno;
		goto ERROR;
	}

	// Walk through everything in the directory
	for (;;) {
		entry = readdir(dir);
		if (!entry)
			break;

		// Skip . and .., and so on
		if (*entry->d_name == '.')
			continue;

		// We are only interested in sub-directories
		switch (entry->d_type) {
			case DT_DIR:
				break;

			default:
				continue;
		}

		// Try to open the cgroup
		r = pakfire_cgroup_child(&child, cgroup, entry->d_name, 0);
		if (r < 0)
			goto ERROR;

		// Destroy the cgroup
		r = pakfire_cgroup_destroy(child, flags);
		if (r < 0)
			goto ERROR;

		// Free the child
		pakfire_cgroup_unref(child);
		child = NULL;
	}

ERROR:
	if (child)
		pakfire_cgroup_unref(child);
	if (dir)
		closedir(dir);

	return r;
}

/*
	Immediately destroys this cgroup
*/
int pakfire_cgroup_destroy(struct pakfire_cgroup* cgroup, int flags) {
	int r;

	// Cannot call this for the root group
	if (!cgroup->parent)
		return -EPERM;

	// Destroy recursively?
	if (flags & PAKFIRE_CGROUP_DESTROY_RECURSIVE) {
		r = pakfire_cgroup_destroy_children(cgroup, flags);
		if (r < 0)
			return r;
	}

	DEBUG(cgroup->ctx, "Destroying cgroup %s\n", pakfire_cgroup_path(cgroup));

	// Delete the directory
	r = unlinkat(cgroup->parent->fd, cgroup->name, AT_REMOVEDIR);
	if (r) {
		ERROR(cgroup->ctx, "Could not destroy cgroup %s: %m\n", pakfire_cgroup_path(cgroup));
		return -errno;
	}

	// Close the file descriptor
	if (cgroup->fd >= 0) {
		close(cgroup->fd);
		cgroup->fd = -EBADF;
	}

	return r;
}

int pakfire_cgroup_fd(struct pakfire_cgroup* cgroup) {
	return cgroup->fd;
}

/*
	Immediately kills all processes in this cgroup
*/
int pakfire_cgroup_killall(struct pakfire_cgroup* cgroup) {
	DEBUG(cgroup->ctx, "Killing all processes in %s\n",
		pakfire_cgroup_path(cgroup));

	return pakfire_cgroup_write(cgroup, "cgroup.kill", "1");
}

// Accounting

int pakfire_cgroup_enable_accounting(struct pakfire_cgroup* cgroup) {
	int r;

	// CPU
	r = pakfire_cgroup_enable_controller(cgroup, PAKFIRE_CGROUP_CONTROLLER_CPU);
	if (r < 0)
		return r;

	// Memory
	r = pakfire_cgroup_enable_controller(cgroup, PAKFIRE_CGROUP_CONTROLLER_MEMORY);
	if (r < 0)
		return r;

	// IO
	r = pakfire_cgroup_enable_controller(cgroup, PAKFIRE_CGROUP_CONTROLLER_IO);
	if (r < 0)
		return r;

	return 0;
}

// Memory

int pakfire_cgroup_set_guaranteed_memory(struct pakfire_cgroup* cgroup, size_t mem) {
	int r;

	// Enable the memory controller
	r = pakfire_cgroup_enable_controller(cgroup, PAKFIRE_CGROUP_CONTROLLER_MEMORY);
	if (r < 0)
		return r;

	DEBUG(cgroup->ctx, "%s: Setting guaranteed memory to %zu byte(s)\n",
		pakfire_cgroup_path(cgroup), mem);

	// Set value
	r = pakfire_cgroup_write(cgroup, "memory.min", "%zu\n", mem);
	if (r < 0)
		ERROR(cgroup->ctx, "%s: Could not set guaranteed memory: %m\n",
			pakfire_cgroup_path(cgroup));

	return r;
}

int pakfire_cgroup_set_memory_limit(struct pakfire_cgroup* cgroup, size_t mem) {
	int r;

	// Enable the memory controller
	r = pakfire_cgroup_enable_controller(cgroup, PAKFIRE_CGROUP_CONTROLLER_MEMORY);
	if (r < 0)
		return r;

	DEBUG(cgroup->ctx, "%s: Setting memory limit to %zu byte(s)\n",
		pakfire_cgroup_path(cgroup), mem);

	// Set value
	r = pakfire_cgroup_write(cgroup, "memory.max", "%zu\n", mem);
	if (r < 0)
		ERROR(cgroup->ctx, "%s: Could not set memory limit: %m\n",
			pakfire_cgroup_path(cgroup));

	return r;
}

// PIDs

int pakfire_cgroup_set_pid_limit(struct pakfire_cgroup* cgroup, size_t limit) {
	int r;

	// Enable the PID controller
	r = pakfire_cgroup_enable_controller(cgroup, PAKFIRE_CGROUP_CONTROLLER_PIDS);
	if (r < 0)
		return r;

	DEBUG(cgroup->ctx, "%s: Setting PID limit to %zu\n",
		pakfire_cgroup_path(cgroup), limit);

	// Set value
	r = pakfire_cgroup_write(cgroup, "pids.max", "%zu\n", limit);
	if (r < 0)
		ERROR(cgroup->ctx, "%s: Could not set PID limit: %m\n",
			pakfire_cgroup_path(cgroup));

	return r;
}

// Stats

static int __pakfire_cgroup_read_stats_line(struct pakfire_cgroup* cgroup,
		int (*callback)(struct pakfire_cgroup* cgroup, const char* key, unsigned long val, void* data),
		void* data, char* line) {
	char* p = NULL;
	int r;

	char key[NAME_MAX];
	unsigned long val = 0;

	// Number of the field
	int i = 0;

	char* elem = strtok_r(line, " ", &p);
	while (elem) {
		switch (i++) {
			// First field is the key
			case 0:
				// Copy the key
				r = pakfire_string_set(key, elem);
				if (r < 0)
					return r;
				break;

			// The second field is some value
			case 1:
				val = strtoul(elem, NULL, 10);
				break;

			// Ignore the rest
			default:
				DEBUG(cgroup->ctx, "%s: Unknown value in cgroup stats (%d): %s\n",
					pakfire_cgroup_path(cgroup), i, elem);
				break;
		}

		elem = strtok_r(NULL, " ", &p);
	}

	// Check if we parsed both fields
	if (i < 2) {
		ERROR(cgroup->ctx, "Could not parse line\n");
		return -EINVAL;
	}

	// Call the callback
	return callback(cgroup, key, val, data);
}

static int __pakfire_cgroup_read_stats(struct pakfire_cgroup* cgroup, const char* path,
		int (*callback)(struct pakfire_cgroup* cgroup, const char* key, unsigned long val, void* data),
		void* data) {
	char buffer[BUFFER_SIZE];
	char* p = NULL;
	int r;

	DEBUG(cgroup->ctx, "%s: Reading stats from %s\n", pakfire_cgroup_path(cgroup), path);

	// Open the file
	r = pakfire_cgroup_read(cgroup, path, buffer, sizeof(buffer));
	if (r < 0)
		goto ERROR;

	char* line = strtok_r(buffer, "\n", &p);
	while (line) {
		// Parse the line
		r = __pakfire_cgroup_read_stats_line(cgroup, callback, data, line);
		if (r)
			goto ERROR;

		// Move to the next line
		line = strtok_r(NULL, "\n", &p);
	}

ERROR:
	return r;
}

struct pakfire_cgroup_stat_entry {
	const char* key;
	unsigned long* val;
};

static int __pakfire_cgroup_parse_cpu_stats(struct pakfire_cgroup* cgroup,
		const char* key, unsigned long val, void* data) {
	struct pakfire_cgroup_cpu_stats* stats = (struct pakfire_cgroup_cpu_stats*)data;

	const struct pakfire_cgroup_stat_entry entries[] = {
		{ "system_usec", &stats->system_usec },
		{ "usage_usec", &stats->usage_usec },
		{ "user_usec", &stats->user_usec },
		{ "nr_periods", &stats->nr_periods },
		{ "nr_throttled", &stats->nr_throttled },
		{ "throttled_usec", &stats->throttled_usec },
		{ "nr_bursts", &stats->nr_bursts },
		{ "burst_usec", &stats->burst_usec },
		{ NULL, NULL },
	};
	// Find and store value
	for (const struct pakfire_cgroup_stat_entry* entry = entries; entry->key; entry++) {
		if (strcmp(entry->key, key) == 0) {
			*entry->val = val;
			return 0;
		}
	}

	DEBUG(cgroup->ctx, "Unknown key for CPU stats: %s = %lu\n", key, val);

	return 0;
}

static int __pakfire_cgroup_parse_memory_stats(struct pakfire_cgroup* cgroup,
		const char* key, unsigned long val, void* data) {
	struct pakfire_cgroup_memory_stats* stats = (struct pakfire_cgroup_memory_stats*)data;

	const struct pakfire_cgroup_stat_entry entries[] = {
		{ "anon", &stats->anon },
		{ "file", &stats->file },
		{ "kernel", &stats->kernel },
		{ "kernel_stack", &stats->kernel_stack },
		{ "pagetables", &stats->pagetables },
		{ "sec_pagetables", &stats->sec_pagetables },
		{ "percpu", &stats->percpu },
		{ "sock", &stats->sock },
		{ "vmalloc", &stats->vmalloc },
		{ "shmem", &stats->shmem },
		{ "zswap", &stats->zswap },
		{ "zswapped", &stats->zswapped },
		{ "file_mapped", &stats->file_mapped },
		{ "file_dirty", &stats->file_dirty },
		{ "file_writeback", &stats->file_writeback },
		{ "swapcached", &stats->swapcached },
		{ "anon_thp", &stats->anon_thp },
		{ "file_thp", &stats->file_thp },
		{ "shmem_thp", &stats->shmem_thp },
		{ "inactive_anon", &stats->inactive_anon },
		{ "active_anon", &stats->active_anon },
		{ "inactive_file", &stats->inactive_file },
		{ "active_file", &stats->active_file },
		{ "unevictable", &stats->unevictable },
		{ "slab_reclaimable", &stats->slab_reclaimable },
		{ "slab_unreclaimable", &stats->slab_unreclaimable },
		{ "slab", &stats->slab },
		{ "workingset_refault_anon", &stats->workingset_refault_anon },
		{ "workingset_refault_file", &stats->workingset_refault_file },
		{ "workingset_activate_anon", &stats->workingset_activate_anon },
		{ "workingset_activate_file", &stats->workingset_activate_file },
		{ "workingset_restore_anon", &stats->workingset_restore_anon },
		{ "workingset_restore_file", &stats->workingset_restore_file },
		{ "workingset_nodereclaim", &stats->workingset_nodereclaim },
		{ "pgfault", &stats->pgfault },
		{ "pgmajfault", &stats->pgmajfault },
		{ "pgrefill", &stats->pgrefill },
		{ "pgscan", &stats->pgscan },
		{ "pgsteal", &stats->pgsteal },
		{ "pgscan_kswapd", &stats->pgscan_kswapd },
		{ "pgscan_direct", &stats->pgscan_direct },
		{ "pgsteal_kswapd", &stats->pgsteal_kswapd },
		{ "pgsteal_direct", &stats->pgsteal_direct },
		{ "pgactivate", &stats->pgactivate },
		{ "pgdeactivate", &stats->pgdeactivate },
		{ "pglazyfree", &stats->pglazyfree },
		{ "pglazyfreed", &stats->pglazyfreed },
		{ "thp_fault_alloc", &stats->thp_fault_alloc },
		{ "thp_collapse_alloc", &stats->thp_collapse_alloc },
		{ NULL, NULL },
	};

	// Find and store value
	for (const struct pakfire_cgroup_stat_entry* entry = entries; entry->key; entry++) {
		if (strcmp(entry->key, key) == 0) {
			*entry->val = val;
			return 0;
		}
	}

	// Log any unknown keys
	DEBUG(cgroup->ctx, "Unknown key for memory stats: %s = %lu\n", key, val);

	return 0;
}

int pakfire_cgroup_stat(struct pakfire_cgroup* cgroup,
		struct pakfire_cgroup_stats* stats) {
	int r;

	// Check input
	if (!stats)
		return -EINVAL;

	// Read CPU stats
	r = __pakfire_cgroup_read_stats(cgroup, "cpu.stat",
			__pakfire_cgroup_parse_cpu_stats, &stats->cpu);
	if (r < 0)
		goto ERROR;

	// Read memory stats
	if (cgroup->controllers & PAKFIRE_CGROUP_CONTROLLER_MEMORY) {
		r = __pakfire_cgroup_read_stats(cgroup, "memory.stat",
				__pakfire_cgroup_parse_memory_stats, &stats->memory);
		if (r < 0)
			goto ERROR;
	}

ERROR:
	if (r)
		ERROR(cgroup->ctx, "%s: Could not read cgroup stats: %m\n",
			pakfire_cgroup_path(cgroup));

	return r;
}

int pakfire_cgroup_stat_dump(struct pakfire_cgroup* cgroup,
		const struct pakfire_cgroup_stats* stats) {
	// Check input
	if (!stats)
		return -EINVAL;

	DEBUG(cgroup->ctx, "%s: Total CPU time usage: %lu\n",
		pakfire_cgroup_path(cgroup), stats->cpu.usage_usec);

	return 0;
}
