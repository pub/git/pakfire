/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2017 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <stdlib.h>

#include <pakfire/constants.h>
#include <pakfire/ctx.h>
#include <pakfire/deps.h>
#include <pakfire/i18n.h>
#include <pakfire/logging.h>
#include <pakfire/pakfire.h>
#include <pakfire/problem.h>
#include <pakfire/solution.h>
#include <pakfire/transaction.h>
#include <pakfire/util.h>

struct pakfire_problem {
	struct pakfire_ctx* ctx;
	struct pakfire* pakfire;
	int nrefs;

	struct pakfire_transaction* transaction;
	Id id;
	char* string;

	Solver* solver;
};

static char* pakfire_problem_make_string(struct pakfire_problem* problem) {
	Solver* solver = pakfire_transaction_get_solver(problem->transaction);
	Pool* pool = solver->pool;

	// Get the problem rule
	Id rule = solver_findproblemrule(solver, problem->id);

	// Extract some information about that rule
	Id dep;
	Id source;
	Id target;

	SolverRuleinfo type = solver_ruleinfo(solver, rule, &source, &target, &dep);

	char* s = NULL;
	int r = 1;

	switch (type) {
		case SOLVER_RULE_DISTUPGRADE:
			r = asprintf(&s, _("%s does not belong to a distupgrade repository"),
				pool_solvid2str(pool, source));
			break;

		case SOLVER_RULE_INFARCH:
			r = asprintf(&s, _("%s has inferior architecture"),
				pool_solvid2str(pool, source));
			break;

		case SOLVER_RULE_UPDATE:
			r = asprintf(&s, _("problem with installed package %s"),
				pool_solvid2str(pool, source));
			break;

		case SOLVER_RULE_JOB:
			r = asprintf(&s, _("conflicting requests"));
			break;

		case SOLVER_RULE_JOB_UNSUPPORTED:
			r = asprintf(&s, _("unsupported request"));
			break;

		case SOLVER_RULE_JOB_NOTHING_PROVIDES_DEP:
			r = asprintf(&s, _("nothing provides requested %s"),
				pakfire_dep2str(problem->pakfire, dep));
			break;

		case SOLVER_RULE_JOB_UNKNOWN_PACKAGE:
			r = asprintf(&s, _("package %s does not exist"),
				pakfire_dep2str(problem->pakfire, dep));
			break;

		case SOLVER_RULE_JOB_PROVIDED_BY_SYSTEM:
			r = asprintf(&s, _("%s is provided by the system"),
				pakfire_dep2str(problem->pakfire, dep));
			break;

		case SOLVER_RULE_RPM:
			r = asprintf(&s, _("some dependency problem"));
			break;

		case SOLVER_RULE_BEST:
			if (source > 0)
				r = asprintf(&s, _("cannot install the best update candidate for package %s"),
					pool_solvid2str(pool, source));
			else
				r = asprintf(&s, _("cannot install the best candidate for the job"));
			break;

		case SOLVER_RULE_RPM_NOT_INSTALLABLE:
			r = asprintf(&s, _("package %s is not installable"),
				pool_solvid2str(pool, source));
			break;

		case SOLVER_RULE_RPM_NOTHING_PROVIDES_DEP:
			r = asprintf(&s, _("nothing provides %s needed by %s"),
				pakfire_dep2str(problem->pakfire, dep), pool_solvid2str(pool, source));
			break;

		case SOLVER_RULE_RPM_SAME_NAME:
			r = asprintf(&s, _("cannot install both %s and %s"),
				pool_solvid2str(pool, source), pool_solvid2str(pool, target));
			break;

		case SOLVER_RULE_RPM_PACKAGE_CONFLICT:
			r = asprintf(&s, _("package %s conflicts with %s provided by %s"),
				pool_solvid2str(pool, source), pakfire_dep2str(problem->pakfire, dep),
				pool_solvid2str(pool, target));
			break;

		case SOLVER_RULE_RPM_PACKAGE_OBSOLETES:
			r = asprintf(&s, _("package %s obsoletes %s provided by %s"),
				pool_solvid2str(pool, source), pakfire_dep2str(problem->pakfire, dep),
				pool_solvid2str(pool, target));
			break;

		case SOLVER_RULE_RPM_INSTALLEDPKG_OBSOLETES:
			r = asprintf(&s, _("installed package %s obsoletes %s provided by %s"),
				pool_solvid2str(pool, source), pakfire_dep2str(problem->pakfire, dep),
				pool_solvid2str(pool, target));
			break;

		case SOLVER_RULE_RPM_IMPLICIT_OBSOLETES:
			r = asprintf(&s, _("package %s implicitely obsoletes %s provided by %s"),
				pool_solvid2str(pool, source), pakfire_dep2str(problem->pakfire, dep),
				pool_solvid2str(pool, target));
			break;

		case SOLVER_RULE_RPM_PACKAGE_REQUIRES:
			r = asprintf(&s, _("package %s requires %s, but none of the providers can be installed"),
				pool_solvid2str(pool, source), pakfire_dep2str(problem->pakfire, dep));
			break;

		case SOLVER_RULE_RPM_SELF_CONFLICT:
			r = asprintf(&s, _("package %s conflicts with %s provided by itself"),
				pool_solvid2str(pool, source), pakfire_dep2str(problem->pakfire, dep));
			break;

		case SOLVER_RULE_YUMOBS:
			r = asprintf(&s, _("both package %s and %s obsolete %s"),
				pool_solvid2str(pool, source), pool_solvid2str(pool, target),
				pakfire_dep2str(problem->pakfire, dep));
			break;

		case SOLVER_RULE_BLACK:
			r = asprintf(&s, _("package %s can only be installed by direct request"),
				pool_solvid2str(pool, source));
			break;

		case SOLVER_RULE_PKG_CONSTRAINS:
			r = asprintf(&s, _("package %s has constraint %s conflicting with %s"),
				pool_solvid2str(pool, source), pakfire_dep2str(problem->pakfire, dep),
				pool_solvid2str(pool, target));
			break;

		default:
			r = asprintf(&s, _("bad rule type"));
			break;
	}

	// Return nothing if asprintf failed
	if (r < 0)
		return NULL;

	return s;
}

int pakfire_problem_create(struct pakfire_problem** problem,
		struct pakfire* pakfire, struct pakfire_transaction* transaction, Id id) {
	struct pakfire_problem* p = NULL;

	// Allocate some memory
	p = calloc(1, sizeof(*p));
	if (!p)
		return -errno;

	// Store a reference to the context
	p->ctx = pakfire_ctx(pakfire);

	// Store a reference to Pakfire
	p->pakfire = pakfire_ref(pakfire);

	// Initialize the reference counter
	p->nrefs = 1;

	// Store a reference to the transaction
	p->transaction = pakfire_transaction_ref(transaction);

	// Store the ID
	p->id = id;

	// Fetch a reference to the solver
	p->solver = pakfire_transaction_get_solver(transaction);

	// Return the pointer
	*problem = p;

	return 0;
}

static void pakfire_problem_free(struct pakfire_problem* problem) {
	if (problem->transaction)
		pakfire_transaction_unref(problem->transaction);
	if (problem->string)
		free(problem->string);
	if (problem->pakfire)
		pakfire_unref(problem->pakfire);
	if (problem->ctx)
		pakfire_ctx_unref(problem->ctx);
	free(problem);
}

struct pakfire_problem* pakfire_problem_ref(struct pakfire_problem* problem) {
	problem->nrefs++;

	return problem;
}

struct pakfire_problem* pakfire_problem_unref(struct pakfire_problem* problem) {
	if (--problem->nrefs > 0)
		return problem;

	pakfire_problem_free(problem);
	return NULL;
}

const char* pakfire_problem_to_string(struct pakfire_problem* problem) {
	if (!problem->string)
		problem->string = pakfire_problem_make_string(problem);

	return problem->string;
}

Id pakfire_problem_get_id(struct pakfire_problem* problem) {
	return problem->id;
}

struct pakfire_transaction* pakfire_problem_get_transaction(struct pakfire_problem* problem) {
	return pakfire_transaction_ref(problem->transaction);
}

struct pakfire_solution** pakfire_problem_get_solutions(
		struct pakfire_problem* problem) {
	struct pakfire_solution** solutions = NULL;
	struct pakfire_solution* solution = NULL;
	unsigned int count = 0;
	Id id = ID_NULL;
	int r;

	// Fetch how many solutions we have
	count = solver_solution_count(problem->solver, problem->id);
	if (!count)
		return NULL;

	// Allocate some space
	solutions = calloc(count + 1, sizeof(*solutions));
	if (!solutions) {
		r = -errno;
		goto ERROR;
	}

	for (unsigned int i = 0; i < count; i++) {
		id = solver_next_solution(problem->solver, problem->id, id);
		if (!id)
			break;

		// Create a new solution
		r = pakfire_solution_create(&solution, problem->pakfire, problem, id);
		if (r)
			goto ERROR;

		// Store the reference
		solutions[i] = solution;
	}

	return solutions;

ERROR:
	ERROR(problem->ctx, "Could not import solutions: %s\n", strerror(r));

	if (solutions) {
		for (struct pakfire_solution** s = solutions; *s; s++)
			pakfire_solution_unref(*s);

		free(solutions);
	}

	return NULL;
}
