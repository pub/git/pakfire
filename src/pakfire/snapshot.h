/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_SNAPSHOT_H
#define PAKFIRE_SNAPSHOT_H

#include <pakfire/ctx.h>
#include <pakfire/pakfire.h>

struct pakfire_snapshot;

int pakfire_snapshot_create(
	struct pakfire_snapshot** snapshot, struct pakfire_ctx* ctx, const char* path);

struct pakfire_snapshot* pakfire_snapshot_ref(struct pakfire_snapshot* self);
struct pakfire_snapshot* pakfire_snapshot_unref(struct pakfire_snapshot* self);

int pakfire_snapshot_find(struct pakfire_snapshot** snapshot, struct pakfire* pakfire);

const char* pakfire_snapshot_path(struct pakfire_snapshot* self);

int pakfire_snapshot_mount(struct pakfire_snapshot* self, const char* path);
int pakfire_snapshot_umount(struct pakfire_snapshot* self);

int pakfire_snapshot_make(struct pakfire_snapshot** snapshot, struct pakfire* pakfire);

int pakfire_snapshot_clean(struct pakfire* pakfire);

#endif /* PAKFIRE_SNAPSHOT_H */
