/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2022 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <linux/limits.h>
#include <stddef.h>
#include <sys/mount.h>
#include <sys/stat.h>
#include <sys/sysmacros.h>
#include <sys/types.h>

#include <pakfire/arch.h>
#include <pakfire/logging.h>
#include <pakfire/pakfire.h>
#include <pakfire/parse.h>
#include <pakfire/path.h>
#include <pakfire/mount.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

static const struct pakfire_mountpoint {
	pakfire_mntns_t ns;
	const char* source;
	const char* target;
	const char* fstype;
	int flags;
	const char* options;
} mountpoints[] = {
	// Mount a new instance of /proc
	{
		PAKFIRE_MNTNS_INNER|PAKFIRE_MNTNS_OUTER,
		"pakfire_proc",
		"proc",
		"proc",
		MS_NOSUID|MS_NOEXEC|MS_NODEV,
		NULL,
	},

	/*
		XXX it is kind of problematic to mount /proc twice as a process inside the
		jail can umount /proc and will then see the host's /proc.
	*/

	// Make /proc/sys read-only (except /proc/sys/net)
	{
		PAKFIRE_MNTNS_INNER,
		"/proc/sys",
		"proc/sys",
		"bind",
		MS_BIND|MS_REC,
		NULL,
	},
	{
		PAKFIRE_MNTNS_INNER,
		"/proc/sys/net",
		"proc/sys/net",
		"bind",
		MS_BIND|MS_REC,
		NULL,
	},
	{
		PAKFIRE_MNTNS_INNER,
		"/proc/sys",
		"proc/sys",
		"bind",
		MS_BIND|MS_RDONLY|MS_NOSUID|MS_NOEXEC|MS_NODEV|MS_REMOUNT,
		NULL,
	},

	// Deny write access to /proc/sysrq-trigger (can be used to restart the host)
	{
		PAKFIRE_MNTNS_INNER,
		"/proc/sysrq-trigger",
		"proc/sysrq-trigger",
		"bind",
		MS_BIND|MS_REC,
		NULL,
	},
	{
		PAKFIRE_MNTNS_INNER,
		"/proc/sysrq-trigger",
		"proc/sysrq-trigger",
		"bind",
		MS_BIND|MS_RDONLY|MS_NOSUID|MS_NOEXEC|MS_NODEV|MS_REMOUNT,
		NULL,
	},

	// Make /proc/irq read-only
	{
		PAKFIRE_MNTNS_INNER,
		"/proc/irq",
		"proc/irq",
		"bind",
		MS_BIND|MS_REC,
		NULL,
	},
	{
		PAKFIRE_MNTNS_INNER,
		"/proc/irq",
		"proc/irq",
		"bind",
		MS_BIND|MS_RDONLY|MS_NOSUID|MS_NOEXEC|MS_NODEV|MS_REMOUNT,
		NULL,
	},

	// Make /proc/bus read-only
	{
		PAKFIRE_MNTNS_INNER,
		"/proc/bus",
		"proc/bus",
		"bind",
		MS_BIND|MS_REC,
		NULL,
	},
	{
		PAKFIRE_MNTNS_INNER,
		"/proc/bus",
		"proc/bus",
		"bind",
		MS_BIND|MS_RDONLY|MS_NOSUID|MS_NOEXEC|MS_NODEV|MS_REMOUNT,
		NULL,
	},

	// Bind-Mount /sys ready-only
	{
		PAKFIRE_MNTNS_OUTER,
		"/sys",
		"sys",
		"bind",
		MS_BIND|MS_REC,
		NULL,
	},
	{
		PAKFIRE_MNTNS_OUTER,
		"/sys",
		"sys",
		"bind",
		MS_BIND|MS_RDONLY|MS_NOSUID|MS_NOEXEC|MS_NODEV|MS_REMOUNT,
		NULL,
	},

	// Create a new /dev
	{
		PAKFIRE_MNTNS_OUTER,
		"pakfire_dev",
		"dev",
		"tmpfs",
		MS_NOSUID|MS_NOEXEC,
		"mode=0755,size=4m,nr_inodes=64k",
	},
	{
		PAKFIRE_MNTNS_OUTER,
		"pakfire_dev_pts",
		"dev/pts",
		"devpts",
		MS_NOSUID|MS_NOEXEC,
		"newinstance,ptmxmode=0666,mode=620",
	},

	// Create a new /dev/shm
	{
		PAKFIRE_MNTNS_OUTER,
		"pakfire_dev_shm",
		"dev/shm",
		"tmpfs",
		MS_NOSUID|MS_NODEV|MS_STRICTATIME,
		"mode=1777,size=1024m",
	},

	// Mount /dev/mqueue
	{
		PAKFIRE_MNTNS_INNER,
		"mqueue",
		"dev/mqueue",
		"mqueue",
		MS_NOSUID|MS_NOEXEC|MS_NODEV,
		NULL,
	},

	// Create a new /run
	{
		PAKFIRE_MNTNS_OUTER,
		"pakfire_run",
		"run",
		"tmpfs",
		MS_NOSUID|MS_NOEXEC|MS_NODEV,
		"mode=755,size=256m,nr_inodes=1k",
	},

	// Create a new /tmp
	{
		PAKFIRE_MNTNS_OUTER,
		"pakfire_tmp",
		"tmp",
		"tmpfs",
		MS_NOSUID|MS_NODEV|MS_STRICTATIME,
		"mode=1777,size=4096m",
	},

	// The end
	{},
};

static const struct pakfire_devnode {
	const char* path;
	int major;
	int minor;
	mode_t mode;
	int flags;
} devnodes[] = {
	{ "/dev/null",      1,  3, S_IFCHR|S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH, 0 },
	{ "/dev/zero",      1,  5, S_IFCHR|S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH, 0 },
	{ "/dev/full",      1,  7, S_IFCHR|S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH, 0 },
	{ "/dev/random",    1,  8, S_IFCHR|S_IRUSR|S_IRGRP|S_IROTH, 0 },
	{ "/dev/urandom",   1,  9, S_IFCHR|S_IRUSR|S_IRGRP|S_IROTH, 0 },
	{ "/dev/kmsg",      1, 11, S_IFCHR|S_IRUSR|S_IRGRP|S_IROTH, 0 },
	{ "/dev/tty",       5,  0, S_IFCHR|S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH, 0 },
	{ "/dev/rtc0",    252,  0, S_IFCHR|S_IRUSR|S_IWUSR, 0 },

	// Loop Devices
	{ "/dev/loop-control", 10, 237, S_IFCHR|S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP, PAKFIRE_MOUNT_LOOP_DEVICES },
	{ "/dev/loop0",         7,   0, S_IFBLK|S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP, PAKFIRE_MOUNT_LOOP_DEVICES },
	{ "/dev/loop1",         7,   1, S_IFBLK|S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP, PAKFIRE_MOUNT_LOOP_DEVICES },
	{ "/dev/loop2",         7,   2, S_IFBLK|S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP, PAKFIRE_MOUNT_LOOP_DEVICES },
	{ "/dev/loop3",         7,   3, S_IFBLK|S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP, PAKFIRE_MOUNT_LOOP_DEVICES },
	{ "/dev/loop4",         7,   4, S_IFBLK|S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP, PAKFIRE_MOUNT_LOOP_DEVICES },
	{ "/dev/loop5",         7,   5, S_IFBLK|S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP, PAKFIRE_MOUNT_LOOP_DEVICES },
	{ "/dev/loop6",         7,   6, S_IFBLK|S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP, PAKFIRE_MOUNT_LOOP_DEVICES },
	{ "/dev/loop7",         7,   7, S_IFBLK|S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP, PAKFIRE_MOUNT_LOOP_DEVICES },

	{ NULL },
};

static const struct pakfire_symlink {
	const char* target;
	const char* path;
} symlinks[] = {
	{ "/dev/pts/ptmx",   "/dev/ptmx", },
	{ "/proc/self/fd",   "/dev/fd", },
	{ "/proc/self/fd/0", "/dev/stdin" },
	{ "/proc/self/fd/1", "/dev/stdout" },
	{ "/proc/self/fd/2", "/dev/stderr" },
	{ "/proc/kcore",     "/dev/core" },
	{ NULL },
};

int pakfire_mount_change_propagation(struct pakfire_ctx* ctx, const char* path, int propagation) {
	DEBUG(ctx, "Changing mount propagation on %s\n", path);

	int r = mount(NULL, path, NULL, propagation|MS_REC, NULL);
	if (r)
		ERROR(ctx, "Failed to change mount propagation on %s: %m\n", path);

	return r;
}

static int pakfire_mount_is_mountpoint(struct pakfire_ctx* ctx, const char* path) {
	// XXX THIS STILL NEEDS TO BE IMPLEMENTED
	return 1;
}

int pakfire_mount_make_mounpoint(struct pakfire_ctx* ctx, const char* path) {
	int r;

	// Check if path already is a mountpoint
	r = pakfire_mount_is_mountpoint(ctx, path);
	switch (r) {
		// Already is a mountpoint
		case 0:
			return 0;

		// Is not a mountpoint
		case 1:
			break;

		default:
			ERROR(ctx, "Could not determine whether %s is a mountpoint: %m\n", path);
			return r;
	}

	// Bind-mount to self
	r = mount(path, path, NULL, MS_BIND|MS_REC, NULL);
	if (r) {
		ERROR(ctx, "Could not make %s a mountpoint: %m\n", path);
		return r;
	}

	return 0;
}

static int pakfire_mount(struct pakfire_ctx* ctx, const char* source, const char* target,
		const char* fstype, unsigned long mflags, const void* data) {
	const char* options = (const char*)data;

	// Check for some basic inputs
	if (!source || !target) {
		errno = EINVAL;
		return 1;
	}

	DEBUG(ctx, "Mounting %s from %s (%s - %s)\n", target, source, fstype, options);

	// Perform mount()
	int r = mount(source, target, fstype, mflags, data);
	if (r) {
		ERROR(ctx, "Could not mount %s: %m\n", target);
	}

	return r;
}

static int __pakfire_mount_list(char* line, size_t length, void* data) {
	struct pakfire_ctx* ctx = data;

	// Send the line to the logger
	DEBUG(ctx, "  %.*s", (int)length, line);

	return 0;
}

int pakfire_mount_list(struct pakfire_ctx* ctx) {
	DEBUG(ctx, "Mountpoints:\n");

	return pakfire_parse_file("/proc/self/mounts", __pakfire_mount_list, ctx);
}

int pakfire_populate_dev(struct pakfire_ctx* ctx, struct pakfire* pakfire, int flags) {
	char path[PATH_MAX];

	// Create device nodes
	for (const struct pakfire_devnode* devnode = devnodes; devnode->path; devnode++) {
		DEBUG(ctx, "Creating device node %s\n", devnode->path);

		// Check if flags match
		if (devnode->flags && !(flags & devnode->flags))
			continue;

		int r = pakfire_path(pakfire, path, "%s", devnode->path);
		if (r)
			return r;

		dev_t dev = makedev(devnode->major, devnode->minor);

		r = mknod(path, devnode->mode, dev);

		// Continue if mknod was successful
		if (r == 0)
			continue;

		// If we could not create the device node because of permission issues,
		// it might be likely that we are running in a user namespace where creating
		// device nodes is not permitted. Try bind-mounting them.
		if (errno == EPERM)
			goto MOUNT;

		// Otherwise log an error and end
		ERROR(ctx, "Could not create %s: %m\n", devnode->path);
		return r;

MOUNT:
		// Create an empty file
		r = pakfire_touch(path, 0444);
		if (r) {
			ERROR(ctx, "Could not create %s: %m\n", path);
			return r;
		}

		// Create a bind-mount over the file
		r = pakfire_mount(ctx, devnode->path, path, "bind", MS_BIND, NULL);
		if (r)
			return r;
	}

	// Create symlinks
	for (const struct pakfire_symlink* s = symlinks; s->target; s++) {
		DEBUG(ctx, "Creating symlink %s -> %s\n", s->path, s->target);

		int r = pakfire_path(pakfire, path, "%s", s->path);
		if (r)
			return r;

		r = symlink(s->target, path);
		if (r) {
			ERROR(ctx, "Could not create symlink %s: %m\n", s->path);
			return r;
		}
	}

	return 0;
}

int pakfire_mount_interpreter(struct pakfire_ctx* ctx, struct pakfire* pakfire) {
	char target[PATH_MAX];

	// Fetch the target architecture
	const char* arch = pakfire_get_effective_arch(pakfire);

	// Can we emulate this architecture?
	char* interpreter = pakfire_arch_find_interpreter(arch);

	// No interpreter required
	if (!interpreter)
		return 0;

	DEBUG(ctx, "Mounting interpreter %s for %s\n", interpreter, arch);

	// Where to mount this?
	int r = pakfire_path(pakfire, target, "%s", interpreter);
	if (r)
		return r;

	// Create directory
	r = pakfire_mkparentdir(target, 0755);
	if (r)
		return r;

	// Create an empty file
	FILE* f = fopen(target, "w");
	if (!f)
		return 1;
	fclose(f);

	r = pakfire_mount(ctx, interpreter, target, NULL, MS_BIND|MS_RDONLY, NULL);
	if (r)
		ERROR(ctx, "Could not mount interpreter %s to %s: %m\n", interpreter, target);

	return r;
}

int pakfire_mount_all(struct pakfire_ctx* ctx, struct pakfire* pakfire, pakfire_mntns_t ns, int flags) {
	char target[PATH_MAX];
	int r;

	const char* root = "/";

	// Fetch Pakfire's root directory
	if (ns == PAKFIRE_MNTNS_OUTER)
		root = pakfire_get_path(pakfire);

	for (const struct pakfire_mountpoint* mp = mountpoints; mp->source; mp++) {
		if (!(mp->ns & ns))
			continue;

		// Figure out where to mount
		r = pakfire_path_append(target, root, mp->target);
		if (r)
			return r;

		// Create target if it doesn't exist
		if (!pakfire_path_exists(target)) {
			r = pakfire_mkdir(target, 0755);
			if (r) {
				ERROR(ctx, "Could not create %s: %m\n", target);
				return r;
			}
		}

		// Perform mount()
		r = pakfire_mount(ctx, mp->source, target, mp->fstype, mp->flags, mp->options);
		if (r)
			return r;
	}

	return 0;
}

int pakfire_bind(struct pakfire_ctx* ctx, struct pakfire* pakfire,
		const char* src, const char* dst, int flags) {
	struct stat st;
	char mountpoint[PATH_MAX];

	if (!dst)
		dst = src;

	int r = pakfire_path(pakfire, mountpoint, "%s", dst);
	if (r)
		return r;

	DEBUG(ctx, "Bind-mounting %s to %s\n", src, mountpoint);

	r = stat(src, &st);
	if (r < 0) {
		ERROR(ctx, "Could not stat %s: %m\n", src);
		return 1;
	}

	// Make sure the mountpoint exists
	switch (st.st_mode & S_IFMT) {
		case S_IFDIR:
			r = pakfire_mkdir(mountpoint, st.st_mode);
			if (r && errno != EEXIST)
				return r;
			break;

		case S_IFREG:
		case S_IFLNK:
			// Make parent directory
			r = pakfire_mkparentdir(mountpoint, 0755);
			if (r)
				return r;

			// Create a file
			FILE* f = fopen(mountpoint, "w");
			if (!f)
				return 1;
			fclose(f);
			break;

		default:
			errno = ENOTSUP;
			return 1;
	}

	// The Linux kernel seems to be quite funny when trying to bind-mount something
	// as read-only and requires us to mount the source first, and then remount it
	// again using MS_RDONLY.
	if (flags & MS_RDONLY) {
		r = pakfire_mount(ctx, src, mountpoint, "bind", MS_BIND|MS_REC, NULL);
		if (r)
			return r;

		// Add the remount flag
		flags |= MS_REMOUNT;
	}

	// Perform mount
	return pakfire_mount(ctx, src, mountpoint, "bind", flags|MS_BIND|MS_REC, NULL);
}
