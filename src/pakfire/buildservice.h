/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_BUILDSERVICE_H
#define PAKFIRE_BUILDSERVICE_H

struct pakfire_buildservice;

#include <json.h>

#include <pakfire/ctx.h>

int pakfire_buildservice_create(struct pakfire_buildservice** service,
	struct pakfire_ctx* ctx, const char* url);

struct pakfire_buildservice* pakfire_buildservice_ref(struct pakfire_buildservice* service);
struct pakfire_buildservice* pakfire_buildservice_unref(struct pakfire_buildservice* service);

const char* pakfire_buildservice_get_url(struct pakfire_buildservice* service);

// Builds

typedef enum pakfire_buildservice_build_flags {
	PAKFIRE_BUILDSERVICE_DISABLE_TESTS = (1 << 0),
} pakfire_buildservice_build_flags_t;

int pakfire_buildservice_build(struct pakfire_buildservice* service, const char* upload,
	const char* repo, const char** arches, int flags);

// Uploads

int pakfire_buildservice_upload(struct pakfire_buildservice* service,
	const char* path, const char* filename, char** uuid);
int pakfire_buildservice_list_uploads(
	struct pakfire_buildservice* service, struct json_object** uploads);
int pakfire_buildservice_delete_upload(
	struct pakfire_buildservice* service, const char* uuid);

// Repositories

int pakfire_buildservice_list_repos(struct pakfire_buildservice* service,
	const char* distro, struct json_object** repos);
int pakfire_buildservice_get_repo(struct pakfire_buildservice* service,
	const char* distro, const char* name, struct json_object** repo);
int pakfire_buildservice_create_repo(struct pakfire_buildservice* service,
	const char* distro, const char* name, const char* description, struct json_object** repo);
int pakfire_buildservice_delete_repo(struct pakfire_buildservice* service,
	const char* distro, const char* name);

#endif /* PAKFIRE_BUILDSERVICE_H */
