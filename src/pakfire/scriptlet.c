/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include <pakfire/ctx.h>
#include <pakfire/jail.h>
#include <pakfire/logging.h>
#include <pakfire/pakfire.h>
#include <pakfire/scriptlet.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

const char* pakfire_scriptlet_types[] = {
	"prein",
	"preun",
	"preup",
	"postin",
	"postun",
	"postup",
	"pretransin",
	"pretransun",
	"pretransup",
	"posttransin",
	"posttransun",
	"posttransup",
	NULL,
};

struct pakfire_scriptlet {
	struct pakfire_ctx* ctx;
	int nrefs;

	// Type
	char type[NAME_MAX];

	// Data and size
	char* data;
	size_t size;
};

static int pakfire_scriptlet_valid_type(const char* type) {
	for (const char** t = pakfire_scriptlet_types; *t; t++) {
		if (strcmp(*t, type) == 0)
			return 1;
	}

	return 0;
}

static int pakfire_scriptlet_set(struct pakfire_scriptlet* scriptlet, const char* data, size_t size) {
	if (scriptlet->data)
		free(scriptlet->data);

	// Fill size if unset
	if (!size)
		size = strlen(data);

	// Allocate space for data
	scriptlet->data = malloc(size);
	if (!scriptlet->data)
		return -errno;

	// Copy data
	memcpy(scriptlet->data, data, size);
	scriptlet->size = size;

	return 0;
}

static void pakfire_scriptlet_free(struct pakfire_scriptlet* scriptlet) {
	if (scriptlet->data)
		free(scriptlet->data);
	if (scriptlet->ctx)
		pakfire_ctx_unref(scriptlet->ctx);
	free(scriptlet);
}

int pakfire_scriptlet_create(struct pakfire_scriptlet** scriptlet,
		struct pakfire_ctx* ctx, const char* type, const char* data, size_t size) {
	int r;

	// Check inputs
	if (!type || !data)
		return -EINVAL;

	// Do we know this type?
	if (!pakfire_scriptlet_valid_type(type))
		return -ENOTSUP;

	// Allocate some memory
	struct pakfire_scriptlet* s = calloc(1, sizeof(*s));
	if (!s)
		return -errno;

	// Store reference to the context
	s->ctx = pakfire_ctx_ref(ctx);

	// Initialize the reference counter
	s->nrefs = 1;

	// Set type
	r = pakfire_string_set(s->type, type);
	if (r)
		goto ERROR;

	// Store the payload
	r = pakfire_scriptlet_set(s, data, size);
	if (r)
		goto ERROR;

	// Return the pointer
	*scriptlet = s;

	return 0;

ERROR:
	pakfire_scriptlet_free(s);

	return r;
};

struct pakfire_scriptlet* pakfire_scriptlet_ref(struct pakfire_scriptlet* scriptlet) {
	scriptlet->nrefs++;

	return scriptlet;
}

struct pakfire_scriptlet* pakfire_scriptlet_unref(struct pakfire_scriptlet* scriptlet) {
	if (--scriptlet->nrefs > 0)
		return scriptlet;

	pakfire_scriptlet_free(scriptlet);
	return NULL;
}

const char* pakfire_scriptlet_get_type(struct pakfire_scriptlet* scriptlet) {
	return scriptlet->type;
}

const char* pakfire_scriptlet_get_data(struct pakfire_scriptlet* scriptlet, size_t* size) {
	if (size)
		*size = scriptlet->size;

	return scriptlet->data;
}

static int pakfire_scriptlet_is_shell_script(struct pakfire_scriptlet* scriptlet) {
	const char* interpreter = "#!/bin/sh";

	// data must be long enough
	if (scriptlet->size <= strlen(interpreter))
		return 0;

	// If the string begins with the interpreter, this is a match
	if (strncmp(scriptlet->data, interpreter, strlen(interpreter)) == 0)
		return 1;

	return 0;
}

int pakfire_scriptlet_execute(struct pakfire_scriptlet* scriptlet, struct pakfire* pakfire) {
	// Detect what kind of script this is and run it
	if (pakfire_scriptlet_is_shell_script(scriptlet))
		return pakfire_jail_run_script(pakfire, scriptlet->data, scriptlet->size, NULL, NULL, 0);

	ERROR(scriptlet->ctx, "Scriptlet is of an unknown kind\n");
	return -ENOTSUP;
}
