/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2024 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_PTY_H
#define PAKFIRE_PTY_H

// libsystemd
#include <systemd/sd-event.h>

#include <pakfire/ctx.h>
#include <pakfire/filelist.h>

struct pakfire_pty;

enum pakfire_pty_flags {
	PAKFIRE_PTY_CONNECT_STDIN  = (1 << 0),
	PAKFIRE_PTY_CONNECT_STDOUT = (1 << 1),
	PAKFIRE_PTY_CONNECT_STDERR = (1 << 2),
	PAKFIRE_PTY_INTERACTIVE    = \
		PAKFIRE_PTY_CONNECT_STDIN | \
		PAKFIRE_PTY_CONNECT_STDOUT| \
		PAKFIRE_PTY_CONNECT_STDERR,
	PAKFIRE_PTY_CAPTURE_OUTPUT = (1 << 3),
};

int pakfire_pty_create(struct pakfire_pty** pty,
	struct pakfire_ctx* ctx, sd_event* loop, int flags);

struct pakfire_pty* pakfire_pty_ref(struct pakfire_pty* pty);
struct pakfire_pty* pakfire_pty_unref(struct pakfire_pty* pty);

int pakfire_pty_open(struct pakfire_pty* pty);

int pakfire_pty_drain(struct pakfire_pty* pty);

char* pakfire_pty_output(struct pakfire_pty* pty, size_t* length);

typedef ssize_t (*pakfire_pty_stdin_callback)(
	struct pakfire_ctx* ctx, void* data, char* buffer, size_t length);
typedef int (*pakfire_pty_stdout_callback)(
	struct pakfire_ctx* ctx, void* data, const char* line, const size_t length);

// Standard Input
void pakfire_pty_set_stdin_callback(struct pakfire_pty* pty,
	pakfire_pty_stdin_callback callback, void* data);
void pakfire_pty_set_stdout_callback(struct pakfire_pty* pty,
	pakfire_pty_stdout_callback callback, void* data);

struct pakfire_pty_buffer {
	const char* data;
	size_t length;
};

ssize_t pakfire_pty_send_buffer(struct pakfire_ctx* ctx,
	void* data, char* buffer, size_t length);

// Stream a filelist

struct pakfire_pty_filelist {
	struct pakfire_filelist* filelist;
	size_t i;

	// Buffer for the path
	char buffer[PATH_MAX];
	const char* p;
};

ssize_t pakfire_pty_send_filelist(struct pakfire_ctx* ctx,
	void* data, char* buffer, size_t length);

#endif /* PAKFIRE_PTY_H */
