/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_MIRROR_H
#define PAKFIRE_MIRROR_H

struct pakfire_mirror;

#include <pakfire/ctx.h>
#include <pakfire/xfer.h>

int pakfire_mirror_create(struct pakfire_mirror** mirror,
	struct pakfire_ctx* ctx, const char* url);

struct pakfire_mirror* pakfire_mirror_ref(struct pakfire_mirror* mirror);
struct pakfire_mirror* pakfire_mirror_unref(struct pakfire_mirror* mirror);

const char* pakfire_mirror_get_url(struct pakfire_mirror* mirror);

// Enabled?

int pakfire_mirror_is_enabled(struct pakfire_mirror* mirror);

int pakfire_mirror_xfer_failed(struct pakfire_mirror* self, pakfire_xfer_error_code_t code);

#endif /* PAKFIRE_MIRROR_H */
