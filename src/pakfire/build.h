/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_BUILD_H
#define PAKFIRE_BUILD_H

#include <pakfire/archive.h>
#include <pakfire/config.h>
#include <pakfire/ctx.h>

struct pakfire_build;

enum pakfire_build_flags {
	PAKFIRE_BUILD_ENABLE_SNAPSHOT  = (1 << 0),
	PAKFIRE_BUILD_INTERACTIVE      = (1 << 1),
	PAKFIRE_BUILD_DISABLE_CCACHE   = (1 << 2),
	PAKFIRE_BUILD_DISABLE_TESTS    = (1 << 3),
	PAKFIRE_BUILD_LOCAL            = (1 << 4),
};

typedef int (*pakfire_build_result_callback)(struct pakfire_ctx* ctx, struct pakfire* pakfire,
	struct pakfire_build* build, struct pakfire_archive* archive, void* data);

int pakfire_build_create(struct pakfire_build** build, struct pakfire_ctx* ctx,
	struct pakfire_config* config, const char* arch, const char* id, int flags);

struct pakfire_build* pakfire_build_ref(struct pakfire_build* build);
struct pakfire_build* pakfire_build_unref(struct pakfire_build* build);

int pakfire_build_set_ccache_path(struct pakfire_build* build, const char* path);

int pakfire_build_shell(struct pakfire_build* build, const char* argv[], const char** packages);

int pakfire_build_exec(struct pakfire_build* build, const char* path,
	pakfire_build_result_callback result_callback, void* data);

int pakfire_build_mkimage(struct pakfire_build* build,
	const char* type, FILE* f);

int pakfire_build_clean(struct pakfire* pakfire, int flags);

#endif /* PAKFIRE_BUILD_H */
