/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2024 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_LOG_STREAM_H
#define PAKFIRE_LOG_STREAM_H

#include <stdarg.h>

#include <systemd/sd-event.h>

#include <pakfire/ctx.h>

struct pakfire_log_stream;

typedef int (*pakfire_log_stream_callback)(struct pakfire_log_stream* stream,
	const char* line, size_t length, void* data);

int pakfire_log_stream_create(struct pakfire_log_stream** stream, struct pakfire_ctx* ctx,
	pakfire_log_stream_callback callback, void* data);
struct pakfire_log_stream* pakfire_log_stream_ref(struct pakfire_log_stream* stream);
struct pakfire_log_stream* pakfire_log_stream_unref(struct pakfire_log_stream* stream);

int pakfire_log_stream_in_parent(struct pakfire_log_stream* stream, sd_event* loop);
int pakfire_log_stream_in_child(struct pakfire_log_stream* stream);

int pakfire_log_stream_printf(struct pakfire_log_stream* stream,
	const char* format, ...) __attribute__((format(printf, 2, 3)));
int pakfire_log_stream_vprintf(struct pakfire_log_stream* stream,
	const char* format, va_list args) __attribute__((format(printf, 2, 0)));
int pakfire_log_stream_write(struct pakfire_log_stream* stream,
	const char* buffer, size_t length);

int pakfire_log_stream_close(struct pakfire_log_stream* stream);

#endif /* PAKFIRE_LOG_STREAM_H */
