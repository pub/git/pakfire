/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_XFER_H
#define PAKFIRE_XFER_H

#include <stdarg.h>

#include <curl/curl.h>

#include <json.h>

#include <systemd/sd-event.h>

struct pakfire_xfer;

typedef enum pakfire_xfer_error_code {
	PAKFIRE_XFER_OK = 0,

	// General/Unknown Failure
	PAKFIRE_XFER_FAILED,

	// An unsupported protocol was requested
	PAKFIRE_XFER_UNSUPPORTED,

	// Invalid URL
	PAKFIRE_XFER_INVALID_URL,

	// The server responded with something weird
	PAKFIRE_XFER_INVALID_RESPONSE,

	// DNS Problem
	PAKFIRE_XFER_DNS_ERROR,

	// Authentication
	PAKFIRE_XFER_AUTH_ERROR,
	PAKFIRE_XFER_ACCESS_DENIED,

	// Some connection problem
	PAKFIRE_XFER_TRANSPORT_ERROR,

	// Timeout
	PAKFIRE_XFER_TIMEOUT,

	// Local operation errors
	PAKFIRE_XFER_WRITE_ERROR,
	PAKFIRE_XFER_READ_ERROR,
	PAKFIRE_XFER_ABORTED,

	// Digest Mismatch
	PAKFIRE_XFER_DIGEST_MISMATCH,
} pakfire_xfer_error_code_t;

#include <pakfire/ctx.h>
#include <pakfire/hasher.h>
#include <pakfire/hashes.h>
#include <pakfire/httpclient.h>
#include <pakfire/mirrorlist.h>
#include <pakfire/progress.h>

typedef enum pakfire_xfer_flags {
	PAKFIRE_XFER_NO_PROGRESS = (1 << 0),
} pakfire_xfer_flags_t;

typedef enum pakfire_transfer_method {
	PAKFIRE_METHOD_DELETE,
} pakfire_xfer_method_t;

int pakfire_xfer_create_simple(
	struct pakfire_xfer** xfer, struct pakfire_ctx* ctx, const char* url);

int pakfire_xfer_create(struct pakfire_xfer** xfer, struct pakfire_ctx* ctx,
	const char* url, va_list args) __attribute__((format(printf, 3, 0)));

struct pakfire_xfer* pakfire_xfer_ref(struct pakfire_xfer* xfer);
struct pakfire_xfer* pakfire_xfer_unref(struct pakfire_xfer* xfer);

CURL* pakfire_xfer_handle(struct pakfire_xfer* xfer);

int pakfire_xfer_set_method(struct pakfire_xfer* xfer,
	const pakfire_xfer_method_t method);

// Title
const char* pakfire_xfer_get_title(struct pakfire_xfer* xfer);
int pakfire_xfer_set_title(struct pakfire_xfer* xfer, const char* title);

int pakfire_xfer_set_baseurl(struct pakfire_xfer* xfer, const char* baseurl);
const char* pakfire_xfer_get_effective_url(struct pakfire_xfer* xfer);

int pakfire_xfer_set_mirrorlist(struct pakfire_xfer* xfer, struct pakfire_mirrorlist* mirrors);

size_t pakfire_xfer_get_size(struct pakfire_xfer* xfer);
int pakfire_xfer_set_size(struct pakfire_xfer* xfer, size_t size);

int pakfire_xfer_verify_hashes(struct pakfire_xfer* self, const struct pakfire_hashes* hashes);

int pakfire_xfer_add_query(struct pakfire_xfer* xfer,
	const char* key, const char* format, ...) __attribute__((format(printf, 3, 4)));
int pakfire_xfer_add_param(struct pakfire_xfer* xfer,
	const char* key, const char* format, ...) __attribute__((format(printf, 3, 4)));

// Output
int pakfire_xfer_set_output(struct pakfire_xfer* xfer, FILE* f);
int pakfire_xfer_set_output_buffer(struct pakfire_xfer* xfer, char** buffer, size_t* length);
int pakfire_xfer_set_output_path(struct pakfire_xfer* xfer, const char* path);

// Input
int pakfire_xfer_set_input(struct pakfire_xfer* xfer, FILE* f);

// Authentication
int pakfire_xfer_auth(struct pakfire_xfer* xfer);

int pakfire_xfer_prepare(struct pakfire_xfer* xfer, struct pakfire_progress* progress, int flags);
pakfire_xfer_error_code_t pakfire_xfer_done(struct pakfire_xfer* xfer, sd_event* loop, int code);

pakfire_xfer_error_code_t pakfire_xfer_run(struct pakfire_xfer* xfer, int flags);
pakfire_xfer_error_code_t pakfire_xfer_run_api_request(
	struct pakfire_xfer* xfer, struct json_object** response);

// WebSocket
typedef int (*pakfire_xfer_open_callback)(struct pakfire_xfer* xfer, void* data);
typedef int (*pakfire_xfer_recv_callback)(struct pakfire_xfer* xfer, const char* message, const size_t size, void* data);
typedef int (*pakfire_xfer_send_callback)(struct pakfire_xfer* xfer, void* data);
typedef int (*pakfire_xfer_close_callback)(struct pakfire_xfer* xfer, int code, void* data);

int pakfire_xfer_socket(struct pakfire_xfer* xfer, pakfire_xfer_open_callback open,
	pakfire_xfer_recv_callback recv, pakfire_xfer_send_callback send, pakfire_xfer_close_callback close, void* data);

int pakfire_xfer_send_message(struct pakfire_xfer* xfer, const char* message, const size_t length);
int pakfire_xfer_is_ready_to_send(struct pakfire_xfer* self);

#endif /* PAKFIRE_XFER_H */
