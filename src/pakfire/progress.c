/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <stdlib.h>
#include <time.h>

#include <pakfire/ctx.h>
#include <pakfire/logging.h>
#include <pakfire/progress.h>

struct pakfire_progress {
	struct pakfire_ctx* ctx;
	int nrefs;

	// Title
	char* title;

	// Flags what to show
	int flags;

	// Parent
	struct pakfire_progress* parent;

	// Status
	enum pakfire_progress_status {
		PAKFIRE_PROGRESS_INIT = 0,
		PAKFIRE_PROGRESS_RUNNING,
		PAKFIRE_PROGRESS_FINISHED,
	} status;

	// Callbacks
	struct pakfire_progress_callbacks {
		void* data;

		pakfire_progress_start_callback start;
		pakfire_progress_finish_callback finish;
		pakfire_progress_update_callback update;
		pakfire_progress_free_callback free;
	} callbacks;

	// Start Time
	struct timespec time_start;
	struct timespec time_finished;

	// Values
	unsigned long int value;
	unsigned long int max_value;
};

static void pakfire_progress_free(struct pakfire_progress* p) {
	// Ensure this is finished
	pakfire_progress_finish(p);

	// Call the free callback
	if (p->callbacks.free)
		p->callbacks.free(p->ctx, p, p->callbacks.data);

	if (p->parent)
		pakfire_progress_unref(p->parent);
	if (p->title)
		free(p->title);
	if (p->ctx)
		pakfire_ctx_unref(p->ctx);
	free(p);
}

static int pakfire_progress_default_start_callback(struct pakfire_ctx* ctx,
		struct pakfire_progress* p, void* data, unsigned long int value) {
	// Fetch the title
	const char* title = pakfire_progress_get_title(p);

	// Log the title
	if (title)
		INFO(ctx, "%s\n", title);

	return 0;
}

int pakfire_progress_create(struct pakfire_progress** progress,
		struct pakfire_ctx* ctx, int flags, struct pakfire_progress* parent) {
	struct pakfire_progress* p = NULL;
	int r;

	// Allocate a new object
	p = calloc(1, sizeof(*p));
	if (!p)
		return -errno;

	// Store a reference to the context
	p->ctx = pakfire_ctx_ref(ctx);

	// Initialize the reference counter
	p->nrefs = 1;

	// Store the flags
	p->flags = flags;

	// Initialize status
	p->status = PAKFIRE_PROGRESS_INIT;

	// Store a reference to the parent
	if (parent)
		p->parent = pakfire_progress_ref(parent);

	// Configure some default callbacks
	p->callbacks.start = pakfire_progress_default_start_callback;

	// Call setup
	if (!pakfire_progress_has_flag(p, PAKFIRE_PROGRESS_NO_PROGRESS)) {
		r = pakfire_ctx_setup_progress(p->ctx, p);
		if (r)
			goto ERROR;
	}

	// Success
	*progress = p;
	return 0;

ERROR:
	if (p)
		pakfire_progress_unref(p);

	return r;
}

struct pakfire_progress* pakfire_progress_ref(struct pakfire_progress* p) {
	++p->nrefs;

	return p;
}

struct pakfire_progress* pakfire_progress_unref(struct pakfire_progress* p) {
	if (--p->nrefs > 0)
		return p;

	pakfire_progress_free(p);
	return NULL;
}

int pakfire_progress_has_flag(struct pakfire_progress* p, int flag) {
	return p->flags & flag;
}

/*
	Callback Configuration
*/

void pakfire_progress_set_callback_data(struct pakfire_progress* p, void* data) {
	p->callbacks.data = data;
}

void pakfire_progress_set_start_callback(
		struct pakfire_progress* p, pakfire_progress_start_callback callback) {
	p->callbacks.start = callback;
}

void pakfire_progress_set_finish_callback(
		struct pakfire_progress* p, pakfire_progress_finish_callback callback) {
	p->callbacks.finish = callback;
}

void pakfire_progress_set_update_callback(
		struct pakfire_progress* p, pakfire_progress_update_callback callback) {
	p->callbacks.update = callback;
}

void pakfire_progress_set_free_callback(
		struct pakfire_progress* p, pakfire_progress_free_callback callback) {
	p->callbacks.free = callback;
}

int pakfire_progress_start(struct pakfire_progress* p, unsigned long int value) {
	int r;

	// This can only be called once
	if (p->status == PAKFIRE_PROGRESS_RUNNING)
		return -EINVAL;

	// We are now running...
	p->status = PAKFIRE_PROGRESS_RUNNING;

	// Store the max value
	p->max_value = value;

	// Set start time
	r = clock_gettime(CLOCK_REALTIME, &p->time_start);
	if (r)
		return r;

	// No-op
	if (pakfire_progress_has_flag(p, PAKFIRE_PROGRESS_NO_PROGRESS))
		return 0;

	// Call the start callback
	if (p->callbacks.start) {
		r = p->callbacks.start(p->ctx, p, p->callbacks.data, value);
		if (r)
			return r;
	}

	// Call the first update
	return pakfire_progress_update(p, 0);
}

int pakfire_progress_finish(struct pakfire_progress* p) {
	int r;

	// Do nothing if already finished
	if (p->status == PAKFIRE_PROGRESS_FINISHED)
		return 0;

	// No-op
	if (pakfire_progress_has_flag(p, PAKFIRE_PROGRESS_NO_PROGRESS))
		return 0;

	// Set finished time
	r = clock_gettime(CLOCK_REALTIME, &p->time_finished);
	if (r)
		return r;

	// Call the finish callback
	if (p->callbacks.finish) {
		r = p->callbacks.finish(p->ctx, p, p->callbacks.data);
		if (r)
			return r;
	}

	return 0;
}

int pakfire_progress_update(struct pakfire_progress* p, unsigned long int value) {
	int r = 0;

	// Report the change to the parent progress
	if (p->parent) {
		r = pakfire_progress_increment(p->parent, value - p->value);
		if (r)
			return r;
	}

	// Store the new value
	p->value = value;

	// No-op
	if (pakfire_progress_has_flag(p, PAKFIRE_PROGRESS_NO_PROGRESS))
		return 0;

	// Call the update callback
	if (p->callbacks.update) {
		r = p->callbacks.update(p->ctx, p, p->callbacks.data, value);
		if (r)
			return r;
	}

	return r;
}

int pakfire_progress_increment(struct pakfire_progress* p, unsigned long int value) {
	return pakfire_progress_update(p, p->value + value);
}

int pakfire_progress_reset(struct pakfire_progress* p) {
	int r;

	switch (p->status) {
		case PAKFIRE_PROGRESS_INIT:
			return 0;

		// If we are running, let's pretend we finished
		case PAKFIRE_PROGRESS_RUNNING:
			r = pakfire_progress_finish(p);
			if (r)
				return r;

			break;

		default:
			break;
	}

	// Reset values
	r = pakfire_progress_update(p, 0);
	if (r)
		return r;

	// Reset max value
	pakfire_progress_set_max_value(p, 0);

	return 0;
}

struct pakfire_progress* pakfire_progress_get_parent(struct pakfire_progress* p) {
	if (p->parent)
		return pakfire_progress_ref(p->parent);

	return NULL;
}

unsigned long int pakfire_progress_get_value(struct pakfire_progress* p) {
	return p->value;
}

unsigned long int pakfire_progress_get_max_value(struct pakfire_progress* p) {
	return p->max_value;
}

void pakfire_progress_set_max_value(struct pakfire_progress* p, unsigned long int value) {
	p->max_value = value;
}

const char* pakfire_progress_get_title(struct pakfire_progress* p) {
	return p->title;
}

int pakfire_progress_set_title(struct pakfire_progress* p, const char* format, ...) {
	va_list args;
	int r;

	// Format the title
	va_start(args, format);
	r = vasprintf(&p->title, format, args);
	va_end(args);

	// Fail
	if (r < 0)
		return r;

	return 0;
}

double pakfire_progress_get_percentage(struct pakfire_progress* p) {
	if (!p->max_value)
		return 0;

	return p->value * 100.0 / p->max_value;
}

time_t pakfire_progress_get_elapsed_time(struct pakfire_progress* p) {
	struct timespec now;
	int r;

	switch (p->status) {
		case PAKFIRE_PROGRESS_INIT:
			return 0;

		case PAKFIRE_PROGRESS_RUNNING:
			r = clock_gettime(CLOCK_REALTIME, &now);
			if (r)
				return -1;

			return now.tv_sec - p->time_start.tv_sec;

		case PAKFIRE_PROGRESS_FINISHED:
			return p->time_finished.tv_sec - p->time_start.tv_sec;
	}

	return -1;
}

time_t pakfire_progress_get_eta(struct pakfire_progress* p) {
	if (!p->value || !p->max_value)
		return 0;

	const time_t t = pakfire_progress_get_elapsed_time(p);
	if (t < 0)
		return t;

	return t * p->max_value / p->value - t;
}

double pakfire_progress_get_transfer_speed(struct pakfire_progress* p) {
	const time_t t = pakfire_progress_get_elapsed_time(p);
	if (t <= 0)
		return t;

	return p->value / t;
}
