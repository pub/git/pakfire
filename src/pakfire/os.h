/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_OS_H
#define PAKFIRE_OS_H

#include <stdint.h>
#include <unistd.h>

#define OS_VENDOR_MAX 32
#define OS_NAME_MAX   64

// System Info

struct pakfire_sysinfo {
	char vendor[OS_VENDOR_MAX];
	char name[OS_NAME_MAX];
};

int pakfire_sysinfo(struct pakfire_sysinfo* sysinfo);

// CPU info

struct pakfire_cpuinfo {
	char vendor[OS_VENDOR_MAX];
	char model[OS_NAME_MAX];
	unsigned int count;
};

int pakfire_cpuinfo(struct pakfire_cpuinfo* cpuinfo);

// CPU stat

struct pakfire_cpustat {
	double user;
	double nice;
	double system;
	double idle;
	double iowait;
	double irq;
	double softirq;
	double steal;
	double guest;
	double guest_nice;
};

int pakfire_cpustat(struct pakfire_cpustat* cpustat);

// Load Average

struct pakfire_loadavg {
	double load1;
	double load5;
	double load15;
};

int pakfire_loadavg(struct pakfire_loadavg* loadavg);

// Meminfo

struct pakfire_meminfo {
	uint64_t total;
	uint64_t used;
	uint64_t free;
	uint64_t available;
	uint64_t buffers;
	uint64_t cached;
	uint64_t shared;
	uint64_t active;
	uint64_t inactive;

	// Swap
	uint64_t swap_total;
	uint64_t swap_used;
	uint64_t swap_free;
};

int pakfire_meminfo(struct pakfire_meminfo* meminfo);

// Distro

struct pakfire_distro {
	char pretty_name[256];
	char name[256];
	char id[256];
	char slogan[256];
	char vendor[256];
	char version[256];
	char version_codename[256];
	char version_id[256];
	char tag[256];
};

int pakfire_distro(struct pakfire_distro* distro, const char* path);

#endif /* PAKFIRE_OS_H */
