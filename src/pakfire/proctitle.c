/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2025 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <stddef.h>
#include <stdio.h>

#include <pakfire/proctitle.h>
#include <pakfire/string.h>

extern char** environ;

static char** argv0 = NULL;
static size_t argv0_length = 0;

int pakfire_proctitle_init(int argc, char** argv) {
	char** e = NULL;
	size_t length = 0;
	int r;

	// Count the length of the current environment
	length = pakfire_strings_length(environ);

	// Allocate a new environment
	e = calloc(length + 1, sizeof(*e));
	if (!e)
		return -errno;

	// Copy the entire environment
	for (unsigned int i = 0; i < length; i++) {
		e[i] = strdup(environ[i]);

		// Fail if we could not copy
		if (!e[i]) {
			r = -errno;
			goto ERROR;
		}
	}

	// Determine how much space we have for a new process title
	if (length > 0) {
		argv0_length = environ[length - 1] + strlen(environ[length - 1]) + 1 - argv[0];
	} else {
		argv0_length = argv[argc - 1] + strlen(argv[argc - 1]) + 1 - argv[0];
	}

	// If there is some space, we remember the start address
	if (!argv0_length) {
		r = -ENOBUFS;
		goto ERROR;
	}

	// Replace the environment
	environ = e;

	// Store argv
	argv0 = argv;

	return 0;

ERROR:
	if (e)
		pakfire_strings_free(e);

	return r;
}

int pakfire_proctitle_set(const char* format, ...) {
	char buffer[2048];
	va_list args;
	int r;

	// Ensure argv0 is initialized
	if (!argv0 || !argv0_length)
		return -ENOTSUP;

	va_start(args, format);
	r = pakfire_string_vformat(buffer, format, args);
	va_end(args);
	if (r < 0)
		return r;

	// Truncate the buffer
	pakfire_string_truncate(buffer, argv0_length - 1);

	// Copy the new content
	memcpy(argv0[0], buffer, strlen(buffer) + 1);

	// Reset any other arguments
	argv0[1] = NULL;

	return 0;
}
