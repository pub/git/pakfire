/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2025 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_JSON_H
#define PAKFIRE_JSON_H

#include <stdint.h>

#include <json.h>

#include <pakfire/ctx.h>

// Parse
struct json_object* pakfire_json_parse(struct pakfire_ctx* ctx,
	const char* buffer, const size_t length);

// Parse from file
struct json_object* pakfire_json_parse_from_file(struct pakfire_ctx* ctx, const char* path);

struct json_object* pakfire_json_new_object(void);

int pakfire_json_add_string(struct json_object* json, const char* name, const char* value);
int pakfire_json_add_stringn(struct json_object* json, const char* name, const char* value, size_t length);
int pakfire_json_add_stringf(struct json_object* json, const char* name, const char* format, ...)
	__attribute__((format(printf, 3, 4)));
int pakfire_json_add_int64(struct json_object* json, const char* name, int64_t value);
int pakfire_json_add_uint64(struct json_object* json, const char* name, uint64_t value);
int pakfire_json_add_double(struct json_object* json, const char* name, double value);
int pakfire_json_add_string_array(struct json_object* json, const char* name, char** array);
int pakfire_json_add_object(struct json_object* json, const char* name, struct json_object** o);
int pakfire_json_add_array(struct json_object* json, const char* name, struct json_object** array);

int pakfire_json_array_add_string(struct json_object* array, const char* s);
int pakfire_json_array_add_stringf(struct json_object* array, const char* format, ...)
	__attribute__((format(printf, 2, 3)));

int pakfire_json_get_string(struct json_object* json, const char* key, const char** value);
int pakfire_json_get_int64(struct json_object* json, const char* key, int64_t* value);
int pakfire_json_get_object(struct json_object* json, const char* key, struct json_object** object);
int pakfire_json_get_array(struct json_object* json, const char* key, struct json_object** array);

int pakfire_json_write(struct json_object* json, const char* path);

#endif /* PAKFIRE_JSON_H */
