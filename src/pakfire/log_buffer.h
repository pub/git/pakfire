/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2024 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_LOG_BUFFER_H
#define PAKFIRE_LOG_BUFFER_H

#include <sys/time.h>

#include <pakfire/ctx.h>

/*
	Ring buffer for log messages
*/

struct pakfire_log_buffer;

int pakfire_log_buffer_create(struct pakfire_log_buffer** buffer, struct pakfire_ctx* ctx, size_t size);
struct pakfire_log_buffer* pakfire_log_buffer_ref(struct pakfire_log_buffer* buffer);
struct pakfire_log_buffer* pakfire_log_buffer_unref(struct pakfire_log_buffer* buffer);

int pakfire_log_buffer_enqueue(struct pakfire_log_buffer* buffer, int priority, const char* line, ssize_t length);
int pakfire_log_buffer_dequeue(struct pakfire_log_buffer* buffer,
	struct timeval* timestamp, int* priority, char** line, size_t* length);

int pakfire_log_buffer_dump(struct pakfire_log_buffer* buffer, char** result, size_t* length);

#endif /* PAKFIRE_LOG_BUFFER_H */
