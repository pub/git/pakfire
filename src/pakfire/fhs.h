/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_FHS_H
#define PAKFIRE_FHS_H

#include <pakfire/file.h>
#include <pakfire/pakfire.h>

enum pakfire_fhs_status {
	PAKFIRE_FHS_MUSTNOTEXIST   = (1 << 0),
	PAKFIRE_FHS_UNAME_MISMATCH = (1 << 1),
	PAKFIRE_FHS_GNAME_MISMATCH = (1 << 2),
	PAKFIRE_FHS_PERMS_MISMATCH = (1 << 3),
	PAKFIRE_FHS_WORLDWRITABLE  = (1 << 4),
	PAKFIRE_FHS_NOEXEC         = (1 << 5),
};

int pakfire_fhs_check_file(struct pakfire_ctx* ctx, struct pakfire_file* file);

#endif /* PAKFIRE_FHS_H */
