/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <sys/queue.h>

#include <curl/curl.h>

#include <json.h>

#include <systemd/sd-event.h>

#include <pakfire/ctx.h>
#include <pakfire/hasher.h>
#include <pakfire/hex.h>
#include <pakfire/json.h>
#include <pakfire/mirrorlist.h>
#include <pakfire/path.h>
#include <pakfire/progress.h>
#include <pakfire/string.h>
#include <pakfire/util.h>
#include <pakfire/xfer.h>

struct pakfire_xfer {
	struct pakfire_ctx* ctx;
	int nrefs;

	// Reference to the progress indicator
	struct pakfire_progress* progress;

	// cURL handle
	CURL* handle;

	// Headers
	struct curl_slist* headers;

	// URL
	CURLU* fullurl;

	char url[PATH_MAX];
	char title[NAME_MAX];
	char path[PATH_MAX];
	char tmpfile[PATH_MAX];
	int tries;

	// GET Query Arguments
	char** queries;

	// POST MIME Object
	curl_mime* mime;

	// Xfer direction
	enum {
		PAKFIRE_XFER_DOWNLOAD = 0,
		PAKFIRE_XFER_UPLOAD   = 1,
		PAKFIRE_XFER_SOCKET   = 2,
	} direction;

	// Size
	size_t expected_size;
	size_t xferred;

	// File handles for streams
	FILE* fin;
	FILE* fout;

	// Expected Hashes
	struct pakfire_hashes expected_hashes;

	// Hasher
	struct pakfire_hasher* hasher;

	// Mirrors
	char baseurl[PATH_MAX];
	struct pakfire_mirrorlist* mirrors;
	struct pakfire_mirror* mirror;

	// Effective URL
	const char* effective_url;

	// Authentication
	unsigned int auth;

	// Space for cURL error message
	char error[CURL_ERROR_SIZE];

	// Event Loop
	sd_event_source* event;

	// Callbacks
	struct pakfire_xfer_callbacks {
		pakfire_xfer_open_callback open;
		pakfire_xfer_recv_callback recv;
		pakfire_xfer_send_callback send;
		pakfire_xfer_close_callback close;
		void* data;
	} callbacks;

	// WebSocket Receive Buffer
	struct pakfire_xfer_buffer {
		char* data;
		size_t length;
	} recv_buffer;

	// WebSocket Send Buffer
	struct pakfire_send_buffer {
		char* data;
		size_t length;
	} send_buffer;

	// State
	enum pakfire_xfer_state {
		PAKFIRE_XFER_READY_TO_SEND = (1 << 0),
	} state;
};

static void pakfire_xfer_free(struct pakfire_xfer* xfer) {
	// Unlink
	if (*xfer->tmpfile)
		unlink(xfer->tmpfile);

	// Close any streams
	if (xfer->fin)
		fclose(xfer->fin);

	// systemd
	if (xfer->event)
		sd_event_source_unref(xfer->event);

	// Receive Buffer
	if (xfer->recv_buffer.data)
		free(xfer->recv_buffer.data);
	if (xfer->send_buffer.data)
		free(xfer->send_buffer.data);

	// Query Arguments
	if (xfer->queries)
		pakfire_strings_free(xfer->queries);

	// cURL stuff
	if (xfer->handle)
		curl_easy_cleanup(xfer->handle);
	if (xfer->headers)
		curl_slist_free_all(xfer->headers);
	if (xfer->mime)
		curl_mime_free(xfer->mime);
	if (xfer->fullurl)
		curl_url_cleanup(xfer->fullurl);

	if (xfer->hasher)
		pakfire_hasher_unref(xfer->hasher);
	if (xfer->mirror)
		pakfire_mirror_unref(xfer->mirror);
	if (xfer->mirrors)
		pakfire_mirrorlist_unref(xfer->mirrors);
	if (xfer->progress)
		pakfire_progress_unref(xfer->progress);
	if (xfer->ctx)
		pakfire_ctx_unref(xfer->ctx);

	free(xfer);
}

#ifdef ENABLE_DEBUG
static int pakfire_xfer_debug_callback(CURL *handle, curl_infotype type,
		char* data, size_t size, void* private) {
	struct pakfire_ctx* ctx = private;

	switch (type) {
		case CURLINFO_TEXT:
			DEBUG(ctx, "cURL: %.*s", (int)size, data);
			break;

		// Log headers
		case CURLINFO_HEADER_IN:
			DEBUG(ctx, "cURL: < %.*s", (int)size, data);
			break;

		case CURLINFO_HEADER_OUT:
			DEBUG(ctx, "cURL: > %.*s", (int)size, data);
			break;

		// Ignore everything else
		default:
			break;
	}

	return 0;
}
#endif

static size_t pakfire_xfer_read(char* data, size_t size, size_t nmemb, void* p) {
	struct pakfire_xfer* xfer = p;

	return fread(data, size, nmemb, xfer->fout);
}

static int pakfire_xfer_seek(void* p, curl_off_t offset, int origin) {
	struct pakfire_xfer* xfer = p;
	int r;

	// Perform the seek
	r = fseek(xfer->fout, (long)offset, origin);
	if (r < 0)
		return CURL_SEEKFUNC_CANTSEEK;

	return CURL_SEEKFUNC_OK;
}

static size_t pakfire_xfer_write(
		char* data, size_t size, size_t nmemb, void* p) {
	struct pakfire_xfer* xfer = p;
	int r;

	// Do not write empty blocks
	if (!nmemb)
		return nmemb;

	// Update the hasher
	if (xfer->hasher) {
		r = pakfire_hasher_update(xfer->hasher, data, nmemb);
		if (r < 0)
			return r;
	}

	// If there is no output steam, we just pretent that we have consumed the data
	if (!xfer->fin)
		return nmemb;

	// Write everything to the allocated file descriptor
	return fwrite(data, size, nmemb, xfer->fin);
}

static int pakfire_xfer_setup(struct pakfire_xfer* xfer) {
	struct pakfire_config* config = NULL;
	const char* proxy = NULL;
	int r;

	CURLSH* share = pakfire_ctx_curl_share(xfer->ctx);

	// Configure the share handle
	r = curl_easy_setopt(xfer->handle, CURLOPT_SHARE, share);
	if (r) {
		ERROR(xfer->ctx, "Could not configure cURL share handle: %s\n",
			curl_easy_strerror(r));
		return r;
	}

	// Fetch global configuration
	config = pakfire_ctx_get_config(xfer->ctx);

	// Set global configuration
	if (config) {
		proxy = pakfire_config_get(config, "general", "proxy", NULL);
		if (proxy)
			curl_easy_setopt(xfer->handle, CURLOPT_PROXY, proxy);
	}

	// Be a good net citizen and set a user agent
	curl_easy_setopt(xfer->handle, CURLOPT_USERAGENT,
		"Mozilla/5.0 (compatible; " PACKAGE_NAME "/" PACKAGE_VERSION "; +https://www.ipfire.org/)");

#ifdef ENABLE_DEBUG
	// Enable logging/debugging
	curl_easy_setopt(xfer->handle, CURLOPT_VERBOSE, 1L);

	curl_easy_setopt(xfer->handle, CURLOPT_DEBUGFUNCTION, pakfire_xfer_debug_callback);
	curl_easy_setopt(xfer->handle, CURLOPT_DEBUGDATA, xfer->ctx);
#endif

	// Set error buffer
	r = curl_easy_setopt(xfer->handle, CURLOPT_ERRORBUFFER, xfer->error);
	if (r)
		goto ERROR;

	// Limit protocols to HTTPS, HTTP, FTP, FILE and WebSocket over TLS
#if LIBCURL_VERSION_NUM >= 0x075500
	curl_easy_setopt(xfer->handle, CURLOPT_PROTOCOLS_STR, "HTTPS,HTTP,FTP,FILE,WSS");
#else
	curl_easy_setopt(xfer->handle, CURLOPT_PROTOCOLS,
		CURLPROTO_HTTP|CURLPROTO_HTTPS|CURLPROTO_FTP|CURLPROTO_FILE);
#endif

	// Raise any HTTP errors
	r = curl_easy_setopt(xfer->handle, CURLOPT_FAILONERROR, 1L);
	if (r)
		goto ERROR;

	// Allow all support encodings
	curl_easy_setopt(xfer->handle, CURLOPT_ACCEPT_ENCODING, "");

	// Reference back to this xfer
	curl_easy_setopt(xfer->handle, CURLOPT_PRIVATE, xfer);

	// Follow any redirects
	curl_easy_setopt(xfer->handle, CURLOPT_FOLLOWLOCATION, 1);

	// Only follow up to 30 redirects
	curl_easy_setopt(xfer->handle, CURLOPT_MAXREDIRS, 30L);

	// Read any data from a callback function
	curl_easy_setopt(xfer->handle,
		CURLOPT_READFUNCTION, pakfire_xfer_read);
	curl_easy_setopt(xfer->handle, CURLOPT_READDATA, xfer);

	// Write all data to the callback function
	curl_easy_setopt(xfer->handle,
		CURLOPT_WRITEFUNCTION, pakfire_xfer_write);
	curl_easy_setopt(xfer->handle, CURLOPT_WRITEDATA, xfer);

	// Register the seek callback
	curl_easy_setopt(xfer->handle,
		CURLOPT_SEEKFUNCTION, pakfire_xfer_seek);
	curl_easy_setopt(xfer->handle, CURLOPT_SEEKDATA, xfer);

	// Success
	r = 0;

ERROR:
	if (config)
		pakfire_config_unref(config);

	return r;
}

static int __pakfire_xfer_create_simple(struct pakfire_xfer** xfer,
	struct pakfire_ctx* ctx, const char* url, ...) __attribute__((format(printf, 3, 4)));

// A helper function to create va_list
static int __pakfire_xfer_create_simple(
		struct pakfire_xfer** xfer, struct pakfire_ctx* ctx, const char* url, ...) {
	va_list args;
	int r;

	va_start(args, url);

	// Create a new xfer
	r = pakfire_xfer_create(xfer, ctx, url, args);

	va_end(args);

	return r;
}

int pakfire_xfer_create_simple(
		struct pakfire_xfer** xfer, struct pakfire_ctx* ctx, const char* url) {
	return __pakfire_xfer_create_simple(xfer, ctx, "%s", url);
}

int pakfire_xfer_create(struct pakfire_xfer** xfer,
		struct pakfire_ctx* ctx, const char* url, va_list args) {
	struct pakfire_xfer* x = NULL;
	int r;

	// Fail if the context is flagged as offline
	if (pakfire_ctx_has_flag(ctx, PAKFIRE_CTX_OFFLINE)) {
		ERROR(ctx, "Cannot initialize a transfer in offline mode\n");
		return -EPERM;
	}

	// Allocate a new xfer
	x = calloc(1, sizeof(*x));
	if (!x)
		return -errno;

	// Store a reference to the context
	x->ctx = pakfire_ctx_ref(ctx);

	// Initialize the reference counter
	x->nrefs = 1;

	// Store the URL
	r = pakfire_string_vformat(x->url, url, args);
	if (r)
		goto ERROR;

	// Allocate a handle
	x->handle = curl_easy_init();
	if (!x->handle) {
		r = 1;
		goto ERROR;
	}

	// Setup the xfer
	r = pakfire_xfer_setup(x);
	if (r)
		goto ERROR;

	// Return the reference
	*xfer = pakfire_xfer_ref(x);

ERROR:
	if (x)
		pakfire_xfer_unref(x);

	return r;
}

struct pakfire_xfer* pakfire_xfer_ref(struct pakfire_xfer* xfer) {
	++xfer->nrefs;

	return xfer;
}

struct pakfire_xfer* pakfire_xfer_unref(struct pakfire_xfer* xfer) {
	if (--xfer->nrefs > 0)
		return xfer;

	pakfire_xfer_free(xfer);
	return NULL;
}

CURL* pakfire_xfer_handle(struct pakfire_xfer* xfer) {
	return xfer->handle;
}

int pakfire_xfer_set_method(struct pakfire_xfer* xfer,
		const pakfire_xfer_method_t method) {
	const char* m = NULL;

	switch (method) {
		case PAKFIRE_METHOD_DELETE:
			m = "DELETE";
			break;

		default:
			return -EINVAL;
	}

	return curl_easy_setopt(xfer->handle, CURLOPT_CUSTOMREQUEST, m);
}

const char* pakfire_xfer_get_title(struct pakfire_xfer* xfer) {
	char title[PATH_MAX];
	int r;

	// Default to the filename if no title is set
	if (!*xfer->title) {
		// Only use the basename
		r = pakfire_path_basename(title, xfer->url);
		if (r)
			return NULL;

		// Store the title
		r = pakfire_xfer_set_title(xfer, title);
		if (r)
			return NULL;
	}

	return xfer->title;
}

int pakfire_xfer_set_title(struct pakfire_xfer* xfer, const char* title) {
	return pakfire_string_set(xfer->title, title);
}

int pakfire_xfer_set_baseurl(struct pakfire_xfer* xfer, const char* baseurl) {
	int r;

	// Store the URL
	r = pakfire_string_set(xfer->baseurl, baseurl);
	if (r < 0)
		return r;

	// If the URL does not end with a /, let's add it
	if (!pakfire_string_endswith(xfer->baseurl, "/")) {
		r = pakfire_string_append(xfer->baseurl, "/");
		if (r < 0)
			return r;
	}

	return 0;
}

const char* pakfire_xfer_get_effective_url(struct pakfire_xfer* xfer) {
	return xfer->effective_url;
}

/*
	Helper function to set a chosen mirror
*/
static int pakfire_xfer_select_mirror(struct pakfire_xfer* self, struct pakfire_mirror* mirror) {
	// Free the previous mirror
	if (self->mirror) {
		pakfire_mirror_unref(self->mirror);
		self->mirror = NULL;
	}

	// Set the new mirror (if available)
	if (mirror) {
		DEBUG(self->ctx, "Selected mirror %s\n", pakfire_mirror_get_url(mirror));

		self->mirror = pakfire_mirror_ref(mirror);

	// If we have no more mirrors and no base URL, we log a message
	} else if (!*self->baseurl) {
		ERROR(self->ctx, "No more mirrors left to try\n");
	}

	return 0;
}

int pakfire_xfer_set_mirrorlist(struct pakfire_xfer* xfer, struct pakfire_mirrorlist* mirrors) {
	struct pakfire_mirror* mirror = NULL;
	int r;

	// Drop all references to a former mirrorlist
	if (xfer->mirrors) {
		pakfire_mirrorlist_unref(xfer->mirrors);
		xfer->mirrors = NULL;
	}

	// If a new list was passed, we store a reference to it
	if (mirrors) {
		xfer->mirrors = pakfire_mirrorlist_ref(mirrors);

		// Select the first mirror from the list
		mirror = pakfire_mirrorlist_get_first(xfer->mirrors);
	}

	// Select the mirror (or reset if NULL)
	r = pakfire_xfer_select_mirror(xfer, mirror);
	if (r < 0)
		goto ERROR;

ERROR:
	if (mirror)
		pakfire_mirror_unref(mirror);

	return r;
}

// Size

size_t pakfire_xfer_get_size(struct pakfire_xfer* xfer) {
	return xfer->expected_size;
}

int pakfire_xfer_set_size(struct pakfire_xfer* xfer, size_t size) {
	xfer->expected_size = size;

	return 0;
}

int pakfire_xfer_verify_hashes(struct pakfire_xfer* self, const struct pakfire_hashes* hashes) {
	return pakfire_hashes_import(&self->expected_hashes, hashes);
}

int pakfire_xfer_add_query(struct pakfire_xfer* xfer,
		const char* key, const char* format, ...) {
	char* value = NULL;
	va_list args;
	int r;

	// Format the value
	va_start(args, format);
	r = vasprintf(&value, format, args);
	va_end(args);

	// Break on error
	if (r < 0)
		return -errno;

	// Append the argument
	r = pakfire_strings_appendf(&xfer->queries, "%s=%s", key, value);
	if (value)
		free(value);

	return r;
}

int pakfire_xfer_add_param(struct pakfire_xfer* xfer,
		const char* key, const char* format, ...) {
	curl_mimepart* part = NULL;
	char* buffer = NULL;
	va_list args;
	int r;

	// Allocate the MIME object if not done, yet
	if (!xfer->mime) {
		xfer->mime = curl_mime_init(xfer->handle);

		if (!xfer->mime) {
			ERROR(xfer->ctx, "Could not allocate the MIME object: %s\n",
				strerror(errno));
			r = -errno;
			goto ERROR;
		}
	}

	// Format value
	va_start(args, format);
	r = vasprintf(&buffer, format, args);
	va_end(args);

	// Abort if we could not format the value
	if (r < 0)
		goto ERROR;

	// Allocate another MIME part
	part = curl_mime_addpart(xfer->mime);
	if (!part) {
		ERROR(xfer->ctx, "Could not allocate MIME part: %s\n",
			strerror(errno));
		r = errno;
		goto ERROR;
	}

	// Set the key
	r = curl_mime_name(part, key);
	if (r) {
		ERROR(xfer->ctx, "Could not set parameter key (%s): %s\n",
			key, curl_easy_strerror(r));
		goto ERROR;
	}

	// Set the data
	r = curl_mime_data(part, buffer, CURL_ZERO_TERMINATED);
	if (r) {
		ERROR(xfer->ctx, "Could not set parameter data (%s): %s\n",
			key, curl_easy_strerror(r));
		goto ERROR;
	}

ERROR:
	if (buffer)
		free(buffer);

	return r;
}

static void pakfire_xfer_reset_output(struct pakfire_xfer* xfer) {
	if (xfer->fin) {
		fclose(xfer->fin);
		xfer->fin = NULL;
	}
}

int pakfire_xfer_set_output(struct pakfire_xfer* xfer, FILE* f) {
	pakfire_xfer_reset_output(xfer);

	// Store the new stream
	xfer->fin = f;

	return 0;
}

int pakfire_xfer_set_output_buffer(struct pakfire_xfer* xfer,
		char** buffer, size_t* length) {
	FILE* f = NULL;

	// Open a memory stream
	f = open_memstream(buffer, length);
	if (!f) {
		ERROR(xfer->ctx, "Could not open memory stream: %s\n", strerror(errno));
		return -errno;
	}

	return pakfire_xfer_set_output(xfer, f);
}

int pakfire_xfer_set_input(struct pakfire_xfer* xfer, FILE* f) {
	struct stat stat;
	int r;

	// Read from the beginning
	r = pakfire_rewind(f);
	if (r < 0)
		return r;

	// Fetch the file descriptor
	const int fd = fileno(f);

	// Change direction
	xfer->direction = PAKFIRE_XFER_UPLOAD;

	// Store the file handle
	xfer->fout = f;

	// Try to find the upload size
	if (fd > 0) {
		r = fstat(fd, &stat);
		if (r < 0)
			return -errno;

		// Store the expected filesize
		xfer->expected_size = stat.st_size;
	}

	return 0;
}

static int pakfire_xfer_allocate_tmpfile_legacy(struct pakfire_xfer* xfer, const char* path) {
	FILE* f = NULL;
	int r;

	// Compose the path
	r = pakfire_string_format(xfer->tmpfile, "%s.XXXXXX", path);
	if (r)
		return r;

	// Create a temporary file
	f = pakfire_mktemp(xfer->tmpfile, 0600);
	if (!f) {
		ERROR(xfer->ctx, "Could not open temporary file for %s: %s\n",
			path, strerror(errno));
		return -errno;
	}

	// Set the handle as output
	return pakfire_xfer_set_output(xfer, f);
}

static int pakfire_xfer_allocate_tmpfile(struct pakfire_xfer* xfer, const char* path) {
	char dirname[PATH_MAX];
	FILE* f = NULL;
	int fd = -1;
	int r;

	// Find the directory name
	r = pakfire_path_dirname(dirname, path);
	if (r)
		return r;

	// Ensure the directory exists
	r = pakfire_mkdir(dirname, 0755);
	if (r)
		return r;

	// Open a new temporary file
	fd = open(dirname, O_TMPFILE|O_RDWR, 0600);
	if (fd < 0) {
		switch (errno) {
			// Fall back to another way for filesystems that don't support O_TMPFILE
			case ENOTSUP:
				return pakfire_xfer_allocate_tmpfile_legacy(xfer, path);

			default:
				ERROR(xfer->ctx, "Could not open temporary file in %s: %s\n",
					dirname, strerror(errno));
				return -errno;
		}
	}

	// Turn the file descriptor into a FILE handle
	f = fdopen(fd, "w+");
	if (!f)
		return -errno;

	// Set the handle as output
	return pakfire_xfer_set_output(xfer, f);
}

int pakfire_xfer_set_output_path(struct pakfire_xfer* xfer, const char* path) {
	int r;

	// Store the output path
	r = pakfire_string_set(xfer->path, path);
	if (r)
		return r;

	// Allocate a temporary file
	r = pakfire_xfer_allocate_tmpfile(xfer, path);
	if (r)
		return r;

	return 0;
}

int pakfire_xfer_auth(struct pakfire_xfer* xfer) {
	// Enable authentication
	xfer->auth = 1;

	return 0;
}

/*
	This function translates any cURL error codes into xfer codes
*/
static pakfire_xfer_error_code_t pakfire_xfer_code(CURLcode code, int http_status) {
	switch (code) {
		case CURLE_OK:
			return PAKFIRE_XFER_OK;

		// Unsupported requests
		case CURLE_UNSUPPORTED_PROTOCOL:
		case CURLE_NOT_BUILT_IN:
		case CURLE_RANGE_ERROR:
			return PAKFIRE_XFER_UNSUPPORTED;

		// Invalid requests
		case CURLE_URL_MALFORMAT:
			return PAKFIRE_XFER_INVALID_URL;

		// Invalid responses
		case CURLE_TOO_MANY_REDIRECTS:
		case CURLE_GOT_NOTHING:
		case CURLE_BAD_CONTENT_ENCODING:
			return PAKFIRE_XFER_INVALID_RESPONSE;

		// DNS Errors
		case CURLE_COULDNT_RESOLVE_PROXY:
		case CURLE_COULDNT_RESOLVE_HOST:
		case CURLE_FTP_CANT_GET_HOST:
			return PAKFIRE_XFER_DNS_ERROR;

		// Authentication
		case CURLE_AUTH_ERROR:
			return PAKFIRE_XFER_AUTH_ERROR;

		// Transport Errors
		case CURLE_PROXY:
		case CURLE_SSL_CONNECT_ERROR:
		case CURLE_SSL_CERTPROBLEM:
		case CURLE_SSL_CIPHER:
		case CURLE_PEER_FAILED_VERIFICATION:
			return PAKFIRE_XFER_TRANSPORT_ERROR;

		// Access Denied
		case CURLE_REMOTE_ACCESS_DENIED:
			return PAKFIRE_XFER_ACCESS_DENIED;

		// Timeout
		case CURLE_FTP_ACCEPT_TIMEOUT:
		case CURLE_OPERATION_TIMEDOUT:
			return PAKFIRE_XFER_TIMEOUT;

		// Write error
		case CURLE_WRITE_ERROR:
			return PAKFIRE_XFER_WRITE_ERROR;

		// Read error
		case CURLE_READ_ERROR:
		case CURLE_FILE_COULDNT_READ_FILE:
			return PAKFIRE_XFER_READ_ERROR;

		// Aborted
		case CURLE_ABORTED_BY_CALLBACK:
			return PAKFIRE_XFER_ABORTED;

		// HTTP Errors
		case CURLE_HTTP_RETURNED_ERROR:
			switch (http_status) {
				// Proxy Error
				case 502:
					return PAKFIRE_XFER_TRANSPORT_ERROR;

				// Service Unavailable
				case 503:
					return PAKFIRE_XFER_TRANSPORT_ERROR;

				default:
					return PAKFIRE_XFER_FAILED;
			}
			break;

		// Return "unknown error" for any other codes
		default:
			return PAKFIRE_XFER_FAILED;
	}
}

/*
	Called after something went wrong and we want to try again with another mirror
*/
static int pakfire_xfer_next_mirror(struct pakfire_xfer* xfer) {
	struct pakfire_mirror* mirror = NULL;
	int r = 0;

	// If our mirror is broken, we select the next one
	if (xfer->mirror) {
		// Choose the next mirror
		mirror = pakfire_mirrorlist_get_next(xfer->mirrors, xfer->mirror);

		// Store the new mirror
		r = pakfire_xfer_select_mirror(xfer, mirror);
		if (r < 0)
			goto ERROR;
	}

ERROR:
	if (mirror)
		pakfire_mirror_unref(mirror);

	return r;
}

static int pakfire_xfer_fail(struct pakfire_xfer* xfer, int code) {
	int r;

	// Drop the hasher
	if (xfer->hasher) {
		pakfire_hasher_unref(xfer->hasher);
		xfer->hasher = NULL;
	}

	// Throw away any downloaded data
	if (xfer->fin) {
		// Get file descriptor
		int fd = fileno(xfer->fin);

		// Truncate downloaded data
		if (fd >= 0) {
			r = ftruncate(fd, 0);
			if (r)
				return r;
		}

		// Rewind
		r = pakfire_rewind(xfer->fin);
		if (r < 0) {
			ERROR(xfer->ctx, "Could not rewind output file: %m\n");
			return -errno;
		}
	}

	// Did we use a mirror?
	if (xfer->mirror) {
		// Tell the mirror about the failure
		r = pakfire_mirror_xfer_failed(xfer->mirror, code);
		if (r < 0)
			return r;

		// Select the next mirror
		r = pakfire_xfer_next_mirror(xfer);
		if (r < 0)
			return r;

		// Try again with another mirror
		return -EAGAIN;
	}

	// Call the close callback for WebSockets
	if (xfer->direction == PAKFIRE_XFER_SOCKET) {
		if (xfer->callbacks.close) {
			r = xfer->callbacks.close(xfer, code, xfer->callbacks.data);
			if (r)
				return r;
		}
	}

	return code;
}

static const char* curl_http_version(long v) {
	switch (v) {
#ifdef CURL_HTTP_VERSION_3_0
		case CURL_HTTP_VERSION_3_0:
			return "HTTP/3.0";
#endif

		case CURL_HTTP_VERSION_2_0:
			return "HTTP/2.0";

		case CURL_HTTP_VERSION_1_1:
			return "HTTP/1.1";

		case CURL_HTTP_VERSION_1_0:
			return "HTTP/1.0";
	}

	return "unknown";
}

static int pakfire_xfer_allocate(struct pakfire_xfer* xfer, size_t length) {

	// Otherwise, we resize the buffer
	xfer->recv_buffer.data = pakfire_realloc(xfer->recv_buffer.data, length);
	if (!xfer->recv_buffer.data) {
		ERROR(xfer->ctx, "Could not allocate memory: %m\n");

		return -errno;
	}

	// Update the length
	xfer->recv_buffer.length = length;

	return 0;
}

static int pakfire_xfer_socket_send(struct pakfire_xfer* self) {
	int r;

	// The socket is now ready to send
	self->state |= PAKFIRE_XFER_READY_TO_SEND;

	// Finish sending any partially sent messages
	if (self->send_buffer.data) {
		r = pakfire_xfer_send_message(self, self->send_buffer.data, self->send_buffer.length);
		if (r < 0)
			return r;
	}

	// Just call the callback (if there is one)
	if (self->callbacks.send) {
		r = self->callbacks.send(self, self->callbacks.data);
		if (r)
			return r;
	}

	return 0;
}

static int pakfire_xfer_socket_recv(struct pakfire_xfer* xfer) {
	const struct curl_ws_frame* meta = NULL;
	char* buffer[4096];
	int r;

	size_t bytes_received = 0;

	// Read as many bytes as possible
	r = curl_ws_recv(xfer->handle, buffer, sizeof(buffer), &bytes_received, &meta);
	switch (r) {
		case CURLE_OK:
			break;

		case CURLE_AGAIN:
			return 0;

		// We seem to have lost the connection
		case CURLE_GOT_NOTHING:
			return pakfire_xfer_fail(xfer, PAKFIRE_XFER_TRANSPORT_ERROR);

		default:
			ERROR(xfer->ctx, "Could not read from WebSocket: %s\n", curl_easy_strerror(r));

			return r;
	}

	DEBUG(xfer->ctx, "Read %zu byte(s) from WebSocket\n", bytes_received);

	// If we have not received anything, we will wait for being called again
	if (!bytes_received)
		return 0;

	// Allocate some buffer space
	r = pakfire_xfer_allocate(xfer, meta->offset + bytes_received);
	if (r)
		return r;

	// Copy the received message
	memcpy(xfer->recv_buffer.data + meta->offset, buffer, bytes_received);

	// Call again if this was not the entire message
	if (meta->flags & CURLWS_CONT)
		return pakfire_xfer_socket_recv(xfer);

	DEBUG(xfer->ctx, "We have received a message of %zu byte(s)\n", xfer->recv_buffer.length);

	if (meta->flags & CURLWS_TEXT) {
		DEBUG(xfer->ctx, "The message is a text\n");
	} else if (meta->flags & CURLWS_BINARY) {
		DEBUG(xfer->ctx, "The message is binary\n");
	}

	// Once we have received the entire message we call the callback
	if (xfer->callbacks.recv) {
		r = xfer->callbacks.recv(xfer, xfer->recv_buffer.data, xfer->recv_buffer.length, xfer->callbacks.data);
		if (r)
			return r;
	}

	return 0;
}

static int __pakfire_xfer_socket(sd_event_source* s, int fd, uint32_t events, void* data) {
	struct pakfire_xfer* xfer = data;
	int r;

	// Is there any data to read?
	if (events & EPOLLIN) {
		r = pakfire_xfer_socket_recv(xfer);
		if (r)
			return r;
	}

	// Is there any data to send?
	if (events & EPOLLOUT) {
		r = pakfire_xfer_socket_send(xfer);
		if (r)
			return r;
	}

	return 0;
}

/*
	This is being called once a WebSocket connection has connected and is ready to
	send or receive data.
*/
static pakfire_xfer_error_code_t pakfire_xfer_done_socket(
		struct pakfire_xfer* xfer, sd_event* loop, int code) {
	curl_socket_t socket = -1;
	int events = 0;
	int r;

	// Fetch the socket
	r = curl_easy_getinfo(xfer->handle, CURLINFO_ACTIVESOCKET, &socket);
	if (r) {
		ERROR(xfer->ctx, "Could not fetch the socket: %s\n", curl_easy_strerror(r));
		goto ERROR;
	}

	DEBUG(xfer->ctx, "Connection is using socket %d\n", socket);

	// Check what callbacks we have
	if (xfer->callbacks.recv)
		events |= EPOLLET|EPOLLIN;

	if (xfer->callbacks.send)
		events |= EPOLLET|EPOLLOUT;

	// Register a callback with the event loop
	r = sd_event_add_io(loop, &xfer->event, socket, events, __pakfire_xfer_socket, xfer);
	if (r < 0) {
		ERROR(xfer->ctx, "Could not register socket %d: %s\n", socket, strerror(-r));

		goto ERROR;
	}

	DEBUG(xfer->ctx, "WebSocket registered with event loop\n");

	// Call the open callback
	if (xfer->callbacks.open) {
		r = xfer->callbacks.open(xfer, xfer->callbacks.data);
		if (r)
			goto ERROR;
	}

	// If we got to here, we want to keep the handle open and connected
	r = -EINPROGRESS;

ERROR:
	if (loop)
		sd_event_unref(loop);

	return r;
}

/*
	This function checks if the hashes match (if set up)
*/
static int pakfire_xfer_verify(struct pakfire_xfer* self) {
	struct pakfire_hashes computed_hashes = {};
	int r;

	// Nothing to do if there is no hasher
	if (!self->hasher)
		return 0;

	// Finish the hasher
	r = pakfire_hasher_finalize(self->hasher, &computed_hashes);
	if (r < 0)
		return r;

	// Compare the hashes
	r = pakfire_hashes_compare(self->ctx, &self->expected_hashes, &computed_hashes);
	switch (r) {
		case 0:
			DEBUG(self->ctx, "Payload matches\n");
			return 0;

		case 1:
			ERROR(self->ctx, "Download checksum for %s didn't match:\n", self->effective_url);

			// Show the expected hashes
			ERROR(self->ctx, " Expected Hashes:\n");
			pakfire_hashes_dump(self->ctx, &self->expected_hashes, LOG_ERR);

			// Show the computed hashes
			ERROR(self->ctx, " Computed Hashes:\n");
			pakfire_hashes_dump(self->ctx, &computed_hashes, LOG_ERR);

			// Consider the download failed
			return pakfire_xfer_fail(self, PAKFIRE_XFER_DIGEST_MISMATCH);

		// Errors
		default:
			ERROR(self->ctx, "Failed to compare hashes: %s\n", strerror(-r));
			return r;
	}
}

static int pakfire_xfer_save(struct pakfire_xfer* xfer) {
	int fd = -EBADF;
	int r;

	// Flush any buffered data out to disk
	if (xfer->fin)
		fflush(xfer->fin);

	// Nothing to do if path isn't set
	if (!*xfer->path)
		return 0;

	DEBUG(xfer->ctx, "Download successful. Storing result in %s\n", xfer->path);

	if (xfer->fin)
		fd = fileno(xfer->fin);

	// Make sure the parent directory exists
	r = pakfire_mkparentdir(xfer->path, 0755);
	if (r)
		return r;

	// Unlink the destination file (if it exists)
	unlink(xfer->path);

	// Move the temporary file to its destination
	if (*xfer->tmpfile) {
		r = link(xfer->tmpfile, xfer->path);
		if (r) {
			ERROR(xfer->ctx, "Could not link destination file %s: %m\n", xfer->path);
			return r;
		}

	// Link the file descriptor
	} else if (fd >= 0) {
		r = linkat(fd, "", AT_FDCWD, xfer->path, AT_EMPTY_PATH);
		if (r) {
			ERROR(xfer->ctx, "Could not link destination file %s: %m\n", xfer->path);
			return r;
		}

	// Fail if we don't know how to link the destination file
	} else {
		ERROR(xfer->ctx, "Don't know how to write to the destination file\n");
		return -ENOTSUP;
	}

	return 0;
}

pakfire_xfer_error_code_t pakfire_xfer_done(
		struct pakfire_xfer* xfer, sd_event* loop, int code) {
	CURL* h = xfer->handle;
	int r;
	const char* scheme = NULL;
	long response_code;
	long http_version;
	double total_time;

	curl_off_t download_size = 0;
	curl_off_t download_speed = 0;
	curl_off_t upload_size = 0;
	curl_off_t upload_speed = 0;

	// Finish progress
	if (xfer->progress) {
		r = pakfire_progress_finish(xfer->progress);
		if (r < 0)
			return r;
	}

	// Log the result
	DEBUG(xfer->ctx, "cURL xfer done: %d - %s\n", code, curl_easy_strerror(code));
	if (*xfer->error)
		DEBUG(xfer->ctx, "  Error: %s\n", xfer->error);

	// Protocol
	curl_easy_getinfo(h, CURLINFO_SCHEME, &scheme);

	// Effective URL
	curl_easy_getinfo(h, CURLINFO_EFFECTIVE_URL, &xfer->effective_url);
	if (xfer->effective_url)
		DEBUG(xfer->ctx, "  Effective URL: %s\n", xfer->effective_url);

	// Response code
	curl_easy_getinfo(h, CURLINFO_RESPONSE_CODE, &response_code);
	if (response_code)
		DEBUG(xfer->ctx, "  Response code: %ld\n", response_code);

	// HTTP Version
	curl_easy_getinfo(h, CURLINFO_HTTP_VERSION, &http_version);
	if (http_version)
		DEBUG(xfer->ctx, "  HTTP Version: %s\n", curl_http_version(http_version));

	// Total Times
	curl_easy_getinfo(h, CURLINFO_TOTAL_TIME, &total_time);
	DEBUG(xfer->ctx, "  Total Time: %.2fs\n", total_time);

	// Download Size
	r = curl_easy_getinfo(h, CURLINFO_SIZE_DOWNLOAD_T, &download_size);
	if (r)
		return r;

	if (download_size)
		DEBUG(xfer->ctx, "  Download Size: %ld bytes\n", download_size);

	// Download Speed
	r = curl_easy_getinfo(h, CURLINFO_SPEED_DOWNLOAD_T, &download_speed);
	if (r)
		return r;

	if (download_speed)
		DEBUG(xfer->ctx, "  Download Speed: %ld bps\n", download_speed);

	// Upload Size
	r = curl_easy_getinfo(h, CURLINFO_SIZE_UPLOAD_T, &upload_size);
	if (r)
		return r;

	if (upload_size)
		DEBUG(xfer->ctx, "  Upload Size: %ld bytes\n", upload_size);

	// Upload Speed
	r = curl_easy_getinfo(h, CURLINFO_SPEED_UPLOAD_T, &upload_speed);
	if (r)
		return r;

	if (upload_speed)
		DEBUG(xfer->ctx, "  Upload Speed: %ld bps\n", upload_speed);

	// All okay?
	if (code == CURLE_OK) {
		switch (xfer->direction) {
			case PAKFIRE_XFER_DOWNLOAD:
				// Verify the received payload
				r = pakfire_xfer_verify(xfer);
				if (r)
					return r;

				// Save the payload
				r = pakfire_xfer_save(xfer);
				if (r < 0)
					return r;
				break;

			case PAKFIRE_XFER_SOCKET:
				r = pakfire_xfer_done_socket(xfer, loop, code);
				if (r < 0)
					return r;

			default:
				break;
		}

	// Handle any errors
	} else {
		DEBUG(xfer->ctx, "Xfer failed: %s\n", curl_easy_strerror(code));

		// Convert the code
		code = pakfire_xfer_code(code, code);

		// Report that something went wrong
		r = pakfire_xfer_fail(xfer, code);
		if (r < 0)
			return r;
	}

	return 0;
}

static int pakfire_xfer_update(void* data,
		curl_off_t dltotal, curl_off_t dlnow, curl_off_t ultotal, curl_off_t ulnow) {
	struct pakfire_xfer* xfer = data;

	switch (xfer->direction) {
		case PAKFIRE_XFER_DOWNLOAD:
			// Update the expected size
			xfer->expected_size = dltotal;

			// Update the xferred counter
			xfer->xferred = dlnow;
			break;

		case PAKFIRE_XFER_UPLOAD:
			// Update the expected size
			xfer->expected_size = ultotal;

			// Update the xferred counter
			xfer->xferred = ulnow;
			break;

		case PAKFIRE_XFER_SOCKET:
			break;
	}

	// Do nothing if no progress indicator has been set up
	if (!xfer->progress)
		return 0;

	// Set maximum (because this might have changed)
	pakfire_progress_set_max_value(xfer->progress, xfer->expected_size);

	// Update current value
	return pakfire_progress_update(xfer->progress, xfer->xferred);
}

static int pakfire_xfer_prepare_progress(struct pakfire_xfer* xfer,
		struct pakfire_progress* parent, int flags) {
	const char* title = NULL;
	int progress_flags =
		PAKFIRE_PROGRESS_SHOW_PERCENTAGE |
		PAKFIRE_PROGRESS_SHOW_BYTES_TRANSFERRED |
		PAKFIRE_PROGRESS_SHOW_TRANSFER_SPEED |
		PAKFIRE_PROGRESS_SHOW_ETA;
	int r;

	// If this has already been set up, we skip it
	if (xfer->progress)
		return 0;

	// Show no progress if requested
	if (flags & PAKFIRE_XFER_NO_PROGRESS)
		progress_flags |= PAKFIRE_PROGRESS_NO_PROGRESS;

	// Show no progress if we have a parent progress
	else if (parent)
		progress_flags |= PAKFIRE_PROGRESS_NO_PROGRESS;

	// Make a new progress meter
	r = pakfire_progress_create(&xfer->progress, xfer->ctx, progress_flags, parent);
	if (r)
		return r;

	// Set the title
	title = pakfire_xfer_get_title(xfer);
	if (title) {
		r = pakfire_progress_set_title(xfer->progress, "%s", title);
		if (r)
			return r;
	}

	// Configure callbacks
	curl_easy_setopt(xfer->handle, CURLOPT_XFERINFOFUNCTION,
		pakfire_xfer_update);
	curl_easy_setopt(xfer->handle, CURLOPT_XFERINFODATA, xfer);
	curl_easy_setopt(xfer->handle, CURLOPT_NOPROGRESS, 0L);

	// Start progress
	r = pakfire_progress_start(xfer->progress, xfer->expected_size);
	if (r)
		return r;

	return 0;
}

static int pakfire_xfer_prepare_url(struct pakfire_xfer* xfer) {
	int r;

	// If this has been called before, we free the former object
	if (xfer->fullurl)
		curl_url_cleanup(xfer->fullurl);

	// Create a new URL object
	xfer->fullurl = curl_url();
	if (!xfer->fullurl) {
		ERROR(xfer->ctx, "Could not allocate a new URL: %m\n");
		r = -errno;
		goto ERROR;
	}

	// Check if our selected mirror has not been disabled in the meantime
	if (xfer->mirror && !pakfire_mirror_is_enabled(xfer->mirror)) {
		r = pakfire_xfer_next_mirror(xfer);
		if (r < 0)
			goto ERROR;
	}

	// Simply set absolute URLs
	if (pakfire_string_is_url(xfer->url)) {
		r = curl_url_set(xfer->fullurl, CURLUPART_URL, xfer->url, 0);
		if (r)
			goto ERROR;

	// Join path if we are using a mirror
	} else if (xfer->mirror) {
		// Set the mirror's base URL first
		r = curl_url_set(xfer->fullurl, CURLUPART_URL,
			pakfire_mirror_get_url(xfer->mirror), 0);
		if (r)
			goto ERROR;

		// Then append our own part
		r = curl_url_set(xfer->fullurl, CURLUPART_URL, xfer->url, 0);
		if (r)
			goto ERROR;

	// Use baseurl
	} else if (*xfer->baseurl) {
		// Set the base URL first
		r = curl_url_set(xfer->fullurl, CURLUPART_URL, xfer->baseurl, 0);
		if (r)
			goto ERROR;

		// Then append our own part
		r = curl_url_set(xfer->fullurl, CURLUPART_URL, xfer->url, 0);
		if (r)
			goto ERROR;

	// Fail if we could not set the URL
	} else {
		ERROR(xfer->ctx, "Invalid xfer %s\n", xfer->url);
		r = -EINVAL;
		goto ERROR;
	}

	// Replace the schema if we are using SOCKET
	if (xfer->direction == PAKFIRE_XFER_SOCKET) {
		r = curl_url_set(xfer->fullurl, CURLUPART_SCHEME, "wss", 0);
		if (r) {
			ERROR(xfer->ctx, "Could not change to WebSocket: %s\n", curl_url_strerror(r));
			goto ERROR;
		}
	}

	// Append any query arguments
	if (xfer->queries) {
		for (char** query = xfer->queries; *query; query++) {
			r = curl_url_set(xfer->fullurl, CURLUPART_QUERY,
					*query, CURLU_APPENDQUERY|CURLU_URLENCODE);
			if (r) {
				ERROR(xfer->ctx, "Could not set query argument '%s': %s\n",
						*query, curl_url_strerror(r));
				goto ERROR;
			}
		}
	}

	// Set the URL
	r = curl_easy_setopt(xfer->handle, CURLOPT_CURLU, xfer->fullurl);
	if (r) {
		ERROR(xfer->ctx, "Could not set the URL: %s\n", curl_easy_strerror(r));
		goto ERROR;
	}

ERROR:
	return r;
}

int pakfire_xfer_prepare(struct pakfire_xfer* xfer, struct pakfire_progress* progress, int flags) {
	int r;

	// Increment tries
	xfer->tries++;

	// Set special options for direction
	switch (xfer->direction) {
		case PAKFIRE_XFER_DOWNLOAD:
			if (xfer->expected_hashes.types) {
				r = pakfire_hasher_create(&xfer->hasher, xfer->ctx, xfer->expected_hashes.types);
				if (r < 0) {
					ERROR(xfer->ctx, "Failed to setup the hasher: %s\n", strerror(-r));
					return r;
				}
			}
			break;

		case PAKFIRE_XFER_UPLOAD:
			// Let cURL know that we are uploading things
			r = curl_easy_setopt(xfer->handle, CURLOPT_UPLOAD, 1L);
			if (r) {
				ERROR(xfer->ctx, "Could not enable upload\n");
				return r;
			}

			// Tell it the expected upload size
			if (xfer->expected_size) {
				r = curl_easy_setopt(xfer->handle,
					CURLOPT_INFILESIZE_LARGE, (curl_off_t)xfer->expected_size);
				if (r) {
					ERROR(xfer->ctx, "Could not set upload size\n");
					return r;
				}
			}
			break;

		case PAKFIRE_XFER_SOCKET:
			// Ask cURL to connect and let us handle the rest
			r = curl_easy_setopt(xfer->handle, CURLOPT_CONNECT_ONLY, 2L);
			if (r) {
				ERROR(xfer->ctx, "Could not enable CONNECT_ONLY\n");
				return r;
			}

			break;
	}

	// Compose the URL
	r = pakfire_xfer_prepare_url(xfer);
	if (r < 0) {
		ERROR(xfer->ctx, "Could not compose URL: %s\n", strerror(-r));
		return r;
	}

	// Add any headers
	if (xfer->headers) {
		r = curl_easy_setopt(xfer->handle, CURLOPT_HTTPHEADER, xfer->headers);
		if (r) {
			ERROR(xfer->ctx, "Could not set headers: %s\n", curl_easy_strerror(r));
			return r;
		}
	}

	// Add any payload
	if (xfer->mime) {
		r = curl_easy_setopt(xfer->handle, CURLOPT_MIMEPOST, xfer->mime);
		if (r) {
			ERROR(xfer->ctx, "Could not set POST payload: %s\n", curl_easy_strerror(r));
			return r;
		}
	}

	// Authentication
	if (xfer->auth) {
		// Request SPNEGO
		r = curl_easy_setopt(xfer->handle, CURLOPT_HTTPAUTH, CURLAUTH_NEGOTIATE);
		if (r) {
			ERROR(xfer->ctx, "Could not enable SPNEGO\n");
			return r;
		}

		// Set an empty username
		r = curl_easy_setopt(xfer->handle, CURLOPT_USERPWD, ":");
		if (r) {
			ERROR(xfer->ctx, "Could not set username\n");
			return r;
		}
	}

	// Setup progress
	r = pakfire_xfer_prepare_progress(xfer, progress, flags);
	if (r)
		return r;

	return 0;
}

int pakfire_xfer_socket(struct pakfire_xfer* xfer, pakfire_xfer_open_callback open,
		pakfire_xfer_recv_callback recv, pakfire_xfer_send_callback send,
		pakfire_xfer_close_callback close, void* data) {
	xfer->direction = PAKFIRE_XFER_SOCKET;

	// Store the callbacks
	xfer->callbacks.open  = open;
	xfer->callbacks.recv  = recv;
	xfer->callbacks.send  = send;
	xfer->callbacks.close = close;
	xfer->callbacks.data  = data;

	return 0;
}

static int pakfire_xfer_store_message(struct pakfire_xfer* self,
		const char* message, const size_t length) {
	// Resize the buffer
	self->send_buffer.data = pakfire_realloc(self->send_buffer.data, length);
	if (!self->send_buffer.data) {
		ERROR(self->ctx, "Failed to allocate memory: %m\n");
		return -errno;
	}

	// Store the message
	memcpy(self->send_buffer.data, message, length);
	self->send_buffer.length = length;

	return 0;
}

/*
	This function sends a WebSocket message
*/
static int __pakfire_xfer_send_message(struct pakfire_xfer* xfer,
		const char* message, const size_t length) {
	size_t bytes_sent = 0;
	size_t offset = 0;
	int r = 0;

	// Send the message
	while (offset < length) {
		r = curl_ws_send(xfer->handle, message + offset, length - offset,
				&bytes_sent, 0, CURLWS_TEXT);

		// Update offset
		offset += bytes_sent;

		switch (r) {
			// Done, but make sure we have sent the entire buffer
			case CURLE_OK:
				continue;

			// We could not send all data, store the message
			// and wait until the socket is ready
			case CURLE_AGAIN:
				// We cannot send no more
				xfer->state &= ~PAKFIRE_XFER_READY_TO_SEND;

				// Store the message
				r = pakfire_xfer_store_message(xfer, message + offset, length - offset);
				if (r < 0)
					return r;

				return -EAGAIN;

			default:
				ERROR(xfer->ctx, "Could not send message: %s\n", curl_easy_strerror(r));
				break;
		}
	}

	return r;
}

int pakfire_xfer_send_message(struct pakfire_xfer* self,
		const char* message, const size_t length) {
	int r;

	// Send any left over fragments from a previous message before sending a new one
	if (self->send_buffer.length) {
		r = __pakfire_xfer_send_message(self, self->send_buffer.data, self->send_buffer.length);
		if (r < 0) {
			ERROR(self->ctx, "Failed to send previous fragment: %s\n", strerror(-r));
			return r;
		}
	}

	// Send the actual message
	return __pakfire_xfer_send_message(self, message, length);
}

int pakfire_xfer_is_ready_to_send(struct pakfire_xfer* self) {
	// This function is only supported for sockets
	switch (self->direction) {
		case PAKFIRE_XFER_SOCKET:
			break;

		default:
			return -ENOTSUP;
	}

	return (self->state & PAKFIRE_XFER_READY_TO_SEND);
}

pakfire_xfer_error_code_t pakfire_xfer_run(struct pakfire_xfer* xfer, int flags) {
	int r;

	// Prepare the xfer
	r = pakfire_xfer_prepare(xfer, NULL, flags);
	if (r) {
		ERROR(xfer->ctx, "Could not prepare xfer %s: %s\n",
			xfer->url, strerror(-r));
		return r;
	}

	// Perform the action
	r = curl_easy_perform(xfer->handle);

	// Handle the result
	r = pakfire_xfer_done(xfer, NULL, r);
	if (r < 0) {
		switch (-r) {
			// Repeat if asked
			case EAGAIN:
				return pakfire_xfer_run(xfer, flags);

			default:
				break;
		}
	}

	return r;
}

static int pakfire_xfer_handle_api_error(
		struct pakfire_xfer* xfer, const struct json_object* error) {
	struct json_object* message = NULL;
	struct json_object* code = NULL;
	const char* m = NULL;
	unsigned int c = 0;

	// Fetch the URL
	const char* url = pakfire_xfer_get_effective_url(xfer);

	// Fetch the code
	if (!json_object_object_get_ex(error, "code", &code))
		return -EBADMSG;

	// Check if the code is an integer
	if (!json_object_is_type(code, json_type_int))
		return -EBADMSG;

	// Fetch the message
	if (!json_object_object_get_ex(error, "message", &message))
		return -EBADMSG;

	// Check if the message is a string
	if (!json_object_is_type(message, json_type_string))
		return -EBADMSG;

	c = json_object_get_uint64(code);
	m = json_object_get_string(message);

	// Log the error
	ERROR(xfer->ctx, "%s responded with error %u (%s):\n  %s\n",
		url, c, strerror(c), m);

	return -c;
}

/*
	This function parses an API response
*/
static int pakfire_xfer_parse_api_response(struct pakfire_xfer* xfer,
		const char* buffer, const size_t length, struct json_object** object) {
	struct json_object* error = NULL;
	struct json_object* o = NULL;
	int r;

	// Check if we received any data
	if (!length) {
		DEBUG(xfer->ctx, "Received an empty response\n");

		// Reset the object
		if (object)
			*object = NULL;

		return 0;
	}

	// XXX Maybe fetch the parser's error message here?!

	// Parse the buffer
	o = pakfire_json_parse(xfer->ctx, buffer, length);
	if (!o) {
		ERROR(xfer->ctx, "Could not parse the response\n");
		r = -EBADMSG;
		goto ERROR;
	}

	// Check if the response is a dictionary
	if (!json_object_is_type(o, json_type_object)) {
		ERROR(xfer->ctx, "The received object is not a JSON dict\n");
		r = -EBADMSG;
		goto ERROR;
	}

	// Fetch error
	r = json_object_object_get_ex(o, "error", &error);
	if (r) {
		r = pakfire_xfer_handle_api_error(xfer, error);
		goto ERROR;
	}

	// Return the object
	if (object)
		*object = json_object_get(o);

ERROR:
	if (o)
		json_object_put(o);

	return r;
}

static pakfire_xfer_error_code_t pakfire_xfer_run_api_request_once(
		struct pakfire_xfer* xfer, struct json_object** response) {
	char* buffer = NULL;
	size_t length = 0;
	int r;

	// Write the response to the buffer
	r = pakfire_xfer_set_output_buffer(xfer, &buffer, &length);
	if (r)
		goto ERROR;

	// Run the xfer
	r = pakfire_xfer_run(xfer, PAKFIRE_XFER_NO_PROGRESS);
	if (r)
		goto ERROR;

	// Parse the response
	r = pakfire_xfer_parse_api_response(xfer, buffer, length, response);
	if (r) {
		ERROR(xfer->ctx, "Could not parse the API response: %s\n", strerror(-r));
		goto ERROR;
	}

ERROR:
	// Reset the output stream
	pakfire_xfer_reset_output(xfer);

	if (buffer)
		free(buffer);

	return r;
}

/*
	This function sends a request and automatically parses the response.
	The response might optionally be returned if response is not NULL.
*/
pakfire_xfer_error_code_t pakfire_xfer_run_api_request(
		struct pakfire_xfer* xfer, struct json_object** response) {
	pakfire_xfer_error_code_t r;

	// Loop indefinitely...
	for (;;) {
		r = pakfire_xfer_run_api_request_once(xfer, response);
		switch (r) {
			// Try again after five seconds for recoverable errors
			case PAKFIRE_XFER_DNS_ERROR:
			case PAKFIRE_XFER_TRANSPORT_ERROR:
			case PAKFIRE_XFER_TIMEOUT:
				usleep(5000000);
				break;

			default:
				return r;
		}
	}

	return 0;
}
