/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2014 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <fnmatch.h>
#include <libgen.h>
#include <limits.h>
#include <linux/limits.h>
#include <stdlib.h>
#include <string.h>
#include <sys/capability.h>
#include <sys/mman.h>
#include <sys/sendfile.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>

#include <archive_entry.h>

#include <pakfire/ctx.h>
#include <pakfire/constants.h>
#include <pakfire/hashes.h>
#include <pakfire/elf.h>
#include <pakfire/file.h>
#include <pakfire/logging.h>
#include <pakfire/pakfire.h>
#include <pakfire/path.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

enum pakfire_file_verification_status {
	PAKFIRE_FILE_NOENT                = (1 << 0),
	PAKFIRE_FILE_TYPE_CHANGED         = (1 << 1),
	PAKFIRE_FILE_PERMISSIONS_CHANGED  = (1 << 2),
	PAKFIRE_FILE_DEV_CHANGED          = (1 << 3),
	PAKFIRE_FILE_SIZE_CHANGED         = (1 << 4),
	PAKFIRE_FILE_OWNER_CHANGED        = (1 << 5),
	PAKFIRE_FILE_GROUP_CHANGED        = (1 << 6),
	PAKFIRE_FILE_CTIME_CHANGED        = (1 << 7),
	PAKFIRE_FILE_MTIME_CHANGED        = (1 << 8),
	PAKFIRE_FILE_PAYLOAD_CHANGED      = (1 << 9),
};

struct pakfire_file {
	struct pakfire_ctx* ctx;
	struct pakfire* pakfire;
	int nrefs;

	// Use the libarchive entry to store common attributes
	struct archive_entry* entry;

	// Capabilities
	cap_t caps;

	// Flags
	int flags;

	// Hashes
	struct pakfire_hashes hashes;

	// MIME Type
	char mimetype[NAME_MAX];

	// Class
	int class;

	// Verification Status
	int verify_status;

	// File Issues
	int issues;
	int check_done:1;

	#warning TODO data
	//int is_datafile;
};

/*
	Capabilities
*/
static int pakfire_file_read_fcaps(struct pakfire_file* file,
		const struct vfs_cap_data* cap_data, size_t length) {
	int max = 0;
	int r;

	uint32_t magic_etc = le32toh(cap_data->magic_etc);

	// Which version are we dealing with?
	switch (magic_etc & VFS_CAP_REVISION_MASK) {
		case VFS_CAP_REVISION_1:
			length -= XATTR_CAPS_SZ_1;
			max = VFS_CAP_U32_1;
			break;

		case VFS_CAP_REVISION_2:
			length -= XATTR_CAPS_SZ_2;
			max = VFS_CAP_U32_2;
			break;

		case VFS_CAP_REVISION_3:
			length -= XATTR_CAPS_SZ_3;
			max = VFS_CAP_U32_3;
			break;

		// Unknown version
		default:
			errno = EINVAL;
			return 1;
	}

	// Check if we have received the correct data
	if (length) {
		errno = EINVAL;
		return 1;
	}

	// Allocate capabilities
	file->caps = cap_init();
	if (!file->caps) {
		ERROR(file->ctx, "Could not allocate capabilities: %m\n");
		r = 1;
		goto ERROR;
	}

	int mask;
	int index;

	int cap_permitted;
	int cap_inheritable;
	int cap_effective = 0;

	for (unsigned int cap = 0; cap_valid(cap); cap++) {
		// Find the index where to find this cap
		index = CAP_TO_INDEX(cap);
		mask  = CAP_TO_MASK(cap);

		// End if we have reached the end of the data
		if (index > max)
			break;

		// Check for permitted/inheritable flag set
		cap_permitted   = le32toh(cap_data->data[index].permitted)   & mask;
		cap_inheritable = le32toh(cap_data->data[index].inheritable) & mask;

		// Check for effective
		if (magic_etc & VFS_CAP_FLAGS_EFFECTIVE)
			cap_effective = cap_permitted | cap_inheritable;

		cap_value_t caps[] = { cap };

		if (cap_permitted) {
			r = cap_set_flag(file->caps, CAP_PERMITTED, 1, caps, CAP_SET);
			if (r) {
				ERROR(file->ctx, "Could not set capability %u: %m\n", cap);
				goto ERROR;
			}
		}

		if (cap_inheritable) {
			r = cap_set_flag(file->caps, CAP_INHERITABLE, 1, caps, CAP_SET);
			if (r) {
				ERROR(file->ctx, "Could not set capability %u: %m\n", cap);
				goto ERROR;
			}
		}

		if (cap_effective) {
			r = cap_set_flag(file->caps, CAP_EFFECTIVE, 1, caps, CAP_SET);
			if (r) {
				ERROR(file->ctx, "Could not set capability %u: %m\n", cap);
				goto ERROR;
			}
		}
	}

#ifdef ENABLE_DEBUG
	char* text = cap_to_text(file->caps, NULL);
	if (text) {
		DEBUG(file->ctx, "%s: Capabilities %s\n", pakfire_file_get_path(file), text);
		cap_free(text);
	}
#endif

	return 0;

ERROR:
	if (file->caps) {
		cap_free(file->caps);
		file->caps = NULL;
	}

	return r;
}

int pakfire_file_write_fcaps(struct pakfire_file* file, struct vfs_cap_data* cap_data) {
	cap_flag_value_t cap_permitted;
	cap_flag_value_t cap_inheritable;
	cap_flag_value_t cap_effective;
	int r;

	// This should not be called when we have no caps
	if (!file->caps) {
		errno = EINVAL;
		return 1;
	}

	uint32_t magic = VFS_CAP_REVISION_2;

	for (unsigned int cap = 0; cap_valid(cap); cap++) {
		// Find the index where to find this cap
		int index = CAP_TO_INDEX(cap);
		int mask  = CAP_TO_MASK(cap);

		// Fetch CAP_PERMITTED
		r = cap_get_flag(file->caps, cap, CAP_PERMITTED, &cap_permitted);
		if (r) {
			ERROR(file->ctx, "Could not fetch capability %u: %m\n", cap);
			goto ERROR;
		}

		// Fetch CAP_INHERITABLE
		r = cap_get_flag(file->caps, cap, CAP_INHERITABLE, &cap_inheritable);
		if (r) {
			ERROR(file->ctx, "Could not fetch capability %u: %m\n", cap);
			goto ERROR;
		}

		// Fetch CAP_EFFECTIVE
		r = cap_get_flag(file->caps, cap, CAP_EFFECTIVE, &cap_effective);
		if (r) {
			ERROR(file->ctx, "Could not fetch capability %u: %m\n", cap);
			goto ERROR;
		}

		// Store CAP_PERMITTED
		if (cap_permitted)
			cap_data->data[index].permitted |= htole32(mask);

		// Store CAP_INHERITED
		if (cap_inheritable)
			cap_data->data[index].inheritable |= htole32(mask);

		// Set EFFECTIVE flag if CAP_EFFECTIVE is set
		if (cap_effective)
			magic |= VFS_CAP_FLAGS_EFFECTIVE;
	}

	// Store the magic value
	cap_data->magic_etc = htole32(magic);

ERROR:
	return r;
}

static int pakfire_file_from_archive_entry(struct pakfire_file* file, struct archive_entry* entry) {
	char path[PATH_MAX];
	char* buffer = NULL;
	const char* attr = NULL;
	const void* value = NULL;
	size_t size = 0;
	int r = 0;

	// Make the analyzer happy
	if (!file)
		return -EINVAL;

	// Remove the existing entry
	if (file->entry)
		archive_entry_free(file->entry);

	// Clone the given entry
	file->entry = archive_entry_clone(entry);
	if (!file->entry)
		return -errno;

	// Make the path absolute
	r = pakfire_path_absolute(path, archive_entry_pathname(file->entry));
	if (r < 0)
		goto ERROR;

	// Store the absolute path
	archive_entry_copy_pathname(file->entry, path);

	// Reset iterating over extended attributes
	archive_entry_xattr_reset(file->entry);

	// Read any extended attributes
	while (archive_entry_xattr_next(entry, &attr, &value, &size) == ARCHIVE_OK) {
		// Config Files
		if (strcmp(attr, "PAKFIRE.config") == 0) {
			r = pakfire_file_set_flags(file, PAKFIRE_FILE_CONFIG);
			if (r)
				goto ERROR;

		// MIME type
		} else if (strcmp(attr, "PAKFIRE.mimetype") == 0) {
			// Copy the value into a NULL-terminated buffer
			r = asprintf(&buffer, "%.*s", (int)size, (const char*)value);
			if (r < 0)
				goto ERROR;

			// Assign the value
			r = pakfire_file_set_mimetype(file, buffer);
			if (r)
				goto ERROR;

		// Digest: SHA-3-512
		} else if (strcmp(attr, "PAKFIRE.digests.sha3_512") == 0) {
			r = pakfire_file_set_checksum(file, PAKFIRE_HASH_SHA3_512, value, size);
			if (r < 0)
				goto ERROR;

		// Digest: SHA-3-256
		} else if (strcmp(attr, "PAKFIRE.digests.sha3_256") == 0) {
			r = pakfire_file_set_checksum(file, PAKFIRE_HASH_SHA3_256, value, size);
			if (r < 0)
				goto ERROR;

		// Digest: BLAKE2b512
		} else if (strcmp(attr, "PAKFIRE.digests.blake2b512") == 0) {
			r = pakfire_file_set_checksum(file, PAKFIRE_HASH_BLAKE2B512, value, size);
			if (r < 0)
				goto ERROR;

		// Digest: BLAKE2s256
		} else if (strcmp(attr, "PAKFIRE.digests.blake2s256") == 0) {
			r = pakfire_file_set_checksum(file, PAKFIRE_HASH_BLAKE2S256, value, size);
			if (r < 0)
				goto ERROR;

		// Digest: SHA-2-512
		} else if (strcmp(attr, "PAKFIRE.digests.sha2_512") == 0) {
			r = pakfire_file_set_checksum(file, PAKFIRE_HASH_SHA2_512, value, size);
			if (r < 0)
				goto ERROR;

		// Digest: SHA-2-256
		} else if (strcmp(attr, "PAKFIRE.digests.sha2_256") == 0) {
			r = pakfire_file_set_checksum(file, PAKFIRE_HASH_SHA2_256, value, size);
			if (r < 0)
				goto ERROR;

		// Capabilities
		} else if (strcmp(attr, "security.capability") == 0) {
			r = pakfire_file_read_fcaps(file, value, size);
			if (r)
				goto ERROR;

		} else {
			DEBUG(file->ctx, "Received an unknown extended attribute: %s\n", attr);
		}
	}

ERROR:
	if (buffer)
		free(buffer);

	return r;
}

int pakfire_file_create(struct pakfire_file** file,
		struct pakfire* pakfire, const char* path) {
	struct pakfire_file* f = NULL;
	int r = 0;

	// Allocate a new object
	f = calloc(1, sizeof(*f));
	if (!f)
		return -errno;

	// Store a reference to the context
	f->ctx = pakfire_ctx(pakfire);

	// Store reference to Pakfire
	f->pakfire = pakfire_ref(pakfire);

	// Initialize reference counter
	f->nrefs = 1;

	// Create a new archive entry
	f->entry = archive_entry_new();
	if (!f->entry) {
		r = -errno;
		goto ERROR;
	}

	// Store path
	if (path) {
		r = pakfire_file_set_path(f, path);
		if (r < 0)
			goto ERROR;
	}

	// Return the pointer
	*file = pakfire_file_ref(f);

ERROR:
	if (f)
		pakfire_file_unref(f);

	return r;
}

static int pakfire_file_compute_hashes(struct pakfire_file* file,
		const enum pakfire_hash_type types, struct pakfire_hashes* hashes) {
	FILE* f = NULL;
	int r;

	const mode_t mode = pakfire_file_get_mode(file);

	// Skip this for anything that isn't a regular file
	if (!S_ISREG(mode))
		return 0;

	// Reset hashes
	pakfire_hashes_reset(hashes);

	// Open the file
	f = pakfire_file_fopen(file, "r");
	if (!f) {
		r = -errno;
		goto ERROR;
	}

	// Compute hashes
	r = pakfire_hash_file(file->ctx, f, types, hashes);
	if (r < 0)
		goto ERROR;

ERROR:
	if (f)
		fclose(f);

	return r;
}

int pakfire_file_create_from_archive_entry(struct pakfire_file** file, struct pakfire* pakfire,
		struct archive_entry* entry) {
	struct pakfire_file* f = NULL;
	int r;

	r = pakfire_file_create(&f, pakfire, NULL);
	if (r < 0)
		goto ERROR;

	// Copy archive entry
	r = pakfire_file_from_archive_entry(f, entry);
	if (r < 0)
		goto ERROR;

	// Return the file
	*file = pakfire_file_ref(f);

ERROR:
	if (f)
		pakfire_file_unref(f);

	return r;
}

struct archive_entry* pakfire_file_archive_entry(struct pakfire_file* file, const enum pakfire_hash_type hashes) {
	struct archive_entry* entry = NULL;
	struct vfs_cap_data cap_data = {};
	int r;

	// Clone the entry
	entry = archive_entry_clone(file->entry);
	if (!entry)
		goto ERROR;

	// Flags
	if (pakfire_file_has_flag(file, PAKFIRE_FILE_CONFIG)) {
		archive_entry_xattr_add_entry(entry,
			"PAKFIRE.config", "1", strlen("1"));
	}

	// Set MIME type
	const char* mimetype = pakfire_file_get_mimetype(file);
	if (mimetype) {
		archive_entry_xattr_add_entry(entry,
			"PAKFIRE.mimetype", mimetype, strlen(mimetype));
	}

	// Compute any required file hashes
	if (hashes) {
		r = pakfire_file_compute_hashes(file, hashes, &file->hashes);
		if (r < 0)
			goto ERROR;
	}

	// SHA-3-512
	if (pakfire_hashes_has(&file->hashes, PAKFIRE_HASH_SHA3_512))
		archive_entry_xattr_add_entry(entry, "PAKFIRE.digests.sha3_512",
			file->hashes.sha3_512, sizeof(file->hashes.sha3_512));

	// SHA-3-256
	if (pakfire_hashes_has(&file->hashes, PAKFIRE_HASH_SHA3_256))
		archive_entry_xattr_add_entry(entry, "PAKFIRE.digests.sha3_256",
			file->hashes.sha3_256, sizeof(file->hashes.sha3_256));

	// BLAKE2b512
	if (pakfire_hashes_has(&file->hashes, PAKFIRE_HASH_BLAKE2B512))
		archive_entry_xattr_add_entry(entry, "PAKFIRE.digests.blake2b512",
			file->hashes.blake2b512, sizeof(file->hashes.blake2b512));

	// BLAKE2s256
	if (pakfire_hashes_has(&file->hashes, PAKFIRE_HASH_BLAKE2S256))
		archive_entry_xattr_add_entry(entry, "PAKFIRE.digests.blake2s256",
			file->hashes.blake2s256, sizeof(file->hashes.blake2s256));

	// SHA-2-512
	if (pakfire_hashes_has(&file->hashes, PAKFIRE_HASH_SHA2_512))
		archive_entry_xattr_add_entry(entry, "PAKFIRE.digests.sha2_512",
			file->hashes.sha2_512, sizeof(file->hashes.sha2_512));

	// SHA-2-256
	if (pakfire_hashes_has(&file->hashes, PAKFIRE_HASH_SHA2_256))
		archive_entry_xattr_add_entry(entry, "PAKFIRE.digests.sha2_256",
			file->hashes.sha2_256, sizeof(file->hashes.sha2_256));

	// Capabilities
	if (file->caps) {
		r = pakfire_file_write_fcaps(file, &cap_data);
		if (r) {
			ERROR(file->ctx, "Could not export capabilities: %m\n");
			goto ERROR;
		}

		// Store capabilities in archive entry
		archive_entry_xattr_add_entry(entry,
			"security.capability", &cap_data, sizeof(cap_data));
	}

	return entry;

ERROR:
	if (entry)
		archive_entry_free(entry);

	return NULL;
}

static void pakfire_file_free(struct pakfire_file* file) {
	// Free capabilities
	if (file->caps)
		cap_free(file->caps);
	if (file->entry)
		archive_entry_free(file->entry);
	if (file->pakfire)
		pakfire_unref(file->pakfire);
	if (file->ctx)
		pakfire_ctx_unref(file->ctx);
	free(file);
}

struct pakfire_file* pakfire_file_ref(struct pakfire_file* file) {
	file->nrefs++;

	return file;
}

struct pakfire_file* pakfire_file_unref(struct pakfire_file* file) {
	if (--file->nrefs > 0)
		return file;

	pakfire_file_free(file);
	return NULL;
}

/*
	Flags
*/

int pakfire_file_has_flag(struct pakfire_file* file, int flag) {
	return file->flags & flag;
}

int pakfire_file_set_flags(struct pakfire_file* file, int flag) {
	file->flags |= flag;

	return 0;
}

#define pakfire_file_strmode(file, buffer) \
	__pakfire_file_strmode(file, buffer, sizeof(buffer))

static int __pakfire_file_strmode(struct pakfire_file* file, char* s, const size_t length) {
	const char* hardlink = NULL;
	int r;

	static const mode_t permbits[] = {
		0400,
		0200,
		0100,
		0040,
		0020,
		0010,
		0004,
		0002,
		0001
	};

	const mode_t mode = pakfire_file_get_mode(file);
	const mode_t type = pakfire_file_get_type(file);

	// Set some default string
	r = __pakfire_string_set(s, length, "?rwxrwxrwx ");
	if (r)
		return r;

	switch (type) {
		case S_IFREG:
			s[0] = '-';
			break;

		case S_IFBLK:
			s[0] = 'b';
			break;

		case S_IFCHR:
			s[0] = 'c';
			break;

		case S_IFDIR:
			s[0] = 'd';
			break;

		case S_IFLNK:
			s[0] = 'l';
			break;

		case S_IFSOCK:
			s[0] = 's';
			break;

		case S_IFIFO:
			s[0] = 'p';
			break;

		default:
			hardlink = pakfire_file_get_hardlink(file);

			if (hardlink) {
				s[0] = 'h';
				break;
			}
	}

	for (unsigned int i = 0; i < 9; i++) {
		if (mode & permbits[i])
			continue;

		s[i+1] = '-';
	}

	if (mode & S_ISUID) {
		if (mode & 0100)
			s[3] = 's';
		else
			s[3] = 'S';
	}

	if (mode & S_ISGID) {
		if (mode & 0010)
			s[6] = 's';
		else
			s[6] = 'S';
	}

	if (mode & S_ISVTX) {
		if (mode & 0001)
			s[9] = 't';
		else
			s[9] = 'T';
	}

#if 0
	if (file->caps)
		s[10] = '+';
#endif

	return 0;
}

char* pakfire_file_dump(struct pakfire_file* file, int flags) {
	char buffer[LINE_MAX] = "";
	char mode[12];
	char time[32];
	int r;

	// Format mode
	if (flags & PAKFIRE_FILE_DUMP_MODE) {
		r = pakfire_file_strmode(file, mode);
		if (r)
			return NULL;

		r = pakfire_string_appendf(buffer, " %s", mode);
		if (r < 0)
			return NULL;
	}

	// Format ownership
	if (flags & PAKFIRE_FILE_DUMP_OWNERSHIP) {
		const char* uname = pakfire_file_get_uname(file);
		const char* gname = pakfire_file_get_gname(file);

		r = pakfire_string_appendf(buffer, " %s/%s", uname, gname);
		if (r < 0)
			return NULL;
	}

	// Format size
	if (flags & PAKFIRE_FILE_DUMP_SIZE) {
		const size_t size = pakfire_file_get_size(file);

		r = pakfire_string_appendf(buffer, " %8zu", size);
		if (r < 0)
			return NULL;
	}

	// Format time
	if (flags & PAKFIRE_FILE_DUMP_TIME) {
		const time_t ctime = pakfire_file_get_ctime(file);

		r = pakfire_strftime(time, "%Y-%m-%d %H:%M", ctime);
		if (r)
			return NULL;

		r = pakfire_string_appendf(buffer, " %s", time);
		if (r < 0)
			return NULL;
	}

	// Format path
	r = pakfire_string_appendf(buffer, " %s", pakfire_file_get_path(file));
	if (r < 0)
		return NULL;

	// Append symlink target
	if (flags & PAKFIRE_FILE_DUMP_LINK_TARGETS) {
		switch (pakfire_file_get_type(file)) {
			case S_IFLNK:
				r = pakfire_string_appendf(buffer, " -> %s", pakfire_file_get_symlink(file));
				if (r < 0)
					return NULL;

			default:
				break;
		}
	}

	return strdup(buffer);
}

int pakfire_file_cmp(struct pakfire_file* file1, struct pakfire_file* file2) {
	const char* path1 = pakfire_file_get_path(file1);
	const char* path2 = pakfire_file_get_path(file2);

	return strcmp(path1, path2);
}

const char* pakfire_file_get_abspath(struct pakfire_file* file) {
	return archive_entry_sourcepath(file->entry);
}

const char* pakfire_file_get_path(struct pakfire_file* file) {
	return archive_entry_pathname(file->entry);
}

int pakfire_file_set_path(struct pakfire_file* file, const char* path) {
	char buffer[PATH_MAX];
	int r = 0;

	// Check input
	if (!path)
		return -EINVAL;

	// Strip any leading dots from paths
	if (pakfire_string_startswith(path, "./"))
		path++;

	// Make the path absolute
	r = pakfire_path_absolute(buffer, path);
	if (r < 0)
		return r;

	// Store the path
	archive_entry_set_pathname(file->entry, buffer);

	// Make the absolute path
	r = pakfire_path(file->pakfire, buffer, "%s", buffer);
	if (r < 0)
		return r;

	// Store the abspath
	archive_entry_copy_sourcepath(file->entry, buffer);

	return 0;
}

const char* pakfire_file_get_hardlink(struct pakfire_file* file) {
	return archive_entry_hardlink(file->entry);
}

void pakfire_file_set_hardlink(struct pakfire_file* file, const char* link) {
	archive_entry_set_hardlink(file->entry, link);
}

const char* pakfire_file_get_symlink(struct pakfire_file* file) {
	return archive_entry_symlink(file->entry);
}

void pakfire_file_set_symlink(struct pakfire_file* file, const char* link) {
	archive_entry_set_symlink(file->entry, link);
}

nlink_t pakfire_file_get_nlink(struct pakfire_file* file) {
	return archive_entry_nlink(file->entry);
}

void pakfire_file_set_nlink(struct pakfire_file* file, const nlink_t nlink) {
	archive_entry_set_nlink(file->entry, nlink);
}

ino_t pakfire_file_get_inode(struct pakfire_file* file) {
	return archive_entry_ino64(file->entry);
}

void pakfire_file_set_inode(struct pakfire_file* file, const ino_t ino) {
	archive_entry_set_ino64(file->entry, ino);
}

dev_t pakfire_file_get_dev(struct pakfire_file* file) {
	return archive_entry_dev(file->entry);
}

void pakfire_file_set_dev(struct pakfire_file* file, const dev_t dev) {
	archive_entry_set_dev(file->entry, dev);
}

int pakfire_file_get_type(struct pakfire_file* file) {
	return archive_entry_filetype(file->entry);
}

off_t pakfire_file_get_size(struct pakfire_file* file) {
	return archive_entry_size(file->entry);
}

void pakfire_file_set_size(struct pakfire_file* file, off_t size) {
	archive_entry_set_size(file->entry, size);
}

const char* pakfire_file_get_uname(struct pakfire_file* file) {
	return archive_entry_uname(file->entry);
}

int pakfire_file_set_uname(struct pakfire_file* file, const char* uname) {
	archive_entry_set_uname(file->entry, uname);

	return 0;
}

const char* pakfire_file_get_gname(struct pakfire_file* file) {
	return archive_entry_gname(file->entry);
}

int pakfire_file_set_gname(struct pakfire_file* file, const char* gname) {
	archive_entry_set_gname(file->entry, gname);

	return 0;
}

mode_t pakfire_file_get_mode(struct pakfire_file* file) {
	return archive_entry_mode(file->entry);
}

void pakfire_file_set_mode(struct pakfire_file* file, mode_t mode) {
	archive_entry_set_mode(file->entry, mode);
}

mode_t pakfire_file_get_perms(struct pakfire_file* file) {
	return archive_entry_perm(file->entry);
}

void pakfire_file_set_perms(struct pakfire_file* file, const mode_t perms) {
	archive_entry_set_mode(file->entry, pakfire_file_get_type(file) | perms);
}

int pakfire_file_is_executable(struct pakfire_file* file) {
	return pakfire_file_get_mode(file) & (S_IXUSR|S_IXGRP|S_IXOTH);
}

time_t pakfire_file_get_ctime(struct pakfire_file* file) {
	return archive_entry_ctime(file->entry);
}

void pakfire_file_set_ctime(struct pakfire_file* file, time_t time) {
	archive_entry_set_ctime(file->entry, time, 0);
}

time_t pakfire_file_get_mtime(struct pakfire_file* file) {
	return archive_entry_mtime(file->entry);
}

void pakfire_file_set_mtime(struct pakfire_file* file, time_t time) {
	archive_entry_set_mtime(file->entry, time, 0);
}

int pakfire_file_has_payload(struct pakfire_file* file) {
	// File must be a regular file
	switch (pakfire_file_get_type(file)) {
		case S_IFREG:
			break;

		default:
			return 0;
	}

	// File must be larger than zero
	return pakfire_file_get_size(file) > 0;
}

int pakfire_file_get_checksum(struct pakfire_file* file, const enum pakfire_hash_type type,
		const unsigned char** checksum, size_t* checksum_length) {
	int r;

	// Fetch the checksum
	r = pakfire_hashes_get(&file->hashes, type, checksum, checksum_length);
	if (r < 0) {
		ERROR(file->ctx, "Failed to fetch checksum for %s: %s\n",
				pakfire_file_get_path(file), strerror(-r));
		return r;
	}

	return 0;
}

int pakfire_file_set_checksum(struct pakfire_file* file, const enum pakfire_hash_type type,
		const unsigned char* checksum, const size_t checksum_length) {
	int r;

	// Check inputs
	if (!checksum)
		return -EINVAL;

	// Store the checksum
	r = pakfire_hashes_set(&file->hashes, type, checksum, checksum_length);
	if (r < 0) {
		ERROR(file->ctx, "Failed to set checksum for %s: %s\n",
				pakfire_file_get_path(file), strerror(-r));
		return r;
	}

	return 0;
}

int pakfire_file_has_caps(struct pakfire_file* file) {
	if (file->caps)
		return 1;

	return 0;
}

char* pakfire_file_get_caps(struct pakfire_file* file) {
	char* copy = NULL;
	char* text = NULL;
	ssize_t length = 0;

	if (file->caps) {
		text = cap_to_text(file->caps, &length);
		if (!text) {
			ERROR(file->ctx, "Could not export capabilities: %m\n");
			goto ERROR;
		}

		// libcap is being weird and uses its own allocator so we have to copy the string
		copy = strndup(text, length);
		if (!copy)
			goto ERROR;
	}

ERROR:
	if (text)
		cap_free(text);

	return copy;
}

static int pakfire_file_levels(struct pakfire_file* file) {
	const char* path = pakfire_file_get_path(file);
	if (!path)
		return 0;

	int levels = 0;

	for (const char* p = path; *p; p++) {
		if (*p == '/')
			levels++;
	}

	return levels;
}

int pakfire_file_open(struct pakfire_file* file, int flags) {
	const char* path = NULL;
	int fd = -EBADF;

	// Fetch the absolute path of the file
	path = pakfire_file_get_abspath(file);
	if (!path)
		return -EBADF;

	// Fix the path if we have entered the jail
	if (pakfire_ctx_has_flag(file->ctx, PAKFIRE_CTX_IN_JAIL)) {
		path = pakfire_relpath(file->pakfire, path);
		if (!path)
			return -EBADF;
	}

	// Open the file
	fd = open(path, flags|O_CLOEXEC);
	if (fd < 0) {
		ERROR(file->ctx, "Could not open %s: %m\n", path);
		return -errno;
	}

	return fd;
}

FILE* pakfire_file_fopen(struct pakfire_file* file, const char* mode) {
	const char* path = NULL;
	FILE* f = NULL;

	// Fetch the absolute path of the file
	path = pakfire_file_get_abspath(file);
	if (!path)
		return NULL;

	// Return a file handle
	f = fopen(path, mode);
	if (!f) {
		ERROR(file->ctx, "Could not open file handle for %s: %m\n",
			pakfire_file_get_path(file));
		return NULL;
	}

	return f;
}

/*
	Opens the file and returns a mapping of the payload in memory
*/
static int pakfire_file_mmap(struct pakfire_file* file, char** data, size_t* length) {
	int fd = -EBADF;
	int r;

	// Open the file
	fd = r = pakfire_file_open(file, O_RDONLY);
	if (fd < 0)
		goto ERROR;

	// Map the file
	r = pakfire_mmap(fd, data, length);
	if (r < 0) {
		ERROR(file->ctx, "Could not map %s: %s\n", pakfire_file_get_path(file), strerror(-r));
		goto ERROR;
	}

ERROR:
	if (fd >= 0)
		close(fd);

	return r;
}

int pakfire_file_contains(struct pakfire_file* file, const char* needle, ssize_t length) {
	char* haystack = NULL;
	size_t l = 0;
	int pos;
	int r;

	// Check inputs
	if (!needle)
		return -EINVAL;

	const mode_t mode = pakfire_file_get_mode(file);

	// Only run for regular files
	if (!S_ISREG(mode))
		return 0;

	// If the file is empty it cannot contain anything
	if (!pakfire_file_get_size(file))
		return 0;

	// Map the file into memory
	r = pakfire_file_mmap(file, &haystack, &l);
	if (r < 0)
		goto ERROR;

	// Perform the search
	pos = pakfire_string_search(haystack, l, needle, length);

	// No match found
	if (pos == -1)
		r = 0;

	// Match found
	else if (pos >= 0)
		r = pos;

	// Error
	else if (pos < 0)
		r = pos;

ERROR:
	if (haystack)
		munmap(haystack, l);

	return r;
}

static int pakfire_file_remove(struct pakfire_file* file) {
	int r;

	const char* path    = pakfire_file_get_path(file);
	const char* abspath = pakfire_file_get_abspath(file);

	DEBUG(file->ctx, "Removing %s...\n", path);

	// We cannot delete if we don't have the absolute path
	if (!abspath)
		return -ENOTSUP;

	switch (pakfire_file_get_type(file)) {
		// Call rmdir() for directories
		case S_IFDIR:
			r = rmdir(abspath);
			if (r) {
				switch (errno) {
					// Ignore when the directory is not empty
					case ENOTEMPTY:
						r = 0;
						break;

					// Ignore if the directory didn't exist
					case ENOENT:
						r = 0;
						break;

					// Ignore if the directory changed type
					case ENOTDIR:
						r = 0;
						break;

					// Log anything else
					default:
						ERROR(file->ctx, "Could not remove directory %s: %m\n", path);
						break;
				}
			}
			break;

		// Call unlink() for everything else
		default:
			r = unlink(abspath);
			if (r) {
				switch (errno) {
					// Ignore if the file didn't exist
					case ENOENT:
						r = 0;
						break;

					default:
						ERROR(file->ctx, "Could not remove %s: %m\n", path);
						break;
				}
			}
			break;
	}

	return r;
}

int pakfire_file_symlink_target_exists(struct pakfire_file* file) {
	const mode_t mode = pakfire_file_get_mode(file);

	// Fail if this got called for anything that isn't a symlink
	if (!S_ISLNK(mode))
		return -EINVAL;

	const char* path = pakfire_file_get_abspath(file);

	return pakfire_path_exists(path);
}

/*
	MIME Type
*/

int pakfire_file_detect_mimetype(struct pakfire_file* file) {
	const mode_t mode = pakfire_file_get_mode(file);

	// Only process regular files
	if (!S_ISREG(mode))
		return 0;

	// Skip if MIME type is already set
	if (*file->mimetype)
		return 0;

	// Fetch the magic cookie
	magic_t magic = pakfire_ctx_magic(file->ctx);
	if (!magic)
		return 1;

	const char* path = pakfire_file_get_abspath(file);

	// Check the file
	const char* mimetype = magic_file(magic, path);
	if (!mimetype) {
		ERROR(file->ctx, "Could not classify %s: %s\n",
			pakfire_file_get_path(file), magic_error(magic));
		return 1;
	}

	DEBUG(file->ctx, "Classified %s as %s\n", pakfire_file_get_path(file), mimetype);

	// Store the value
	return pakfire_file_set_mimetype(file, mimetype);
}

const char* pakfire_file_get_mimetype(struct pakfire_file* file) {
	// Return nothing on an empty mimetype
	if (!*file->mimetype)
		return NULL;

	return file->mimetype;
}

int pakfire_file_set_mimetype(
		struct pakfire_file* file, const char* mimetype) {
	// Store the value
	return pakfire_string_set(file->mimetype, mimetype);
}

/*
	Classification
*/

static int pakfire_file_classify_mode(struct pakfire_file* file) {
	const mode_t mode = pakfire_file_get_mode(file);

	// Check for regular files
	if (S_ISREG(mode)) {
		file->class |= PAKFIRE_FILE_REGULAR;

		// Does the file have executable permissions?
		if (mode & (S_IXUSR|S_IXGRP|S_IXOTH))
			file->class |= PAKFIRE_FILE_EXECUTABLE;

	// Check for directories
	} else if (S_ISDIR(mode))
		file->class |= PAKFIRE_FILE_DIRECTORY;

	// Check for symlinks
	else if (S_ISLNK(mode))
		file->class |= PAKFIRE_FILE_SYMLINK;

	// Check for character devices
	else if (S_ISCHR(mode))
		file->class |= PAKFIRE_FILE_CHARACTER;

	// Check for block devices
	else if (S_ISBLK(mode))
		file->class |= PAKFIRE_FILE_BLOCK;

	// Check for FIFO pipes
	else if (S_ISFIFO(mode))
		file->class |= PAKFIRE_FILE_FIFO;

	// Check for sockets
	else if (S_ISSOCK(mode))
		file->class |= PAKFIRE_FILE_SOCKET;

	return 0;
}

static const struct pattern {
	const char* pattern;
	int class;
} patterns[] = {
	{ "**.pm", PAKFIRE_FILE_PERL },
	{ NULL },
};

static int pakfire_file_classify_pattern(struct pakfire_file* file) {
	for (const struct pattern* p = patterns; p->pattern; p++) {
		if (pakfire_file_matches(file, p->pattern)) {
			file->class |= p->class;
			break;
		}
	}

	return 0;
}

static const struct mimetype {
	const char* mimetype;
	int class;
} mimetypes[] = {
 	{ "text/x-perl", PAKFIRE_FILE_PERL },
	{ NULL, 0 },
};

static int pakfire_file_classify_magic(struct pakfire_file* file) {
	int r;

	// Detect the MIME type
	r = pakfire_file_detect_mimetype(file);
	if (r)
		return r;

	// Fetch the MIME type
	const char* mimetype = pakfire_file_get_mimetype(file);
	if (!mimetype)
		return 1;

	// Match the MIME type with a flag
	for (const struct mimetype* m = mimetypes; m->mimetype; m++) {
		if (strcmp(m->mimetype, mimetype) == 0) {
			file->class |= m->class;
			break;
		}
	}

	return 0;
}

static int pakfire_file_classify_elf(struct pakfire_file* file) {
	struct pakfire_elf* elf = NULL;
	int r;

	// Don't run this if we already know that file is an ELF file
	if (file->class & PAKFIRE_FILE_ELF)
		return 0;

	// Try to open the file as ELF
	r = pakfire_elf_open_file(&elf, file->ctx, file);
	if (r < 0) {
		switch (-r) {
			// This is not an ELF file
			case ENOTSUP:
				return 0;

			default:
				goto ERROR;
		}
	}

	// Mark this file as an ELF file
	file->class |= PAKFIRE_FILE_ELF;

ERROR:
	if (elf)
		pakfire_elf_unref(elf);

	return r;
}

int pakfire_file_classify(struct pakfire_file* file) {
	int r;

	if (!file->class) {
		// First, check the mode so that we won't run magic on directories, symlinks, ...
		r = pakfire_file_classify_mode(file);
		if (r)
			goto ERROR;

		// Only run this for regular files
		if (file->class & PAKFIRE_FILE_REGULAR) {
			// Then check for patterns
			r = pakfire_file_classify_pattern(file);
			if (r)
				goto ERROR;

			// After that, we will use libmagic...
			r = pakfire_file_classify_magic(file);
			if (r)
				goto ERROR;

			// Check if the file is an ELF file
			r = pakfire_file_classify_elf(file);
			if (r)
				goto ERROR;
		}
	}

	return file->class;

ERROR:
	// Reset the class
	file->class = PAKFIRE_FILE_UNKNOWN;

	return r;
}

int pakfire_file_matches_class(struct pakfire_file* file, const int class) {
	return pakfire_file_classify(file) & class;
}

/*
	This function tries to remove the file after it has been packaged.

	It will try to delete any parent directories as well and ignore if directories
	cannot be deleted because they might contain other files
*/
int pakfire_file_cleanup(struct pakfire_file* file, int flags) {
	char path[PATH_MAX];

	// Try removing the file
	int r = pakfire_file_remove(file);
	if (r)
		return r;

	// Try to tidy up afterwards
	if (flags & PAKFIRE_FILE_CLEANUP_TIDY) {
		const char* abspath = pakfire_file_get_abspath(file);

		// Create a working copy of abspath
		r = pakfire_string_set(path, abspath);
		if (r)
			return r;

		// See how many levels this file has
		int levels = pakfire_file_levels(file);

		// Walk all the way up and remove all parent directories if possible
		while (--levels) {
			dirname(path);

			// Break if path is suddenly empty
			if (!*path)
				break;

			DEBUG(file->ctx, "Trying to remove parent directory %s\n", path);

			r = rmdir(path);

			// Break on any error
			if (r)
				break;
		}
	}

	return 0;
}

static int pakfire_file_verify_mode(struct pakfire_file* file, const struct stat* st) {
	const mode_t type = pakfire_file_get_type(file);

	// Did the type change?
	if (type != (st->st_mode & S_IFMT)) {
		file->verify_status |= PAKFIRE_FILE_TYPE_CHANGED;

		DEBUG(file->ctx, "%s: File Type changed\n", pakfire_file_get_path(file));
	}

	const mode_t perms = pakfire_file_get_perms(file);

	// Check permissions
	if (perms != (st->st_mode & 0777)) {
		file->verify_status |= PAKFIRE_FILE_PERMISSIONS_CHANGED;

		DEBUG(file->ctx, "%s: Permissions changed\n", pakfire_file_get_path(file));
	}

#if 0
	// XXX This does not check what it is supposed to check

	// Check if device node changed
	if (S_ISCHR(st->st_mode) || S_ISBLK(st->st_mode)) {
		const dev_t dev = pakfire_file_get_dev(file);

		if (dev != st->st_dev) {
			file->verify_status |= PAKFIRE_FILE_DEV_CHANGED;

			DEBUG(file->ctx, "%s: Device Node changed\n", pakfire_file_get_path(file));
		}
	}
#endif

	return 0;
}

static int pakfire_file_verify_size(struct pakfire_file* file, const struct stat* st) {
	const ssize_t size = pakfire_file_get_size(file);

	// Nothing to do if size matches
	if (size == st->st_size)
		return 0;

	// Size differs
	file->verify_status |= PAKFIRE_FILE_SIZE_CHANGED;

	DEBUG(file->ctx, "%s: Filesize differs (expected %zd, got %zd byte(s))\n",
		pakfire_file_get_path(file), size, st->st_size);

	return 0;
}

static int pakfire_file_verify_ownership(struct pakfire_file* file, const struct stat* st) {
	// Fetch UID/GID
#if 0
	const uid_t uid = pakfire_unmap_id(file->pakfire, st->st_uid);
	const gid_t gid = pakfire_unmap_id(file->pakfire, st->st_gid);
#else
	const uid_t uid = st->st_uid;
	const gid_t gid = st->st_gid;
#endif

	const char* uname = pakfire_file_get_uname(file);
	const char* gname = pakfire_file_get_gname(file);

	// Fetch owner & group
	struct passwd* owner = pakfire_getpwnam(file->pakfire, uname);
	struct group*  group = pakfire_getgrnam(file->pakfire, gname);

	// Check if owner matches
	if (!owner || owner->pw_uid != uid) {
		file->verify_status |= PAKFIRE_FILE_OWNER_CHANGED;

		DEBUG(file->ctx, "%s: Owner differs\n", pakfire_file_get_path(file));
	}

	// Check if group matches
	if (!group || group->gr_gid != gid) {
		file->verify_status |= PAKFIRE_FILE_GROUP_CHANGED;

		DEBUG(file->ctx, "%s: Group differs\n", pakfire_file_get_path(file));
	}

	return 0;
}

static int pakfire_file_verify_timestamps(struct pakfire_file* file, const struct stat* st) {
	const time_t ctime = pakfire_file_get_ctime(file);
	const time_t mtime = pakfire_file_get_mtime(file);

	// Check creation time
	if (ctime != st->st_ctime) {
		file->verify_status |= PAKFIRE_FILE_CTIME_CHANGED;

		DEBUG(file->ctx, "%s: Creation time changed\n", pakfire_file_get_path(file));
	}

	// Check modification time
	if (mtime != st->st_mtime) {
		file->verify_status |= PAKFIRE_FILE_MTIME_CHANGED;

		DEBUG(file->ctx, "%s: Modification time changed\n", pakfire_file_get_path(file));
	}

	return 0;
}

static int pakfire_file_verify_payload(struct pakfire_file* file, const struct stat* st) {
	struct pakfire_hashes computed_hashes = {};
	int r;

	// Nothing to do for anything that isn't a regular file
	if (!S_ISREG(st->st_mode))
		return 0;

	// Fast-path if size changed. The payload will have changed, too
	if (file->verify_status & PAKFIRE_FILE_SIZE_CHANGED) {
		file->verify_status |= PAKFIRE_FILE_PAYLOAD_CHANGED;
		return 0;
	}

	// Fail if the file has no hashes
	if (!file->hashes.types) {
		ERROR(file->ctx, "%s: No checksums available\n", pakfire_file_get_path(file));
		return 0;
	}

	// Compute digests
	r = pakfire_file_compute_hashes(file, file->hashes.types, &computed_hashes);
	if (r < 0)
		return r;

	// Compare hashes
	r = pakfire_hashes_compare(file->ctx, &file->hashes, &computed_hashes);
	if (r) {
		file->verify_status |= PAKFIRE_FILE_PAYLOAD_CHANGED;

		DEBUG(file->ctx, "%s: Checksum(s) do not match\n", pakfire_file_get_path(file));
	}

	return 0;
}

/*
	Verify the file - i.e. does the metadata match what is on disk?
*/
int pakfire_file_verify(struct pakfire_file* file, int* status) {
	struct stat st;
	int r;

	DEBUG(file->ctx, "Verifying %s...\n", pakfire_file_get_path(file));

	const char* abspath = pakfire_file_get_abspath(file);

	// stat() the file
	r = lstat(abspath, &st);
	if (r) {
		// File does not exist
		if (errno == ENOENT) {
			file->verify_status |= PAKFIRE_FILE_NOENT;
			return 1;
		}

		// Raise any other errors from stat()
		return r;
	}

	// Verify mode
	r = pakfire_file_verify_mode(file, &st);
	if (r)
		return r;

	// Verify size
	r = pakfire_file_verify_size(file, &st);
	if (r)
		return r;

	// Verify ownership
	r = pakfire_file_verify_ownership(file, &st);
	if (r)
		return r;

	// Verify timestamps
	r = pakfire_file_verify_timestamps(file, &st);
	if (r)
		return r;

	// Verify payload
	r = pakfire_file_verify_payload(file, &st);
	if (r)
		return r;

	return 0;
}

int pakfire_file_matches(struct pakfire_file* file, const char* pattern) {
	// Don't match on no pattern
	if (!pattern)
		return 0;

	const char* path = pakfire_file_get_path(file);

	return pakfire_path_match(pattern, path);
}

static int pakfire_file_fconsume(struct pakfire_file* file, FILE* src) {
	FILE* dst = NULL;
	int fd = -EBADF;
	int r;

	// Use sendfile if we have a file descriptor
	fd = fileno(src);
	if (fd >= 0)
		return pakfire_file_consume(file, fd);

	// Rewind the file
	r = pakfire_rewind(src);
	if (r < 0) {
		ERROR(file->ctx, "Could not rewind file descriptor: %m\n");
		r = -errno;
		goto ERROR;
	}

	// Open the file for writing
	dst = pakfire_file_fopen(file, "w");
	if (!dst)
		goto ERROR;

	// Copy the content
	r = pakfire_copy(file->ctx, src, dst);
	if (r < 0) {
		ERROR(file->ctx, "Could not consume to %s: %m\n", pakfire_file_get_path(file));
		goto ERROR;
	}

ERROR:
	if (dst)
		fclose(dst);

	return r;
}

int pakfire_file_consume(struct pakfire_file* file, int srcfd) {
	ssize_t bytes_written = 0;
	struct stat st = {};
	int fd = -EBADF;
	int r;

	// Stat the source
	r = fstat(srcfd, &st);
	if (r < 0) {
		ERROR(file->ctx, "Could not stat the source file descriptor: %m\n");
		r = -errno;
		goto ERROR;
	}

	// Open the file for writing
	fd = r = pakfire_file_open(file, O_WRONLY|O_TRUNC);
	if (r < 0)
		goto ERROR;

	// Write everything to the file
	bytes_written = sendfile(fd, srcfd, 0, st.st_size);
	if (bytes_written < st.st_size) {
		ERROR(file->ctx, "Could not sendfile() to %s: %m\n", pakfire_file_get_path(file));
		r = -errno;
		goto ERROR;
	}

	DEBUG(file->ctx, "Wrote %zd byte(s) to %s\n", bytes_written, pakfire_file_get_abspath(file));

	// Reset r
	r = 0;

ERROR:
	if (fd >= 0)
		close(fd);

	return r;
}

int pakfire_file_fix_interpreter(struct pakfire_file* file) {
	const char* interpreter = NULL;
	char* buffer = NULL;
	FILE* f = NULL;
	FILE* b = NULL;
	size_t l = 0;
	int r;

	char* line = NULL;
	size_t length = 0;
	char* p = NULL;

	char command[PATH_MAX];
	const char* args = NULL;

	const char* path = pakfire_file_get_path(file);

	// Create a place where we can write all the data to
	b = open_memstream(&buffer, &l);
	if (!b) {
		ERROR(file->ctx, "Could not open a memory buffer: %m\n");
		r = -errno;
		goto ERROR;
	}

	// Open the file
	f = pakfire_file_fopen(file, "r");
	if (!f) {
		ERROR(file->ctx, "Could not open %s: %m\n", path);
		r = -errno;
		goto ERROR;
	}

	for (unsigned int lineno = 0;; lineno++) {
		r = getline(&line, &length, f);
		if (r < 0) {
			// We finished reading when we reach the end
			if (feof(f))
				break;

			ERROR(file->ctx, "Could not read line from %s: %m\n", path);
			r = -errno;
			goto ERROR;
		}

		switch (lineno) {
			case 0:
				// Check if the first line starts with #!
				if (!pakfire_string_startswith(line, "#!")) {
					r = 0;
					goto ERROR;
				}

				// Remove the trailing newline
				pakfire_string_rstrip(line);

				// Start at the beginning of the line (after shebang)
				p = line + 2;

				// Consume any whitespace
				while (*p && isspace(*p))
					p++;

				// Check if the interpreter is /usr/bin/env
				if (!pakfire_string_startswith(p, "/usr/bin/env")) {
					r = 0;
					goto ERROR;
				}

				// Consume the old interpreter
				while (*p && !isspace(*p))
					p++;

				// Consume any whitespace
				while (*p && isspace(*p))
					p++;

				// The actual interpreter begins here
				interpreter = p;

				// Find the end of the interpreter
				while (*p && !isspace(*p))
					p++;

				// Terminate the interpreter
				*p++ = '\0';

				// Any arguments begin here (if we didn't consume the entire string, yet)
				if (*p)
					args = p;

				DEBUG(file->ctx, "%s: Found command %s (%s)\n", path, interpreter, args);

				// Find the real path
				r = pakfire_which(file->pakfire, command, interpreter);
				if (r < 0) {
					ERROR(file->ctx, "%s: Could not resolve %s: %m\n", path, interpreter);
					goto ERROR;
				}

				// If we could not resolve the command, this file has an invalid interpreter
				if (!*command) {
					ERROR(file->ctx, "%s: Could not find path for command %s\n", path, interpreter);
					r = -ENOENT;
					goto ERROR;
				}

				// Write the new first line to the buffer
				r = fprintf(b, "#!%s", command);
				if (r < 0)
					goto ERROR;

				// Append arguments if any
				if (args) {
					r = fprintf(b, " %s", args);
					if (r < 0)
						goto ERROR;
				}

				// Terminate the line
				r = fprintf(b, "\n");
				if (r < 0)
					goto ERROR;

				// Process the next line
				break;

			default:
				// Append the line to the buffer
				r = fprintf(b, "%s", line);
				if (r < 0)
					goto ERROR;

				break;
		}
	}

	// Write back the new content
	r = pakfire_file_fconsume(file, b);
	if (r < 0)
		goto ERROR;

ERROR:
	if (b)
		fclose(b);
	if (buffer)
		free(buffer);
	if (line)
		free(line);
	if (f)
		fclose(f);

	return r;
}
