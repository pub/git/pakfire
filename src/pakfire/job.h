/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2024 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_JOB_H
#define PAKFIRE_JOB_H

#include <pakfire/ctx.h>
#include <pakfire/daemon.h>

struct pakfire_job;

int pakfire_job_create(struct pakfire_job** worker,
	struct pakfire_ctx* ctx, struct pakfire_daemon* daemon, json_object* data);

struct pakfire_job* pakfire_job_ref(struct pakfire_job* worker);
struct pakfire_job* pakfire_job_unref(struct pakfire_job* worker);

// ID
int pakfire_job_has_id(struct pakfire_job* job, const char* id);

// Launch
int pakfire_job_launch(struct pakfire_job* job);

// Terminate
int pakfire_job_terminate(struct pakfire_job* worker, int signal);

// Log Stream
int pakfire_job_stream_logs(struct pakfire_job* self);

// Message Received
int pakfire_job_handle_message(struct pakfire_job* self, struct json_object* message);

#endif /* PAKFIRE_JOB_H */
