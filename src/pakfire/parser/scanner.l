/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2019 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

%option bison-bridge
%option noinput noyywrap yylineno
%option nodefault
%option reentrant
%option stack noyy_top_state
%option warn
%option extra-type="struct pakfire_parser_state*"

/* Enable to debug the scanner */
/* %option debug */

%{
#define YY_DECL int yylex (YYSTYPE* yylval_param, yyscan_t yyscanner)

#include <ctype.h>
#include <stdint.h>
#include <stdlib.h>

#include <pakfire/parser.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

#include "grammar.h"

static char* unquote(const char* input) {
	char* output = NULL;

	// Do nothing if there is no input
	if (!input)
		return NULL;

	// Copy the string
	output = strdup(input);
	if (!output)
		return NULL;

	// Unquote the string
	pakfire_string_unquote(output);

	return output;
}

// Forward declaration
YY_DECL;

#define unput_string(s) for (int i = strlen(s) - 1; i >= 0; i--) unput(s[i])

static char* copy_string(const char* s) {
	char* buffer = NULL;

	// Return NULL on NULL input
	if (!s)
		return NULL;

	// Copy the string
	buffer = strdup(s);

	// Fail if we could not copy the string
	if (!buffer)
		return NULL;

	// Strip any leading or trailing whitespace
	pakfire_string_strip(buffer);

	// Remove any linebreaks
	pakfire_string_remove_linebreaks(buffer);

	return buffer;
}

%}

%x INDENT
%x NOKEYWORD
%x READLINE
%x READLINES

letter					[A-Za-z]
digit					[0-9]
underscore				_
whitespace				[ \t]+
quoted_string			\"([^\"])*\"

key						({letter}|{underscore})(({letter}|{digit}|{underscore})*({letter}|{digit}))?
named_key				{key}:{key}

keys					({key}|{named_key})

subparsers				(build|dependencies|distribution|packages?|quality\-agent)

named_subparsers		(template|package)

define					def(ine)?
scriptlet				script(let)?

%%

<*>\n					{
							// Handles line numbers without consuming the newline character
							yyextra->lineno++; REJECT;
						}

<*>#.*$					{
							/* ignore comments */
						}

\n						{
							// Jump back into indentation processing after a newline
							yyextra->current_indent = 0;

							yy_push_state(INDENT, yyscanner);

							return T_EOL;
						}

{whitespace} 			{ /* consume any whitespace */ }

<NOKEYWORD>{keys}$		{
							// Leave NOKEYWORD immediately again
							yy_pop_state(yyscanner);

							// Copy the value
							yylval->string = strdup(yytext);

							// Enter into READLINES mode
							yy_push_state(READLINES, yyscanner);

							return T_KEY;
						}

<NOKEYWORD>{keys}		{
							// Leave NOKEYWORD immediately again
							yy_pop_state(yyscanner);

							// Copy the value
							yylval->string = strdup(yytext);

							return T_KEY;
						}

<NOKEYWORD>.			{
							// Unexpected character
							fprintf(stderr, "Unexpected character: %s\n", yytext);
							abort();
						}

^{subparsers}$			{
							yylval->string = strdup(yytext);

							return T_SUBPARSER;
						}

{named_subparsers}		{
							yy_push_state(READLINE, yyscanner);

							yylval->string = strdup(yytext);
							return T_SUBPARSER;
						}

{define}				{
							// Just ignore
						}

{scriptlet}{whitespace}	{
							unput_string("scriptlet:");
						}

{quoted_string}			{
							// Remove quotes
							yylval->string = unquote(yytext);

							return T_STRING;
						}

"end"					{ return T_END; }

"if"					{ return T_IF; }
"else"					{ return T_ELSE; }

"export"				{ return T_EXPORT; }

"=="					{ return T_EQUALS; }

"="						{
							// Read everything after this
							yy_push_state(READLINE, yyscanner);

							return T_ASSIGN;
						}

"+="					{
							// Read everything after this
							yy_push_state(READLINE, yyscanner);

							return T_APPEND;
						}

.						{
							// Do not consume input
							yyless(0);

							// None of the keywords have matched
							yy_push_state(NOKEYWORD, yyscanner);
						}

<READLINE>.*\\\n		{
							// Continue if a line ends with a "\"
							yymore();
						}

<READLINE>.*$			{
							// Return to caller
							yy_pop_state(yyscanner);

							// Copy the entire string
							yylval->string = copy_string(yytext);

							return T_STRING;
						}

<READLINES>\t			{
							if (yyextra->readline_indent &&
									yyextra->current_indent >= yyextra->readline_indent)
								REJECT;

							yyextra->current_indent++;
						}
<READLINES>\n			{
							// Jump back into indentation processing after a newline
							yyextra->current_indent = 0;

							return T_EOL;
						}
<READLINES>.			{
							yyless(0);

							// This is the first line
							if (!yyextra->readline_indent) {
								// If indentation is above indent level,
								// we are good to continue
								if (yyextra->current_indent > yyextra->indent_level) {
									yyextra->readline_indent = yyextra->current_indent;
									return T_INDENT;

								// If we found the same or less indentation we go back
								// to the previous state and continue parsing there
								} else {
									yy_pop_state(yyscanner);
								}
							}

							// <-- ?
							else if (yyextra->current_indent < yyextra->readline_indent) {
								yyextra->readline_indent = 0;
								yy_pop_state(yyscanner);
								return T_OUTDENT;

							// The indentation stayed the same, read the next line
							} else {
								yy_push_state(READLINE, yyscanner);
							}
						}

<INDENT>\t				{ yyextra->current_indent++; }
<INDENT>\n				{ yyextra->current_indent = 0; }
<INDENT>.				{
							// Put the read character back for the next round
							yyless(0);

							// Mark beginning of line to match ^
							if (yyextra->current_indent == 0)
								yy_set_bol(1);

							// --> - More indentation?
							if (yyextra->current_indent > yyextra->indent_level) {
								yyextra->indent_level++;
								yy_pop_state(yyscanner);

								return T_INDENT;

							// <-- - If indentation has become less
							} else if (yyextra->current_indent < yyextra->indent_level) {
								yyextra->indent_level--;
								yy_pop_state(yyscanner);

								return T_OUTDENT;

							// If indentation has stayed the same
							} else {
								yy_pop_state(yyscanner);
							}
						}

%%
