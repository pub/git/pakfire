/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2024 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_HEX_H
#define PAKFIRE_HEX_H

#define pakfire_hexlify(digest) __pakfire_hexlify(digest, sizeof(digest))

char* __pakfire_hexlify(const unsigned char* digest, const size_t length);

#define pakfire_unhexlify(dst, src) __pakfire_unhexlify(dst, sizeof(dst), src)

int __pakfire_unhexlify(unsigned char* dst, const size_t l, const char* src);

#endif /* PAKFIRE_HEX_H */
