/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2025 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_HASHES_H
#define PAKFIRE_HASHES_H

#include <openssl/sha.h>

// Pakfire knows these hashes
enum pakfire_hash_type {
	PAKFIRE_HASH_UNDEFINED  = 0,
	PAKFIRE_HASH_SHA2_256   = (1 << 0),
	PAKFIRE_HASH_SHA2_512   = (1 << 1),
	PAKFIRE_HASH_BLAKE2S256 = (1 << 2),
	PAKFIRE_HASH_BLAKE2B512 = (1 << 3),
	PAKFIRE_HASH_SHA3_256   = (1 << 4),
	PAKFIRE_HASH_SHA3_512   = (1 << 5),
};

#define PAKFIRE_HASHES_ALL ( \
	PAKFIRE_HASH_SHA2_256 \
	| PAKFIRE_HASH_SHA2_512 \
	| PAKFIRE_HASH_BLAKE2S256 \
	| PAKFIRE_HASH_BLAKE2B512 \
	| PAKFIRE_HASH_SHA3_256 \
	| PAKFIRE_HASH_SHA3_512 \
)

#define PAKFIRE_HASHES_FOREACH(hash) \
	for (hash = 1; hash & PAKFIRE_HASHES_ALL; hash <<= 1)

// Define BLAKE2's digest lengths
#ifndef BLAKE2S256_DIGEST_LENGTH
#  define BLAKE2S256_DIGEST_LENGTH 32
#endif /* BLAKE2S256_DIGEST_LENGTH */

#ifndef BLAKE2B512_DIGEST_LENGTH
#  define BLAKE2B512_DIGEST_LENGTH 64
#endif /* BLAKE2B512_DIGEST_LENGTH */

struct pakfire_hashes {
	// The set hashes
	enum pakfire_hash_type types;

	// SHA-3-512
	unsigned char sha3_512[SHA512_DIGEST_LENGTH];

	// SHA-3-256
	unsigned char sha3_256[SHA256_DIGEST_LENGTH];

	// BLAKE2b512
	unsigned char blake2b512[BLAKE2B512_DIGEST_LENGTH];

	// BLAKE2s256
	unsigned char blake2s256[BLAKE2S256_DIGEST_LENGTH];

	// SHA2-512
	unsigned char sha2_512[SHA512_DIGEST_LENGTH];

	// SHA2-256
	unsigned char sha2_256[SHA256_DIGEST_LENGTH];
};

#include <pakfire/ctx.h>

const char* pakfire_hash_name(const enum pakfire_hash_type hash);
enum pakfire_hash_type pakfire_hash_by_name(const char* hash);

void pakfire_hashes_reset(struct pakfire_hashes* hashes);
int pakfire_hashes_import(struct pakfire_hashes* dst, const struct pakfire_hashes* src);

int pakfire_hashes_has(const struct pakfire_hashes* hashes, const enum pakfire_hash_type type);

int pakfire_hashes_get(const struct pakfire_hashes* hashes,
	const enum pakfire_hash_type type, const unsigned char** hash, size_t* length);
int pakfire_hashes_set(struct pakfire_hashes* hashes,
	const enum pakfire_hash_type type, const unsigned char* hash, const size_t length);

int pakfire_hashes_get_hex(const struct pakfire_hashes* hashes,
	const enum pakfire_hash_type type, char** hexdigest);
int pakfire_hashes_set_hex(struct pakfire_hashes* hashes,
	const enum pakfire_hash_type type, const char* hexdigest);

int pakfire_hashes_dump(struct pakfire_ctx* ctx, const struct pakfire_hashes* hashes, int level);

int pakfire_hashes_compare(struct pakfire_ctx* ctx,
	const struct pakfire_hashes* hashes1, const struct pakfire_hashes* hashes2);

#endif /* PAKFIRE_HASHES_H */
