/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2024 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

// OpenSSL
#include <openssl/bio.h>
#include <openssl/err.h>
#include <openssl/evp.h>

#include <pakfire/ctx.h>
#include <pakfire/base64.h>
#include <pakfire/util.h>

#define OPENSSL_ERROR_MAX 1024

int pakfire_b64encode(struct pakfire_ctx* ctx, char** output,
		const void* buffer, const size_t length) {
	char error[OPENSSL_ERROR_MAX];
	BIO* b64 = NULL;
	BIO* bio = NULL;
	const char* p = NULL;
	int r;

	// Initialize the base64 encoder
	b64 = BIO_new(BIO_f_base64());
	if (!b64) {
		ERR_error_string_n(ERR_get_error(), error, sizeof(error));

		ERROR(ctx, "Could not initialize the base64 encoder: %s\n", error);
		r = 1;
		goto ERROR;
	}

	// Initialize a memory buffer
	bio = BIO_new(BIO_s_mem());
	if (!bio) {
		ERR_error_string_n(ERR_get_error(), error, sizeof(error));

		ERROR(ctx, "Could not initialize memory buffer: %s\n", error);
		r = 1;
		goto ERROR;
	}

	// Connect both things
	bio = BIO_push(b64, bio);

	// Disable line breaks and a trailing newline
	BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);

	// Write the input
	r = BIO_write(bio, buffer, length);
	if (r < 1) {
		ERR_error_string_n(ERR_get_error(), error, sizeof(error));

		ERROR(ctx, "%s\n", error);
		r = 1;
		goto ERROR;
	}

	// Flush
	BIO_flush(bio);

	// Fetch a pointer to the output and determine its length
	const size_t l = BIO_get_mem_data(bio, &p);

	// Copy the output to the heap
	*output = strndup(p, l);
	if (!*output) {
		ERROR(ctx, "Could not copy base64 encoded string to heap: %m\n");
		r = 1;
		goto ERROR;
	}

	// Success
	r = 0;

ERROR:
	if (bio)
		BIO_free_all(bio);

	return r;
}

int pakfire_b64decode(struct pakfire_ctx* ctx, void** output, size_t* length,
		const char* buffer) {
	char error[OPENSSL_ERROR_MAX];
	char chunk[1024];
	BIO* b64 = NULL;
	BIO* bio = NULL;
	char* p = NULL;
	int r;

	// Reset length
	*length = 0;

	// Initialize the base64 decoder
	b64 = BIO_new(BIO_f_base64());
	if (!b64) {
		ERR_error_string_n(ERR_get_error(), error, sizeof(error));

		ERROR(ctx, "Could not initialize the base64 decoder: %s\n", error);
		r = 1;
		goto ERROR;
	}

	BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);

	// Initialize a memory buffer
	bio = BIO_new_mem_buf(buffer, strlen(buffer));
	if (!bio) {
		ERR_error_string_n(ERR_get_error(), error, sizeof(error));

		ERROR(ctx, "Could not initialize memory buffer: %s\n", error);
		r = 1;
		goto ERROR;
	}

	// Connect both things
	bio = BIO_push(b64, bio);

	for (;;) {
		// Read a chunk of data
		ssize_t bytes_read = BIO_read(bio, chunk, sizeof(chunk));

		// Handle any errors
		if (bytes_read < 0) {
			ERR_error_string_n(ERR_get_error(), error, sizeof(error));

			ERROR(ctx, "Could not read data: %s\n", error);
			r = -EINVAL;
			goto ERROR;

		// Break if no more data could be read
		} else if (bytes_read == 0) {
			break;

		// Handle the chunk
		} else {
			// Update total length of data
			*length += bytes_read;

			// Allocate an output buffer
			p = pakfire_realloc(p, *length);
			if (!p) {
				ERROR(ctx, "Could not allocate buffer: %m\n");
				r = -errno;
				goto ERROR;
			}

			// Copy the chunk
			memcpy(p + *length - bytes_read, chunk, bytes_read);
		}
	}

	// Set output pointer
	*output = p;

	// Success!
	r = 0;

	goto CLEANUP;

ERROR:
	if (p)
		free(p);

CLEANUP:
	if (bio)
		BIO_free_all(bio);

	return r;
}
