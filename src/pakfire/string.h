/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2022 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_STRING_H
#define PAKFIRE_STRING_H

#include <ctype.h>
#include <errno.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/*
	Formats a string and stores it in the given buffer.
*/
#define pakfire_string_format(s, format, ...) \
	__pakfire_string_format(s, sizeof(s), format, __VA_ARGS__)
int __pakfire_string_format(char* s, const size_t length,
	const char* format, ...) __attribute__((format(printf, 3, 4)));

#define pakfire_string_vformat(s, format, args) \
	__pakfire_string_vformat(s, sizeof(s), format, args)
int __pakfire_string_vformat(char* s, const size_t length,
	const char* format, va_list args) __attribute__((format(printf, 3, 0)));

/*
	Simpler version when a string needs to be copied.
*/
#define pakfire_string_set(s, value) \
	__pakfire_string_set(s, sizeof(s), value)
int __pakfire_string_set(char* s, const size_t length, const char* value);

#define pakfire_string_setn(s, value, l) \
	__pakfire_string_setn(s, sizeof(s), value, l)
int __pakfire_string_setn(char* s, const size_t length, const char* value, const size_t l);

int pakfire_string_startswith(const char* s, const char* prefix);
int pakfire_string_endswith(const char* s, const char* suffix);
int pakfire_string_matches(const char* s, const char* pattern);

void pakfire_string_truncate(char* s, const size_t l);

// Strip
void pakfire_string_lstrip(char* s);
void pakfire_string_rstrip(char* s);
void pakfire_string_strip(char* s);
void pakfire_string_unquote(char* s);

int pakfire_string_partition(const char* s, const char* delim, char** s1, char** s2);
char* pakfire_string_replace(const char* s, const char* pattern, const char* repl);
void pakfire_string_remove_linebreaks(char* src);
char* pakfire_string_join(const char** list, const char* delim);

inline int pakfire_string_equals(const char* s1, const char* s2) {
	return strcmp(s1, s2) == 0;
}

/*
	Simple operations, usually used in the linter...
*/
static inline int pakfire_string_contains_whitespace(const char* s) {
	while (s && *s) {
		if (isspace(*s))
			return 1;

		s++;
	}

	return 0;
}

#define pakfire_string_append(s, appendix) \
	__pakfire_string_append(s, sizeof(s), appendix)
int __pakfire_string_append(char* buffer, size_t length, const char* appendix);

#define pakfire_string_appendf(s, format, ...) \
	__pakfire_string_appendf(s, sizeof(s), format, __VA_ARGS__)
int __pakfire_string_appendf(char* buffer, size_t length, const char* format, ...)
	__attribute__((format(printf, 3, 4)));

int pakfire_string_search(const char* haystack, ssize_t lhaystack,
	const char* needle, ssize_t lneedle);

/*
	String Arrays
*/

size_t pakfire_strings_length(char** array);

void pakfire_strings_free(char** array);

int pakfire_strings_contain(char** array, const char* string);

int pakfire_strings_append(char*** array, const char* s);
int pakfire_strings_appendf(char*** array, const char* format, ...)
	__attribute__((format(printf, 2, 3)));
int pakfire_strings_appendm(char*** array, const char** strings);

int pakfire_strings_dump(char** array);

#define TIME_STRING_MAX 32

#define pakfire_format_size(dst, value) \
	__pakfire_format_size(dst, sizeof(dst), value)
int __pakfire_format_size(char* dst, size_t length, double value);

#define pakfire_format_speed(dst, value) \
	__pakfire_format_speed(dst, sizeof(dst), value)
int __pakfire_format_speed(char* dst, size_t length, double value);

#define pakfire_strftime(buffer, format, t) \
	__pakfire_strftime(buffer, sizeof(buffer), format, t)
int __pakfire_strftime(char* buffer, const size_t length,
	const char* format, const time_t t) __attribute__((format(strftime, 3, 0)));

#define pakfire_strftime_now(buffer, format) \
	__pakfire_strftime_now(buffer, sizeof(buffer), format)
int __pakfire_strftime_now(char* buffer, const size_t length, const char* format)
	__attribute__((format(strftime, 3, 0)));

#define pakfire_format_time(buffer, t) \
	__pakfire_format_time(buffer, sizeof(buffer), t)
int __pakfire_format_time(char* buffer, const size_t length, const time_t t);

#define pakfire_timeval_to_iso8601(buffer, t) \
	__pakfire_timeval_to_iso8601(buffer, sizeof(buffer), t)
int __pakfire_timeval_to_iso8601(char* buffer, const size_t length, const struct timeval* t);

size_t pakfire_string_parse_bytes(const char* s);

#define pakfire_string_format_interval(buffer, t) \
	__pakfire_string_format_interval(buffer, sizeof(buffer), t)
int __pakfire_string_format_interval(char* buffer, size_t length, const time_t t);

time_t pakfire_string_parse_interval(const char* buffer);

int pakfire_string_is_url(const char* s);

#endif /* PAKFIRE_STRING_H */
