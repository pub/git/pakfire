/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2025 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

// json-c
#include <json.h>

#include <pakfire/archive_writer.h>
#include <pakfire/ctx.h>
#include <pakfire/hasher.h>
#include <pakfire/hashes.h>
#include <pakfire/i18n.h>
#include <pakfire/json.h>
#include <pakfire/logging.h>
#include <pakfire/oci.h>
#include <pakfire/pakfire.h>
#include <pakfire/path.h>
#include <pakfire/string.h>
#include <pakfire/util.h>
#include <pakfire/xfopen.h>

struct pakfire_oci_writer {
	// Context
	struct pakfire_ctx* ctx;

	// Pakfire
	struct pakfire* pakfire;

	// Image writer
	struct pakfire_archive_writer* image;

	// Diff ID of the base layer
	char* diffid;

	// JSON Objects
	struct json_object* config;
	struct json_object* layers;
	struct json_object* manifests;
};

static const char* pakfire_oci_arch(struct pakfire* pakfire) {
	const char* arch = pakfire_get_arch(pakfire);

	// aarch64
	if (pakfire_string_equals(arch, "aarch64"))
		return "arm64";

	// riscv64
	if (pakfire_string_equals(arch, "riscv64"))
		return "riscv64";

	// x86_64
	if (pakfire_string_equals(arch, "x86_64"))
		return "amd64";

	// Otherwise fall back to the actual architecture
	return arch;
}

static int pakfire_oci_writer_write_oci_layout(struct pakfire_oci_writer* self) {
	struct json_object* o = NULL;
	int r;

	// Make a new object
	o = pakfire_json_new_object();
	if (!o) {
		r = -errno;
		goto ERROR;
	}

	// Set version
	r = pakfire_json_add_string(o, "imageLayoutVersion", "1.0.0");
	if (r < 0)
		goto ERROR;

	// Write the file
	r = pakfire_archive_writer_create_file_from_json(self->image, "oci-layout", 0644, o);
	if (r < 0)
		goto ERROR;

ERROR:
	if (o)
		json_object_put(o);

	return r;
}

static int pakfire_oci_writer_write_json_blob(
		struct pakfire_oci_writer* self, struct json_object* json, char** hexdigest, size_t* size) {
	struct pakfire_hashes hashes = {};
	const char* buffer = NULL;
	char* hd = NULL;
	char path[PATH_MAX];
	int r;

	// Serialize the JSON object
	buffer = json_object_to_json_string_ext(json, 0);
	if (!buffer) {
		r = -errno;
		goto ERROR;
	}

	// Hash the serialized JSON object
	r = pakfire_hash_buffer(self->ctx, buffer, strlen(buffer), PAKFIRE_HASH_SHA2_256, &hashes);
	if (r < 0)
		goto ERROR;

	// Fetch the hexdigest
	r = pakfire_hashes_get_hex(&hashes, PAKFIRE_HASH_SHA2_256, &hd);
	if (r < 0)
		goto ERROR;

	// Make path
	r = pakfire_path_format(path, "blobs/sha256/%s", hd);
	if (r < 0)
		goto ERROR;

	// Write the file
	r = pakfire_archive_writer_create_file(self->image, path, 0644, buffer, strlen(buffer));
	if (r < 0) {
		ERROR(self->ctx, "Failed to write JSON blob %s: %s\n", path, strerror(-r));
		goto ERROR;
	}

	// If requested return the hexdigest
	if (hexdigest)
		*hexdigest = strdup(hd);

	// If requested return the size
	if (size)
		*size = strlen(buffer);

ERROR:
	if (hd)
		free(hd);

	return r;
}

static int pakfire_oci_writer_write_index(struct pakfire_oci_writer* self) {
	struct json_object* o = NULL;
	int r;

	// Make a new object
	o = pakfire_json_new_object();
	if (!o) {
		r = -errno;
		goto ERROR;
	}

	// Set schema version
	r = pakfire_json_add_int64(o, "schemaVersion", 2);
	if (r < 0)
		goto ERROR;

	// Set media type
	r = pakfire_json_add_string(o, "mediaType", "application/vnd.oci.image.index.v1+json");
	if (r < 0)
		goto ERROR;

	// Add manifests
	r = json_object_object_add(o, "manifests", json_object_get(self->manifests));
	if (r < 0)
		goto ERROR;

	// Write the file
	r = pakfire_archive_writer_create_file_from_json(self->image, "index.json", 0644, o);
	if (r < 0)
		goto ERROR;

ERROR:
	if (o)
		json_object_put(o);

	return r;
}

static int pakfire_oci_writer_write_config(struct pakfire_oci_writer* self) {
	struct json_object* o = NULL;
	char* hexdigest = NULL;
	char created[64];
	size_t size = 0;
	int r;

	struct json_object* config = NULL;
	struct json_object* cmd = NULL;
	struct json_object* rootfs = NULL;
	struct json_object* diffids = NULL;

	// Make a new object
	o = pakfire_json_new_object();
	if (!o) {
		r = -errno;
		goto ERROR;
	}

	// Format creation timestamp
	r = pakfire_strftime_now(created, "%Y-%m-%dT%H:%M:%SZ");
	if (r < 0)
		goto ERROR;

	// Add the creation timestamp
	r = pakfire_json_add_string(o, "created", created);
	if (r < 0)
		goto ERROR;

	// Add author
	const char* vendor = pakfire_get_distro_vendor(self->pakfire);
	if (vendor) {
		r = pakfire_json_add_string(o, "author", vendor);
		if (r < 0)
			goto ERROR;
	}

	// Add OS
	r = pakfire_json_add_string(o, "os", "linux");
	if (r < 0)
		goto ERROR;

	// Fetch OS version
	const char* version_id = pakfire_get_distro_version_id(self->pakfire);

	// Add OS version
	if (version_id) {
		r = pakfire_json_add_string(o, "os.version", version_id);
		if (r < 0)
			goto ERROR;
	}

	// Fetch arch
	const char* arch = pakfire_oci_arch(self->pakfire);

	// Add arch
	r = pakfire_json_add_string(o, "architecture", arch);
	if (r < 0)
		goto ERROR;

	// Add config (yes I know)
	r = pakfire_json_add_object(o, "config", &config);
	if (r < 0)
		goto ERROR;

	// Add user
	r = pakfire_json_add_string(config, "User", "root");
	if (r < 0)
		goto ERROR;

	// Add working directory
	r = pakfire_json_add_string(config, "WorkingDir", "/root");
	if (r < 0)
		goto ERROR;

	// Add command
	r = pakfire_json_add_array(config, "Cmd", &cmd);
	if (r < 0)
		goto ERROR;

	// Add bash
	r = pakfire_json_array_add_string(cmd, "bash");
	if (r < 0)
		goto ERROR;

	// Add rootfs
	r = pakfire_json_add_object(o, "rootfs", &rootfs);
	if (r < 0)
		goto ERROR;

	// Add type
	r = pakfire_json_add_string(rootfs, "type", "layers");
	if (r < 0)
		goto ERROR;

	// Add diff IDs
	r = pakfire_json_add_array(rootfs, "diff_ids", &diffids);
	if (r < 0)
		goto ERROR;

	// Add the diff ID of the base layer
	r = pakfire_json_array_add_stringf(diffids, "sha256:%s", self->diffid);
	if (r < 0)
		goto ERROR;

	// Write as blob
	r = pakfire_oci_writer_write_json_blob(self, o, &hexdigest, &size);
	if (r < 0)
		goto ERROR;

	// Add media type
	r = pakfire_json_add_string(self->config, "mediaType", "application/vnd.oci.image.config.v1+json");
	if (r < 0)
		goto ERROR;

	// Add digest
	r = pakfire_json_add_stringf(self->config, "digest", "sha256:%s", hexdigest);
	if (r < 0)
		goto ERROR;

	// Add size
	r = pakfire_json_add_int64(self->config, "size", size);
	if (r < 0)
		goto ERROR;

ERROR:
	if (hexdigest)
		free(hexdigest);
	if (o)
		json_object_put(o);

	return r;
}

static int pakfire_oci_writer_write_manifest(struct pakfire_oci_writer* self) {
	struct json_object* manifest = NULL;
	struct json_object* platform = NULL;
	struct json_object* o = NULL;
	char* hexdigest = NULL;
	size_t size = 0;
	int r;

	// Make a new object
	o = pakfire_json_new_object();
	if (!o) {
		r = -errno;
		goto ERROR;
	}

	// Set schema version
	r = pakfire_json_add_int64(o, "schemaVersion", 2);
	if (r < 0)
		goto ERROR;

	// Set media type
	r = pakfire_json_add_string(o, "mediaType", "application/vnd.oci.image.manifest.v1+json");
	if (r < 0)
		goto ERROR;

	// Add config
	r = json_object_object_add(o, "config", json_object_get(self->config));
	if (r < 0)
		goto ERROR;

	// Add layer to the layers array
	r = json_object_object_add(o, "layers", json_object_get(self->layers));
	if (r < 0)
		goto ERROR;

	// Write as blob
	r = pakfire_oci_writer_write_json_blob(self, o, &hexdigest, &size);
	if (r < 0)
		goto ERROR;

	// Make another new object
	manifest = pakfire_json_new_object();
	if (!manifest) {
		r = -errno;
		goto ERROR;
	}

	// Add media type
	r = pakfire_json_add_string(manifest, "mediaType", "application/vnd.oci.image.manifest.v1+json");
	if (r < 0)
		goto ERROR;

	// Add size
	r = pakfire_json_add_int64(manifest, "size", size);
	if (r < 0)
		goto ERROR;

	// Add digest
	r = pakfire_json_add_stringf(manifest, "digest", "sha256:%s", hexdigest);
	if (r < 0)
		goto ERROR;

	// Add platform
	r = pakfire_json_add_object(manifest, "platform", &platform);
	if (r < 0)
		goto ERROR;

	// Fetch architecture
	const char* arch = pakfire_oci_arch(self->pakfire);

	// Add architecture
	r = pakfire_json_add_string(platform, "architecture", arch);
	if (r < 0)
		goto ERROR;

	// Add OS
	r = pakfire_json_add_string(platform, "os", "linux");
	if (r < 0)
		goto ERROR;

	// Add the manifest to the other manifests
	r = json_object_array_add(self->manifests, json_object_get(manifest));
	if (r < 0) {
		ERROR(self->ctx, "Failed to add the manifest to the metadata: %m\n");
		r = -errno;
		goto ERROR;
	}

ERROR:
	if (manifest)
		json_object_put(manifest);
	if (o)
		json_object_put(o);
	if (hexdigest)
		free(hexdigest);

	return r;
}

static int pakfire_oci_writer_make_layer(struct pakfire_oci_writer* self) {
	char path[PATH_MAX] = PAKFIRE_TMP_DIR "/pakfire-oci-layer.XXXXXX";
	struct pakfire_archive_writer* layer = NULL;
	struct pakfire_hashes hashes = {};
	struct json_object* json = NULL;
	char filename[PATH_MAX];
	char* hexdigest = NULL;
	FILE* f = NULL;
	size_t size = 0;
	int r;

	// Fetch the pakfire root
	const char* root = pakfire_get_path(self->pakfire);

	// Make a new temporary file
	f = pakfire_mktemp(path, 0);
	if (!f) {
		ERROR(self->ctx, "Failed to create a temporary file: %m\n");
		r = -errno;
		goto ERROR;
	}

	// Drop the file straight away
	unlink(path);

	// Make a new archive writer
	r = pakfire_archive_writer_create(&layer, self->pakfire, PAKFIRE_FORMAT_OCI_LAYER, f);
	if (r < 0)
		goto ERROR;

	// Set the title
	r = pakfire_archive_writer_set_title(layer, "%s", _("Writing Layer"));
	if (r < 0)
		goto ERROR;

	// Write all files
	r = pakfire_archive_writer_write_everything(layer, root);
	if (r < 0)
		goto ERROR;

	// Fetch the size
	size = ftell(f);

	// Rewind
	r = pakfire_rewind(f);
	if (r < 0)
		goto ERROR;

	// Compute hashes of the file
	r = pakfire_hash_file(self->ctx, f, PAKFIRE_HASH_SHA2_256, &hashes);
	if (r < 0)
		goto ERROR;

	// Read the hexdigest of the file
	r = pakfire_hashes_get_hex(&hashes, PAKFIRE_HASH_SHA2_256, &hexdigest);
	if (r < 0)
		goto ERROR;

	// Make the filename of the layer
	r = pakfire_path_format(filename, "blobs/sha256/%s", hexdigest);
	if (r < 0)
		goto ERROR;

	// Create a new JSON object
	json = pakfire_json_new_object();
	if (!json) {
		r = -errno;
		goto ERROR;
	}

	// Add the media type
	r = pakfire_json_add_string(json, "mediaType", "application/vnd.oci.image.layer.v1.tar+gzip");
	if (r < 0)
		goto ERROR;

	// Add the digest
	r = pakfire_json_add_stringf(json, "digest", "sha256:%s", hexdigest);
	if (r < 0)
		goto ERROR;

	// Add the size
	r = pakfire_json_add_int64(json, "size", size);
	if (r < 0)
		goto ERROR;

	// Add the layer to the other layers
	r = json_object_array_add(self->layers, json_object_get(json));
	if (r < 0) {
		ERROR(self->ctx, "Failed to add the layer to the metadata: %m\n");
		r = -errno;
		goto ERROR;
	}

	// Rewind
	r = pakfire_rewind(f);
	if (r < 0)
		goto ERROR;

	// Write the layer to the image
	r = pakfire_archive_writer_create_file_from_file(self->image, filename, 0644, f);
	if (r < 0) {
		ERROR(self->ctx, "Failed to add the layer to the image: %s\n", strerror(-r));
		goto ERROR;
	}

	// Rewind
	r = pakfire_rewind(f);
	if (r < 0)
		goto ERROR;

	// Replace the file handle to decompress the payload again
	f = pakfire_gzfopen(f, "r");
	if (!f) {
		ERROR(self->ctx, "Failed to re-open the layer for decompression: %m\n");
		r = -errno;
		goto ERROR;
	}

	// Hash the file
	r = pakfire_hash_file(self->ctx, f, PAKFIRE_HASH_SHA2_256, &hashes);
	if (r < 0)
		goto ERROR;

	// Extract the diff ID
	r = pakfire_hashes_get_hex(&hashes, PAKFIRE_HASH_SHA2_256, &self->diffid);
	if (r < 0)
		goto ERROR;

ERROR:
	if (layer)
		pakfire_archive_writer_unref(layer);
	if (json)
		json_object_put(json);
	if (f)
		fclose(f);

	return r;
}

int pakfire_oci_mkimage(struct pakfire* pakfire, FILE* f) {
	struct pakfire_oci_writer writer = {
		.ctx     = pakfire_ctx(pakfire),
		.pakfire = pakfire,
	};
	int r;

	// Reference to the config
	writer.config = json_object_new_object();
	if (!writer.config) {
		r = -errno;
		goto ERROR;
	}

	// To store all layers
	writer.layers = json_object_new_array();
	if (!writer.layers) {
		r = -errno;
		goto ERROR;
	}

	// To store all manifests
	writer.manifests = json_object_new_array();
	if (!writer.manifests) {
		r = -errno;
		goto ERROR;
	}

	// Make a new archive writer
	r = pakfire_archive_writer_create(&writer.image, pakfire, PAKFIRE_FORMAT_OCI, f);
	if (r < 0)
		goto ERROR;

	// Write oci-layout file
	r = pakfire_oci_writer_write_oci_layout(&writer);
	if (r < 0) {
		ERROR(writer.ctx, "Failed to write the OCI layout file: %s\n", strerror(-r));
		goto ERROR;
	}

	// Make the layer
	r = pakfire_oci_writer_make_layer(&writer);
	if (r < 0) {
		ERROR(writer.ctx, "Failed to write the layer: %s\n", strerror(-r));
		goto ERROR;
	}

	// Write the config
	r = pakfire_oci_writer_write_config(&writer);
	if (r < 0) {
		ERROR(writer.ctx, "Failed to write the config: %s\n", strerror(-r));
		goto ERROR;
	}

	// Write the manifest
	r = pakfire_oci_writer_write_manifest(&writer);
	if (r < 0) {
		ERROR(writer.ctx, "Failed to write the manifest: %s\n", strerror(-r));
		goto ERROR;
	}

	// Write the index
	r = pakfire_oci_writer_write_index(&writer);
	if (r < 0) {
		ERROR(writer.ctx, "Failed to write the index: %s\n", strerror(-r));
		goto ERROR;
	}

ERROR:
	if (writer.manifests)
		json_object_put(writer.manifests);
	if (writer.layers)
		json_object_put(writer.layers);
	if (writer.config)
		json_object_put(writer.config);
	if (writer.image)
		pakfire_archive_writer_unref(writer.image);
	if (writer.ctx)
		pakfire_ctx_unref(writer.ctx);
	if (writer.diffid)
		free(writer.diffid);

	return r;
}
