/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2014 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#define PY_SSIZE_T_CLEAN
#include <Python.h>

#include <pakfire/archive.h>
#include <pakfire/filelist.h>
#include <pakfire/package.h>
#include <pakfire/repo.h>
#include <pakfire/util.h>

#include "archive.h"
#include "archive_file.h"
#include "errors.h"
#include "key.h"
#include "package.h"
#include "repo.h"
#include "util.h"

PyObject* new_archive(PyTypeObject* type, struct pakfire_archive* archive) {
	ArchiveObject* self = (ArchiveObject *)type->tp_alloc(type, 0);
	if (self) {
		self->archive = pakfire_archive_ref(archive);
	}

	return (PyObject*)self;
}

static PyObject* Archive_new(PyTypeObject* type, PyObject* args, PyObject* kwds) {
	ArchiveObject* self = (ArchiveObject *)type->tp_alloc(type, 0);
	if (self) {
		self->archive = NULL;
	}

	return (PyObject *)self;
}

static void Archive_dealloc(ArchiveObject* self) {
	pakfire_archive_unref(self->archive);

	Py_TYPE(self)->tp_free((PyObject *)self);
}

static int Archive_init(ArchiveObject* self, PyObject* args, PyObject* kwds) {
	PakfireObject* pakfire = NULL;
	const char* filename = NULL;

	if (!PyArg_ParseTuple(args, "O!s", &PakfireType, &pakfire, &filename))
		return -1;

	errno = -pakfire_archive_open(&self->archive, pakfire->pakfire, filename);
	if (errno) {
		PyErr_SetFromErrno(PyExc_OSError);
		return -1;
	}

	return 0;
}

static PyObject* Archive_get_format(ArchiveObject* self) {
	unsigned int format = pakfire_archive_get_format(self->archive);

	return PyLong_FromUnsignedLong(format);
}

static PyObject* Archive_read(ArchiveObject* self, PyObject* args) {
	PyObject* file = NULL;
	FILE* f = NULL;
	const char* filename = NULL;

	if (!PyArg_ParseTuple(args, "s", &filename))
		return NULL;

	Py_BEGIN_ALLOW_THREADS

	// Try to open the file
	f = pakfire_archive_read(self->archive, filename, PAKFIRE_ARCHIVE_READ_FOLLOW_SYMLINKS);
	if (!f) {
		Py_BLOCK_THREADS
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	Py_END_ALLOW_THREADS

	// Map the file to Python
	file = new_archive_file(&ArchiveFileType, f);
	if (!file)
		goto ERROR;

	return file;

ERROR:
	if (f)
		fclose(f);

	return NULL;
}

static PyObject* Archive_verify(ArchiveObject* self) {
	int status;

	Py_BEGIN_ALLOW_THREADS

	// Verify this archive
	int r = pakfire_archive_verify(self->archive, &status);
	if (r) {
		Py_BLOCK_THREADS
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	Py_END_ALLOW_THREADS

	if (status)
		Py_RETURN_TRUE;
	else
		Py_RETURN_FALSE;
}

static PyObject* Archive_extract(ArchiveObject* self, PyObject* args, PyObject* kwargs) {
	const char* kwlist[] = { "path", NULL };
	const char* path = NULL;
	const int flags = 0;

	if (!PyArg_ParseTupleAndKeywords(args, kwargs, "|z", (char**)kwlist, &path))
		return NULL;

	// Extract payload
	errno = -pakfire_archive_extract(self->archive, path, flags);
	if (errno) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	Py_RETURN_NONE;
}

static PyObject* Archive_get_package(ArchiveObject* self, PyObject* args) {
	struct pakfire_package* package = NULL;
	RepoObject* repo = NULL;

	if (!PyArg_ParseTuple(args, "|O!", &RepoType, &repo))
		return NULL;

	Py_BEGIN_ALLOW_THREADS

	// Make the package
	int r = pakfire_archive_make_package(self->archive, (repo) ? repo->repo : NULL, &package);
	if (r) {
		Py_BLOCK_THREADS
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	Py_END_ALLOW_THREADS

	// Make the Python object
	PyObject* ret = new_package(&PackageType, package);

	// Cleanup
	pakfire_package_unref(package);

	return ret;
}

static PyObject* Archive_get_path(ArchiveObject* self) {
	const char* path = pakfire_archive_get_path(self->archive);
	if (!path)
		Py_RETURN_NONE;

	return PyUnicode_FromString(path);
}

static PyObject* Archive_get_filelist(ArchiveObject* self) {
	struct pakfire_filelist* filelist = NULL;
	int r;

	Py_BEGIN_ALLOW_THREADS

	// Fetch the filelist
	r = pakfire_archive_get_filelist(self->archive, &filelist);
	if (r < 0) {
		Py_BLOCK_THREADS

		errno = -r;
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	Py_END_ALLOW_THREADS

	// Convert to filelist
	PyObject* result = PyList_FromFileList(filelist);

	// Cleanup
	pakfire_filelist_unref(filelist);

	return result;
}

static struct PyMethodDef Archive_methods[] = {
	{
		"extract",
		(PyCFunction)Archive_extract,
		METH_VARARGS|METH_KEYWORDS,
		NULL
	},
	{
		"get_package",
		(PyCFunction)Archive_get_package,
		METH_VARARGS,
		NULL
	},
	{
		"read",
		(PyCFunction)Archive_read,
		METH_VARARGS,
		NULL
	},
	{
		"verify",
		(PyCFunction)Archive_verify,
		METH_NOARGS,
		NULL
	},
	{ NULL },
};

static struct PyGetSetDef Archive_getsetters[] = {
	{
		"filelist",
		(getter)Archive_get_filelist,
		NULL,
		NULL,
		NULL
	},
	{
		"format",
		(getter)Archive_get_format,
		NULL,
		NULL,
		NULL
	},
	{
		"path",
		(getter)Archive_get_path,
		NULL,
		NULL,
		NULL
	},
	{ NULL },
};

PyTypeObject ArchiveType = {
	PyVarObject_HEAD_INIT(NULL, 0)
	.tp_name            = "pakfire.Archive",
	.tp_basicsize       = sizeof(ArchiveObject),
	.tp_flags           = Py_TPFLAGS_DEFAULT|Py_TPFLAGS_BASETYPE,
	.tp_new             = Archive_new,
	.tp_dealloc         = (destructor)Archive_dealloc,
	.tp_init            = (initproc)Archive_init,
	.tp_doc             = "Archive Object",
	.tp_methods         = Archive_methods,
	.tp_getset          = Archive_getsetters,
};
