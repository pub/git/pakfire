/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2011 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#define PY_SSIZE_T_CLEAN
#include <Python.h>

#include <pakfire/arch.h>
#include <pakfire/deps.h>

#include "archive.h"
#include "archive_file.h"
#include "ctx.h"
#include "errors.h"
#include "file.h"
#include "key.h"
#include "package.h"
#include "pakfire.h"
#include "problem.h"
#include "repo.h"
#include "solution.h"
#include "util.h"

PyMODINIT_FUNC PyInit_pakfire(void);

PyObject* PyExc_BadSignatureError;
PyObject* PyExc_CommandExecutionError;
PyObject* PyExc_DependencyError;

PyObject* PyExc_CheckError;
PyObject* PyExc_CheckFileVerificationError;

static PyObject* Pakfire_supported_arches(void) {
	const char** arches = NULL;

	// Fetch all architectures
	arches = pakfire_supported_arches();
	if (!arches) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	return PyUnicodeList_FromStringArray(arches);
}

static PyObject* Pakfire_version_compare(PyObject* self, PyObject* args) {
	const char* evr1 = NULL;
	const char* evr2 = NULL;
	int r;

	if (!PyArg_ParseTuple(args, "ss", &evr1, &evr2))
		return NULL;

	// Compare versions
	r = pakfire_static_version_compare(evr1, evr2);

	// Return the return code
	return PyLong_FromLong(r);
}

static PyMethodDef pakfireModuleMethods[] = {
	{
		"supported_arches",
		(PyCFunction)Pakfire_supported_arches,
		METH_NOARGS,
		NULL,
	},
	{
		"version_compare",
		(PyCFunction)Pakfire_version_compare,
		METH_VARARGS,
		NULL,
	},
	{ NULL, NULL, 0, NULL },
};

static struct PyModuleDef moduledef = {
	.m_base = PyModuleDef_HEAD_INIT,
	.m_name = "pakfire",
	.m_size = -1,
	.m_methods = pakfireModuleMethods,
};

PyMODINIT_FUNC PyInit_pakfire(void) {
	int r;

	// Create the module
	PyObject* module = PyModule_Create(&moduledef);
	if (!module)
		return NULL;

	// Add the version
	r = PyModule_AddStringConstant(module, "__version__", PACKAGE_FULLVERSION);
	if (r < 0)
		goto ERROR;

	PyExc_DependencyError = PyErr_NewException("pakfire.DependencyError", NULL, NULL);
	Py_INCREF(PyExc_DependencyError);
	PyModule_AddObject(module, "DependencyError", PyExc_DependencyError);

	// Pakfire
	if (PyType_Ready(&PakfireType) < 0)
		return NULL;

	Py_INCREF(&PakfireType);
	PyModule_AddObject(module, "Pakfire", (PyObject *)&PakfireType);

	// Archive
	if (PyType_Ready(&ArchiveType) < 0)
		return NULL;

	Py_INCREF(&ArchiveType);
	PyModule_AddObject(module, "Archive", (PyObject *)&ArchiveType);

	// Archive File
	if (PyType_Ready(&ArchiveFileType) < 0)
		return NULL;

	Py_INCREF(&ArchiveFileType);
	PyModule_AddObject(module, "ArchiveFile", (PyObject*)&ArchiveFileType);

	// Ctx
	if (PyType_Ready(&CtxType) < 0)
		return NULL;

	Py_INCREF(&CtxType);
	PyModule_AddObject(module, "Ctx", (PyObject*)&CtxType);

	// File
	if (PyType_Ready(&FileType) < 0)
		return NULL;

	Py_INCREF(&FileType);
	PyModule_AddObject(module, "File", (PyObject *)&FileType);

	// Key
	if (PyType_Ready(&KeyType) < 0)
		return NULL;

	Py_INCREF(&KeyType);
	PyModule_AddObject(module, "Key", (PyObject *)&KeyType);

	// Package
	if (PyType_Ready(&PackageType) < 0)
		return NULL;

	Py_INCREF(&PackageType);
	PyModule_AddObject(module, "Package", (PyObject *)&PackageType);

	// Problem
	if (PyType_Ready(&ProblemType) < 0)
		return NULL;
	Py_INCREF(&ProblemType);
	PyModule_AddObject(module, "Problem", (PyObject *)&ProblemType);

	// Repo
	if (PyType_Ready(&RepoType) < 0)
		return NULL;

	Py_INCREF(&RepoType);
	PyModule_AddObject(module, "Repo", (PyObject *)&RepoType);

	// Solution
	if (PyType_Ready(&SolutionType) < 0)
		return NULL;

	Py_INCREF(&SolutionType);
	PyModule_AddObject(module, "Solution", (PyObject *)&SolutionType);

	// Constants
	if (PyModule_AddIntMacro(module, PAKFIRE_KEY_ALGO_NULL) < 0)
		goto ERROR;
	if (PyModule_AddIntMacro(module, PAKFIRE_KEY_ALGO_ED25519) < 0)
		goto ERROR;

	return module;

ERROR:
	Py_DECREF(module);

	return NULL;
}
