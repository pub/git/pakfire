/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2017 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#define PY_SSIZE_T_CLEAN
#include <Python.h>
#include <errno.h>
#include <syslog.h>

#include <pakfire/archive.h>
#include <pakfire/build.h>
#include <pakfire/config.h>
#include <pakfire/constants.h>
#include <pakfire/ctx.h>
#include <pakfire/dist.h>
#include <pakfire/jail.h>
#include <pakfire/logging.h>
#include <pakfire/mount.h>
#include <pakfire/packagelist.h>
#include <pakfire/pakfire.h>
#include <pakfire/key.h>
#include <pakfire/repo.h>
#include <pakfire/repolist.h>
#include <pakfire/transaction.h>
#include <pakfire/util.h>

#include "archive.h"
#include "ctx.h"
#include "errors.h"
#include "key.h"
#include "pakfire.h"
#include "repo.h"
#include "util.h"

static PyObject* Pakfire_new(PyTypeObject* type, PyObject* args, PyObject* kwds) {
	PakfireObject* self = (PakfireObject *)type->tp_alloc(type, 0);
	if (self) {
		self->ctx = NULL;
		self->pakfire = NULL;
	}

	return (PyObject *)self;
}

static int Pakfire_init(PakfireObject* self, PyObject* args, PyObject* kwds) {
	const char* kwlist[] = { "ctx", "path", "arch", "config", "stub", NULL };
	struct pakfire_config* c = NULL;
	const char* config = NULL;
	const char* path = NULL;
	const char* arch = NULL;
	CtxObject* ctx = NULL;
	int flags = 0;
	int stub = 0;
	int r;

	if (!PyArg_ParseTupleAndKeywords(args, kwds, "O!|zzzb", (char**)kwlist,
			&CtxType, &ctx, &path, &arch, &config, &stub))
		return -1;

	// Setup flags
	if (stub)
		flags |= PAKFIRE_FLAGS_STUB;

	// Create a new configuration object
	r = pakfire_config_create(&c);
	if (r < 0) {
		PyErr_SetFromErrno(PyExc_OSError);
		goto ERROR;
	}

	// Parse the configuration
	if (config) {
		r = pakfire_config_parse(c, config, -1);
		if (r < 0) {
			errno = -r;

			PyErr_SetFromErrno(PyExc_OSError);
			goto ERROR;
		}
	}

	Py_BEGIN_ALLOW_THREADS

	// Store a reference to the context
	self->ctx = pakfire_ctx_ref(ctx->ctx);

	// Create a new Pakfire instance
	r = pakfire_create(&self->pakfire, self->ctx, c, path, arch, flags);
	if (r < 0) {
		Py_BLOCK_THREADS
		errno = -r;

		PyErr_SetFromErrno(PyExc_OSError);
		goto ERROR;
    }

	Py_END_ALLOW_THREADS

ERROR:
	if (c)
		pakfire_config_unref(c);

	return r;
}

static void Pakfire_dealloc(PakfireObject* self) {
	Py_BEGIN_ALLOW_THREADS

	if (self->pakfire)
		pakfire_unref(self->pakfire);
	if (self->ctx)
		pakfire_ctx_unref(self->ctx);

	Py_END_ALLOW_THREADS

	Py_TYPE(self)->tp_free((PyObject *)self);
}

static PyObject* Pakfire_repr(PakfireObject* self) {
	const char* path = pakfire_get_path(self->pakfire);
	const char* arch = pakfire_get_arch(self->pakfire);

	return PyUnicode_FromFormat("<_pakfire.Pakfire %s (%s)>", path, arch);
}

static PyObject* Pakfire_get_path(PakfireObject* self) {
	const char* path = pakfire_get_path(self->pakfire);

	return PyUnicode_FromString(path);
}

static PyObject* Pakfire_get_arch(PakfireObject* self) {
	const char* arch = pakfire_get_arch(self->pakfire);

	return PyUnicode_FromString(arch);
}

static PyObject* Pakfire_get_repo(PakfireObject* self, PyObject* args) {
	const char* name = NULL;

	if (!PyArg_ParseTuple(args, "s", &name))
		return NULL;

	struct pakfire_repo* repo = pakfire_get_repo(self->pakfire, name);
	if (!repo)
		Py_RETURN_NONE;

	PyObject* obj = new_repo(&RepoType, repo);
	pakfire_repo_unref(repo);

	return obj;
}

/*
	XXX This could be moved out of here as this no longer depends on Pakfire
*/
static PyObject* Pakfire_generate_key(PakfireObject* self, PyObject* args, PyObject* kwds) {
	const char* kwlist[] = { "algorithm", "comment", NULL };
	struct pakfire_key* key = NULL;
	pakfire_key_algo_t algo = PAKFIRE_KEY_ALGO_NULL;
	const char* comment = NULL;

	if (!PyArg_ParseTupleAndKeywords(args, kwds, "is", (char**)kwlist, &algo, &comment))
		return NULL;

	// Generate a new key
	int r = pakfire_key_generate(&key, self->ctx, algo, comment);
	if (r) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	PyObject* object = new_key(&KeyType, key);
	pakfire_key_unref(key);

	return object;
}
/*
	XXX This could be moved out of here as this no longer depends on Pakfire
*/
static PyObject* Pakfire_import_key(PakfireObject* self, PyObject* args) {
	struct pakfire_key* key = NULL;
	PyObject* object = NULL;
	char* data = NULL;
	Py_ssize_t data_length = 0;
	int r;

	// Parse arguments
	if (!PyArg_ParseTuple(args, "s#", &data, &data_length))
		return NULL;

	// Map the object
	FILE* f = fmemopen(data, data_length, "r");
	if (!f)
		return NULL;

	// Import the key
	r = pakfire_key_import(&key, self->ctx, f);
	if (r) {
		PyErr_SetFromErrno(PyExc_OSError);
		goto ERROR;
	}

	// Convert the key into a Key object
	object = new_key(&KeyType, key);
	if (!object)
		goto ERROR;

ERROR:
	if (key)
		pakfire_key_unref(key);
	if (f)
		fclose(f);

	return object;
}

static PyObject* Pakfire_whatprovides(PakfireObject* self, PyObject* args) {
	const char* provides = NULL;
	struct pakfire_packagelist* list = NULL;
	PyObject* ret = NULL;
	int r;

	if (!PyArg_ParseTuple(args, "s", &provides))
		return NULL;

	// Create a new list
	r = pakfire_packagelist_create(&list, self->ctx);
	if (r < 0) {
		errno = -r;
		PyErr_SetFromErrno(PyExc_OSError);
		goto ERROR;
	}

	r = pakfire_whatprovides(self->pakfire, provides, 0, list);
	if (r < 0) {
		errno = -r;
		PyErr_SetFromErrno(PyExc_OSError);
		goto ERROR;
	}

	// Create a Python list from the package list
	ret = PyList_FromPackageList(list);

ERROR:
	if (list)
		pakfire_packagelist_unref(list);

	return ret;
}

static PyObject* Pakfire_whatrequires(PakfireObject* self, PyObject* args) {
	struct pakfire_packagelist* list = NULL;
	const char* requires = NULL;
	PyObject* ret = NULL;
	int r;

	if (!PyArg_ParseTuple(args, "s", &requires))
		return NULL;

	Py_BEGIN_ALLOW_THREADS

	// Create a new list
	r = pakfire_packagelist_create(&list, self->ctx);
	if (r < 0) {
		Py_BLOCK_THREADS
		errno = -r;
		PyErr_SetFromErrno(PyExc_OSError);
		goto ERROR;
	}

	r = pakfire_whatrequires(self->pakfire, requires, 0, list);
	if (r < 0) {
		Py_BLOCK_THREADS
		errno = -r;
		PyErr_SetFromErrno(PyExc_OSError);
		goto ERROR;
	}

	Py_END_ALLOW_THREADS

	// Create a Python list from the package list
	ret = PyList_FromPackageList(list);

ERROR:
	if (list)
		pakfire_packagelist_unref(list);

	return ret;
}

static PyObject* Pakfire_search(PakfireObject* self, PyObject* args, PyObject* kwds) {
	const char* kwlist[] = { "pattern", "name_only", NULL };
	struct pakfire_packagelist* list = NULL;
	const char* pattern = NULL;
	int name_only = 0;
	int flags = 0;
	PyObject* ret = NULL;
	int r;

	if (!PyArg_ParseTupleAndKeywords(args, kwds, "s|$p", (char**)kwlist, &pattern, &name_only))
		return NULL;

	// Search for package names only
	if (name_only)
		flags |= PAKFIRE_SEARCH_NAME_ONLY;

	r = pakfire_packagelist_create(&list, self->ctx);
	if (r < 0) {
		errno = -r;
		PyErr_SetFromErrno(PyExc_OSError);
		goto ERROR;
	}

	r = pakfire_search(self->pakfire, pattern, flags, list);
	if (r < 0) {
		errno = -r;
		PyErr_SetFromErrno(PyExc_OSError);
		goto ERROR;
	}

	ret = PyList_FromPackageList(list);

ERROR:
	if (list)
		pakfire_packagelist_unref(list);

	return ret;
}

static PyObject* Pakfire_version_compare(PakfireObject* self, PyObject* args) {
	const char* evr1 = NULL;
	const char* evr2 = NULL;

	if (!PyArg_ParseTuple(args, "ss", &evr1, &evr2))
		return NULL;

	int cmp = pakfire_version_compare(self->pakfire, evr1, evr2);

	return PyLong_FromLong(cmp);
}

static PyObject* Pakfire_dist(PakfireObject* self, PyObject* args) {
	struct pakfire_archive* archive = NULL;
	const char* path = NULL;
	const char* target = NULL;
	char* result = NULL;
	int r;

	if (!PyArg_ParseTuple(args, "s|z", &path, &target))
		return NULL;

	Py_BEGIN_ALLOW_THREADS

	r = pakfire_dist(self->pakfire, path, &archive, &result);
	if (r) {
		Py_BLOCK_THREADS
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	Py_END_ALLOW_THREADS

	PyObject* ret = PyUnicode_FromString(result);
	free(result);

	// XXX This is now very broken

	return ret;
}

static PyObject* Pakfire_get_repos(PakfireObject* self) {
	struct pakfire_repolist* repos = pakfire_get_repos(self->pakfire);
	if (!repos) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	const size_t l = pakfire_repolist_size(repos);

	PyObject* list = PyList_New(l);
	if (!list)
		goto ERROR;

	for (unsigned int i = 0; i < l; i++) {
		struct pakfire_repo* repo = pakfire_repolist_get(repos, i);
		if (!repo)
			continue;

		PyObject* obj = new_repo(&RepoType, repo);
		PyList_SET_ITEM(list, i, obj);

		pakfire_repo_unref(repo);
	}

ERROR:
	pakfire_repolist_unref(repos);

	return list;
}

static PyObject* Pakfire_clean(PakfireObject* self) {
	int r;

	Py_BEGIN_ALLOW_THREADS

	r = pakfire_clean(self->pakfire, 0);
	if (r) {
		Py_BLOCK_THREADS
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	Py_END_ALLOW_THREADS

	Py_RETURN_NONE;
}

static PyObject* Pakfire_open(PakfireObject* self, PyObject* args) {
	struct pakfire_archive* archive = NULL;
	const char* path = NULL;

	if (!PyArg_ParseTuple(args, "s", &path))
		return NULL;

	Py_BEGIN_ALLOW_THREADS

	int r = pakfire_archive_open(&archive, self->pakfire, path);
	if (r) {
		Py_BLOCK_THREADS
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	Py_END_ALLOW_THREADS

	// Create Python object
	PyObject* object = new_archive(&ArchiveType, archive);
	pakfire_archive_unref(archive);

	return object;
}

static PyObject* Pakfire_repo_compose(PakfireObject* self, PyObject* args, PyObject* kwargs) {
	const char* kwlist[] = { "path", "files", "key", NULL };
	const char* path = NULL;
	PyObject* list = NULL;
	KeyObject* key = NULL;

	PyObject* ret = NULL;

	if (!PyArg_ParseTupleAndKeywords(args, kwargs, "sO|O!", (char**)kwlist,
			&path, &list, &KeyType, &key))
		return NULL;

	// List must be a sequence
	if (!PySequence_Check(list)) {
		PyErr_SetString(PyExc_ValueError, "Expected a sequence.");
		return NULL;
	}

	// How many new files do we have?
	ssize_t num_files = PySequence_Length(list);
	if (num_files < 0)
		return NULL;

	// Allocate files array
	const char** files = calloc(num_files + 1, sizeof(*files));
	if (!files) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	for (int i = 0; i < num_files; i++) {
		PyObject* file = PySequence_GetItem(list, i);
		if (!file)
			goto ERROR;

		// Check if file is a Unicode object
		if (!PyUnicode_Check(file)) {
			PyErr_SetString(PyExc_ValueError, "Expected a string.");
			goto ERROR;
		}

		// Add pointer to string to files array
		files[i] = PyUnicode_AsUTF8(file);
		if (!files[i])
			goto ERROR;

		Py_DECREF(file);
	}

	Py_BEGIN_ALLOW_THREADS

	int r = pakfire_repo_compose(self->pakfire, path, (key) ? key->key : NULL, files);
	if (r) {
		Py_BLOCK_THREADS
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	Py_END_ALLOW_THREADS

	// Return None on success
	ret = Py_None;
	Py_INCREF(ret);

ERROR:
	if (files)
		free(files);

	return ret;
}

static struct PyMethodDef Pakfire_methods[] = {
	{
		"clean",
		(PyCFunction)Pakfire_clean,
		METH_NOARGS,
		NULL,
	},
	{
		"dist",
		(PyCFunction)Pakfire_dist,
		METH_VARARGS,
		NULL
	},
	{
		"generate_key",
		(PyCFunction)Pakfire_generate_key,
		METH_VARARGS|METH_KEYWORDS,
		NULL
	},
	{
		"get_repo",
		(PyCFunction)Pakfire_get_repo,
		METH_VARARGS,
		NULL
	},
	{
		"import_key",
		(PyCFunction)Pakfire_import_key,
		METH_VARARGS,
		NULL,
	},
	{
		"open",
		(PyCFunction)Pakfire_open,
		METH_VARARGS,
		NULL
	},
	{
		"repo_compose",
		(PyCFunction)Pakfire_repo_compose,
		METH_VARARGS|METH_KEYWORDS,
		NULL
	},
	{
		"search",
		(PyCFunction)Pakfire_search,
		METH_VARARGS|METH_KEYWORDS,
		NULL
	},
	{
		"version_compare",
		(PyCFunction)Pakfire_version_compare,
		METH_VARARGS,
		NULL
	},
	{
		"whatprovides",
		(PyCFunction)Pakfire_whatprovides,
		METH_VARARGS,
		NULL
	},
	{
		"whatrequires",
		(PyCFunction)Pakfire_whatrequires,
		METH_VARARGS,
		NULL
	},
	{ NULL },
};

static struct PyGetSetDef Pakfire_getsetters[] = {
	{
		"arch",
		(getter)Pakfire_get_arch,
		NULL,
		NULL,
		NULL
	},
    {
		"path",
		(getter)Pakfire_get_path,
		NULL,
		NULL,
		NULL
	},
	{
		"repos",
		(getter)Pakfire_get_repos,
		NULL,
		NULL,
		NULL
	},
    { NULL },
};

PyTypeObject PakfireType = {
	PyVarObject_HEAD_INIT(NULL, 0)
	.tp_name            = "pakfire.Pakfire",
	.tp_basicsize       = sizeof(PakfireObject),
	.tp_flags           = Py_TPFLAGS_DEFAULT|Py_TPFLAGS_BASETYPE,
	.tp_new             = Pakfire_new,
	.tp_dealloc         = (destructor)Pakfire_dealloc,
	.tp_init            = (initproc)Pakfire_init,
	.tp_doc             = "Pakfire Object",
	.tp_methods         = Pakfire_methods,
	.tp_getset          = Pakfire_getsetters,
	.tp_repr            = (reprfunc)Pakfire_repr,
};
