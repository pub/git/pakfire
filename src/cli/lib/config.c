/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <pakfire/ctx.h>
#include <pakfire/config.h>
#include <pakfire/string.h>

#include "config.h"

const char* cli_get_default_distro(struct pakfire_ctx* ctx) {
	struct pakfire_config* config = NULL;
	const char* distro = NULL;

	// Fetch the configuration
	config = pakfire_ctx_get_config(ctx);
	if (!config)
		return NULL;

	// Fetch the distro
	distro = pakfire_config_get(config, "build", "distro", NULL);

	// Cleanup
	if (config)
		pakfire_config_unref(config);

	return distro;
}

int cli_read_distro_config(struct pakfire_config* config, const char* distro) {
	char path[PATH_MAX];
	int r;

	// XXX hard-coded path

	// Make the path
	r = pakfire_string_format(path, "/etc/pakfire/distros/%s.conf", distro);
	if (r < 0)
		return r;

	// Read the configuration
	r = pakfire_config_read_path(config, path);
	if (r < 0)
		return r;

	return 0;
}
