/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <argp.h>

#include <pakfire/pakfire.h>

#include "command.h"
#include "pakfire.h"
#include "transaction.h"
#include "update.h"

static const char* args_doc = "update [OPTIONS...] PACKAGES...";

static const char* doc = "Update packages";

#define MAX_EXCLUDES 128
#define MAX_PACKAGES 128

struct cli_local_args {
	int transaction_flags;

	// Excludes
	char* excludes[MAX_EXCLUDES];
	unsigned int num_excludes;

	// Packages
	char* packages[MAX_PACKAGES];
	unsigned int num_packages;
};

enum {
	OPT_ALLOW_DOWNGRADE = 1,
	OPT_ALLOW_UNINSTALL = 2,
};

static struct argp_option options[] = {
	{ "allow-downgrade", OPT_ALLOW_DOWNGRADE, NULL, 0, "Allow downgrading packages", 0 },
	{ "allow-uninstall", OPT_ALLOW_UNINSTALL, NULL, 0, "Allow uninstalling packages", 0 },
	{ "exclude",         'x',            "PACKAGE", 0, "Exclude a package from being updated", 0 },
	{ NULL },
};

static error_t parse(int key, char* arg, struct argp_state* state, void* data) {
	struct cli_local_args* args = data;

	switch (key) {
		case OPT_ALLOW_DOWNGRADE:
			args->transaction_flags |= PAKFIRE_TRANSACTION_ALLOW_DOWNGRADE;
			break;

		case OPT_ALLOW_UNINSTALL:
			args->transaction_flags |= PAKFIRE_TRANSACTION_ALLOW_UNINSTALL;
			break;

		case 'x':
			if (args->num_excludes >= MAX_EXCLUDES)
				return -ENOBUFS;

			args->excludes[args->num_excludes++] = arg;
			break;

		case ARGP_KEY_ARG:
			if (args->num_packages >= MAX_PACKAGES)
				return -ENOBUFS;

			args->packages[args->num_packages++] = arg;
			break;

		default:
			return ARGP_ERR_UNKNOWN;
	}

	return 0;
}

static int __cli_update(struct pakfire_transaction* transaction, int argc, char* argv[], void* data) {
	struct cli_local_args* args = data;
	int r;

	// Exclude any packages
	for (unsigned int i = 0; i < args->num_excludes; i++) {
		r = pakfire_transaction_request(transaction, PAKFIRE_JOB_LOCK, args->excludes[i], 0);
		if (r)
			return r;
	}

	// Did the user pass any packages?
	if (argc) {
		// Add the remaining command line options as packages
		for (unsigned int i = 0; i < args->num_packages; i++) {
			r = pakfire_transaction_request(transaction,
					PAKFIRE_JOB_UPDATE, args->packages[i], 0);
			if (r) {
				fprintf(stderr, "Could not find '%s': %m\n", argv[i]);
				return r;
			}
		}

	// Otherwise update everything
	} else {
		r = pakfire_transaction_request(transaction, PAKFIRE_JOB_UPDATE_ALL, NULL, 0);
		if (r)
			return r;
	}

	return 0;
}

int cli_update(void* data, int argc, char* argv[]) {
	struct cli_global_args* global_args = data;
	struct cli_local_args local_args = {};
	struct pakfire* pakfire = NULL;
	int r;

	// Parse the command line
	r = cli_parse(options, NULL, args_doc, doc, parse, 0, argc, argv, &local_args);
	if (r)
		goto ERROR;

	// Setup Pakfire
	r = cli_setup_pakfire(&pakfire, global_args);
	if (r)
		goto ERROR;

	r = cli_transaction(pakfire, argc, argv, local_args.transaction_flags, __cli_update, &local_args);

ERROR:
	if (pakfire)
		pakfire_unref(pakfire);

	return r;
}
