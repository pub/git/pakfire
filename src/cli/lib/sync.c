/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <argp.h>

#include <pakfire/pakfire.h>

#include "command.h"
#include "pakfire.h"
#include "sync.h"
#include "transaction.h"

static const char* args_doc = "sync [OPTIONS...]";

static const char* doc = "Synchronize packages";

struct cli_local_args {
	int job_flags;
};

enum {
	OPT_KEEP_ORPHANED = 1,
};

static struct argp_option options[] = {
	{ "keep-orphaned", OPT_KEEP_ORPHANED, NULL, 0, "Keep orphaned packages", 0 },
	{ NULL },
};

static error_t parse(int key, char* arg, struct argp_state* state, void* data) {
	struct cli_local_args* args = data;

	switch (key) {
		case OPT_KEEP_ORPHANED:
			args->job_flags |= PAKFIRE_JOB_KEEP_ORPHANED;
			break;

		default:
			return ARGP_ERR_UNKNOWN;
	}

	return 0;
}

static int __cli_sync(struct pakfire_transaction* transaction, int argc, char* argv[], void* data) {
	struct cli_local_args* args = data;
	int r;

	// Request sync
	r = pakfire_transaction_request(transaction, PAKFIRE_JOB_SYNC, NULL, args->job_flags);
	if (r) {
		fprintf(stderr, "Could not request job: %m\n");
		return r;
	}

	return 0;
}

int cli_sync(void* data, int argc, char* argv[]) {
	struct cli_global_args* global_args = data;
	struct cli_local_args local_args = {};
	struct pakfire* pakfire = NULL;
	int r;

	// Parse the command line
	r = cli_parse(options, NULL, args_doc, doc, parse, 0, argc, argv, &local_args);
	if (r)
		goto ERROR;

	// Setup Pakfire
	r = cli_setup_pakfire(&pakfire, global_args);
	if (r)
		goto ERROR;

	r = cli_transaction(pakfire, argc, argv, 0, __cli_sync, &local_args);

ERROR:
	if (pakfire)
		pakfire_unref(pakfire);

	return r;
}
