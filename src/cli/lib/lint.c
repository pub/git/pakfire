/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2024 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <argp.h>

#include <pakfire/ctx.h>
#include <pakfire/archive.h>
#include <pakfire/package.h>
#include <pakfire/pakfire.h>

#include "color.h"
#include "command.h"
#include "lint.h"
#include "pakfire.h"

static const char* args_doc = "lint [OPTIONS...] ARCHIVES...";

static const char* doc = "Lint archives";

#define MAX_ARCHIVES 128

struct cli_local_args {
	// Archives
	char* archives[MAX_ARCHIVES];
	unsigned int num_archives;
};

static error_t parse(int key, char* arg, struct argp_state* state, void* data) {
	struct cli_local_args* args = data;

	switch (key) {
		case ARGP_KEY_ARG:
			if (args->num_archives >= MAX_ARCHIVES)
				return -ENOBUFS;

			args->archives[args->num_archives++] = arg;
			break;

		default:
			return ARGP_ERR_UNKNOWN;
	}

	return 0;
}

static int cli_linter_result(struct pakfire_ctx* ctx, struct pakfire_archive* archive,
		struct pakfire_package* package, struct pakfire_file* file, int priority,
		const char* result, void* data) {
	const char* nevra = pakfire_package_get_string(package, PAKFIRE_PKG_NEVRA);

	switch (priority) {
		case PAKFIRE_LINTER_INFO:
			printf("%s: %s%s%s\n", nevra, color_highlight(), result, color_reset());
			break;

		case PAKFIRE_LINTER_WARNING:
			printf("%s: %s%s%s\n", nevra, color_yellow(), result, color_reset());
			break;

		case PAKFIRE_LINTER_ERROR:
			printf("%s: %s%s%s\n", nevra, color_red(), result, color_reset());
			break;

		default:
			return -ENOTSUP;
	}

	return 0;
}

static int do_lint(struct pakfire* pakfire, const char* path) {
	struct pakfire_archive* archive = NULL;
	int r;

	// Open the archive
	r = pakfire_archive_open(&archive, pakfire, path);
	if (r < 0) {
		fprintf(stderr, "Could not open %s: %s\n", path, strerror(-r));
		goto ERROR;
	}

	// Perform the linting...
	r = pakfire_archive_lint(archive, cli_linter_result, NULL);
	if (r < 0) {
		fprintf(stderr, "Could not lint %s: %s\n", path, strerror(-r));
		goto ERROR;
	}

ERROR:
	if (archive)
		pakfire_archive_unref(archive);

	return r;
}

int cli_lint(void* data, int argc, char* argv[]) {
	struct cli_global_args* global_args = data;
	struct cli_local_args local_args = {};
	struct pakfire* pakfire = NULL;
	int r;

	// Parse the command line
	r = cli_parse(NULL, NULL, args_doc, doc, parse, 0, argc, argv, &local_args);
	if (r)
		goto ERROR;

	// Setup Pakfire
	r = cli_setup_pakfire(&pakfire, global_args);
	if (r)
		goto ERROR;

	// Lint all archives
	for (unsigned int i = 0; i < local_args.num_archives; i++) {
		r = do_lint(pakfire, local_args.archives[i]);
		if (r < 0)
			goto ERROR;
	}

ERROR:
	if (pakfire)
		pakfire_unref(pakfire);

	return r;
}

