/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <argp.h>

#include <pakfire/pakfire.h>

#include "command.h"
#include "install.h"
#include "pakfire.h"
#include "transaction.h"

static const char* args_doc = "install [OPTIONS...] PACKAGES...";

static const char* doc = "Install packages";

#define MAX_PACKAGES 128

struct cli_local_args {
	int transaction_flags;
	int job_flags;

	// Packages
	char* packages[MAX_PACKAGES];
	unsigned int num_packages;
};

enum {
	OPT_BEST                = 1,
	OPT_ALLOW_DOWNGRADE     = 2,
	OPT_ALLOW_UNINSTALL     = 3,
	OPT_WITHOUT_RECOMMENDED = 4,
};

static struct argp_option options[] = {
	{ "allow-downgrade",     OPT_ALLOW_DOWNGRADE, NULL, 0, "Allow downgrading packages", 0 },
	{ "allow-uninstall",     OPT_ALLOW_UNINSTALL, NULL, 0, "Allow uninstalling packages", 0 },
	{ "best",                OPT_BEST,            NULL, 0, "Only install the best package(s)", 0 },
	{ "without-recommended", OPT_ALLOW_DOWNGRADE, NULL, 0, "Do not install recommended packages", 0 },
	{ NULL },
};

static error_t parse(int key, char* arg, struct argp_state* state, void* data) {
	struct cli_local_args* args = data;

	switch (key) {
		case OPT_BEST:
			args->job_flags |= PAKFIRE_JOB_BEST;
			break;

		case OPT_ALLOW_DOWNGRADE:
			args->transaction_flags |= PAKFIRE_TRANSACTION_ALLOW_DOWNGRADE;
			break;

		case OPT_ALLOW_UNINSTALL:
			args->transaction_flags |= PAKFIRE_TRANSACTION_ALLOW_UNINSTALL;
			break;

		case OPT_WITHOUT_RECOMMENDED:
			args->transaction_flags |= PAKFIRE_TRANSACTION_WITHOUT_RECOMMENDED;
			break;

		case ARGP_KEY_ARG:
			if (args->num_packages >= MAX_PACKAGES)
				return -ENOBUFS;

			args->packages[args->num_packages++] = arg;
			break;

		default:
			return ARGP_ERR_UNKNOWN;
	}

	return 0;
}

static int __cli_install(struct pakfire_transaction* transaction, int argc, char* argv[], void* data) {
	struct cli_local_args* args = data;
	int r;

	// Add the remaining command line options as packages
	for (unsigned int i = 0; i < args->num_packages; i++) {
		r = pakfire_transaction_request(transaction,
				PAKFIRE_JOB_INSTALL, args->packages[i], args->job_flags);
		if (r) {
			fprintf(stderr, "Could not find '%s': %m\n", argv[i]);
			return r;
		}
	}

	return 0;
}

int cli_install(void* data, int argc, char* argv[]) {
	struct cli_global_args* global_args = data;
	struct cli_local_args local_args = {};
	struct pakfire* pakfire = NULL;
	int r;

	// Parse the command line
	r = cli_parse(options, NULL, args_doc, doc, parse, 0, argc, argv, &local_args);
	if (r)
		goto ERROR;

	// Setup Pakfire
	r = cli_setup_pakfire(&pakfire, global_args);
	if (r)
		goto ERROR;

	r = cli_transaction(pakfire, argc, argv, local_args.transaction_flags, __cli_install, &local_args);

ERROR:
	if (pakfire)
		pakfire_unref(pakfire);

	return r;
}
