/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <stdio.h>
#include <stdlib.h>

#include <pakfire/pakfire.h>
#include <pakfire/transaction.h>

#include "transaction.h"

/*
	This is a convenience function that creates a new transaction and
	calls a callback function which is supposed to create some jobs.

	The transaction is then solved and executed.
*/
int cli_transaction(struct pakfire* pakfire, int argc, char* argv[], int flags,
		cli_transaction_callback callback, void* data) {
	struct pakfire_transaction* transaction = NULL;
	char* problems = NULL;
	int r;

	// Create a new transaction
	r = pakfire_transaction_create(&transaction, pakfire, flags);
	if (r) {
		fprintf(stderr, "Could not setup the transaction\n");
		goto ERROR;
	}

	// Call the callback
	r = callback(transaction, argc, argv, data);
	if (r)
		goto ERROR;

	// Solve the transaction
	r = pakfire_transaction_solve(transaction, 0, &problems);
	if (r) {
		fprintf(stderr, "Could not solve request:\n%s\n", problems);
		goto ERROR;
	}

	// Run the transaction
	r = pakfire_transaction_run(transaction);
	if (r)
		goto ERROR;

ERROR:
	if (transaction)
		pakfire_transaction_unref(transaction);
	if (problems)
		free(problems);

	return r;
}
