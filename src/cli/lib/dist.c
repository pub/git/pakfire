/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <argp.h>

#include "command.h"
#include "dist.h"
#include "pakfire.h"

#include <pakfire/dist.h>
#include <pakfire/pakfire.h>
#include <pakfire/path.h>

static const char* args_doc = "dist MAKEFILES...";

static const char* doc = "Creates source packages from makefiles";

#define MAX_MAKEFILES 32

struct cli_local_args {
	const char* resultdir;

	// Makefiles
	char* makefiles[MAX_MAKEFILES];
	unsigned int num_makefiles;
};

static error_t parse(int key, char* arg, struct argp_state* state, void* data) {
	struct cli_local_args* args = data;

	switch (key) {
		case ARGP_KEY_ARG:
			if (args->num_makefiles >= MAX_MAKEFILES)
				return -ENOBUFS;

			args->makefiles[args->num_makefiles++] = arg;
			break;

		default:
			return ARGP_ERR_UNKNOWN;
	}

	return 0;
}

static int do_dist(struct pakfire* pakfire, const char* makefile, const char* resultdir) {
	struct pakfire_archive* archive = NULL;
	char* filename = NULL;
	char path[PATH_MAX];
	int r;

	// Create the archive
	r = pakfire_dist(pakfire, makefile, &archive, &filename);
	if (r < 0)
		goto ERROR;

	// Make the final path
	r = pakfire_path_append(path, resultdir, filename);
	if (r < 0)
		goto ERROR;

	// Copy or link the archive
	r = pakfire_archive_link_or_copy(archive, path);
	if (r < 0)
		goto ERROR;

	// Unlink the temporary archive
	r = pakfire_archive_unlink(archive);
	if (r < 0)
		goto ERROR;

ERROR:
	if (archive)
		pakfire_archive_unref(archive);
	if (filename)
		free(filename);

	return r;
}

int cli_dist(void* data, int argc, char* argv[]) {
	struct cli_global_args* global_args = data;
	struct cli_local_args local_args = {};
	struct pakfire* pakfire = NULL;
	char cwd[PATH_MAX];
	int r;

	// Parse the command line
	r = cli_parse(NULL, NULL, args_doc, doc, parse, 0, argc, argv, &local_args);
	if (r)
		goto ERROR;

	// Set result directory to PWD
	if (!local_args.resultdir)
		local_args.resultdir = getcwd(cwd, sizeof(cwd));

	// Setup pakfire
	r = cli_setup_pakfire(&pakfire, global_args);
	if (r)
		goto ERROR;

	// Process all packages
	for (unsigned int i = 0; i < local_args.num_makefiles; i++) {
		r = do_dist(pakfire, local_args.makefiles[i], local_args.resultdir);
		if (r < 0) {
			fprintf(stderr, "Could not dist %s: %s\n", local_args.makefiles[i], strerror(-r));
			goto ERROR;
		}
	}

ERROR:
	if (pakfire)
		pakfire_unref(pakfire);

	return r;
}
