/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <pakfire/ctx.h>
#include <pakfire/daemon.h>

#include "daemon.h"

int cli_daemon_main(struct pakfire_ctx* ctx) {
	struct pakfire_daemon* daemon = NULL;
	int r;

	// Create the daemon
	r = pakfire_daemon_create(&daemon, ctx);
	if (r < 0)
		goto ERROR;

	// Run the daemon
	r = pakfire_daemon_main(daemon);
	if (r < 0)
		goto ERROR;

ERROR:
	// Cleanup
	if (daemon)
		pakfire_daemon_unref(daemon);

	return r;
}
