/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <argp.h>

#include <pakfire/buildservice.h>

#include "client-build.h"
#include "command.h"
#include "pakfire.h"

static const char* args_doc = "[OPTIONS...] MAKEFILES...";

static const char* doc = "Build packages remotely";

#define MAX_ARCHES     8
#define MAX_UPLOADS   32
#define MAX_PACKAGES  32
#define MAX_MAKEFILES 32

struct cli_local_args {
	const char* repo;
	int flags;

	const char* arches[MAX_ARCHES];
	unsigned int num_arches;

	// Packages
	char* packages[MAX_PACKAGES];
	unsigned int num_packages;

	// Uploads
	char* uploads[MAX_UPLOADS];
	unsigned int num_uploads;
};

enum {
	OPT_ARCH          = 1,
	OPT_DISABLE_TESTS = 2,
	OPT_REPO          = 3,
};

static struct argp_option options[] = {
	{ "arch",          OPT_ARCH,          "ARCH", 0, "Build for this architecture",   0 },
	{ "disable-tests", OPT_DISABLE_TESTS,   NULL, 0, "Do not run tests",              0 },
	{ "repo",          OPT_REPO,          "REPO", 0, "Build against this repository", 0 },
	{ NULL },
};

static error_t parse(int key, char* arg, struct argp_state* state, void* data) {
	struct cli_local_args* args = data;

	switch (key) {
		case OPT_ARCH:
			if (args->num_arches >= MAX_ARCHES)
				return -ENOBUFS;

			args->arches[args->num_arches++] = arg;
			break;

		case OPT_DISABLE_TESTS:
			args->flags |= PAKFIRE_BUILDSERVICE_DISABLE_TESTS;
			break;

		case OPT_REPO:
			args->repo = arg;
			break;

		case ARGP_KEY_ARG:
			if (args->num_packages >= MAX_PACKAGES)
				return -ENOBUFS;

			args->packages[args->num_packages++] = arg;
			break;

		default:
			return ARGP_ERR_UNKNOWN;
	}

	return 0;
}

int cli_client_build(void* data, int argc, char* argv[]) {
	struct cli_global_args* global_args = data;
	struct cli_local_args local_args = {};
	struct pakfire_buildservice* service = NULL;
	char* upload = NULL;
	int r;

	// Parse the command line
	r = cli_parse(options, NULL, args_doc, doc, parse, 0, argc, argv, &local_args);
	if (r)
		goto ERROR;

	// Connect to the build service
	r = cli_setup_buildservice(&service, global_args);
	if (r < 0)
		goto ERROR;

	// Upload all packages
	for (unsigned int i = 0; i < local_args.num_packages; i++) {
		r = pakfire_buildservice_upload(service, local_args.packages[i], NULL, &upload);
		if (r)
			goto ERROR;

		// Store the upload ID
		local_args.uploads[local_args.num_uploads++] = upload;
	}

	// No uploads
	if (!local_args.num_uploads)
		goto ERROR;

	// Build all the things
	for (unsigned int i = 0; i < local_args.num_uploads; i++) {
		r = pakfire_buildservice_build(service, local_args.uploads[i], local_args.repo,
			local_args.arches, local_args.flags);
		if (r)
			goto ERROR;

		// Free the upload
		free(local_args.uploads[i]);
		local_args.uploads[i] = NULL;
	}

ERROR:
	// Delete & free all uploads that could not be processed
	for (unsigned int i = 0; i < local_args.num_uploads; i++) {
		if (local_args.uploads[i]) {
			pakfire_buildservice_delete_upload(service, local_args.uploads[i]);
			free(local_args.uploads[i]);
		}
	}
	if (service)
		pakfire_buildservice_unref(service);

	return r;
}
