/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <argp.h>

#include <pakfire/key.h>
#include <pakfire/pakfire.h>

#include "command.h"
#include "repo_compose.h"
#include "pakfire.h"

static const char* args_doc = "[OPTIONS...] PATH PACKAGES...";

static const char* doc = "Create a new repository";

#define MAX_PACKAGES 1024

struct cli_local_args {
	const char* path;
	const char* key;

	char* packages[MAX_PACKAGES];
	unsigned int num_packages;
};

enum {
	OPT_KEY = 1,
};

static struct argp_option options[] = {
	{ "key", OPT_KEY, "KEY", 0, "Use this key to sign the repository", 0 },
	{ NULL },
};

static error_t parse(int key, char* arg, struct argp_state* state, void* data) {
	struct cli_local_args* args = data;

	switch (key) {
		case OPT_KEY:
			args->key = arg;
			break;

		case ARGP_KEY_ARG:
			if (!args->path)
				args->path = arg;

			else if (args->num_packages >= MAX_PACKAGES)
				return -ENOBUFS;

			else
				args->packages[args->num_packages++] = arg;
			break;

		default:
			return ARGP_ERR_UNKNOWN;
	}

	return 0;
}

static int cli_import_key(struct pakfire_key** key,
		struct pakfire_ctx* ctx, const char* path) {
	FILE* f = NULL;
	int r;

	// Open the key file
	f = fopen(path, "r");
	if (!f) {
		fprintf(stderr, "Could not open key from %s: %m\n", path);
		return -errno;
	}

	// Import the key
	r = pakfire_key_import(key, ctx, f);
	if (r) {
		fprintf(stderr, "Could not import key from %s\n", path);
		return r;
	}

	if (f)
		fclose(f);

	return r;
}

int cli_repo_compose(void* data, int argc, char* argv[]) {
	struct cli_global_args* global_args = data;
	struct cli_local_args local_args = {};
	struct pakfire* pakfire = NULL;
	struct pakfire_key* key = NULL;
	int r;

	// Parse the command line
	r = cli_parse(options, NULL, args_doc, doc, parse, 0, argc, argv, &local_args);
	if (r)
		goto ERROR;

	// Setup pakfire
	r = cli_setup_pakfire(&pakfire, global_args);
	if (r)
		goto ERROR;

	// Read the key (if any)
	if (local_args.key) {
		r = cli_import_key(&key, global_args->ctx, local_args.key);
		if (r)
			goto ERROR;
	}

	// Write the repository
	r = pakfire_repo_compose(pakfire, local_args.path, key, (const char**)local_args.packages);

ERROR:
	if (key)
		pakfire_key_unref(key);
	if (pakfire)
		pakfire_unref(pakfire);

	return r;
}
