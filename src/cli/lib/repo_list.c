/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <json.h>

#include <pakfire/buildservice.h>

#include "command.h"
#include "dump.h"
#include "pakfire.h"
#include "repo_list.h"

static const char* doc = "Lists all repositories";

struct cli_local_args {
	const char* distro;
};

static error_t parse(int key, char* arg, struct argp_state* state, void* data) {
	struct cli_local_args* args = data;

	switch (key) {
		case ARGP_KEY_ARG:
			if (!args->distro)
				args->distro = arg;

			else
				argp_usage(state);
			break;

		default:
			return ARGP_ERR_UNKNOWN;
	}

	return 0;
}

int cli_repo_list(void* data, int argc, char* argv[]) {
	struct cli_global_args* global_args = data;
	struct cli_local_args local_args = {};
	struct pakfire_buildservice* service = NULL;
	struct json_object* repos = NULL;
	int r;

	// Parse the command line
	r = cli_parse(NULL, NULL, NULL, doc, parse, 0, argc, argv, &local_args);
	if (r)
		goto ERROR;

	// Connect to the build service
	r = cli_setup_buildservice(&service, global_args);
	if (r < 0)
		goto ERROR;

	// List repos
	r = pakfire_buildservice_list_repos(service, local_args.distro, &repos);
	if (r)
		goto ERROR;

	// Dump everything
	r = cli_dump_json(repos);
	if (r)
		goto ERROR;

ERROR:
	if (service)
		pakfire_buildservice_unref(service);
	if (repos)
		json_object_put(repos);

	return r;
}
