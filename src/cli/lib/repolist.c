/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <argp.h>

#include <pakfire/pakfire.h>
#include <pakfire/repolist.h>

#include "command.h"
#include "dump.h"
#include "pakfire.h"
#include "repolist.h"

static const char* doc = "List all available repositories";

int cli_repolist(void* data, int argc, char* argv[]) {
	struct cli_global_args* global_args = data;
	struct pakfire* pakfire = NULL;
	struct pakfire_repolist* list = NULL;
	int r;

	// Parse the command line
	r = cli_parse(NULL, NULL, NULL, doc, NULL, 0, argc, argv, NULL);
	if (r)
		goto ERROR;

	// Setup Pakfire
	r = cli_setup_pakfire(&pakfire, global_args);
	if (r)
		goto ERROR;

	// Fetch all repositories
	list = pakfire_get_repos(pakfire);
	if (!list) {
		r = -errno;
		goto ERROR;
	}

	// Dump the repolist
	r = cli_dump_repolist(list, 0);

ERROR:
	if (list)
		pakfire_repolist_unref(list);
	if (pakfire)
		pakfire_unref(pakfire);

	return r;
}
