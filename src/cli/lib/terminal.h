/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_CLI_TERMINAL_H
#define PAKFIRE_CLI_TERMINAL_H

#include <pakfire/ctx.h>
#include <pakfire/pakfire.h>
#include <pakfire/transaction.h>

int cli_term_get_dimensions(int* rows, int* cols);

int cli_term_is_interactive(void);

int cli_term_confirm(struct pakfire_ctx* ctx, struct pakfire* pakfire,
	void* data, const char* message, const char* question);
int cli_term_confirm_yes(struct pakfire_ctx* ctx, struct pakfire* pakfire,
	void* data, const char* message, const char* question);

int cli_term_pick_solution(struct pakfire_ctx* ctx, struct pakfire* pakfire,
	void* data, struct pakfire_transaction* transaction);

#endif /* PAKFIRE_CLI_TERMINAL_H */
