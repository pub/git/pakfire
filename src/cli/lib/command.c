/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <argp.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "command.h"

static const struct command* command_find(const struct command* commands, const char* verb) {
	for (const struct command* command = commands; command->verb; command++) {
		if (strcmp(command->verb, verb) == 0)
			return command;
	}

	return NULL;
}

static int count_arguments(int argc, char* argv[]) {
	int arguments = 0;

	for (int i = 1; i < argc; i++) {
		if (*argv[i] == '-')
			continue;

		arguments++;
	}

	return arguments;
}

static int update_program_name(int argc, char* argv[], const char* verb) {
	char* program_name = NULL;
	int r;

	// XXX maybe program_name should move into the ctx?

	// Program name
	r = asprintf(&program_name, "%s %s", program_invocation_short_name, verb);
	if (r < 0)
		return r;

	program_invocation_short_name = argv[0] = program_name;

	return 0;
}

static int command_run(const struct command* command, int argc, char* argv[], void* data) {
	int r;

	if (!command)
		return -EINVAL;

	// Update the program name
	r = update_program_name(argc, argv, command->verb);
	if (r)
		return r;

	// Run the command
	return command->callback(data, argc, argv);
}

struct command_ctx {
	const struct argp_option* options;
	const struct command* commands;
	command_parse parse;
	int flags;
	void* data;

	// The selected command
	const struct command* command;
	int argc;
	char** argv;
};

static error_t __command_parse(int key, char* arg, struct argp_state* state) {
	struct command_ctx* ctx = state->input;

	const struct command* command = NULL;

	// Just call the parse function if we don't have any commands
	if (!ctx->commands) {
		if (!ctx->parse)
			return ARGP_ERR_UNKNOWN;

		return ctx->parse(key, arg, state, ctx->data);
	}

	switch (key) {
		// Show help if no arguments have been passed
		case ARGP_KEY_NO_ARGS:
			argp_failure(state, EXIT_FAILURE, 0, "Missing command");
			break;

		// Try to find a command
		case ARGP_KEY_ARG:
			command = ctx->command = command_find(ctx->commands, arg);

			// Fail if the command wasn't found
			if (!command) {
				argp_failure(state, EXIT_FAILURE, 0, "Unknown command '%s'", arg);
				break;
			}

			// XXX actually we should update the program name here

			// Return UNKNOWN so that we get called for ARGP_KEY_ARGS
			return ARGP_ERR_UNKNOWN;

		// Store all remaining options & arguments
		case ARGP_KEY_ARGS:
			ctx->argc = state->argc - state->next;
			ctx->argv = &state->argv[state->next];
			break;

		// Perform some final checks when parsing has been completed
		case ARGP_KEY_SUCCESS:
			// Check for root privileges
			if (ctx->flags & CLI_REQUIRE_ROOT) {
				if (getuid() || getgid())
					argp_failure(state, EXIT_FAILURE, 0, "Must be run as root");
			}

			if (ctx->command) {
				int args = count_arguments(ctx->argc, ctx->argv);

				// Check if we have a sufficient number of arguments
				if (ctx->command->min_args > 0 && args < ctx->command->min_args)
					argp_error(state, "Not enough arguments");

				else if (ctx->command->max_args >= 0 && args > ctx->command->max_args)
					argp_error(state, "Too many arguments");
			}
			break;

		// Do not pass any other things to the callback
		case ARGP_KEY_END:
		case ARGP_KEY_ERROR:
		case ARGP_KEY_INIT:
		case ARGP_KEY_FINI:
			break;

		// Otherwise call the callback
		default:
			if (!ctx->parse)
				return ARGP_ERR_UNKNOWN;

			return ctx->parse(key, arg, state, ctx->data);
	}

	return 0;
}

int cli_parse(const struct argp_option* options, const struct command* commands,
		const char* args_doc, const char* doc,
		command_parse parse, int flags, int argc, char** argv, void* data) {
	int r;

	// Setup context
	struct command_ctx ctx = {
		.options  = options,
		.commands = commands,
		.parse    = parse,
		.flags    = flags,
		.data     = data,
	};

	// Setup the parser
	struct argp parser = {
		.options   = options,
		.parser    = __command_parse,
		.args_doc  = args_doc,
		.doc       = doc,
	};
	int arg_index = 0;

	// Parse command line options
	r = argp_parse(&parser, argc, argv, ARGP_IN_ORDER, &arg_index, &ctx);
	if (r)
		return r;

	// Dispatch the selected command
	if (commands) {
		// Run the command
		r = command_run(ctx.command, ctx.argc, ctx.argv, ctx.data);
		if (r)
			return r;
	}

	return 0;
}
