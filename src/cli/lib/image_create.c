/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <argp.h>

#include <pakfire/build.h>
#include <pakfire/oci.h>
#include <pakfire/pakfire.h>
#include <pakfire/string.h>

#include "command.h"
#include "image_create.h"
#include "pakfire.h"

struct cli_local_args {
	const char* distro;
	const char* type;
	const char* path;
};

static const char* args_doc = "image create TYPE PATH";

static const char* doc = "Creates images";

const char* packages[] = {
	"system-release",
	NULL,
};

static error_t parse(int key, char* arg, struct argp_state* state, void* data) {
	struct cli_local_args* args = data;

	switch (key) {
		case ARGP_KEY_ARG:
			if (!args->type)
				args->type = arg;

			else if (!args->path)
				args->path = arg;

			break;

		default:
			return ARGP_ERR_UNKNOWN;
	}

	return 0;
}

int cli_image_create(void* data, int argc, char* argv[]) {
	struct cli_global_args* global_args = data;
	struct cli_local_args local_args = {};
	struct pakfire_build* build = NULL;
	struct pakfire* pakfire = NULL;
	FILE* f = NULL;
	int r;

	// Parse the command line
	r = cli_parse(NULL, NULL, args_doc, doc, parse, 0, argc, argv, &local_args);
	if (r)
		goto ERROR;

	f = fopen(local_args.path, "w");
	if (!f) {
		fprintf(stderr, "Could not open %s: %m\n", local_args.path);
		r = -errno;
		goto ERROR;
	}

	// Handle OCI images separately
	if (pakfire_string_equals(local_args.type, "oci")) {
		// Setup Pakfire
		r = cli_setup_pakfire(&pakfire, global_args);
		if (r < 0)
			goto ERROR;

		// Install everything
		r = pakfire_install(pakfire, packages);
		if (r < 0)
			goto ERROR;

		// Create the image
		r = pakfire_oci_mkimage(pakfire, f);
		if (r < 0)
			goto ERROR;

	// For the rest, launch a build environment
	} else {
		// Setup the build environment
		r = cli_setup_build(&build, global_args, 0);
		if (r < 0)
			goto ERROR;

		// Create the image
		r = pakfire_build_mkimage(build, local_args.type, f);
		if (r)
			goto ERROR;
	}

ERROR:
	if (build)
		pakfire_build_unref(build);
	if (pakfire)
		pakfire_unref(pakfire);
	if (f)
		fclose(f);

	return r;
}
