/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_CLI_COLOR_H
#define PAKFIRE_CLI_COLOR_H

typedef enum color_mode {
	COLORS_UNKNOWN = 0,
	COLORS_OFF,
	COLORS_ON,
} color_mode_t;

// Reset
#define COLOR_RESET              "\x1B[0m"

// Highlight
#define COLOR_HIGHLIGHT          "\x1B[0;1;39m"

// Regular Colors
#define COLOR_BLACK              "\x1B[0;30m"
#define COLOR_RED                "\x1B[0;31m"
#define COLOR_GREEN              "\x1B[0;32m"
#define COLOR_YELLOW             "\x1B[0;33m"
#define COLOR_BLUE               "\x1B[0;34m"
#define COLOR_MAGENTA            "\x1B[0;35m"
#define COLOR_CYAN               "\x1B[0;36m"
#define COLOR_WHITE              "\x1B[0;37m"

#define COLOR_BRIGHT_BLACK       "\x1B[0;90m"
#define COLOR_BRIGHT_RED         "\x1B[0;91m"
#define COLOR_BRIGHT_GREEN       "\x1B[0;92m"
#define COLOR_BRIGHT_YELLOW      "\x1B[0;93m"
#define COLOR_BRIGHT_BLUE        "\x1B[0;94m"
#define COLOR_BRIGHT_MAGENTA     "\x1B[0;95m"
#define COLOR_BRIGHT_CYAN        "\x1B[0;96m"
#define COLOR_BRIGHT_WHITE       "\x1B[0;97m"

#define COLOR_HIGHLIGHT_BLACK    "\x1B[0;1;30m"
#define COLOR_HIGHLIGHT_RED      "\x1B[0;1;31m"
#define COLOR_HIGHLIGHT_GREEN    "\x1B[0;1;32m"
#define COLOR_HIGHLIGHT_YELLOW   "\x1B[0;1;33m"
#define COLOR_HIGHLIGHT_BLUE     "\x1B[0;1;34m"
#define COLOR_HIGHLIGHT_MAGENTA  "\x1B[0;1;35m"
#define COLOR_HIGHLIGHT_CYAN     "\x1B[0;1;36m"
#define COLOR_HIGHLIGHT_WHITE    "\x1B[0;1;37m"

// Returns the color mode
color_mode_t color_mode(void);

#define COLOR_FUNC(name, color) \
	static inline const char* color_##name(void) { \
		return (color_mode() == COLORS_ON) ? COLOR_ ## color : ""; \
	}

COLOR_FUNC(reset,     RESET)
COLOR_FUNC(highlight, HIGHLIGHT)
COLOR_FUNC(black,     BLACK)
COLOR_FUNC(red,       RED)
COLOR_FUNC(green,     GREEN)
COLOR_FUNC(yellow,    YELLOW)
COLOR_FUNC(blue,      BLUE)
COLOR_FUNC(magenta,   MAGENTA)
COLOR_FUNC(cyan,      CYAN)
COLOR_FUNC(white,     WHITE)

#endif /* PAKFIRE_CLI_COLOR_H */
