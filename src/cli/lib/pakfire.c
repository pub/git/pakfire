/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <limits.h>

#include <pakfire/build.h>
#include <pakfire/buildservice.h>
#include <pakfire/config.h>
#include <pakfire/pakfire.h>
#include <pakfire/repo.h>

#include "config.h"
#include "pakfire.h"

static void cli_set_repo_enabled(struct pakfire* pakfire, const char* name, int enabled) {
	struct pakfire_repo* repo = NULL;

	// Find the repository
	repo = pakfire_get_repo(pakfire, name);

	// Ignore if the repository could not be found
	if (!repo)
		return;

	// Set status
	pakfire_repo_set_enabled(repo, enabled);
	pakfire_repo_unref(repo);
}

int cli_setup_config(struct pakfire_config** config, struct cli_global_args* args) {
	struct pakfire_config* c = NULL;
	int r;

	// Create a new config object
	r = pakfire_config_create(&c);
	if (r < 0)
		goto ERROR;

	// Open the distro configuration
	if (args->distro) {
		r = cli_read_distro_config(c, args->distro);
		if (r < 0)
			goto ERROR;

	// Open the regular configuration
	} else if (args->config) {
		r = pakfire_config_read_path(c, args->config);
		if (r < 0)
			goto ERROR;
	}

	// Return the configuration
	*config = c;
	return 0;

ERROR:
	if (c)
		pakfire_config_unref(c);

	return r;
}

int cli_setup_pakfire(struct pakfire** pakfire, struct cli_global_args* args) {
	struct pakfire_config* config = NULL;
	struct pakfire* p = NULL;
	int r;

	// Setup the configuration
	r = cli_setup_config(&config, args);
	if (r < 0)
		goto ERROR;

	// Initialize Pakfire
	r = pakfire_create(&p, args->ctx, config, args->root, args->arch, args->flags);
	if (r < 0) {
		fprintf(stderr, "Could not initialize Pakfire: %s\n", strerror(-r));
		goto ERROR;
	}

	// Enable repositories
	for (unsigned int i = 0; i < args->num_enable_repos; i++)
		cli_set_repo_enabled(p, args->enable_repos[i], 1);

	// Disable repositories
	for (unsigned int i = 0; i < args->num_disable_repos; i++)
		cli_set_repo_enabled(p, args->disable_repos[i], 0);

	// Return pointer
	*pakfire = p;

ERROR:
	if (config)
		pakfire_config_unref(config);

	return r;
}

int cli_setup_build(struct pakfire_build** build, struct cli_global_args* args, int flags) {
	struct pakfire_config* config = NULL;
	struct pakfire_build* b = NULL;
	int r;

	// Setup the configuration
	r = cli_setup_config(&config, args);
	if (r < 0)
		goto ERROR;

	// Setup the build environment
	r = pakfire_build_create(&b, args->ctx, config, args->arch, NULL, flags);
	if (r < 0) {
		fprintf(stderr, "Could not setup the build environment: %s\n", strerror(-r));
		goto ERROR;
	}

	// Return pointer
	*build = b;

ERROR:
	if (config)
		pakfire_config_unref(config);

	return r;
}

int cli_setup_buildservice(struct pakfire_buildservice** service, struct cli_global_args* args) {
	struct pakfire_config* config = NULL;
	const char* url = NULL;
	int r;

	// Setup the configuration
	r = cli_setup_config(&config, args);
	if (r < 0)
		goto ERROR;

	// Fetch the URL
	url = pakfire_config_get(config, "client", "url", "https://pakfire.ipfire.org/");

	// Connect to the build service
	r = pakfire_buildservice_create(service, args->ctx, url);
	if (r < 0) {
		fprintf(stderr, "Could not setup the build service: %s\n", strerror(-r));
		goto ERROR;
	}

ERROR:
	if (config)
		pakfire_config_unref(config);

	return r;
}
