/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <argp.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>
#include <unistd.h>

#include <pakfire/arch.h>
#include <pakfire/pakfire.h>
#include <pakfire/string.h>

#include "lib/assert.h"
#include "lib/build.h"
#include "lib/clean.h"
#include "lib/command.h"
#include "lib/config.h"
#include "lib/dist.h"
#include "lib/image.h"
#include "lib/info.h"
#include "lib/lint.h"
#include "lib/pakfire.h"
#include "lib/progressbar.h"
#include "lib/provides.h"
#include "lib/repo.h"
#include "lib/repolist.h"
#include "lib/requires.h"
#include "lib/shell.h"
#include "lib/search.h"
#include "lib/snapshot.h"
#include "lib/terminal.h"
#include "lib/version.h"

const char* argp_program_version = PACKAGE_FULLVERSION;

enum {
	OPT_ARCH         = 1,
	OPT_DEBUG        = 2,
	OPT_DISTRO       = 3,

	OPT_ENABLE_REPO  = 4,
	OPT_DISABLE_REPO = 5,
};

static struct argp_option options[] = {
	{ "arch",         OPT_ARCH,          "ARCH", 0, "Choose an architecture",      0 },
	{ "debug",        OPT_DEBUG,           NULL, 0, "Run in debug mode",           0 },
	{ "distro",       OPT_DISTRO,      "DISTRO", 0, "Build for this distribution", 0 },
	{ "enable-repo",  OPT_ENABLE_REPO,   "REPO", 0, "Enable a repository",         0 },
	{ "disable-repo", OPT_DISABLE_REPO,  "REPO", 0, "Disable a repository",        0 },
	{ NULL },
};

static const struct command commands[] = {
	{ "build",    cli_build,    1, -1, 0 },
	{ "clean",    cli_clean,    0,  0, 0 },
	{ "dist",     cli_dist,     1, -1, 0 },
	{ "image",    cli_image,   -1, -1, 0 },
	{ "info",     cli_info,     1, -1, 0 },
	{ "lint",     cli_lint,     1, -1, 0 },
	{ "provides", cli_provides, 1, -1, 0 },
	{ "repo",     cli_repo,    -1, -1, 0 },
	{ "repolist", cli_repolist, 0,  0, 0 },
	{ "requires", cli_requires, 1, -1, 0 },
	{ "shell",    cli_shell,    0, -1, 0 },
	{ "search",   cli_search,   1, -1, 0 },
	{ "snapshot", cli_snapshot, 1, -1, 0 },
	{ NULL },
};

const char* args_doc =
	"build [OPTIONS...] MAKEFILES...\n"
	"clean\n"
	"dist MAKEFILES...\n"
	"image ...\n"
	"lint [ARCHIVE...]\n"
	"provides PATTERN\n"
	"repo compose PATH PACKAGES...\n"
	"repolist\n"
	"requires PATTERN\n"
	"shell [OPTIONS...] [COMMAND...]\n"
	"search [OPTIONS...] PATTERN\n"
	"snapshot [COMMAND...]";

static error_t parse(int key, char* arg, struct argp_state* state, void* data) {
	struct cli_global_args* args = data;

	switch (key) {
		case OPT_ARCH:
			// Check if the architecture is supported
			if (!pakfire_arch_is_supported_by_host(arg))
				argp_failure(state, EXIT_FAILURE, 0, "Unsupported architecture: %s", arg);

			args->arch = arg;
			break;

		case OPT_DEBUG:
			pakfire_ctx_set_log_level(args->ctx, LOG_DEBUG);
			break;

		case OPT_DISTRO:
			args->distro = arg;
			break;

		// Enable/Disable Repositories

		case OPT_ENABLE_REPO:
			if (args->num_enable_repos >= MAX_REPOS)
				return -ENOBUFS;

			args->enable_repos[args->num_enable_repos++] = arg;
			break;

		case OPT_DISABLE_REPO:
			if (args->num_disable_repos >= MAX_REPOS)
				return -ENOBUFS;

			args->disable_repos[args->num_disable_repos++] = arg;
			break;

		default:
			return ARGP_ERR_UNKNOWN;
	}

	return 0;
}

static int cli_confirm(struct pakfire_ctx* ctx, struct pakfire* pakfire,
		void* data, const char* message, const char* question) {
	// Just print the message
	if (message)
		printf("%s\n", message);

	return 0;
}

int main(int argc, char* argv[]) {
	struct pakfire_ctx* ctx = NULL;
	int r;

	// Setup the context
	r = pakfire_ctx_create(&ctx, NULL);
	if (r)
		goto ERROR;

	// Write logs to the console
	pakfire_ctx_set_log_callback(ctx, pakfire_log_stderr, NULL);

	// Setup confirm callback
	pakfire_ctx_set_confirm_callback(ctx, cli_confirm, NULL);

	// Setup progress callback
	pakfire_ctx_set_progress_callback(ctx, cli_setup_progressbar, NULL);

	// Set the default cache path
	r = pakfire_ctx_set_cache_path(ctx, "~/.cache/pakfire");
	if (r)
		goto ERROR;

	struct cli_global_args args = {
		.ctx      = ctx,
		.distro   = cli_get_default_distro(ctx),
		.arch     = NULL,
		.flags    = PAKFIRE_FLAGS_BUILD,
	};

	// Parse the command line and run any commands
	r = cli_parse(options, commands, args_doc, NULL,
		parse, CLI_REQUIRE_ROOT, argc, argv, &args);

ERROR:
	if (ctx) {
		ctx = pakfire_ctx_unref(ctx);
		if (ctx) {
			fprintf(stderr, "Context was not freed\n");
			return 1;
		}
	}

	return r;
}
